<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Models\Barco;
use App\Models\Bono ;
use App\Models\User ;

use App\Models\Reserva ;
use App\Models\BonoExtra ;


use App\DataTables\UsuariosAdminDataTable;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth::routes();
Auth::routes();

  Route::get('/', function () {
           return view('welcome');
     });
    
      

Route::group(['middleware'=>['web','auth']], function(){



    Route::get('/home', function(UsuariosAdminDataTable $dataTable){

        if(Auth::user()->admin == 0)
        {
            $barco_id = Auth::user('barco_id');
            $barcos=Barco::all();
            $bonos = Bono::all();

            // Log::info($barcos);

            foreach($barcos as $barco)
            {
                $id = $barco->id;
                if($id == $barco_id->barco_id)
                {
                    $nombre_barco = $barco->nombre;
                    $eslora = $barco->eslora;
                    $puerto = $barco->puerto;
                    $ciudad = $barco->ciudad;
                    $barco_identificacion = $barco->id;
                }
                foreach($bonos as $bono)
                {
                    $id_barco_bono = $bono->barco_id;

                    if($id_barco_bono == $barco_id->barco_id)
                    {
                        $coste_bono = $bono->precio;
                        $coste_bono_finde = $bono->precio_findesemana;

                    }
                }
            }
            $user_id = Auth::user()->id;

            // $reservas=Reserva::where('user_id',Auth::user()->id)->get();
            $hoy = date("Y-m-d");
            // Log::info($id);
            $reservas=Reserva::where('user_id',Auth::user()->id)->whereBetween('start', [$hoy, '2100-12-12'])->get();
            $count_reservas_usuario=0;

            foreach($reservas as $ress)
            {
                $count_reservas_usuario++;

                if($ress->mañana==1 && $ress->tarde==1)
                {
                    $count_reservas_usuario++;

                }
            }
            $fechaActual = date('Y-m-d');
            $proxima_reserva = Reserva::where('user_id',Auth::user()->id)->whereBetween('start', [ $fechaActual, '2100-12-12'])->orderBy('start', 'ASC')->first();
            Log::info($proxima_reserva);
            return view('home',[
                'nombre_barco'=>$nombre_barco,
                'eslora'=>$eslora,
                'puerto'=>$puerto,
                'ciudad'=>$ciudad,
                'precio_bono'=>$coste_bono,
                'precio_bono_findesemana'=>$coste_bono_finde,
                'numero_reservas'=>$count_reservas_usuario,
                'barco_identificacion'=>$barco_identificacion,
                'user_id'=>$user_id,
                'proxima_reserva' => $proxima_reserva,

            ]);
        }
        else if(Auth::user()->admin == 1){
            $barcos['barcos']=App\Models\Barco::all();
            $edit=true;
            $users['users']= App\Models\User::all();
            // return view('homeAdmin', $users, $barcos, $edit);
            $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
            $reservas_checking = Reserva::where('checking_verificado','3')->get();
            $bonos_extra = BonoExtra::all();
            $count_bonos_extras=0;
            foreach($bonos_extra as $bono)
            {
    
                if($bono->confirmar=="no")
                {
                    $count_bonos_extras++;
                }
                
            }

            
        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }
    

            return $dataTable->render('/configuracion/index',[
               
                'reservas_checkout' => $reservas_checkout,
                'reservas_checking' => $reservas_checking,
                'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,
               

                        
            ]);
            
        }
    });


    //ruta datos cliente
    Route::get('/datos/cliente', function () {
        return view('/datosUsuario/datos');
    })->name('datos.cliente');
    
        //editar datos usuario

        Route::post('/datos/cliente/{id}/editar', 'App\Http\Controllers\ReservasController@editar_datos_personales')->name('editar.datos.personales');

    Route::get('/contacto/cliente', function () {
        return view('/contacto/contacto');
    })->name('contacto.cliente');


    //rutas bonos //registrados

    Route::get('/bonos/index', 'App\Http\Controllers\BonosController@index')->name('bonos.index');

    //rutas redsys

    Route::get('/redsys', ['as' => 'redsys', 'uses' => 'App\Http\Controllers\RedsysController@index']);

    //rutas reservas

    Route::get('/reservas/{id}/detalle', 'App\Http\Controllers\ReservasController@index')->name('detalles.reservas');
    Route::get('/reservas/{id}/{fecha}/{user_id}/cancelacion', 'App\Http\Controllers\ReservasController@cancelacion')->name('cancelacion.reservas');
    Route::get('/reservas/{id}/checking', 'App\Http\Controllers\ReservasController@checking_index')->name('checking.reservas');
    Route::get('/reservas/{id}/checkout', 'App\Http\Controllers\ReservasController@checkout_index')->name('checkout.reservas');


    Route::get('/reservas/checking/{id}/usuario', 'App\Http\Controllers\ReservasController@checking_usuario')->name('checking.usuario');
    Route::get('/reservas/checking/{id}/usuario/incidencias', 'App\Http\Controllers\ReservasController@checking_usuario_incidencias')->name('checking.incidencias.usuario');
    Route::post('/reservas/incidencias/checking', 'App\Http\Controllers\ReservasController@checking_incidencias')->name('incidencias.checking');


    Route::get('/reservas/checkout/{id}/usuario', 'App\Http\Controllers\ReservasController@checkout_usuario')->name('checkout.usuario');
    Route::get('/reservas/checkout/{id}/usuario/incidencias', 'App\Http\Controllers\ReservasController@checkout_usuario_incidencias')->name('checkout.incidencias.usuario');
    Route::post('/reservas/incidencias/checkout', 'App\Http\Controllers\ReservasController@checkout_incidencias')->name('incidencias.checkout');




    Route::post('/reservas/imagenGasolinaCheking', 'App\Http\Controllers\ReservasController@upload_imagen_gasolina')->name('uploadGasolina.reservas');
    Route::post('/reservas/imagenHorasCheking', 'App\Http\Controllers\ReservasController@upload_imagen_horas')->name('uploadHora.reservas');
    Route::post('/reservas/imagenGasolinaCheckout', 'App\Http\Controllers\ReservasController@upload_imagen_gasolina_checkout')->name('uploadGasolinaCheckout.reservas');
    Route::post('/reservas/imagenHorasCheckout', 'App\Http\Controllers\ReservasController@upload_imagen_horas_checkout')->name('uploadHoraCheckout.reservas');
    Route::post('/reservas/imagenBateriaCheckout', 'App\Http\Controllers\ReservasController@upload_imagen_bateria_checkout')->name('uploadBateriaCheckout.reservas');

    Route::post('/reservas/imagenes/incidencias/checkout', 'App\Http\Controllers\ReservasController@upload_imagen_incidencias_checkout')->name('imgincidencias_checkout.reservas');
    Route::post('/reservas/imagenes/incidencias/checking', 'App\Http\Controllers\ReservasController@upload_imagen_incidencias_checking')->name('imgincidencias_checking.reservas');

    //bono extra

    Route::post('bono/extra', 'App\Http\Controllers\ReservasController@bono_extra')->name('bono.extra');
    Route::post('boton/bono/extra', 'App\Http\Controllers\ReservasController@boton_bono_extra')->name('boton.bono.extra');


    Route::resource('/reservas', App\Http\Controllers\ReservasController::class);

    Route::POST('/reservas/show2', 'App\Http\Controllers\ReservasController@show')->name('reservas.show');
    Route::POST('/dias_festivos/show', 'App\Http\Controllers\ReservasController@dias_festivos_show')->name('dias_festivos.show');




     Route::get('/fecha/consultar', 'App\Http\Controllers\ReservasController@consultar')->name('consulta.reserva');

    //bono ultima hora
    
    Route::post('bono/ultima_hora', 'App\Http\Controllers\ReservasController@store_bono_ultima_hora')->name('bono.ultima.hora');




     //informacion o editar del barco

     Route::get('/barcos/{id}/edit', 'App\Http\Controllers\BarcosController@edit')->name('barcos.edit');

    //Enviar mensaje
    Route::post('/contacto/cliente/{id}/enviar', 'App\Http\Controllers\ContactoController@enviar_mensaje')->name('enviar.mensaje');



     Route::group(['middleware'=>['admin']], function(){


   




        //rutas detalles de barcos //admin
    
        Route::post('/barcos/{update}/update', 'App\Http\Controllers\BarcosController@update')->name('barcos.update');
        Route::post('/barcos/store', 'App\Http\Controllers\BarcosController@store')->name('barcos.store');
    
    
        //rutas imagenes barcos //admin
        Route::post('/barcos/imagenPrincipal', 'App\Http\Controllers\BarcosController@upload_imagen')->name('barcos.imagen');
        Route::post('/barcos/imagenes', 'App\Http\Controllers\BarcosController@upload_imagenes')->name('barcos.imagen');
        Route::post('/barcos/edit/imagenPrincipal/', 'App\Http\Controllers\BarcosController@edit_imagen')->name('edit.imagen');
        Route::post('/barcos/edit/imagenes/', 'App\Http\Controllers\BarcosController@edit_imagenes')->name('edit.imagenes');
        Route::get('/barcos/eliminar/{id}/imagenes/', 'App\Http\Controllers\BarcosController@eliminar_imagenes')->name('eliminar.imagenes');

    
        //configuracion //admin
        //index de las datatables
        Route::get('/configuracion/index', 'App\Http\Controllers\ConfiguracionController@index')->name('configuracion.index');
        Route::get('/configuracion/index/barcos', 'App\Http\Controllers\ConfiguracionController@index_barcos')->name('configuracion.index.barcos');
        Route::get('/configuracion/index/renovaciones', 'App\Http\Controllers\ConfiguracionController@index_renovaciones')->name('configuracion.index.renovaciones');

        Route::get('/configuracion/index_reservas', 'App\Http\Controllers\ConfiguracionController@index_reserva')->name('configuracion.index.reserva');
        Route::get('/configuracion/index_reservas_antiguas', 'App\Http\Controllers\ConfiguracionController@index_reserva_antiguas')->name('configuracion.index.reserva.antiguas');

        Route::get('/configuracion/calendario', 'App\Http\Controllers\ConfiguracionController@index_calendario')->name('configuracion.index.calendario');
        Route::get('/configuracion/calendario2', 'App\Http\Controllers\ConfiguracionController@index_calendario2')->name('configuracion.index.calendario2');
        Route::get('/configuracion/calendario3', 'App\Http\Controllers\ConfiguracionController@index_calendario3')->name('configuracion.index.calendario3');
        Route::get('/configuracion/calendario4', 'App\Http\Controllers\ConfiguracionController@index_calendario4')->name('configuracion.index.calendario4');
        Route::get('/configuracion/calendario5', 'App\Http\Controllers\ConfiguracionController@index_calendario5')->name('configuracion.index.calendario5');

    
        Route::get('/configuracion/bonos_extras', 'App\Http\Controllers\ConfiguracionController@index_bonos_extras')->name('configuracion.index.bonos');
        Route::get('/configuracion/bonos_extras/aceptado', 'App\Http\Controllers\ConfiguracionController@index_bonos_extras_aceptado')->name('configuracion.index.bonos.aceptado');
        Route::get('/configuracion/bonos_extras/denegado', 'App\Http\Controllers\ConfiguracionController@index_bonos_extras_denegado')->name('configuracion.index.bonos.denegado');
        Route::get('/configuracion/bonos_descontar', 'App\Http\Controllers\ConfiguracionController@index_bonos_descontar')->name('configuracion.index.descontar');
        Route::get('/configuracion/incidencias/checking', 'App\Http\Controllers\ConfiguracionController@index_incidencias_checking')->name('configuracion.index.incidencias.checking');
        Route::get('/configuracion/incidencias/checking/antiguo', 'App\Http\Controllers\ConfiguracionController@index_incidencias_checking_antiguo')->name('configuracion.index.incidencias.checking.antiguo');
    
    
        Route::get('/configuracion/incidencias/checkout', 'App\Http\Controllers\ConfiguracionController@index_incidencias_checkout')->name('configuracion.index.incidencias.checkout');
        Route::get('/configuracion/incidencias/checkout/antiguo', 'App\Http\Controllers\ConfiguracionController@index_incidencias_checkout_antiguo')->name('configuracion.index.incidencias.checkout.antiguo');
    
    
        
        Route::resource('/reservas_admin', App\Http\Controllers\ReservasAdminController::class);
        Route::resource('/reservas_admin2', App\Http\Controllers\ReservasAdminController2::class);

        Route::resource('/reservas_admin3', App\Http\Controllers\RerservasAdminController3::class);
        Route::resource('/reservas_admin4', App\Http\Controllers\ReservasAdminController4::class);
        Route::resource('/reservas_admin5', App\Http\Controllers\ReservasAdminController5::class);


    
    
        Route::resource('/configuracion', App\Http\Controllers\ConfiguracionController::class);

        //configuracion renovar suscripcion 
        Route::get('/configuracion/renovaciones/{id}/renovar', 'App\Http\Controllers\ConfiguracionController@renovar_suscripcion')->name('renovar.suscripcion');
       
        //eliminar usuario
        Route::get('/configuracion/eliminar/{id}/usuario', 'App\Http\Controllers\ConfiguracionController@destroy_usuario')->name('eliminar.usuario');

        //configuracion edit usuario
        Route::get('/configuracion/usuario/{id}/index', 'App\Http\Controllers\ConfiguracionController@index_edit_usuario')->name('configuracion_usuario.index');
        Route::post('/configuracion/editar/{id}/usuario', 'App\Http\Controllers\ConfiguracionController@editar_usuario')->name('editar.usuario');

        Route::get('/configuracion/usuario/automatizacion', 'App\Http\Controllers\ConfiguracionController@automatizacion')->name('automatizacion');
        Route::get('/configuracion/usuario/automatizacionImagenes', 'App\Http\Controllers\ConfiguracionController@automatizacion_eliminar_imagenes')->name('automatizacionEliminarImagenes');
        
        //configuracion edit reserva
        
    
        Route::get('/configuracion/reserva/{id}/index', 'App\Http\Controllers\ConfiguracionController@index_edit_reserva')->name('configuracion_reserva.index');
        Route::post('/configuracion/editar/{id}/reserva', 'App\Http\Controllers\ConfiguracionController@editar_reserva')->name('editar.reserva');
        Route::get('/configuracion/borrar/{id}/reserva', 'App\Http\Controllers\ConfiguracionController@destroy_reserva')->name('borrar.reserva');
        Route::post('/configuracion/borrar/reserva_form', 'App\Http\Controllers\ConfiguracionController@destroy_reserva_form')->name('borrar.reserva.form');
        Route::post('configuracion/guardar/ultima_hora', 'App\Http\Controllers\ConfiguracionController@store_bono_ultima_hora_admin')->name('bono.ultima.hora.admin');
     

        //Agregar y eliminar dias festivos por parte del administrador

        Route::post('/configuracion/reserva/dia_festivo', 'App\Http\Controllers\ConfiguracionController@agregar_dia_festivo')->name('add.dia_festivo');
        Route::get('/configuracion/borrar/{id}/dia_festivo', 'App\Http\Controllers\ConfiguracionController@destroy_dia_festivo')->name('borrar.dia_festivo');


        //aceptar o denegar bono extra
        Route::get('/configuracion/bonos_extras/{id}/aceptar', 'App\Http\Controllers\ConfiguracionController@aceptar_bono_extra')->name('aceptar.bono.extra');
        Route::get('/configuracion/bonos_extras/{id}/denegar', 'App\Http\Controllers\ConfiguracionController@denegar_bono_extra')->name('denegar.bono.extra');
    
    
        //verificar incidencias checking reserva 
        Route::get('/configuracion/incidencias/checking/{id}/verificar', 'App\Http\Controllers\ConfiguracionController@verificar_incidencia_checking')->name('verificar.checking');

        Route::get('/configuracion/incidencias/checkout/{id}/verificar', 'App\Http\Controllers\ConfiguracionController@verificar_incidencia_checkout')->name('verificar.checkout');


        //checking y checkout por parte de admin

        Route::get('/configuracion/checking_admin/{id}/index', 'App\Http\Controllers\ConfiguracionController@checking_admin_index')->name('checking.admin.index');
        Route::get('configuracion/reservas/checking/{id}/admin', 'App\Http\Controllers\ConfiguracionController@checking_admin')->name('checking.admin');
        Route::get('configuracion/reservas/checking_incidencia/{id}/admin', 'App\Http\Controllers\ConfiguracionController@checking_incidencia_admin')->name('checking_incidencia.admin');

        Route::get('/configuracion/checkout_admin/{id}/index', 'App\Http\Controllers\ConfiguracionController@checkout_admin_index')->name('checkout.admin.index');
        Route::get('configuracion/reservas/checkout/{id}/admin', 'App\Http\Controllers\ConfiguracionController@checkout_admin')->name('checkout.admin');
        Route::get('configuracion/reservas/checkout_incidencia/{id}/admin', 'App\Http\Controllers\ConfiguracionController@checkout_incidencia_admin')->name('checkout_incidencia.admin');

      

    
    });



});
     


URL::forceScheme('https');