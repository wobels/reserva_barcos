<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHorasImagenesToReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->string('hora_gasolina')->nullable();
            $table->string('hora_horas')->nullable();
            $table->string('hora_gasolina_checkout')->nullable();
            $table->string('hora_horas_checkout')->nullable();
            $table->string('hora_bateria_checkout')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->dropColumn('hora_gasolina');
            $table->dropColumn('hora_horas');
            $table->dropColumn('hora_gasolina_checkout');
            $table->dropColumn('hora_horas_checkout');
            $table->dropColumn('hora_bateria_checkout');



            
        });
    }
}
