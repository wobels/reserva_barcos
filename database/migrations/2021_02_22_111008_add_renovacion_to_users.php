<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRenovacionToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('renovacion')->nullable();
            $table->boolean('check_renovacion')->nullable();
            $table->boolean('recordatorio_renovacion')->default(0)->nullable();


            $table->boolean('inactivo')->default(0)->nullable();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('renovacion');
            $table->dropColumn('check_renovacion');
            $table->dropColumn('recordatorio_renovacion');

            $table->dropColumn('inactivo');

        });
    }
}
