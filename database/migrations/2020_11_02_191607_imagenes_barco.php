<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImagenesBarco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('imagenes_barco', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('barco_id')->unsigned()->nullable();
            $table->foreign('barco_id')->references('id')->on('barcos')->onDelete('set null');
            $table->string('nombre');
            $table->string('nombre_archivo');
            $table->boolean('principal')->default(0);
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagenes_barco');
    }
}
