<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBonoUltimoDiaToReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservas', function (Blueprint $table) {
            
            $table->boolean('bono_ultimo_dia')->default(0)->nullable();
            $table->string('tipo_bono_ultimo_dia')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->dropColumn('bono_ultimo_dia');
            $table->dropColumn('tipo_bono_ultimo_dia');
        });
    }
}
