<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTardeMañanaToBonosExtras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bonos_extras', function (Blueprint $table) {
            $table->boolean('mañana')->default(0)->nullable();
            $table->boolean('tarde')->default(0)->nullable();
            $table->boolean('ultimo_dia')->default(0);
            $table->string('tipo_ultimo_dia')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bonos_extras', function (Blueprint $table) {
            $table->dropColumn('mañana');
            $table->dropColumn('tarde');
            $table->dropColumn('ultimo_dia');
            $table->dropColumn('tipo_ultimo_dia');


        });
    }
}
