<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Bonos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('bonos', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('barco_id')->unsigned()->nullable();
            $table->foreign('barco_id')->references('id')->on('barcos')->onDelete('set null');
            $table->string('codigo');
            $table->string('imagen_principal')->nullable();
            $table->float('precio');
            $table->float('precio_findesemana');

            // $table->boolean('cantidad_disponibles')->default(5000);
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonos');
    }
}
