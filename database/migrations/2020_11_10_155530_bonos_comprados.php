<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BonosComprados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonos_comprados', function (Blueprint $table) {
            $table->increments('id');
          
            $table->bigInteger('barco_id')->unsigned()->nullable();
            $table->foreign('barco_id')->references('id')->on('barcos')->onDelete('set null');

            $table->float('user_id');
            $table->date('fecha');
            $table->float('cantidad');
   

        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonos_comprados');
    }
}
