<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DescontarBonos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descontar_bonos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('barco_id');
            $table->integer('user_id');
            $table->integer('reserva_id');
            $table->string('trimestre');
            $table->string('tipo')->nullable();
            $table->float('cantidad_findesemana')->nullable();
            $table->float('cantidad_entresemana')->nullable();

            



            

            $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descontar_bonos');
        
    }
}
