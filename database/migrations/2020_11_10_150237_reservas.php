<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Reservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id');
            

            $table->string('title',255);
            $table->bigInteger('barco_id')->unsigned()->nullable();
            $table->foreign('barco_id')->references('id')->on('barcos')->onDelete('set null');
            // $table->bigInteger('usuario_id')->unsigned()->nullable();
            // $table->foreign('usuario_id')->references('id')->on('users')->onDelete('set null');
            $table->float('user_id');
            $table->string('user_name')->nullable();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->boolean('mañana')->default(0);
            $table->boolean('tarde')->default(0);
            $table->boolean('checking')->default(0);
            $table->boolean('checkout')->default(0);
            $table->boolean('reserva_bono_extra')->default(0)->nullable();
            $table->string('imagen_gasolina')->nullable();
            $table->string('imagen_horas')->nullable();
            $table->string('imageUrl')->nullable();
            $table->dateTime('hora_checking')->nullable();
            $table->dateTime('hora_checkout')->nullable();
            $table->longText('incidencias_checking')->nullable();
            $table->longText('incidencias_checkout')->nullable();
            $table->boolean('checkout_verificado')->default(0);
            $table->boolean('checking_verificado')->default(0);






            $table->timestamps();

        });
     
       
        
    
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
