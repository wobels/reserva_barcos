<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BonosExtra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonos_extras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('barco_id');
            $table->integer('user_id');
            $table->integer('reserva_id')->nullable();
            $table->string('estado')->default('pendiente');
            $table->string('tipo');
            $table->integer('cantidad')->default(1);
            $table->float('precio');
            $table->string('confirmar')->default('no');


            

            $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonos_extras');
        
    }
}
