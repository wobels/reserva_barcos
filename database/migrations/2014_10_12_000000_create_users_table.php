<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('barcos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('tipo');
            $table->longText('titulo');
            $table->longText('descripcion');
            $table->string('imagen_principal')->nullable();
            $table->string('numero_personas')->nullable();
            $table->string('ciudad');
            $table->string('puerto');
            $table->string('eslora')->nullable();
            $table->string('manga')->nullable();
            $table->string('calado')->nullable();
            $table->string('motor')->nullable();
            $table->string('desplazamiento')->nullable();
            $table->string('velocidad_maxima')->nullable();
            $table->string('velocidad_crucero')->nullable();
            $table->string('camarotes')->nullable();
            $table->string('baños')->nullable();
            $table->string('capacidad_agua_dulce')->nullable();
            $table->string('capacidad_combustible')->nullable();

            $table->timestamps();
          
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->boolean('admin')->default(0);
            $table->boolean('premium')->nullable();
            $table->string('apellidos')->nullable();
            $table->string('dni')->nullable();
            $table->string('telefono')->nullable();
            $table->string('tipo_pago')->nullable();
    
            $table->integer('numero_bonos_entresemana')->nullable();
            $table->integer('numero_bonos_findesemana')->nullable();



            $table->bigInteger('barco_id')->unsigned()->nullable();
            $table->foreign('barco_id')->references('id')->on('barcos')->onDelete('set null');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barcos');
        Schema::dropIfExists('users');
        
    }
}
