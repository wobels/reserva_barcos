<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUltimoDiaToReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->boolean('ultimo_dia')->default(0);
            $table->string('tipo_ultimo_dia')->nullable();
            $table->bigInteger('cantidad_ultimo_dia')->nullable();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->dropColumn('ultimo_dia');
            $table->dropColumn('tipo_ultimo_dia');
            $table->dropColumn('cantidad_ultimo_dia');

        });
    }
}
