@extends('layouts.app')

@section('content')

@if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod'))


    <link rel="stylesheet"  href="{{ asset('css/pc-home-iphone.css') }}"/>
@else
    <link rel="stylesheet"  href="{{ asset('css/pc-home-android.css') }}"/>


@endif

<style>
    @media(max-width: 1920px){
    .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/tarde.png"] {

        margin-top:220px ;
        margin-left:33% ;
    }

    .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana.png"] {
        
        margin-top:65px ;
        margin-left:33% ;
    }

    .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana.png"] {
    
        margin-top:65px ;
        margin-left:33% ;
    }


    .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_tarde.png"] {

        margin-top:225px ;
        margin-left:33% ;
    }  

    #div_tiempo_pc{

        width:100% !important;
        overflow-y:scroll;
    }
    

    #imagen_calendario[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana_tarde.png"] {
        margin-left:33% !important;
        
        
    }

    }
    @media(max-width: 991px){
        #boton_añadir_festivos{
            position:relative !important;
        left:10px;
        float:left !important;
        margin-bottom:5% !important;
        }
    
            .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/tarde.png"] {
            
            margin-left:25% !important;
            margin-top:220px !important ;

        }

        .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana.png"] {
            margin-left:25% !important;
            margin-top:70px !important ;
            
            }
            .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana.png"] {
            margin-left:25% !important;
            margin-top:70px !important ;
            
            }
            .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_tarde.png"] {
            
            margin-left:25% !important;
            margin-top:225px !important ;

        }
            
            #contenedor{
            width:100% !important;
            }
            #div-calendario{
            padding:0px !important;

            }
            #calendar{
            
            height:500px;
            }
            #texto_ver_cam{
            position:absolute; 
            margin-top:45% !important; 
            
            width:100%; text-align:center;
            color:white  
            }
            #imagen_calendario{
            width:40px !important;
            }

            #imagen_cam_tablet_pc{
            margin-top:10%;
            
            }
            #div_tiempo_pc{
            margin-top:5% !important;
            
            }

            #iframe_portus{
            margin-top:3% !important;
            }
        
            #iframe_windy{
            margin-top:-1% !important;
            }

            #div_leyenda_pc{
            
            margin-top:3% !important;


            }
            #table_leyenda_pc{
            display:none !important;
            }
            #table_leyenda_tablet{
            display:block !important;
            }

            #texto_windy{
            padding:20px !important;
            }

            #imagen_calendario[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana_tarde.png"] {
                margin-left:25% !important;
                
                
            }

        
        }

    @media(max-width: 600px) {
        #boton_añadir_festivos{
            position:relative !important;
            left:10px;
        }

        .fc-day-fri {
            background: rgb(221,255,220) !important;
            background: linear-gradient(0deg, rgba(221,255,220,1) 38%, rgba(255,255,255,1) 38%, rgba(255,255,255,1) 100%) !important;
        }
        .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana.png"] {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin:auto !important;
            margin-top:0px !important ;
        
        }
        
        .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/tarde.png"] {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin:auto !important;
            margin-top:45px !important ;
            
        }
        
        .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana.png"] {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin:auto !important;
            margin-top:0px !important ;
        
        }
        
        .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_tarde.png"] {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin:auto !important;
            margin-top:45px !important ;
        
        }
        
        #contenedor{
            width:100% !important;
        }
        #div-calendario{
            padding:0px !important;
        
        }
        #calendar{
        
            height:850px !important;
            margin-top:10% !important;
        }
        
        #imagen_calendario{
            width:40px !important;
            height:40px;
            
        
        
        }
        #imagen_calendario[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana-tarde.png"] {
            height:80px !important;
            width:40px !important;
            
        
        }
        #imagen_calendario[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana_tarde.png"] {
            height:80px !important;
            width:40px !important;
            margin:auto !important;
        
        }
        
        #boton_barco{
             margin-top:2% !important; 
        }
        
        #texto_ver_cam{
            position:absolute; 
            margin-top:50% !important; 
            
            width:100%; text-align:center;
            color:white  
        }
        
        #img_logo_footer{
            margin-left:-17% !important;
        }
        
        #texto_windy{
            padding:20px !important;
        
        }
        #div_windy{
            margin-top:15% !important;
        }

      
        
        
    
    }






    /* CSS general */

    .fc-MiBoton-button{
    margin-top:-1000% !important;
    }

    .fc-day-sat{
        background: #DDFFDC !important;
    }
    .fc-day-sun{
        background: #DDFFDC !important;

    }

    .fc-daygrid-event img {


        margin:auto;
    }

    body {font-family: Arial;}

    /* Style the tab */
    .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
    background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
    background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
    }
    .tabcontent1 {

    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
    }
    #boton_añadir_festivos{
        position:absolute;

    }

    





    #calendar{
        
        height:1300px;
      }


    #iframe_windy{
      width:110% !important;
      margin-top:-3%;
    }
    #div_leyenda_pc{
      margin-top:-3%;

    }

    #texto_ver_cam{
        position:absolute; 
        margin-top:35%; 
        width:100%; text-align:center;
        color:white ;
        margin-left:-2%; 
    }

    .fc-MiBoton-button{
        border:1px solid red !important;
        margin-top:-1000% !important;
    }

    .fc-day-sat{
        background: #DDFFDC !important;
    }
    .fc-day-sun{
        background: #DDFFDC !important;

    }

    .fc-daygrid-event img {
        margin:auto;
    }

    #table_leyenda_tablet{
         display:none;
    }

    .fc-daygrid-day-frame.fc-scrollgrid-sync-inner{
      
        height:40px !important;
    }

    .fc-day-today {
        background:#FFEEEE !important;

    }

  .fc-today-button{
      background:#FEDDDD !important;
      color:#04607c !important;
      font-weight:bold !important;
      border-color:#FEDDDD !important;

  }
  .fc-day-fri {
  
    background: rgb(221,255,220);
    background: linear-gradient(0deg, rgba(221,255,220,1) 45%, rgba(255,255,255,1) 46%, rgba(255,255,255,1) 100%);
  }
  .fc-col-header-cell.fc-day.fc-day-fri{
    background:white !important;
  }







  .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/tarde.png"] {
        position:absolute !important;
     
      }
    
      .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana.png"] {
        position:absolute !important;
        ;
      }
    
      .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana.png"] {
        position:absolute !important;
       
      }
    
    
      .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_tarde.png"] {
        position:absolute !important;
       
      }  
 

</style>

      <!-- <button class="btn btn-primary" >Añadir barco a catalogo</button> -->
<div id="contenedor" class="container-fluid" style="width:90%" >
    
    <div class="tab col-lg-12" style="background: #04607c;">
        <button class="tablinks"><a style="color:white"  href="{{route('configuracion.index')}}">Usuarios</a></button>
        <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.reserva')}}">Reservas</a></button>
        <button class="tablinks" style="background:white  "  ><a style="color:#04607c;"  href="{{route('configuracion.index.calendario')}}">Calendario</a></button>
        <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.bonos')}}">Bonos Extras @if($count_bonos_extras!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>

        <button class="tablinks"> <a style="color:white"  href="{{route('configuracion.index.descontar')}}">Bonos a descontar</a></button>

        <?php

$count_reservas_incidencias = 0;
  foreach($reservas_checking as $RES)
  {

    $count_reservas_incidencias++;
  }
?>

  <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.incidencias.checking')}}">Incidencias check-in  @if($count_reservas_incidencias!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>
  <?php
  $count_reservas_incidencias2 = 0;
    foreach($reservas_checkout as $RES)
    {

      $count_reservas_incidencias2++;
    }
  ?>

<button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.incidencias.checkout')}}">Incidencias checkout @if($count_reservas_incidencias2!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>  
<button class="tablinks" ><a  style="color:white"  href="{{route('configuracion.index.renovaciones')}}"> @if($count_renovaciones!=0)<i class="fas fa-circle text-danger"></i>@endif Renovaciones</a></button> 

<button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.barcos')}}">Barcos</a></button>        


    </div>


</div>

<button style="margin-top:-1000%"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Información de reserva<span id="spanID"> #</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <H4>Información de reserva</H4>
        <form id="formBorrarReserva" method="POST">
            <label>ID</label>
            <input class="form-control" type="text" name="IDInfo" id="IDInfo" readonly>

            <label>Barco</label>
            <input class="form-control" type="text" id="BarcoInfo" readonly>
            

            <label>Usuario</label>
            <input class="form-control" type="text" id="UsuarioInfoName" readonly>

            <input hidden class="form-control" type="text" id="UsuarioInfo" readonly>

            <label>Fecha</label>
            <input class="form-control" type="text" id="FechaInfo" readonly>

            <label>Reserva de mañana</label>
            <input class="form-control" type="text" id="MañanaInfo" readonly>

            <label>Reserva de tarde </label>
            <input class="form-control" type="text" id="TardeInfo" readonly>

            <label>Checking </label>
            <input class="form-control" type="text" id="CheckingInfo" readonly>

            <label>Checkout </label>
            <input class="form-control" type="text" id="CheckoutInfo" readonly>
            {{ csrf_field() }}
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="ir_a_reserva" class="btn btn-primary" data-dismiss="modal"><i class="fas fa-eye"></i> Ver más detalles</button>
        <a onclick="return confirm('Estas seguro de querer borrar esta reserva?');" type="button" id="borrar_reserva" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Borrar reserva</a>
      </div>
    </div>
  </div>
</div>


<button type="button" style="margin-top:-1000%" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
    Launch demo modal
    </button>

 <!-- Modal -->
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reservar</h5>
            <button type="button" class="close cerrar-modal"  aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-lg-12">

            <div class="tab">
                <button class="tablinks" id="reservar_como_mantenimiento"  onclick="openCity(event, 'London')">Como administrador</button>
                <button class="tablinks" id="reservar_a_usuario" onclick="openCity(event, 'Paris')">Para otro usuario</button>
            </div>

            <div id="London" class="tabcontent">
              <!-- form manenimiento -->
                <form method="POST"  id="form_mantenimiento">
                    <label>Usuario</label>
                    <input hidden class="form-control"  type="text" name="txtUsuarioID" id="txtUsuarioID" value="{{Auth::user()->id}}" readonly >
                    <input class="form-control"  type="text" name="txtUsuario" id="txtUsuario" value="{{Auth::user()->name}}" readonly >

                    <label>Barco</label>
                    <select id="txtBarcoID" name="txtBarcoID" class="form-control">
                        @foreach($barcos as $barco)
                            <option value="{{$barco->id}}">{{$barco->nombre}}</option>
                        @endforeach
                    </select>   
                  

                    <input hidden    class="form-control"  type="text" name="txtTipoReserva" id="txtTipoReserva" value="mantenimiento" readonly>
        
                    <label>Fecha</label>
                    <input class="form-control"  type="text" name="txtFecha" id="txtFecha" readonly>
                    <h5 class="mt-3">Chequea un horario:  </h5>
                    <label class="form-control mt-3" > 
                        Mañana
                    <input  type="checkbox" name="txtMañana" id="txtMañana" value="0">
                    &nbsp Horario de 07:00 a 14:00
                    </label> 
                    <label class="form-control mt-3">
                    Tarde 
                    <input type="checkbox" name="txtTarde" id="txtTarde" value="0">
                    &nbsp Horario de 15:00 a 21:00
                </label>

                <div id="errores" class="bg-danger col-lg-12 text-light">
            
                </div>
                
                </form>
            </div>
            <!-- form normal -->
            <div id="Paris" class="tabcontent">
            <form method="POST" id="form_normal">
                <label>Barco</label>
                <select id="txtBarcoID2" name="txtBarcoID" class="form-control">
                    @foreach($barcos as $barco)
                        <option value="{{$barco->id}}">{{$barco->nombre}}</option>
                    @endforeach
                </select>   

                <label>Usuarios</label>
                    <select id="txtUsuarioID2" name="txtUsuarioID" class="form-control">
                        @foreach($usuarios as $usuario)
                            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                        @endforeach
                    </select>   
                    <input hidden    class="form-control"  type="text" name="txtTipoReserva" id="txtTipoReserva2" value="normal" readonly>

                  
                <label>Fecha</label>
                <input class="form-control"  type="text" name="txtFecha" id="txtFecha2" readonly>
                <h5 class="mt-3">Chequea un horario:  </h5>
                <label class="form-control mt-3" > 
                    Mañana
                <input  type="checkbox" name="txtMañana" id="txtMañana2" value="0">
                &nbsp Horario de 07:00 a 14:00
                </label> 
                <label class="form-control mt-3" >
                    Tarde 
                    <input type="checkbox" name="txtTarde" id="txtTarde2" value="0">
                    &nbsp Horario de 15:00 a 21:00
                </label>
                
                <input hidden type="text" value="0" name="ultimo_dia" id="ultimo_dia2">
                <input hidden type="text" value="0" name="tipo_bono" id="tipo_bono2">


                <div id="errores2" class="bg-danger col-lg-12 text-light">
                
                </div>

              <input hidden type="text" name="fecha_actual_control" id="fecha_actual_control2">
                

            </form>

            <!-- form ultimo dia -->
            <form method="POST" id="form_ultimo_dia">
         
                <label>Barco</label>
                <select id="txtBarcoID3" name="txtBarcoID" class="form-control">
                    @foreach($barcos as $barco)
                        <option value="{{$barco->id}}">{{$barco->nombre}}</option>
                    @endforeach
                </select>   

                <label>Usuarios</label>
                    <select id="txtUsuarioID3" name="txtUsuarioID" class="form-control">
                        @foreach($usuarios as $usuario)
                            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                        @endforeach
                    </select>   
                    <input hidden    class="form-control"  type="text" name="txtTipoReserva" id="txtTipoReserva3" value="normal" readonly>

                  
                <label>Fecha</label>
                <input class="form-control"  type="text" name="txtFecha" id="txtFecha3" readonly>
                <h5 class="mt-3">Chequea un horario:  </h5>
                <label class="form-control mt-3" > 
                    Mañana
                <input  type="checkbox" name="txtMañana" id="txtMañana3" value="0">
                &nbsp Horario de 07:00 a 14:00
                </label> 
                <label class="form-control mt-3" >
                    Tarde 
                    <input type="checkbox" name="txtTarde" id="txtTarde3" value="0">
                    &nbsp Horario de 15:00 a 21:00
                </label>
                
                <label class="col-md-12 mt-3">Seleccióna tipo de bono a usar</label>
                <select class="form-control" name="tipo_bono" id="tipo_bono3">
                    <option value="1"> Bono de entre semana </option>
                    <option value="2"> Bono de finde semana </option>

                </select>
            
                <input hidden type="text" value="1" name="ultimo_dia" id="ultimo_dia3">
                <input hidden type="text" value="1" name="ruta_imagen" id="ruta_imagen">


                <div id="errores3" class="bg-danger col-lg-12 text-light">
                
                </div>

              <input hidden type="text" name="fecha_actual_control" id="fecha_actual_control3">

                         {{ csrf_field() }}
                
            </form>
            <button style="width:100%;height:100%;display:none" id="reserva_con_bono_ultima_hora" class="btn btn-primary mt-5 adquiriBonos"><i class="fas fa-stopwatch"></i> Reservar con bono ultima hora</button>


            </div>

        <!-- 
            <div id="correcto" class="bg-success col-lg-12 text-light mt-2">
            
            </div> -->
         
          </div>
          <div class="modal-footer">
            <buttton id="btnAgregar" class="btn btn-primary">Reservar</buttton>
            <buttton id="btnAgregar2" class="btn btn-primary">Reservar</buttton>
             <buttton id="btnAgregar3" class="btn btn-primary">Reservar</buttton>

          
                <buttton id="btnCancelar"  class="btn btn-danger cerrar-modal">Cancelar</buttton>
          </div>
        </div>
      </div>
      
    </div>
    
<!-- Button trigger modal -->
<button type="button" id="boton_añadir_festivos" class="btn btn-primary mt-4" style="" data-toggle="modal" data-target="#exampleModal4">
  Añadir dias festivos
</button>


<!-- Modal para añadir dias festivos -->
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Dias festivos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong>Añadir dia festivo para que cuente como reserva de finde semana</strong>
       
        <label> Fecha:</label>
        <form action="POST" id="formDiaFestivo">
            <input name="start" class="form-control" type="date">
            
            @csrf
        </form>

        <h5 class="mt-3">Dias festivos fijados:</h5>
       <ul style="list-style:none">
       <?php
        $count_dias_festivos = 0;
       ?>
       @foreach($dias_festivos as $dias)
        <?php
        $count_dias_festivos++;
        ?>
        <li><a href="{{route('borrar.dia_festivo',$dias->id)}}" id="eliminar_dia_festivo-<?php echo $dias->id;?>"><i class="fas fa-times text-danger" ></i> </a> {{$dias->start}}</li>
        @endforeach
       </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <a type="button" id="agregar_dia_festivo" class="btn btn-primary">Guardar fecha</a>
      </div>
    </div>
  </div>
</div>
<div class="pl-4" style="display:abolute; width:100%; cursor:pointer;">
    <select onChange="window.location.href=this.value">
        <option>Cambiar de calendario</option>
        <option value="{{route('configuracion.index.calendario')}}">Terreta</option>
        <option value="{{route('configuracion.index.calendario2')}}">Arrels</option>
        <option value="{{route('configuracion.index.calendario4')}}">Vida</option>
        <option value="{{route('configuracion.index.calendario5')}}">Mar</option>

    </select>
    <br>
    <br>
    <strong>CALENDARIO DE :</strong> {{$barco_actual->nombre}}


</div>
<br>
    <div id='calendar' class="container-fluid" style="width:100%"></div>
    </div>


@include('footer')


<script>

fetch("{{route('dias_festivos.show')}}", { 
  headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    "X-CSRF-Token": $('input[name="_token"]').val()
  },
  method: "post",
  credentials: "same-origin",
  body: JSON.stringify({
    key: "value"
  })
})
.then(response => response.json())
.then(data => {
  fechas_formateadas = [];
  for (var b=0; b<data.length; b++)
  {
    var element = data[b]['start'].split(' ');
    var fecha = element[0].split('-');
    fecha_formateada = fecha[0]+'-'+fecha[1]+'-'+fecha[2]
    nuevo_registro_fecha = fechas_formateadas.push(fecha_formateada);
  }
  imageUrl= "{{ URL::to('/') }}/imagenes/reservas/festivo.png ";
  for (var i=0; i<fechas_formateadas.length; i++) 
  { 
    $('[data-date="'+fechas_formateadas[i]+'"]').prepend('<img id="theImg" style="position:absolute !important"src="' + imageUrl + '" />');
  }
});



    $('#btnAgregar2').css('display','none');
    $('#btnAgregar3').css('display','none');
    $('#btnAgregar').css('display','none');
    $('#btnCancelar').css('display','none');


    $("#agregar_dia_festivo").click(function(){
        var ruta = '{{ route("add.dia_festivo") }}';
        
        $.ajax({
            type: "POST",
            url: ruta, // This is what I have updated
            data: $("#formDiaFestivo").serialize(),
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'dia festivo agregado') {
                  
                    alert('Dia festivo añadido con éxito');
                    location.reload();
                }
                if (data.success == 'pedido en curso') {
                    
      
        
                
        
                }

                //errores

                if (data.errors != "" && data.errors != null) {
                
                }
            }
         })
        
    });
   
    $("#reservar_como_mantenimiento").click(function(){
        $('#form_normal').css('display','none');
        $('#form_ultimo_dia').css('display','none');
        $('#form_mantenimiento').css('display','block');
        $('#btnCancelar').css('display','block');


        $('#btnAgregar').css('display','block');
        $('#btnAgregar2').css('display','none');
        $('#btnAgregar3').css('display','none');
        
    });

    $("#reservar_a_usuario").click(function(){
        fecha=$('#txtFecha2').val();

    
            // alert(count);
        fecha_dif=moment(fecha, "YYYY-MM-DD");
        h_dif=moment(h, "YYYY-MM-DD");

        // alert(info.dateStr);
        // alert(h);
        diferencias_dias =  fecha_dif.diff(h_dif, 'days');

    
        if(diferencias_dias<=1)
        {
        
            $('#form_normal').css('display','none');
            $('#form_ultimo_dia').css('display','block');
            $('#form_mantenimiento').css('display','none');
            $('#btnCancelar').css('display','block');


            $('#btnAgregar').css('display','none');
            $('#btnAgregar2').css('display','none');
            $('#btnAgregar3').css('display','block');
        





        }
        else if(diferencias_dias>1){
            $('#form_normal').css('display','block');
            $('#form_ultimo_dia').css('display','none');
            $('#form_mantenimiento').css('display','none');
            $('#btnCancelar').css('display','block');


            $('#btnAgregar').css('display','none');
            $('#btnAgregar2').css('display','block');
            $('#btnAgregar3').css('display','none');   
    
        }

    
    });
        
    $("input[id=txtTarde]").click(function(){
        
        if($('#txtTarde').prop('checked') ) {
            $('#txtTarde').val(1);
            textoTarde="Tarde";
        }
        else{
        $('#txtTarde').val(0);
            textoTarde="";
        }
        img_reservas()
    });

    $("input[id=txtMañana]").click(function(){

        if($('#txtMañana').prop('checked') ) {
        $('#txtMañana').val(1);
        textoMañana="Mañana";
        }
        else{
        $('#txtMañana').val(0);
        textoMañana="";
        }
        img_reservas();
    });
    url_reservas="";
    function img_reservas() {
        
        if($('#txtTipoReserva').val()=="normal")
        {
            
            if($('#txtTarde').val()==1 && $('#txtMañana').val()==0)
            {
                url_reservas="{{ asset('imagenes/reservas/tarde.png') }}";
            }
            else if($('#txtTarde').val()==0 && $('#txtMañana').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mañana.png') }}";
            }
            else if($('#txtTarde').val()==1 && $('#txtMañana').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mañana-tarde.png') }}";
            }

        }
        else if($('#txtTipoReserva').val()=="mantenimiento")
        {
            
            textoTarde="";
            textoMañana="Mantenimiento";

            if($('#txtTarde').val()==1 && $('#txtMañana').val()==0)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_tarde.png') }}";

            }
            else if($('#txtTarde').val()==0 && $('#txtMañana').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_mañana.png') }}";

            }
            else if($('#txtTarde').val()==1 && $('#txtMañana').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_mañana_tarde.png') }}";

            }

        }

    
    }



    $("input[id=txtTarde2]").click(function(){
    
        if($('#txtTarde2').prop('checked') ) {
        $('#txtTarde2').val(1);
        $('#bono_tarde2').val(1);
    
        textoTarde="Tarde";
    
        }
        else{
        $('#txtTarde2').val(0);
        $('#bono_tarde2').val(0);
    
        textoTarde="";
        }
        img_reservas2();
    });

    $("input[id=txtMañana2]").click(function(){
        
        if($('#txtMañana2').prop('checked') ) {
        $('#txtMañana2').val(1);
        $('#bono_mañana2').val(1);

        textoMañana="Mañana";
        }
        else{
        $('#txtMañana2').val(0);
        $('#bono_mañana2').val(0);

        textoMañana="";

        }
        img_reservas2();
    });

    function img_reservas2() 
    {
      
        if($('#txtTipoReserva2').val()=="normal")
        {
                
            if($('#txtTarde2').val()==1 && $('#txtMañana2').val()==0)
            {
                url_reservas="{{ asset('imagenes/reservas/tarde.png') }}";
            }
            else if($('#txtTarde2').val()==0 && $('#txtMañana2').val()==1)
            {
            url_reservas="{{ asset('imagenes/reservas/mañana.png') }}";
            }
            else if($('#txtTarde2').val()==1 && $('#txtMañana2').val()==1)
            {
            url_reservas="{{ asset('imagenes/reservas/mañana-tarde.png') }}";
            }
                
        }
        else if($('#txtTipoReserva2').val()=="mantenimiento")
        {
            textoTarde="";
            textoMañana="Mantenimiento";
            
            if($('#txtTarde2').val()==1 && $('#txtMañana2').val()==0)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_tarde.png') }}";

            }
            else if($('#txtTarde2').val()==0 && $('#txtMañana2').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_mañana.png') }}";

            }
            else if($('#txtTarde2').val()==1 && $('#txtMañana2').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_mañana_tarde.png') }}";

            }
        }
    
    }



    $("input[id=txtTarde3]").click(function(){
    
        if($('#txtTarde3').prop('checked') ) {
        $('#txtTarde3').val(1);
       

        textoTarde="Tarde";

        }
        else{
        $('#txtTarde3').val(0);
     

        textoTarde="";
        }
        img_reservas3();
    });

    $("input[id=txtMañana3]").click(function(){
        
        if($('#txtMañana3').prop('checked') ) {
            $('#txtMañana3').val(1);
            
            textoMañana="Mañana";
        }
        else{
        $('#txtMañana3').val(0);
      

            textoMañana="";

        }
        img_reservas3();
    });

    function img_reservas3() 
    {
        if($('#txtTipoReserva3').val()=="normal")
        {
                
          
            if($('#txtTarde3').val()==1 && $('#txtMañana3').val()==0)
            {
               
                url_reservas="{{ asset('imagenes/reservas/tarde.png') }}";
            }
            else if($('#txtTarde3').val()==0 && $('#txtMañana3').val()==1)
            {
                 url_reservas="{{ asset('imagenes/reservas/mañana.png') }}";
            }
            
            else if($('#txtTarde3').val()==1 && $('#txtMañana3').val()==1)
            {
              

                url_reservas="{{ asset('imagenes/reservas/mañana-tarde.png') }}";
            }
                
        }
        else if($('#txtTipoReserva3').val()=="mantenimiento")
        {
            textoTarde="";
            textoMañana="Mantenimiento";
          
            if($('#txtTarde3').val()==1 && $('#txtMañana3').val()==0)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_tarde.png') }}";

            }
            else if($('#txtTarde3').val()==0 && $('#txtMañana3').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_mañana.png') }}";

            }
            else if($('#txtTarde3').val()==1 && $('#txtMañana3').val()==1)
            {
                url_reservas="{{ asset('imagenes/reservas/mantenimiento_mañana_tarde.png') }}";

            }
        }

    }

    $('#reserva_con_bono_ultima_hora').click(function(){
        var ruta = '{{ route("bono.ultima.hora.admin") }}';

        $('#ruta_imagen').val(url_reservas);
        // alert($('#ruta_imagen').val());
        $.ajax({
            type: "POST",
            url: ruta, // This is what I have updated
            data: $("#form_ultimo_dia").serialize(),
            success:function(data) 
            {
                console.log("en el jquery");
                
                if (data.success == 'Reserva completada') 
                {
                alert('Reserva confirmada con éxito');

                location.reload();
                
                }
                if (data.success == 'Hay bonos de findesemana') 
                {
                $('#errores3').css('display','block');

                $('#errores3').html("Hay bonos disponibles de finde semana para realizar la reserva");
                $('.adquiriBonos').css('display','none');
                $('#correcto3').css('display','none');
                }
                if (data.success == 'Hay bonos de entresemana') 
                {
                $('#errores3').css('display','block');

                $('#errores3').html("Hay bonos disponibles de entre semana para realizar la reserva");
                $('.adquiriBonos').css('display','none');
                $('#correcto3').css('display','none');
                }
                if (data.success == 'No hay bonos de ultima hora')
                {
                $('#errores3').css('display','block');

                $('#errores3').html("No hay bonos disponibles de ultima hora, por favor compre un bono para poder realizar la reserva");
                $('.adquiriBonos').css('display','none');
                $('#correcto3').css('display','none');
                }
                if (data.success == 'No hay bonos suficientes de ultima hora')
                {
                $('#errores3').css('display','block');

                $('#errores3').html("No hay bonos suficientes de ultima hora, por favor compre un bono para poder realizar la reserva");
                $('.adquiriBonos').css('display','none');
                $('#correcto3').css('display','none');
                }

                if (data.success == 'Consume bono entresemana')
                {
                $('#errores3').css('display','block');

                $('#errores3').html("Por favor consume el bono de entre semana primero y despues haga otra reserva con el bono de ultima hora");
                $('.adquiriBonos').css('display','none');
                $('#correcto3').css('display','none');
                }

                if (data.success == 'Consume bono findesemana')
                {
                $('#errores3').css('display','block');

                $('#errores3').html("Por favor consume el bono de finde semana primero y despues haga otra reserva con el bono de ultima hora");
                $('.adquiriBonos').css('display','none');
                $('#correcto3').css('display','none');
                }
                //errores

                if (data.errors != "" && data.errors != null) {
                
                }
            }
        })
    });




$('#ir_a_reserva').click(function(){
    id_reserva = $('#IDInfo').val();

    ruta = "{{ route('configuracion_reserva.index',':id') }}";
    

    ruta = ruta.replace(':id',id_reserva);

    location.href = ruta;

})
  


    var textoTarde="";
    var textoMañana="";
    const events=[];

//sacar todos lops eventos que existen en reservas

fetch("{{route('dias_festivos.show')}}", {
  
  headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    "X-CSRF-Token": $('input[name="_token"]').val()
  },
  method: "post",
  credentials: "same-origin",
  body: JSON.stringify({
    key: "value"
  })
})

.then(response => response.json())
.then(data => {
  console.log(data);
  // console.log(data[0]['start']);

  fechas_formateadas = [];


  for (var b=0; b<data.length; b++)
  {
    var element = data[b]['start'].split(' ');
    var fecha = element[0].split('-');
    fecha_formateada = fecha[0]+'-'+fecha[1]+'-'+fecha[2]
    nuevo_registro_fecha = fechas_formateadas.push(fecha_formateada);


  }


  console.log('Array fechas_formateadas:' + fechas_formateadas);
  for (var i=0; i<fechas_formateadas.length; i++) 
  { 
    $('[data-date="'+fechas_formateadas[i]+'"]').css('background-color','#DDFFDC');
     console.log('fecha-formatead: '+fechas_formateadas[i] )
    
  }

 


});



console.log('events');
console.log(events);

    //calendario

    $('.fc-dayGridMonth-button').html('value');
    $('.adquiriBonos').css('display','none');


    $('.cerrar-modal').click(function(){
    location.reload();

    });

    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
            // initialView: 'timeGridDay',
            initialView: 'dayGridMonth',
            headerToolbar: {
                left: 'prev,next today MiBoton',
                center: 'title',
                right:'',
            },
            firstDay: 1,
            customButtons:{
                
                prev:{
                    click:function(){
                    
                    calendar.prev();
                    fetch("{{route('dias_festivos.show')}}", { 
                        headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-Token": $('input[name="_token"]').val()
                        },
                        method: "post",
                        credentials: "same-origin",
                        body: JSON.stringify({
                        key: "value"
                        })
                    })
                    .then(response => response.json())
                    .then(data => {
                        fechas_formateadas = [];
                        for (var b=0; b<data.length; b++)
                        {
                        var element = data[b]['start'].split(' ');
                        var fecha = element[0].split('-');
                        fecha_formateada = fecha[0]+'-'+fecha[1]+'-'+fecha[2]
                        nuevo_registro_fecha = fechas_formateadas.push(fecha_formateada);
                        }
                        imageUrl= "{{ URL::to('/') }}/imagenes/reservas/festivo.png ";
                        for (var i=0; i<fechas_formateadas.length; i++) 
                        { 
                        $('[data-date="'+fechas_formateadas[i]+'"]').prepend('<img id="theImg" style="position:absolute !important"src="' + imageUrl + '" />');
                        }
                    });

                    }
                },
                next:{
                    click:function(){

                    calendar.next();

                    fetch("{{route('dias_festivos.show')}}", { 
                        headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-Token": $('input[name="_token"]').val()
                        },
                        method: "post",
                        credentials: "same-origin",
                        body: JSON.stringify({
                        key: "value"
                        })
                    })
                    .then(response => response.json())
                    .then(data => {
                        fechas_formateadas = [];
                        for (var b=0; b<data.length; b++)
                        {
                        var element = data[b]['start'].split(' ');
                        var fecha = element[0].split('-');
                        fecha_formateada = fecha[0]+'-'+fecha[1]+'-'+fecha[2]
                        nuevo_registro_fecha = fechas_formateadas.push(fecha_formateada);
                        }
                        imageUrl= "{{ URL::to('/') }}/imagenes/reservas/festivo.png ";
                        for (var i=0; i<fechas_formateadas.length; i++) 
                        { 
                        $('[data-date="'+fechas_formateadas[i]+'"]').prepend('<img id="theImg" style="position:absolute !important"src="' + imageUrl + '" />');
                        }
                    });

                    }
                    
                },

                MiBoton:{
                    text:"Reservar",
                    click:function(){
                    

                        $('#exampleModal').modal('toggle');
                        console.log('hola');
                    } 
                }
            },

            dateClick:function(info){
                 //rellenar datos con fecha del cliente para comprobarla con la del servidor
                hoy_cliente = new Date();
                año_cliente = hoy_cliente.getFullYear();
                dia_cliente= hoy_cliente.getDate();
                count_cliente_dia =  dia_cliente.toString().length;
                if(count_cliente_dia==1)
                {
                dia_cliente="0"+dia_cliente;
                }
                mes_cliente= hoy_cliente.getMonth()+1;
        
                count_cliente_mes =  mes_cliente.toString().length;
                
                if(count_cliente_mes==1)
                {
                mes_cliente="0"+mes_cliente;
                }
                fecha_cliente= año_cliente +"-" + mes_cliente+ '-'+dia_cliente;

            

                $('#fecha_actual_control2').val(fecha_cliente);
                $('#fecha_actual_control3').val(fecha_cliente);


                //pasar datos
                $('#txtFecha').val(info.dateStr);
                // $('#fecha_add_bono').val(info.dateStr);
                $('#txtFecha2').val(info.dateStr);
                $('#txtFecha3').val(info.dateStr);


                hoy = new Date();
                año = hoy.getFullYear();
                dia= hoy.getDate();
                count =  dia.toString().length;
                if(count==1)
                {
                dia="0"+dia;
                }
                mes= hoy.getMonth()+1;
                
                h= año +"-" + mes+ '-'+dia;
                // alert(count);
                fecha_dif=moment(info.dateStr, "YYYY-MM-DD");
                h_dif=moment(h, "YYYY-MM-DD");

                // alert(info.dateStr);
                // alert(h);
                diferencias_dias =  fecha_dif.diff(h_dif, 'days');
        
                if(diferencias_dias<=1)
                {
                
                $('#exampleModal').modal('toggle');
            
            





                }
                else if(diferencias_dias>1){
                $('#exampleModal').modal('toggle');
            
            
                }

                //   $('#exampleModal').modal('toggle');

                
            
                
                // calendar.addEvent({title:"evento x", date:info.dateStr});
            },
        
    
            eventClick: function(info) {
                // console.log(info);
                $('#exampleModal3').modal('toggle');
                $('#IDInfo').val(info.event.id);
                $('#FechaInfo').val(info.event.start);
                $('#BarcoInfo').val(info.event.extendedProps.barco_id);
                $('#UsuarioInfo').val(info.event.extendedProps.user_id);
                $('#UsuarioInfoName').val(info.event.extendedProps.user_name);


                if(info.event.extendedProps.mañana==1)
                {
                    $('#MañanaInfo').val('Si');
                }
                else
                {
                    $('#MañanaInfo').val('No');

                }

                if(info.event.extendedProps.tarde==1)
                {
                    $('#TardeInfo').val('Si');
                }
                else
                {
                    $('#TardeInfo').val('No');

                }

                
                if(info.event.extendedProps.checking==1)
                {
                    $('#CheckingInfo').val('Correcto');
                }
                else if(info.event.extendedProps.checking==2)
                {
                    $('#CheckingInfo').val('Error');

                }
                else if(info.event.extendedProps.checking==0)
                {
                    $('#CheckingInfo').val('No se ha hecho');

                }

                if(info.event.extendedProps.checkout==1)
                {
                    $('#CheckoutInfo').val('Correcto');
                }
                else if(info.event.extendedProps.checkout==2)
                {
                    $('#CheckoutInfo').val('Error');

                }
                else if(info.event.extendedProps.checkout==0)
                {
                    $('#CheckoutInfo').val('No se ha hecho');

                }
               
               
               
     




                $('#spanID').html(' #'+info.event.id);
                
          
                console.log(info.event.title);
                console.log(info.event.start);
                console.log(info.event.extendedProps.descripcion);

            },



            eventContent: function(arg, createElement) 
            {
                if(arg.event.extendedProps.tarde==1 && arg.event.extendedProps.mañana==1  )
                {

                
                    if(arg.event.extendedProps.imageUrl)
                    {

                    return createElement('img', {src: arg.event.extendedProps.imageUrl, width: 50, height: 150, id:"imagen_calendario"});
                    }

                }
                else{

                    if(arg.event.extendedProps.imageUrl)
                    {

                    return createElement('img', {src: arg.event.extendedProps.imageUrl, width: 50, height: 75, id:"imagen_calendario"});
                    }

                }

            
                return arg.event.title;
            },
                eventOrder: "-title",

                events:"{{url('/reservas_admin3/show')}}",
                //  events: events,      

        });

        calendar.setOption('locale','Es');
        calendar.render();

        $('#btnAgregar').click(function(){
            ObjEvento=recolectarDatosGUI("POST");
            EnviarInformacion('',ObjEvento);
    

        });

        $('#btnAgregar2').click(function(){
    
            ObjEvento=recolectarDatosGUI2("POST");
            EnviarInformacion2('',ObjEvento);
    

        });

        
        $('#btnAgregar3').click(function(){
    
            ObjEvento=recolectarDatosGUI3("POST");
         EnviarInformacion3('',ObjEvento);


        });

        function recolectarDatosGUI(method) {
                nuevoEvento = {
                barco_id:$('#txtBarcoID').val(),
                title:"ocupado:"+' '+textoMañana+' '+textoTarde,
                user_id:$('#txtUsuarioID').val(),
                start:$('#txtFecha').val(),
                end:$('#txtFecha').val(),
                mañana:$('#txtMañana').val(),
                tarde:$('#txtTarde').val(),
                image_url:url_reservas,
                '_token':$("meta[name='csrf-token']").attr("content"),
                '_method':method,
                }
            return (nuevoEvento);
        }
        function recolectarDatosGUI2(method) {
            nuevoEvento = {
            barco_id:$('#txtBarcoID2').val(),
            title:"ocupado:"+' '+textoMañana+' '+textoTarde,
            user_id:$('#txtUsuarioID2').val(),
            start:$('#txtFecha2').val(),
            end:$('#txtFecha2').val(),
            mañana:$('#txtMañana2').val(),
            tarde:$('#txtTarde2').val(),
            ultimo_dia:$('#ultimo_dia2').val(),
            tipo_bono : $('#tipo_bono2').val(),
            image_url:url_reservas,
            fecha_actual_control : $('#fecha_actual_control2').val(),
            


            '_token':$("meta[name='csrf-token']").attr("content"),
            '_method':method,
            }
            return (nuevoEvento);
        }
        function recolectarDatosGUI3(method) {
            nuevoEvento = {
            barco_id:$('#txtBarcoID3').val(),
            title:"ocupado:"+' '+textoMañana+' '+textoTarde,
            user_id:$('#txtUsuarioID3').val(),
            start:$('#txtFecha3').val(),
            end:$('#txtFecha3').val(),
            mañana:$('#txtMañana3').val(),
            tarde:$('#txtTarde3').val(),
            ultimo_dia:$('#ultimo_dia3').val(),
            tipo_bono : $('#tipo_bono3').val(),
            image_url:url_reservas,
            fecha_actual_control : $('#fecha_actual_control3').val(),


            '_token':$("meta[name='csrf-token']").attr("content"),
            '_method':method,
            }
            return (nuevoEvento);
        }


        function EnviarInformacion(accion,objEvento){
            $.ajax(
            {
                type:"POST",
                url:"{{url('/reservas_admin')}}"+accion,
                data:objEvento,
                success:function(data){
                
                if (data.success == 'Reserva realizada correctamente') 
                {
                    alert('Reserva confirmada con éxito');
                    location.reload();

                }
                
                    if (data.success == 'dia completo') {

                
                        $('#errores').html('No es posible, ya hay dos reservas confirmadas para ese día');



                    }
                    if (data.success == 'ocupado mañana') {

            
                    $('#errores').html('Ocupado mañana');



                    }

                    if (data.success == 'ocupado tarde') {

            
                        $('#errores').html('Ocupado tarde');



                    }

                    
                    if (data.success == 'no check') {

            
                        $('#errores').html('Selecciona al menos una franja horaria');



                    }
                    if (data.success == 'ocupado') {

                        
                    $('#errores').html('Elimine una reserva antes de intentar reservar las dos franjas horarias');



                    }
            
                },

                
                error:function(data){
                
                alert('Las franjas horarias que has marcadas de este barco se encuentra reservadas intenta otras franjas o fecha');
                // alert(data.errors);
            },



            }   
            );

        }


        function EnviarInformacion2(accion,objEvento){
            $.ajax(
            {
                type:"POST",
                url:"{{url('/reservas')}}"+accion,
                data:objEvento,
                success:function(data){
                
                if (data.success == 'Reserva realizada correctamente') {
                    alert('Reserva confirmada con éxito');
                    location.reload();
                    // alert(data.success );

                }
                if (data.success == 'No tienes bonos') {
                    
                    $('#errores2').html('No hay bonos para realizar la reserva ');
                    $('.adquiriBonos').css('display','block');



                }
                if (data.success == 'Ocupado por la mañana') {

                    $('#errores2').html('No es posible, ya hay una reserva confirmada para ese día');


                }
                if (data.success == 'Reserva ok diferente trimestre') {

                    alert('Esta reserva al tratarse de diferente trimestre se descontrán los bonos del próximo trimestre')
                    location.reload();
                

                }

                if (data.success == 'Ocupado por la tarde') 
                {

                    $('#errores2').html('No es posible, ya hay una reserva confirmada para ese día');


                }
                if (data.success == 'dia completo') 
                {

                    $('#errores2').html('No es posible, ya hay dos reservas confirmadas para ese día');


                }

                if (data.success == 'Hay una reserva como minimo') {

                    $('#errores2').html('No puedes reservar el dica completo ya que el barco tiene una reserva');


                }

                if (data.success == 'no check') {

                    $('#errores2').html('Hay que seleccionar al menos un a franja horaria');



                }

                if (data.success == 'reserva atras') {

                    $('#errores2').html('No puedes seleccionar una fecha antigua');


                }

                if (data.success == 'dias corridos') {

                    $('#errores2').html('No puedes reservar una fecha con más de 90 dias de antelación');


                }

                if (data.success == 'simultaneo') {

                    $('#errores2').html('No es posible, ya tienes dos reservas pendientes');


                }

                if (data.success == 'error hora') {

                    $('#errores2').html('Se ha pasado la hora de reserva');


                    }

                    if (data.success == 'encontrado') {

                        $('#errores2').html('No es posible, ya hay dos reservas confirmadas para ese día');


                    }



                    
                
                // alert('correcto');
                //     alert(data.success );
            
                
                },

                
                error:function(data){
                
                alert('Las franjas horarias que has marcadas de este barco se encuentra reservadas intenta otras franjas o fecha');
                // alert(data.errors);
            },



            }   
            );

        }
        


    
        function EnviarInformacion3(accion,objEvento){
         

            $.ajax(
            {
                
                type:"POST",
                url:"{{url('/reservas')}}"+accion,
                data:objEvento,
                success:function(data){
                
                if (data.success == 'Reserva realizada correctamente') {
                    alert('Reserva confirmada con éxito');
                    location.reload();
                    // alert(data.success );

                }
                if (data.success == 'No tienes bonos') {
                    
                    $('#errores3').html('No hay bonos para realizar la reserva ');
                    $('.adquiriBonos').css('display','block');
                    $('#reserva_con_bono_ultima_hora').css('display', 'block');



                }
                if (data.success == 'Ocupado por la mañana') {

                    $('#errores3').html('No es posible, ya hay una reserva confirmada para ese día');


                }
                if (data.success == 'Reserva ok diferente trimestre') {

                    alert('Esta reserva al tratarse de diferente trimestre se descontrán los bonos del próximo trimestre')
                    location.reload();
                

                }

                if (data.success == 'Ocupado por la tarde') 
                {

                    $('#errores3').html('No es posible, ya hay una reserva confirmada para ese día');


                }
                if (data.success == 'dia completo') 
                {

                    $('#errores3').html('No es posible, ya hay dos reservas confirmadas para ese día');


                }

                if (data.success == 'Hay una reserva como minimo') {

                    $('#errores3').html('No puedes reservar el dica completo ya que el barco tiene una reserva');


                }

                if (data.success == 'no check') {

                    $('#errores3').html('Hay que seleccionar al menos un a franja horaria');



                }

                if (data.success == 'reserva atras') {

                    $('#errores3').html('No puedes seleccionar una fecha antigua');


                }

                if (data.success == 'dias corridos') {

                    $('#errores3').html('No puedes reservar una fecha con más de 90 dias de antelación');


                }

                if (data.success == 'simultaneo') {

                    $('#errores3').html('No es posible, ya tienes dos reservas pendientes');


                }

                if (data.success == 'error hora') {

                    $('#errores3').html('Se ha pasado la hora de reserva');


                    }

                    if (data.success == 'encontrado') {

                        $('#errores3').html('No es posible, ya hay dos reservas confirmadas para ese día');


                    }



                    
                
                // alert('correcto');
                //     alert(data.success );
            
                
                },

                
                error:function(data){
                
                alert('Las franjas horarias que has marcadas de este barco se encuentra reservadas intenta otras franjas o fecha');
                // alert(data.errors);
            },



            }   
            );

        }
        
    


    

    

    });


    $("#borrar_reserva").click(function(){


        // alert('entra');
        var ruta = '{{ route("borrar.reserva.form") }}';

        $.ajax({
        type: "POST",
        url: ruta, // This is what I have updated
        data: $("#formBorrarReserva").serialize(),
        success:function(data) {
            console.log("en el jquery");
            if (data.success == 'reserva borrada') {
                alert('Reserva borrada');
                location.reload();

            }
            
            if (data.success == 'reserva antigua') {
                alert('No se puede borrar la reserva antigua, ponerse en contaco con David');
                location.reload();

    
            }
            if (data.success == 'reserva bono extra') {
                alert('No se puede borrar la reserva sin antes chequear el bono extra');
                location.reload();

    
            }

           
            if (data.success == 'pedido en curso') {
                
            //   $('#errores').css('display','block');

            //     $('#errores').html("Ya has pedido un bono, espera a que confirmemos el anterior");
            //     $('.adquiriBonos').css('display','none');
            //     $('#correcto').css('display','none');

                // $('#correcto').html('Su bono ha sido pedido, en recibir el aviso , procederomos a la carga del bono en su cuenta');
    
              
    
              }

            //errores

            if (data.errors != "" && data.errors != null) {
              
            }
        }
      })

    });


    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

</script>





@endsection

