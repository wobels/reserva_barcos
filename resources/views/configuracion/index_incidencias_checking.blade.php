@extends('layouts.app')

@section('content')
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
.tabcontent1 {

  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
@media only screen and (min-width:0px) and (max-width: 450px) {
			
      #contenedor{
          width:100% !important;
        }
  }

</style>

<div id="contenedor" class="container-fluid" style="width:90% ;" >



<div class="tab col-lg-12" style="background: #04607c;">
  <button class="tablinks"><a style="color:white"  href="{{route('configuracion.index')}}">Usuarios</a></button>
  <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.reserva')}}">Reservas</a></button>
  <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.calendario')}}">Calendario</a></button>

  <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.bonos')}}">Bonos Extras @if($count_bonos_extras!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>
  <button class="tablinks"> <a style="color:white"  href="{{route('configuracion.index.descontar')}}">Bonos a descontar</a></button>
  <?php

  $count_reservas_incidencias = 0;
    foreach($reservas as $RES)
    {

      $count_reservas_incidencias++;
    }
  ?>
   <button class="tablinks" style="background:white;"><a style="color:#04607c"  href="{{route('configuracion.index.incidencias.checking')}}">Incidencias check-in  @if($count_reservas_incidencias!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>

 <?php
  $count_reservas_incidencias2 = 0;
    foreach($reservas_checkout as $RES)
    {

      $count_reservas_incidencias2++;
    }
  ?>


<button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.incidencias.checkout')}}">Incidencias checkout @if($count_reservas_incidencias2!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>  
<button class="tablinks" ><a  style="color:white"  href="{{route('configuracion.index.renovaciones')}}"> @if($count_renovaciones!=0)<i class="fas fa-circle text-danger"></i>@endif Renovaciones</a></button> 
<button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.barcos')}}">Barcos</a></button>



</div>
<div class="tab col-lg-12 pt-3 pb-3">
<a  href="{{route('configuracion.index.incidencias.checking')}}" class="btn btn-danger">No verificado</a>
  <a href="{{route('configuracion.index.incidencias.checking.antiguo')}}" class="btn btn-success">Verificado</a>
</div>


<div id="Paris" class="tabcontent1">
<table class="table">
  {!!$dataTable->table() !!}
  </table>
</div>


   
    
</div>


{!!$dataTable->scripts()!!}
<script>

</script>
@include('footer')

@endsection