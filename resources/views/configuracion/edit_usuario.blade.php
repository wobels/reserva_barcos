@extends('layouts.app')

@section('content')
<div class="container-fluid" style="width:90% !important">
    <div class="row">
  


        <div class="col-lg-6">
        
        <div id="errores" style="display:none" class="alert alert-danger">
                   
        </div>
            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif
            @if (session('alert2'))
                <div class="alert alert-danger">
                    {{ session('alert2') }}
                </div>
            @endif
            <h1>Detalle del usuario</h1>
            <a href="{{route('renovar.suscripcion',$usuario->id)}}" class="btn btn-success">Renovar suscripcion</a>
            @if (Auth::user()->id == 1)
            <a style="display:none"  href="{{route('automatizacion')}}" class="btn btn-danger">PULSAR SOLO ROBLES</a>
            @endif

            <a  id="borrar_usuario" class="btn btn-danger">Eliminar Usuario</a>

            <a  href="{{route('automatizacionEliminarImagenes')}}" id="borrar_imagenes" class="btn btn-danger">Eliminar imagenes</a>


            <form method="POST" action="{!!route('editar.usuario',$usuario->id)!!}">
                <label>ID:</label>
                <input class="form-control" type="text" name="id" value="{{$usuario->id}}" readonly>
                <label>Nombre:</label>
                <input class="form-control" type="text" name="name" value="{{$usuario->name}}" required>
                <label>Apellidos:</label>
                <input class="form-control" type="text" name="apellidos" value="{{$usuario->apellidos}}" required>
                <label>DNI:</label>
                <input class="form-control" type="text" name="dni" value="{{$usuario->dni}}" required>
                <label>Teléfono:</label>
                <input class="form-control" type="text" name="telefono" value="{{$usuario->telefono}}" required>
                <label>Tipo de pago :</label>
               
                    <select class="form-control" name="tipo_pago">
                        <option class="text-success" value="{{$usuario->tipo_pago}}" selected>
                            @if($usuario->tipo_pago==1)
                                Mensual
                            @elseif($usuario->tipo_pago==2)
                                Semestral
                            @elseif($usuario->tipo_pago==3)
                                Anual

                            @endif
                        </option>

                        <option value ="1">Mensual</option>
                        <option value ="2">Semestral</option>
                        <option value ="3">Anual</option>

                       

                       
                    </select>
            
                <label>Email:</label>
                <input class="form-control" type="email" name="email" value="{{$usuario->email}}" required>
                <label>Número de bonos entre semana:</label>
                <input class="form-control" type="number" name="bonos_entresemana" value="{{$usuario->numero_bonos_entresemana}}" required>
                <label>Número de bonos finde semana:</label>
                
                <input class="form-control" type="number" name="bonos_findesemana" value="{{$usuario->numero_bonos_findesemana}}" required>
                <label>Número de bonos última hora:</label>
                
                <input class="form-control" type="number" name="bonos_ultima_hora" value="{{$usuario->numero_bonos_ultima_hora}}" required>

                <input value="Editar" class="btn btn-success mt-3" onclick="return confirm('Estas seguro de querer editar el usuario?');" type="submit">

                @csrf
            </form>


            <!-- <a id="editar_usuario" class="btn btn-success mt-3" >Editar</a> -->

            <br>
            <h4 class="mt-3">Cambiar contraseña del usuario</h4>
            <a  href="https://marnific.com/reserva_barcos/public/password/reset" class="btn btn-primary">Restablecer password</a>
        </div>

        <div class="col-lg-6">
            <h1>Reservas del usuario</h1>
            <input class="form-control" type="text" id="myInput" onkeyup="myFunction()" placeholder="Busca por id.." title="Type in a name">
            <br>
            <table  class="table" id="myTable">
                <thead>
                    <tr class="header">
                        <th scope="col" >Editar</th>
                        <th scope="col" >ID</th>
                        <th scope="col" >Fecha</th>
                        <th scope="col" >Tarde</th>
                        <th  scope="col">Mañana</th>
                        <th  scope="col">Checking</th>
                        <th scope="col">Checkout</th>
                    </tr>
                <thead>
                
                @foreach($reservas as $reserva)
                <tr>
                    <th scope="row"><a href="{{route('configuracion_reserva.index',$reserva->id)}}"><i class="fas fa-edit"></i></a></th>
                    <td >{{$reserva->id}}</td>
                    <td >{{$reserva->start}}</td>
                    @if($reserva->tarde==1)
                        <td>Si</td>
                    @elseif($reserva->tarde==0)
                        <td>No</td>

                    @endif

                    @if($reserva->mañana==1)
                        <td>Si</td>
                    @elseif($reserva->mañana==0)
                        <td>No</td>

                    @endif

                    @if($reserva->checking==1)
                        <td>Si</td>
                    @elseif($reserva->checking==0)
                        <td>No realizado</td>

                    @endif

                    @if($reserva->checkout==1)
                        <td>Si</td>
                    @elseif($reserva->checkout==0)
                        <td>No realizado</td>

                    @endif
                    <tr>
                @endforeach
                
            
            </table>  
            
        </div>
    </div>
</div>
<script>
    function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
        }       
    }
    }

    $( "#editar_usuario" ).click(function() {
			
			var url ="{!!route('editar.usuario',$usuario->id)!!}";
			// console.log(url);	
			console.log("serializew");			
			console.log($("#formEditarUsuario").serialize());
            alert('entra');
			$.ajax({
				type: "POST",
				url: url, // This is what I have updated
				data: $("#formEditarUsuario").serialize(),
				success:function(data) {
					
				
					
					
				
			}
		})
	


	});

    $( "#borrar_usuario" ).click(function() {

        if (window.confirm("Seguro que quieres borrar el usuario?")) {
            
			var url ="{!!route('eliminar.usuario',$usuario->id)!!}";
			apollo="apollo";
			$.ajax({
				type: "GET",
				url: url, // This is what I have updated
				 data: apollo,
				success:function(data) {
					
				// alert(data.success);
                    if (data.success == 'usuario borrado') 
                    {
                        alert('Usuario borrado');
                        history.go(-1); 
                        return false;

                    }

                    if (data.success == 'usuario admin') 
                    {
                        // alert('No se puede eliminar ya que es un administrador');
                        $('#errores').css('display','block')
                        $('#errores').html('No se puede eliminar ya que es un administrador');

                    }


				
			}
		})
	
            
        }


	});

</script>

@include('footer')

@endsection