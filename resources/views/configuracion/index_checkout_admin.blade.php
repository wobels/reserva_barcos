@extends('layouts.app')

@section('content')
<div class="container-fluid text-center " style="width: 90% !important">
    @if($reservas->checkout==0)
        <h1 class="text-center" >¿Esta todo como se espera?</h1>
        <a class="btn btn-primary mt-3"   style="font-size:50px; height:150px; width:150px" id="si" >Si</a>
        <a class="btn btn-danger  mt-3"    style="font-size:50px; height:150px; width:150px" id="no">No</a>
    @else
        <h1>Checkout realizado correctamente <i class="fas fa-check text-success"></i></h1>
        <a class="btn btn-warning" id="atras"> Volver atrás</a>

    @endif
</div>
  

<script>
$("#atras").click(function(){
    history.go(-1); return false
})
  $("#si").click(function(){
    
        var ruta = '{{ route("checkout.admin",$reservas->id) }}';

        $.ajax({
            type: "GET",
            url: ruta, // This is what I have updated
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'checkout correcto realizado') {
                   

                    history.go(-1); return false
                    

                }

                if (data.success == 'checkout incidencia NO realizado') {
               
                    alert('No esta hecho el checking de esta reserva, realizalo primero')
                    // history.go(-1); return false
                    

                }

                //errores

                if (data.errors != "" && data.errors != null) {
      
                }
            }
        })
    });

    $("#no").click(function(){
    
    var ruta = '{{ route("checkout_incidencia.admin",$reservas->id) }}';

    $.ajax({
        type: "GET",
        url: ruta, // This is what I have updated
        success:function(data) {
            console.log("en el jquery");
            if (data.success == 'checkout incidencia realizado') {
               
                history.go(-1); return false
                

            }

            if (data.success == 'checkout incidencia NO realizado') {
               
               alert('No esta hecho el checking de esta reserva, realizalo primero')
               history.go(-1); return false
               

           }


            //errores

            if (data.errors != "" && data.errors != null) {
  
            }
        }
    })
});
</script>
@endsection