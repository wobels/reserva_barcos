@extends('layouts.app')

@section('content')
<div class="container-fluid text-center " style="width: 90% !important">
    @if($reservas->checking==0)
        <h1 class="text-center" >¿Esta todo como se espera?</h1>
        <a class="btn btn-primary mt-3"  style="font-size:50px; height:150px; width:150px" id="si" >Si</a>
        <a class="btn btn-danger  mt-3"   style="font-size:50px; height:150px; width:150px" id="no">No</a>
    @else
        <h1>Checking realizado correctamente <i class="fas fa-check text-success"></i></h1>
        <a class="btn btn-warning" id="atras"> Volver atrás</a>

    @endif
</div>
  

<script>
$("#atras").click(function(){
    history.go(-1); return false
})
  $("#si").click(function(){
    
        var ruta = '{{ route("checking.admin",$reservas->id) }}';

        $.ajax({
            type: "GET",
            url: ruta, // This is what I have updated
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'checking correcto realizado') {
                   

                    history.go(-1); return false
                    

                }

                //errores

                if (data.errors != "" && data.errors != null) {
      
                }
            }
        })
    });

    $("#no").click(function(){
    
    var ruta = '{{ route("checking_incidencia.admin",$reservas->id) }}';

    $.ajax({
        type: "GET",
        url: ruta, // This is what I have updated
        success:function(data) {
            console.log("en el jquery");
            if (data.success == 'checking incidencia realizado') {
               
                alert('entra'); 
                history.go(-1); return false
                

            }

            //errores

            if (data.errors != "" && data.errors != null) {
  
            }
        }
    })
});
</script>
@endsection