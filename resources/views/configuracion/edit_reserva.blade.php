@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="width:90% !important">
        <div class="row">
            <div class="col-lg-12">
                <a href="{!!url('/configuracion/index_reservas')!!}" class="btn btn-success"> <i class="fas fa-hand-point-left"></i> Atrás</a>
                <a id="ir-usuario" class="btn btn-warning"><i class="fas fa-user" style="color:white"></i> Ir al usuario de la reserva</a>
                <a id="hacer-checking" href="{!!route('checking.admin.index',$reserva->id)!!}"  onclick="return confirm('Estas seguro de querer hacer checking de esta reserva?');" class="btn btn-success"><i class="fas fa-door-open"></i> Realizar checking</a>
                <a id="hacer-checking" href="{!!route('checkout.admin.index',$reserva->id)!!}"  onclick="return confirm('Estas seguro de querer hacer checkout de esta reserva?');" class="btn btn-success"><i class="fas fa-door-closed"></i> Realizar checkout</a>
                <a id="borrar-reserva"   onclick="return confirm('Estas seguro de querer borrar esta reserva?');" class="btn btn-danger"><i class="fas fa-trash"></i> Borrar reserva</a>


            </div>
            
            <div class="col-lg-4 mt-4">
                <h1>Información de la reserva</h1>
                    <label>ID:</label>
                    <input class="form-control" type="text" value="{{$reserva->id}}"readonly>
                    <label>Fecha:</label>
                    <input class="form-control" type="text" value="{{$reserva->start}}"readonly>
                    <label>Barco:</label>
                    <input class="form-control" type="text" value="{{$nombre_barco}}" readonly>
                    <label>Usuario:</label>
                    @if($reserva->user_name=="")
                    <input class="form-control" type="text" value="Marnific" readonly> 

                    @else
                    <input class="form-control" type="text" value="{{$reserva->user_name}}" readonly> 
                    @endif
                    <label>Mañana:</label>
                    @if($reserva->mañana==1)
                    <input class="form-control" type="text" value="Si" readonly>
                    @elseif($reserva->mañana==0)
                    <input class="form-control" type="text" value="No" readonly>
                    @endif
                    <label>Tarde:</label>
                    @if($reserva->tarde==1)
                    <input class="form-control" type="text" value="Si" readonly>
                    @elseif($reserva->tarde==0)
                    <input class="form-control" type="text" value="No" readonly>

                    @endif

                  
                    <label>Checking:</label>
                    @if($reserva->checking==1)
                    <input class="form-control" type="text" value="Correcto" readonly>
                    @elseif($reserva->checking==2)
                    <input class="form-control" type="text" value="Erroneo" readonly>

                    @elseif($reserva->checking==0)
                    <input class="form-control" type="text" value="No realizado" readonly>
                    @endif

                    


                    <label>Checkout:</label>
                    @if($reserva->checkout==1)
                    <input class="form-control" type="text" value="Correcto" readonly>
                    @elseif($reserva->checkout==2)
                    <input class="form-control" type="text" value="Erroneo" readonly>

                    @elseif($reserva->checkout==0)
                    <input class="form-control" type="text" value="No realizado" readonly>
                    @endif


                    <br>

                    <div style="background:#04607c;color:white; "class="col-lg-12 mt-5 text-center p-1"><h3>Imgs contador horas y gasolina <span style="color:yellow"> Check in </span></h3></div>

                    <div class="col-lg-12">
                        <div class="row">

                            <div class="col-lg-6">
                            Imagen horas Check in, <span style="color:red;">{{$reserva->hora_horas}}</span>
                                @if($reserva->imagen_horas!="")
                                <a href="{{ URL::to('/') }}/imagenes/checkings/horas/{{$reserva->imagen_horas}}">
                                <img class="col-lg-12" width="100%" height="250px" src=" {{ URL::to('/') }}/imagenes/checkings/horas/{{$reserva->imagen_horas}}">
                                </a>
                                @elseif($reserva->imagen_horas=="")
                                <br>

                                <h5>No hay imágen subida</h5>
                                @endif
                            </div>
                            <div class="col-lg-6">
                            Imagen gasolina Check in, <span style="color:red;">{{$reserva->hora_gasolina}}</span>
                            @if($reserva->imagen_gasolina!="")
                            <a href="{{ URL::to('/') }}/imagenes/checkings/gasolina/{{$reserva->imagen_gasolina}}">
                            <img class="col-lg-12" width="100%" height="250px" src=" {{ URL::to('/') }}/imagenes/checkings/gasolina/{{$reserva->imagen_gasolina}}">
                            </a>
                            @elseif($reserva->imagen_gasolina=="")
                                <br>

                                <h5>No hay imágen subida</h5>
                                @endif
                            </div>
                        </div>
                        
                    </div>
                  
                    <div style="background:#04607c ;color:white;"class="col-lg-12 mt-5 p-1 text-center"><h3>Imgs contador horas, batería y gasolina <span style="color:yellow"> Check out </span></h3></div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                            Imagen horas Check out, <span style="color:red;">{{$reserva->hora_horas_checkout}}</span>
                                @if($reserva->imagen_horas_checkout!="")
                                <a href="{{ URL::to('/') }}/imagenes/checkouts/horas/{{$reserva->imagen_horas_checkout}}">
                                <img class="col-lg-12" width="100%" height="250px" src=" {{ URL::to('/') }}/imagenes/checkouts/horas/{{$reserva->imagen_horas_checkout}}">
                                </a>
                                @elseif($reserva->imagen_horas_checkout=="")
                                <br>

                                <h5>No hay imágen subida</h5>
                                @endif
                            </div>
                            <div class="col-lg-6">
                            Imagen del deposito de gasolina y ticket de gasolinera Check out, <span style="color:red;">{{$reserva->hora_gasolina_checkout}}</span>
                                @if($reserva->imagen_gasolina_checkout!="")
                                <a href="{{ URL::to('/') }}/imagenes/checkouts/gasolina/{{$reserva->imagen_gasolina_checkout}}">
                                <img class="col-lg-12" width="100%" height="250px" src=" {{ URL::to('/') }}/imagenes/checkouts/gasolina/{{$reserva->imagen_gasolina_checkout}}">
                                </a>
                                @elseif($reserva->imagen_gasolina_checkout=="")
                                    <br>

                                    <h5>No hay imágen subida</h5>
                                    @endif
                            </div>

                            <div class="col-lg-6">
                                Imagen batería Check out, <span style="color:red;">{{$reserva->hora_bateria_checkout}}</span>
                                @if($reserva->imagen_bateria!="")
                                <a href="{{ URL::to('/') }}/imagenes/checkouts/bateria/{{$reserva->imagen_bateria}}">
                                <img class="col-lg-12" width="100%" height="250px" src=" {{ URL::to('/') }}/imagenes/checkouts/bateria/{{$reserva->imagen_bateria}}">
                                </a>
                                @elseif($reserva->imagen_bateria=="")
                                    <br>

                                    <h5>No hay imágen subida</h5>
                                    @endif
                            </div>


                        </div>
                        
                    </div>

                    <br>

                  

            </div>

            <!-- <div class="col-lg-4">
             contador horas
            </div>

            <div class="col-lg-4">
                contador gsaolina
            </div> -->
            

            <div class="col-lg-4 mt-4">
                <h1>Incidencias check in</h1>
                @if($reserva->checking==0)
                <br>
                <h5>   <i class="fas fa-clock"></i>Aun no es día para hacer el checking o no se realizo el checking </h5>
                @elseif($reserva->checking==1)
                <br>
                <h5>   <i class="fas fa-check"></i>Checking correcto</h5>

                @elseif($reserva->checking==2)
                    <br>
                    <textarea class="form-control" readonly>{{$reserva->incidencias_checking}}</textarea>
                     <br>
                    
                    <h5>Imágenes incidencias:</h5>
                    <div class="row">
                        @foreach($imagenes_incidencias_checking as $img_checking)
                        <a class="col-lg-6" href="{{ URL::to('/') }}/imagenes/checkings/incidencias_checking/{{$img_checking->nombre_archivo}}">    
                            <img class="col-lg-12" width="50%" height="250px" src=" {{ URL::to('/') }}/imagenes/checkings/incidencias_checking/{{$img_checking->nombre_archivo}}">
                        </a>
                        @endforeach 
                    </div>

                @endif
            

            </div>

          

            <div class="col-lg-4 mt-4">

               
          
                <h1>Incidencias check out</h1>
                @if($reserva->checkout==0)
                <br>
                <h5>   <i class="fas fa-clock"></i>Aun no es día para hacer el Checkout o no se realizo el Checkout</h5>
                @elseif($reserva->checkout==1)
                <br>
                <h5>   <i class="fas fa-check"></i>Checkout correcto</h5>

                @elseif($reserva->checkout==2)
                    <br>
                    <textarea class="form-control" readonly>{{$reserva->incidencias_checkout}}</textarea>
                    <br>

                    <h5>Imágenes incidencias:</h5>
                    <div class="row">
                        @foreach($imagenes_incidencias_checkout as $img_checkout)
                        <a class="col-lg-6" href="{{ URL::to('/') }}/imagenes/checkings/incidencias_checkout/{{$img_checkout->nombre_archivo}}">    
                            
                            <img class="col-lg-12" width="50%" height="250px" src=" {{ URL::to('/') }}/imagenes/checkings/incidencias_checkout/{{$img_checkout->nombre_archivo}}">
                        </a>
                        @endforeach 
                    </div>


                @endif
            </div>

        </div>
    </div>
@include('footer')

<script>


        $('#ir-usuario').click(function(){
            ruta = "{{ route('configuracion_usuario.index',$id_usuario) }}";
            location.href = ruta;

        })

        $( "#borrar-reserva" ).click(function() {
			
			var url ="{!!route('borrar.reserva',$reserva->id)!!}";
			apollo="apollo";
			$.ajax({
				type: "GET",
				url: url, // This is what I have updated
				 data: apollo,
				success:function(data) {
					
				
                    if (data.success == 'reserva borrada') 
                    {
                        alert('Reserva borrada');
                        history.go(-1); 
                        return false;

                    }

                    if (data.success == 'reserva antigua') 
                    {
                        alert('No se puede eliminar una reserva con fecha anterior, avisar a david para explicar');
                        history.go(-1); 
                        return false;

                    }

                    if (data.success == 'reserva bono extra') 
                    {
                        alert('No se puede eliminar ya que la reserva esta ligada a un bono extra pendiente de confirmacion');

                    }


				
			}
		})
	


	});
</script>

@endsection