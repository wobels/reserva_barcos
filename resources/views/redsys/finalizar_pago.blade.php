@extends('layouts.app')
<style>
    #btn_submit{
        BACKGROUND: green;
        color:white ; 
        border:0px
    }
</style>

@section('content')
    

    <div class="container-fluid pb-5 " style="width:90% !important;">
         <h1 class="h1 text-center">FINALIZAR PAGO</h1>
        <div class="row">

            <div class="col-lg-4">
            </div>

            <div class="col-lg-4 border float-center">

                <table class="table">

                    <tr>
                        <td>Fecha de compra:</td>
                        <td><?php echo date("m/d/y");?></td>
                        <td></td>
                    </tr>
                    
                    <tr>
                        <td>Nombre:</td>
                        <td>{{$nombre}}</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>Cantidad:</td>
                        <td>{{$cantidad}} / BONOS</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Precio:</td>
                        <td>{{$precio_unidad}}€ / UND</td>
                        <td></td>
                    </tr>
 

                    <tr>
                        <td>Total:</td>
                        <td>{{$precio}}€</td>
                        <td></td>
                    </tr>

                </table>
                <br>
            
                
                <div class="col-lg-12 text-center">
                    {!!$form!!}
                </div>
                
            </div>

            <div class="col-lg-4 ">
            </div>

        </div>

       
    </div>

            
            
    

<script type="text/javascript">
    $('#btn_submit').attr('value','Finalizar compra');
</script>




@endsection