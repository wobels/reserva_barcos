@extends('layouts.app')
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/css_lightbox_imagenes.css') }}" > -->
@section('content')



    <div class="container-fluid " style="width:90% !important;background:transparent !important">
     
        <?php $num=0;?>
        @foreach($bonos as $bono)
            <?php $num++ ?>
            <div class="form-amount mt-2" id="{{$bono->barco->id}}" >
                <div  class="row p-3 "  >
                    <div    class="form-group col-lg-12 border rounded p-3 pr-4 bg-light"  >
                   
                        <div class="row">

                            <div  class="col-lg-5">
                                 <h1 class="text-center">{{$bono->barco->nombre}}</h1>  
                                <div class="col-lg-12" >
                                    
                                    <img class="xzoom" width="100%" height="400px" src=" {{ URL::to('/') }}/imagenes/barcos/{{$bono->imagen_principal}}">
                                   
                                    <?php 
                                        $contador_imagenes = 0; 
                                        $id_barco = $bono->barco->id;
                                        $imagenes_barco = App\Models\ImagenBarco::where('barco_id', $id_barco)->get();
                                    ?>

                                    @foreach($imagenes_barco as $imagen)
                                     <?php $contador_imagenes++ ?>


                                        <a style="padding:0px !important;" href=" {{ URL::to('/') }}/imagenes/barcos/{{$imagen->nombre_archivo}}">
                                            <img class="xzoom ml-1 mt-4"  width="30%" height="150px" src=" {{ URL::to('/') }}/imagenes/barcos/{{$imagen->nombre_archivo}}">  
                                        </a> 
                                    @endforeach

                                    

                                </div>
                            </div>

                            <div class="col-lg-7 pt-5">
                                <div class="row">
                                <h4 class="col-lg-12 text-left mb-3 ml-2"><u>Detalles tecnicos de :</u><span class="text-muted" style="font-size:15px" > {{$bono->barco->titulo}}</span></h4>
                                    <div class="col-lg-3 " style="padding:0px !important;">
                                        <ul>
                                        <li><label class="ml-2" for="amount"><strong>Eslora</strong>:{{$bono->barco->eslora}}</label></li>
                                        <li><label class="ml-2"for="amount"><strong>Manga</strong>:{{$bono->barco->manga}}</label></li>
                                        <li><label class="ml-2"for="amount"><strong>Calado</strong>:{{$bono->barco->calado}}</label></li>
                                        <li><label class="ml-2"for="amount"><strong>Motor</strong>:{{$bono->barco->motor}}</label></li>
                                        <li><label class="ml-2"for="amount"><strong>Camarotes</strong>:{{$bono->barco->camarotes}}</label></li>
                                        <li><label class="ml-2"for="amount"><strong>Baños</strong>:{{$bono->barco->baños}}</label></li>

                                        </ul>
                                       
                              
                                    </div>
                                    <div class="col-lg-4 " style="padding:0px !important;">
                                       
                                        <ul>
                                        <li><label class="ml-2"for="amount"><strong>Velocidad máxima</strong>:{{$bono->barco->velocidad_maxima}}</label></li>
                                        <li><label class="ml-2"for="amount"><strong>Velocidad de crucero</strong>:{{$bono->barco->velocidad_crucero}}</label></li>
                                        <li> <label class="ml-2"for="amount"><strong>Desplazamiento</strong>:{{$bono->barco->desplazamiento}}</label></li>

                                        <li> <label class="ml-2"for="amount"><strong>Nº personas a bordo</strong>:{{$bono->barco->capacidad}}</label></li>
                                        
                                        <li><label class="ml-2"for="amount"><strong>Capacidad agua dulce</strong>:{{$bono->barco->capacidad_agua_dulce}}</label></li>
                                        <li><label class="ml-2"for="amount"><strong>Capacidad combustible</strong>:{{$bono->barco->capacidad_combustible}}</label></li>
                                        </ul>
                              
                                    </div>
                                 
                                </div>
                                <div class="col-lg-12">
                                    <a href="{{route('barcos.edit',$bono->barco->id)}}"  class="btn btn-secondary" >Información detallada</a> 
                                    <a class="btn btn-secondary" >Mensaje</a> 
                                    <a class="btn btn-secondary" >Ver</a> 
                                </div>
                                <div class="col-lg-12 border pl-3 pr-3 pb-3 mt-2 div_descripcion" style="overflow:hidden" id="div_descripcion-<?php echo $num;?>">
                                
                                    

                                    <strong>Descripcion:</strong><br>

                                    <p id="descripcion_acortada-<?php echo $num;?>">
                                        {{ substr ($bono->barco->descripcion,0,600)}}
                                        <a  style="color:#57CFFF;cursor:pointer" id="leer_mas_descripcion-<?php echo $num;?>" >... [Leer más]
                                        <input class="input_descripcion_corta" hidden id="input_leer_mas_descripcion-<?php echo $num;?>" type="text" value="<?php echo $num;?>"> 
                                        </a>
                                        
                                    </p>

                                    <p class="descripcion_larga" id="descripcion_larga-<?php echo $num;?>">
                                        {{ $bono->barco->descripcion}}
                                        <a style="color:#57CFFF;cursor:pointer"id="leer_menos_descripcion-<?php echo $num;?>" >[Leer menos]
                                            <input class="input_descripcion_corta" hidden id="input_leer_mas_descripcion-<?php echo $num;?>" type="text" value="<?php echo $num;?>"> 
                                        </a>
                                    </p>  
                                </div>

                                <div class="col-lg-12 border pl-3 pr-3 pb-3 mt-2 div_mensaje"  id="div_mensaje-<?php echo $num;?>">
                                    <strong>Escribir mensaje:</strong><br>
                                    <form class="col-lg-12">
                                        <input class="form-control mt-3" placeholder="Correo electrónico">
                                        <input class="form-control mt-3" placeholder="Asunto">
                                        <textarea class="form-control mt-3" placeholder="Mensaje"></textarea>
                                        <input class="btn btn-success mt-3" type="submit">
                                    </form>
                                </div>

                                <br>
                                <div class="row mt-4">
                                    
                                    <div hidden class="col-lg-3 border rounded ml-5 p-0">
                                    
                                        <h5 class="col-lg-12 bg-dark p-2" style="color:white">Comprar bonos</h5> 
                                        <form class="form-amount mt-2 p-3" id="form-comprar-bonos-{{$bono->barco->id}}"  action="{{ url('/redsys') }}" method="get">
                                    

                                            
                                            <label class="text-left mt-2" for="amount">Cantidad:</label>
                                            <input type="text" id="cantidad" name="cantidad" autocomplete="off" class="form-control col-lg-12 float-right" placeholder="Por ejemplo: 450" value="1">
                                            <input hidden name="nombre" type="text" value="{{$bono->barco->nombre}}">
                                            <input hidden name="precio" type="number" value="{{$bono->precio}}">
                                            <br>
                                            <input class="form-control btn btn-primary mt-3 col-lg-12  " name="submitPayment" type="submit" value="Comprar Bonos redsys"> <br>
                                            <a class="form-control btn btn-primary col-lg-12 mt-3">Comprar Bonos pruebas</a>
                                        </form>  
                                    </div>
                                  
                                    <div class="col-lg-7 border rounded ml-5" style="background:#3c3c3b;color:#AEAEAE">
                                        <div class="row">
                                            <div class="col-lg-6"> 
                                                <br>
                                                <br>
                                                <h3 style="color:#F0D340" class="h2">Precios:</h3><br>
                                                <h5>Bono entresemana = {{$bono->precio}}€</h5>
                                                <h5>Bono findesemana = {{$bono->precio_findesemana}}€</h5>
                                               
                                            </div>
                                            <div class="col-lg-6"> 
                                            <h2 style="color:white" class="h2 mt-3 text-center">Tipo de barco</h2>
                                                <img class=" ml-1 mt-4"  width="100%" height="150px" src=" {{ URL::to('/') }}/imagenes/iconos_tipo_barco/yate.png">  
                                                <h2   class="col-lg-12 text-center text-primary">Yate</h2>
                                            </div>


                                        </div>
                                        
                                    </div>
                                </div>
                            

                            </div>

                        </div>
                        
                    </div>
                
                </div>

               
            
            </div>
        @endforeach
    </div>

    <script type="text/javascript">
     
        $('.descripcion_larga').css('display','none');
        $('.div_mensaje').css('display','none');
  
        $('[id^=comprobar_fecha-]').click(function(){
          

            // alert('entra');
            
            var num = $('input.numero_input_reserva', this).val();
             
            var ruta = '{{ route("consulta.reserva") }}';
                // ruta = ruta.replace(':factura_id', factura_id);

            $.ajax({
                type: "GET",
                url: ruta, // This is what I have updated
                data: $("#formReservar-"+num).serialize(),
                success:function(data) {
                    console.log("en el jquery");
                    if (data.success == 'Consulta hecha correctamente') {
                    

                  alert('entra');
                        

                    }

                    //errores

                    if (data.errors != "" && data.errors != null) {
                        // console.log("error");
                        // console.log(data.errors);
                    
                        
                        $("#errores").empty();
                        var erroresAlertas = "<br><div class=\"alert alert-danger \" style=\"float:left;width:100%\" >";
                        jQuery.each(data.errors, function(key, value) {
                            erroresAlertas = erroresAlertas + '<p>' + value + '</p>';
                        });
                        erroresAlertas += '</div>';
                        $("#errores").append(erroresAlertas);
                    }
                }
            })
        });
       
        $('[id^=leer_mas_descripcion-]').click(function(){
                
                var num = $('input.input_descripcion_corta', this).val();

                $('#descripcion_acortada-'+num).css('display','none');
                $('#descripcion_larga-'+num).css('display','block');

                
                
        });

        $('[id^=leer_menos_descripcion-]').click(function(){
            
            var num = $('input.input_descripcion_corta', this).val();

            $('#descripcion_acortada-'+num).css('display','block');
            $('#descripcion_larga-'+num).css('display','none');

            
            
        });





     


    </script>
 
@endsection

