<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ URL::to('/') }}/imagenes/reservas/favicon.png">

    <!-- <title>{{ config('app.name', 'Marnific') }}</title> -->
    <title>Marnific</title>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Dropzone -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />

    <!-- zoom jquery -->

    <link rel="stylesheet" type="text/css" href="https://unpkg.com/xzoom/dist/xzoom.css" media="all" />
    <scrip type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/xzoom/dist/xzoom.min.js"></script>

    <!-- datatable -->

    <link rel="stylesheet"  href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" />
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <!-- buttons -->
    <link rel="stylesheet"  href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css" />
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>



    <!-- Full Calendar -->

    <link href="{{asset('fullcalendar/main.css')}}"  rel='stylesheet' />
    <script src="{{asset('fullcalendar/main.js')}}"></script>

<script src="https://momentjs.com/downloads/moment.min.js"></script>


<!-- impedir escalar -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">




    <!-- Styles -->
    <style>
       
 
        @media only screen and (min-width:0px) and (max-width: 600px) {
            #texto_zona{
                margin-left:10%;
            }
            
         
           
        }
       
      @media only screen and (min-width:768px) and (max-width: 991px) {
                #navbarSupportedContent{
                margin-left:-3%!important;
            }
        }
        @media only screen and (min-width:992px) and (max-width: 1600px) {
            /* #containder-header{
                width:135% !important;

            } */
        }

        .container-fluid{
            background: white !important;
        }
        .py-4{
           
            padding-bottom:0px !important;
        }
        .tituloEdit{
            padding:0px !important;
            font-weight: bold !important;
        }
     </style>
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body class="p-0">
    <div id="app">
        <nav  class="navbar navbar-expand-md navbar-light bg-white shadow-sm" id="containder-header" style="">
            <div class="container col-lg-12">
           

                <a class="navbar-brand" href="{{ url('/home') }}">
                   
                   <img style="" src=" {{ URL::to('/') }}/imagenes/reservas/logo-horizontal-azul.png"/>
                </a>
                <h3 class="mt-3" style="color:#a8c8d2" id="texto_zona">ZONA SOCIOS</h3>
                 @if(Auth::user()!="" && Auth::user()->admin==0 )
                <a class="navbar-brand btn btn-primary ml-5 "  style="background:#21697C !important;color:white" href="{{ url('/home') }}">
                     <i class="fas fa-calendar-alt    " style="color:white" ></i> Calendario
                </a>
                @elseif(Auth::user()!="" && Auth::user()->admin==1)
                <a class="navbar-brand btn btn-primary ml-5 "  style="background:#21697C !important;color:white" href="{{ url('/configuracion/calendario') }}">
                     <i class="fas fa-calendar-alt    " style="color:white" ></i> Calendario
                </a>

                @endif
                <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                    @if(Auth::guest())
                        <div class=" d-block d-sm-block d-md-none ">
                            <a  style="font-size:30px" class="dropdown-item border-bottom" href="https://marnific.com/">
                                <i class="fas fa-globe text-primary"></i> Ir a la web
                            </a>
                        </div>
                    @endif
                  

                    <!-- Right Side Of Navbar -->
                    <ul class="ml-auto col-lg-10" style="list-style:none">
                        <!-- Authentication Links -->
                        @guest
                        
                            <!-- <li class="nav-item float-right">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li> -->
                            @if (Route::has('register'))
                                <!-- <li class="nav-item float-right">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li> -->

                            @endif
                            @else
                                <!-- 
                                <a  class="navbar-brand"  href="{{ url('/home') }}">
                                    Catalogo
                                </a> -->
                                        <!-- 
                                <a  class="navbar-brand"  href="{{ url('/bonos/index') }}">
                                    Adquirir bonos
                                </a> -->

                                <!-- <a  class="navbar-brand"  href="{{ url('/bonos') }}">
                                    Reservar barco
                                </a> -->
                                    <!-- 
                                <a  class="navbar-brand"  href="{{ url('/') }}">
                                    Contactar
                                </a> -->
                                
                            <div class="text-right d-none d-sm-none d-md-block">
                                    
                                @if(Auth::user()->admin==1)
                                    

                                    <a  class="navbar-brand" href="{{ route('register') }}">
                                        <i class="fas fa-plus text-primary"></i> Cliente Nuevo
                                    </a>

                                 

                                    <a  class="navbar-brand" href="{{ route('configuracion.index') }}">
                                        <i class="fas fa-cogs text-primary"></i> Configuración
                                    </a>
                                @endif

                                @if(Auth::user()->admin==0)
                                    <a  target="_blank" href="https://api.whatsapp.com/send?phone=34627834930&amp;text="  class="navbar-brand" href="{{ url('/') }}">
                                        <i class="fab fa-whatsapp-square text-success"></i> Whatshapp
                                    </a>
                                @endif

                                    @if(Auth::user()->admin==0)
                                    <a  class="navbar-brand" href="{{ route('datos.cliente') }}">
                                        <i class="fas fa-user text-primary"></i> Mis datos
                                    </a>
                                @endif


                                @if(Auth::user()->admin==0)
                                    <a  class="navbar-brand" href="https://marnific.com/formulario-app/">
                                        <i class="fas fa-envelope text-primary"></i> Contacto
                                    </a>
                                @endif

                                <a class="navbar-brand" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    <i class="fas fa-power-off text-danger"></i> <strong style="color:#3c3c3b">{{ __('Logout') }}</strong>
                                </a>


                            </div>

                            <li class="nav-item dropdown float-right d-block d-sm-block d-md-none mt-3 border col-lg-12 p-0"   >
                                <!-- <a id="navbarDropdown" class="nav-link dropdown-toggle col-md-4" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a> -->
                                    

                                @if(Auth::user()->admin==1)
                                

                                    <a  style="font-size:30px" class="dropdown-item border-bottom" href="{{ route('register') }}">
                                        <i class="fas fa-plus text-primary"></i> Cliente Nuevo
                                    </a>

                                    

                                    <a   style="font-size:30px" class="dropdown-item border-bottom" href="{{ route('configuracion.index') }}">
                                    <i class="fas fa-cogs text-primary"></i> Configuración
                                    </a>
                                @endif
                                
                                @if(Auth::user()->admin==0)
                                    <a   style="font-size:30px" target="_blank" href="https://api.whatsapp.com/send?phone=34627834930&amp;text="  class="dropdown-item border-bottom" href="{{ url('/') }}">
                                        <i class="fab fa-whatsapp-square text-success"></i> Whatshapp
                                    </a>
                                @endif

                                @if(Auth::user()->admin==0)
                                    <a   style="font-size:30px"  class="dropdown-item border-bottom" href="{{ route('datos.cliente') }}">
                                        <i class="fas fa-user text-primary"></i> Mis datos
                                    </a>
                                @endif


                                @if(Auth::user()->admin==0)
                                    <a  style="font-size:30px"  class="dropdown-item border-bottom" href="{{ route('contacto.cliente') }}">
                                        <i class="fas fa-envelope text-primary"></i> Contacto
                                    </a>
                                @endif

                               
                                
                                <a  style="font-size:30px"  class="dropdown-item border-bottom" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    <i class="fas fa-power-off text-danger"></i> <strong style="color:#3c3c3b">{{ __('Logout') }}</strong>
                                </a>

                               

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                                
                            </li>
                          
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
