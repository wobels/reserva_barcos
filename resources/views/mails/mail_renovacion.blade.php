<div id="header" style="padding:5px; background:#04607c;text-align:center">
        <!-- <img id="img_logo_footer" src=" {{ URL::to('/') }}/imagenes/reservas/"/> -->
    <img id="img_logo_footer" src="https://marnific.com/reserva_barcos/public/imagenes/reservas/Logo-marnific-blanco-2.png" alt="Logo" title="Logo" style="display:block;margin:auto" width="15%" height="50%" />

</div>

<div id="cuerpo" style="border-top:1px solid #CFCDCD;border-left:1px solid #CFCDCD;border-right:1px solid #CFCDCD;">
  
    
    <div style="text-align:center">
        <h1 style="width:100%; text-align:center;color:#04607c;font-weight:normal">Recordatorio</h1>
        <p style="font-size:20px;color:#6c6c6c">
            <strong>Hola {{$name}},<br/>tu suscripción 
            @if($expiracion==3) 
                finaliza el {{$fecha_renovacion}}
            @elseif($expiracion==0)
                finaliza hoy
            @elseif($expiracion==4)
                ha sido renovada hasta {{$fecha_renovacion_mas_anyo}}
            @endif
            </strong>
        </p>
    </div>


       
    <div style="width:100%;border:1px solid #CFCDCD;">
        
        <div style="text-align:center">
            <h3 style="color:#575757;width:100%;text-align:center"><u>Detalles de renovación</u></h3>
            @if($expiracion==3)
                <p>{{$name}} le quedan tres meses para finalizar su suscripción, la fecha de vencimiento es el {{$fecha_renovacion}},
                acuerdese de ponerse en contacto llegado el día para tramitar una nueva suscripción. De todas maneras el dia que le toque renovar le menadaremos
                un correo de recordatorio.</p>
            @elseif($expiracion==0)
                <p>{{$name}} hoy finaliza su suscripción acuerdese de ponerse en contacto con nosotros para poder tramitar una nueva suscripción.
                </p>
       
            @elseif($expiracion==4)
                <p>
                    Suscripción renovada, muchas gracias por repetir esta aventura y depositar toda tu confianza en nosotros.
                </p>
       
            @endif

        </div>

        

        
    </div>

       


          
       
    <div style="margin-top:5%;width:100%;text-align:center ">
        
       

        <a  href ="https://marnific.com/reserva_barcos/public/contacto/cliente"style="text-decoration:none;font-size:22px; background:#04607c; color: white; bordeR:0xp; border-radius:5px; padding:3px">Contactar</a>
    </div>
    
    <br/> 
    <br/> 

</div>


<div id="footer">

    <!-- <img style="marign:auto;width:100%;height:100%;" src=" {{ URL::to('/') }}/imagenes/reservas/footer_mail_reserva_realizada.jpeg"/> -->
    <img id="img_logo_footer" src="https://marnific.com/reserva_barcos/public/imagenes/reservas/footer_mail_reserva_realizada.jpeg" alt="Logo" title="Logo" style="display:block" width="100%" height="50%" />

</div>
