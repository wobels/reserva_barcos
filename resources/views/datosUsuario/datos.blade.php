@extends('layouts.app')

@section('content')

    <div class="container-fluid" style="width:90%">

        <div class="row">
            <div class="col-lg-12">

                <form method="POST" id="formEditarUsuario">
                    <label>Nombre de usuario:</label>
                    <input class="form-control" name="nombre" id="nombre" type="text" value="{{Auth::user()->name}}" required  readonly>

                    <label>Apellidos:</label>
                    <input class="form-control" name="apellidos"   id="apellidos" type="text" value="{{Auth::user()->apellidos}}" required  readonly>

                    
                    <label>Email:</label>
                    <input class="form-control" name="email" id="email"  type="email" value="{{Auth::user()->email}}" required readonly>

                    <label>Dni:</label>
                    <input class="form-control" name="dni" id="dni" type="text" value="{{Auth::user()->dni}}" required  readonly>

                    <label>Telefono:</label>
                    <input class="form-control" name="telefono" id="telefono"  type="text" value="{{Auth::user()->telefono}}" required  readonly>

                    <label>Tipo de pago :</label>
                    <input class="form-control" type="text" value="{{Auth::user()->tipo_pago}}" readonly>

                    <label>Bonos Fin de semana :</label>
                    <input class="form-control" type="text" value="{{Auth::user()->numero_bonos_findesemana}}" readonly >

                    <label>Bonos Entre semana  :</label>
                    <input class="form-control" type="text" value="{{Auth::user()->numero_bonos_entresemana}}" readonly>

                @csrf
                   

                    

                </form>

                <h4 class="mt-3">Restablecer contraseña</h4>
                        <a  href="https://marnific.com/reserva_barcos/public/password/reset" class="btn btn-primary">Restablecer password</a>

            </div>
        </div>

    </div>
@include('footer')

<!-- 
    <script>
         $('#editar_datos').click(function(){
        

             var url ="{!!route('editar.datos.personales',Auth::user()->id)!!}";
	

            // alert('entra');
            if($('#nombre').val()!="" && $('#apellidos').val()!="" && $('#email').val()!="" && $('#dni').val()!="" && $('#telefono').val()!="" )
            {
                    $.ajax({
                        type: "POST",
                        url: url, // This is what I have updated
                        data: $("#formEditarUsuario").serialize(),

                        success:function(data) {

                            if(data.success=="usuario editado")
                            {
                                alert('Usuario editado');
                                location.reload();
                            }
                            
                        
                            
                            
                        
                        }
                    })

            }
            else{
                alert('Todos los campos son obligatorios');
            }
			
	

         })
    </script> -->

@endsection