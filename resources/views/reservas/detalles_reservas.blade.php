@extends('layouts.app')

@section('content')

<style>
    #imprimir_por_trimestres{
        position:absolute;
        margin-left:8%;
    }
    @media only screen and (min-width:0px) and (max-width: 600px) {

            #imprimir_por_trimestres{
                position:relative !important;
                margin-left:8%;
            }
        #reservas-table_filter{
     
            margin-top:10% !important;
        }
        .dt-buttons.btn-group.flex-wrap{
            margin-top:5% !important;
        }
        #contenedor{
            width:100% !important;
        }
    }
    @media only screen and (min-width:992px) and (max-width: 1600px) {
      /* #contenedor{
        width:130% !important;
      }
      #div-calendario{
        padding:0px !important;

      }
      #calendar{
        
        height:1000px;
      }
   
      #containder-header{
        width:135% !important;

      } */

    }
</style>

<div id="contenedor" class="container-fluid" style="width:90%;" >
    @if($error!="")
    <div class=" alert alert-success mt-4">
        {{$error}}
    </div>
    @endif

    @if (session('alert'))
    <div class="alert alert-success">
        {{ session('alert') }}
    </div>
    @endif
   

    <div class="row">
        <div class="col-lg-5">
            <h2 class="pb-3 h2 text-center" style="color:#04607c " ><u>Tus reservas pendientes</u></h2>
            <?php 
            
                if($reservas=="[]")
                {
                   
                    $reserva_id = "";
                }
            
             
            ?>
       
            @foreach($reservas as $reserva)
            <?php 
              
               if($reservas!="")
                {
                    $reserva_id = $reserva->id;

                }
            ?>

            <?php
              $hoy = date("Y-m-d");
              $fecha_start = substr($reserva->start, 0, 10);
              if($hoy==$fecha_start)
              {
                $texto_hoy = "Hoy";

              }
              else{
                $texto_hoy = "";
              }
            ?>
            @if($reserva->reserva_bono_extra==0)
                <h4 class="text-primary">#Reserva {{$reserva_id}} <span style="color:orange"> <?php echo $texto_hoy;?> </span></h4>
            @elseif($reserva->reserva_bono_extra==1)
                <h4 class="text-primary">#Reserva {{$reserva_id}} - Bono Extra <span style="color:orange"> <?php echo $texto_hoy;?> </span></h4>

            @endif
                <h5><strong>Barco:</strong> <span class="text-secondary">{{$reserva->barco->nombre}}</span></h5>
                <h5><strong>Ciudad:</strong><span class="text-secondary">{{$reserva->barco->ciudad}}</span></h5>
                <h5><strong>Puerto:</strong> <span class="text-secondary">{{$reserva->barco->puerto}}</span></h5>

                <h5 ><strong>Fecha:</strong>
                    <span class="text-secondary">
                        <?php 

                            $acortar_fecha = $fecha_start = substr($reserva->start, 0, 10);
                            $porciones = explode("-", $acortar_fecha);


                            echo ($porciones[2].'-'.$porciones[1].'-'.$porciones[0]);
                        ?>
                    </span>
                </h5>
                @if($reserva->mañana==1 && $reserva->tarde==1)
                    <h5 > <strong>Reservado:</strong> <span class="text-secondary">TARDE y MAÑANA</h5>  
                    <h6><strong>Horario de recogida:</strong><span class="text-secondary"> 07:00</span></h6>
                    <h6><strong>Horario de vuelta:</strong><span class="text-secondary"> 21:00</span></h6>

                @elseif($reserva->mañana==1 && $reserva->tarde==0)
                    <h5><strong>Reservado:</strong> MAÑANA</h5>
                    <h6><strong>Horario de recogida:</strong> <span class="text-secondary">07:00</span></h6>
                    <h6><strong>Horario de vuelta:</strong> <span class="text-secondary">14:00</span></h6>
                @elseif($reserva->mañana==0 && $reserva->tarde==1)
                    <h5><strong>Reservado:</strong> TARDE</h5>
                    <h6><strong>Horario de recogida:</strong> <span class="text-secondary">15:00</span></h6>
                    <h6><strong>Horario de vuelta:</strong><span class="text-secondary"> 21:00</span></h6>
                    
                @endif
                 @if($reserva->checking==0 )
                    
                <a class="btn " href="{{route('checking.reservas', $reserva_id)}}" style="background:#04607c;color:white "   id="checking_inicial-<?php echo $reserva->id;?>">Check-in</a >
                @endif

                @if ($reserva->checking==1 && $reserva->imagen_gasolina=="" || $reserva->checking==1 &&  $reserva->imagen_horas=="")
                    <a class="btn btn-success" href="{{route('checking.reservas', $reserva->id)}}" style="background:#04607c;color:white "   id="checking-<?php echo $reserva->id;?>">Checking <i class="fas fa-times text-danger"></i></a >
                @elseif ($reserva->checking==1 && $reserva->imagen_gasolina!="" &&  $reserva->imagen_horas!="" && $reserva->checkout==0)
                    <a class="btn btn-warning"  href="{{route('checkout.reservas', $reserva_id)}}" id="checkout_inicial-<?php echo $reserva->id;?>">Checkout</a >
                @elseif ($reserva->checking==1 && $reserva->imagen_gasolina!="" &&  $reserva->imagen_horas!="" && $reserva->checkout==1)
                    <a href="{{route('checkout.reservas', $reserva->id)}}" class="btn btn-warning">Checkout <i class="fas fa-check text-success"></i></a >
                @elseif ($reserva->checking==1 && $reserva->imagen_gasolina!="" &&  $reserva->imagen_horas!="" && $reserva->checkout==2)
                    <a href="{{route('checkout.reservas', $reserva->id)}}" class="btn btn-warning">Checkout <i class="fas fa-check text-success"></i></a >
               
                @elseif ($reserva->checking==2 && $reserva->incidencias_checking=="" || $reserva->checking==2 && $reserva->imagen_gasolina=="" || $reserva->checking==2 && $reserva->imagen_horas=="")
                    <a class="btn btn-success" href="{{route('checking.reservas', $reserva->id)}}" id="checking-<?php echo $reserva->id;?>">Check-in <i class="fas fa-times text-danger"></i></a >
                @elseif ($reserva->checking==2 && $reserva->incidencias_checking!="" && $reserva->checkout==0)
                    <a  class="btn btn-warning" href="{{route('checkout.reservas', $reserva_id)}}"  id="checkout_inicial-<?php echo $reserva->id;?>">Checkout</a >
                @elseif ($reserva->checking==2 && $reserva->incidencias_checking!=""  && $reserva->checkout==1)
                    <a href="{{route('checkout.reservas', $reserva->id)}}" class="btn btn-warning">Checkout <i class="fas fa-check text-success"></i></a >
                @elseif ($reserva->checking==2 && $reserva->incidencias_checking!=""  && $reserva->checkout==2)
                    <a href="{{route('checkout.reservas', $reserva->id)}}" class="btn btn-warning">Checkout <i class="fas fa-check text-success"></i></a >
                @endif

                @if ($reserva->checking==0)
                     <a   onclick="return confirm('¿Estas seguro de querer cancelar la recerva? Si cancelas una reserva con menos de 48h de antelación no se recargarán los bonos en tu cuenta');"  href="{{ route('cancelacion.reservas', [$reserva->id, $reserva->start, $reserva->user_id]) }}"  class="btn btn-danger">Cancelar reserva</a>
                @endif
                <hr>
                <!-- FUNCIÓN PARA LA UBICACIÓN EN EL PUERTO DE AMARRE -->
                <script>
    // if(navigator.geolocation)
    // {
    //     var success = function(position)
    //     {
    //         var latitud = position.coords.latitude,
    //             longitud = position.coords.longitude;

      
    //             console.log('longitud: '+ longitud);
    //             console.log('latitud: '+ latitud);

    //             // posicion_latitud = 38.340061;
    //             // posicion_longitud = -0.481830;

                
    //             posicion_latitud = 38.340187;
    //             posicion_longitud = -0.481531;

    //             console.log('posicion longitud: '+ posicion_longitud);
    //             console.log('posicion  latidud: '+ posicion_latitud);
             
                
    //             function Dist(latitud, longitud, posicion_latitud, posicion_longitud) {
    //                 rad = function (x) {
    //                     return x * Math.PI / 180;
    //                 }
                
    //                 var R = 6378.137;//Radio de la tierra en km
    //                 var dLat = rad(posicion_latitud - latitud);
    //                 var dLong = rad(posicion_longitud - longitud);
    //                 var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(latitud)) * Math.cos(rad(posicion_latitud)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    //                 var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    //                 var d = R * c;
    //                 return d.toFixed(3);//Retorna tres decimales
    //             }
                
    //             Distancia = Dist(latitud, longitud, posicion_latitud, posicion_longitud);//Retorna numero en Km

    //             var metros = Distancia*1000;

    //             // alert(metros);
              

              
    //             $('[id^=checking_inicial-]').click(function(){
    //                 if(metros<2000)
    //                 {
    //                     // alert('Estas dentro del rango');
    //                     $('[id^=checking_inicial-{{$reserva_id}}]').attr('href', '{{route("checking.reservas", $reserva_id)}}');
    //                 }
    //                 else if(metros>2000)
    //                 {
    //                      alert('Estas fuera del rango para poder realizar el checking');
    //                 }
    //                 // alert(metros);
                    
    //             })

    //             $('[id^=checkout_inicial-]').click(function(){
    //                 // alert('entra');
    //                 if(metros<2000)
    //                 {
    //                     // alert('Estas dentro del rango');
    //                     $('[id^=checkout_inicial-{{$reserva_id}}]').attr('href', '{{route("checkout.reservas", $reserva_id)}}');
    //                 }
    //                 else if(metros>2000)
    //                 {
    //                      alert('Estas fuera del rango para poder realizar el checking');
    //                 }
    //                 // alert(metros);
                    
    //             })
                
  

           




    //     }
    //     navigator.geolocation.getCurrentPosition(success, function(msg)
    //     {
    //         alert('Este navegador no soporta localización o no se han aceptado los permisos para acceso a localización');

    //     });
    // }

  
</script>
            @endforeach
            
           
                
            
        </div>

        <div class="col-lg-7">
        <h2 class="pb-3 h2 text-center" style="color:#04607c "  ><u>Todas las reservas</u></h2>
        <form id="imprimir_por_trimestres" style="" autocomplete="off">
            <select class="form-control" STYLE="float:left;width:48%;">
                <option>Primer trimestres</option>
                <option>Segundo trimestres</option>
                <option>Tercer trimestres</option>
                <option>Cuarto trimestres</option>

            </select>
            <a class="ml-1 btn " type="submit" value="Imprimir trimestre" style="background:#04607c;color:white;float:left;width:48%; " > Imprimir trimestre </a>
        </form>
            {!!$dataTable->table() !!}
        </div>
    </div>
</div>


{!!$dataTable->scripts()!!}

@include('footer')


@endsection