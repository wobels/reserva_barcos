@extends('layouts.app')

@section('content')
<div class="container" style="height:1000px">
    <div class="row">
        <div class="col-lg-12">
            @if($reservas->checking==0)
                <h1 class="text-center" id="hinicial">¿Está todo cómo se espera?</h1>
                <h1 class="text-center" style="background:#04607c !important;color:white !important" id="himagenes">Realice fotos y envíenoslas</h1>

            @elseif($reservas->checking==1 )
                <h1 class="text-center" style="background:#04607c !important;color:white !important"> <i style="color:red"  class="fas fa-times"></i> Faltan imagenes</h1>
                <h4 class="text-center">Aun faltan imágenes por subir  para finalizar correctament el checking</h4>
            @elseif($reservas->checking==2 && $reservas->incidencias_checking=="" || $reservas->checking==2 && $reservas->imagen_gasolina=="" || $reservas->checking==2 && $reservas->imagen_horas=="")
                <div class="col-lg-12 text-center">
                    <h1 class="text-center"> <i class="fas fa-times"></i>Aun faltan cosas por subir cosas para finalizar correctament el checking</h1>
                    <button style="font-size:50px; height:150px; width:220px" id="no"  class="btn btn-danger seleccion">Finalizar</button>
                </div>
            @elseif ($reservas->checking==2 && $reservas->incidencias_checking!="")
                <div class="col-lg-12 text-center">
                    <h1 class="text-center" style="background:#04607c !important;color:white !important"> <i style="color:red"  class="fas fa-times"></i> Faltan imáSgenes</h1>
                    <div class="btn btn-primary "onClick="history.go(-1); return false;">Volver atrás</div> 
                </div>


            @endif
        </div>
        <div class="col-lg-12 text-center mt-5"> 





            @if($reservas->checking==0)   
            <div class="col-lg-12 ">
                <div class="col-lg-12" style="margin:auto; text-left;">
                    <ul class="text-left list-group" style="list-style:none;margin:auto;text-align: left;  display:inline-block" >
                        <li> <i class="fas fa-check" style="color:green"></i>Batería apagada</li>
                        <li > <i class="fas fa-check" style="color:green"></i> Barco limpio y lona consola puesta</li>
                        <li> <i class="fas fa-check" style="color:green"></i> 6 defensas y bichero</li>
                        <li> <i class="fas fa-check" style="color:green"></i> Estado exterior</li>
                        <li> <i class="fas fa-check" style="color:green"></i> Combustible</li>
                    </ul>
                </div>
            </div>
        
            <button style="font-size:50px; height:150px; width:150px" id="si" class="btn btn-primary seleccion mt-5">Sí</button>
            <button style="font-size:50px; height:150px; width:150px" id="no"  class="btn btn-danger seleccion mt-5">No</button>

            @elseif($reservas->checking==1)
            <button id="si" class="btn btn-primary seleccion">Subir las imágenes</button>

            @endif
            <h3 id="hforms" class="h3 text-center col-lg-12">Por favor suba las imágenes que le pedimos a continuación</h3>
                    
            <div class="row mt-4"> 
        
                @if($reservas->imagen_gasolina=="")
                <form id="formAddImagenDepositoGasolina"   action="{{ asset('/reservas/imagenGasolinaCheking') }}" class="dropzone col-lg-6" >
                    
                    <h2 class="col-lg-12 text-center">Imagen Depósito de gasolina</h2>
                    {{ csrf_field() }}
                    <input hidden id="reservas_id" name="reservas_id" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                    <input hidden id="hora_gasolina" name="hora_gasolina" type="text" class="form-control mt-2" placeholder="hora_gasolina" value="" >

                    <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>
                
                </form>
                @endif

                @if($reservas->imagen_horas=="")
                <form id="formAddImagenContadorHoras"   action="{{ asset('/reservas/imagenHorasCheking') }}" class="dropzone col-lg-6" >
                    @if($reservas->barco_id!=5)
                    <h2 class="col-lg-12 text-center">Imagen contador horas</h2>
                    @endif
                    @if($reservas->barco_id==5)
                    <h2 class="col-lg-12 text-center">Imagen contador litros a 0</h2>
                    @endif
                    
                        {{ csrf_field() }}
                        <input hidden id="reservas_id2" name="reservas_id2" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                        <input hidden id="hora_horas" name="hora_horas" type="text" class="form-control mt-2" placeholder="hora_horas" value="" >
                        <div  class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>

                </form>
                @endif

                <div class="col-lg-12 mt-5" id="finalizar-checking">
                    <!-- <a href="{{route('detalles.reservas',$reservas->id)}}"class="btn btn-primary"> Finalizar </a> -->
                    <div class="btn btn-primary "onClick="history.go(-1); return false;">Finalizar</div> 
                </div>

                
            </div>

           
        </div>

        <div class="col-lg-12" id="incidencias">

              
            <h3 id="hincidencias" class="text-center">Infórmenos de las incidencias mediante el siguiente formulario de envío</h3>
            <div class="col-lg-12" id="incidencias">
                <form method="POST" id="formIncidenciasChecking">
                    <label>Incidencias:</label>
                    @if($reservas->incidencias_checking=="")
                        <textarea  style="height:200px" name="incidencias"  class="form-control"></textarea>
                    @elseif($reservas->incidencias_checking!="")
                    <textarea  style="height:200px" name="incidencias"  class="form-control">{{$reservas->incidencias_checking}}</textarea>

                    @endif
                    <input hidden type="text" name="reserva_id" value="{{$reservas->id}}">
                    <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>

                    {{ csrf_field() }}
                </form>
                <label><input id="addimg" type="checkbox" class="mt-3"> Añadir imagenes de las incidencias</label>
                <div class="col-lg-12">
                    <form id="formAddImagenIncidencias"  action="{{ asset('/reservas/imagenes/incidencias/checking') }}" class="dropzone col-lg-6" >
                        <h2 class="col-lg-12 text-center">Imágenes incidencias</h2>
                        {{ csrf_field() }}
                        <input hidden id="reservas_id" name="reservas_id" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                        <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>
                    
                    </form>
                </div>
                <div class="col-lg-12">
                <label>Imágenes contadores; </label> </br>
                <div class="row">
                    <form id="formAddImagenContadorHoras"   action="{{ asset('/reservas/imagenHorasCheking') }}" class="dropzone col-lg-6" >
                        
                        @if($reservas->barco_id!=5)
                        <h2 class="col-lg-12 text-center">Imagen contador horas</h2>
                        @endif
                        @if($reservas->barco_id==5)
                        <h2 class="col-lg-12 text-center">Imagen contador litros a 0</h2>
                        @endif
                    
                        {{ csrf_field() }}
                        <input hidden id="reservas_id2" name="reservas_id2" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                        <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>
                    </form>

                    <form id="formAddImagenDepositoGasolina"   action="{{ asset('/reservas/imagenGasolinaCheking') }}" class="dropzone col-lg-6" >
                        
                        <h2 class="col-lg-12 text-center">Imagen Depósito de gasolina</h2>
                        {{ csrf_field() }}
                        <input hidden id="reservas_id" name="reservas_id" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                        <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>
                    
                    </form>
                    
                </div>

                

                <div class="col-lg-12 text-center mt-3">
                    <a id="enviar_incidencia" class="btn btn-primary">Enviar</a>
                </div>
                <p class="h5 col-lg-12 text-center mt-3" >O bíen puede contactar con nosotros vía telefónica o enviandonos un whathsapp</p>
                <div class="row">
                    <div class="col-lg-6 text-center border-right mt-3 " style="height:200px" >
                    <i class="fas fa-phone-square-alt"></i> +34 66666666
                    </div>
                    <div class="col-lg-6 text-center mt-3" style="height:200px" >
                
                    <a id="urlwhat" href="https://api.whatsapp.com/send?phone=34656825982">
                    <i  for="urlwhat" style="color:#25d366 !important" class="fab fa-whatsapp-square fa-3x mb-3 text-muted"></i> 
                    Via Whatshapp</a>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</div>
@include('footer')

<script type="text/javascript">
$('#formAddImagenContadorHoras').css('display','none');  
$('#formAddImagenDepositoGasolina').css('display','none');  
$('#himagenes').css('display','none');  
$('#hforms').css('display','none');  
$('#incidencias').css('display','none');  
$('#finalizar-checking').css('display','none');  
$('#formAddImagenIncidencias').css('display','none');  

// Poner hora de subida de las imagenes de gasolina y horas

var date = new Date();
var seconds = date.getSeconds();
var minutes = date.getMinutes();
var hour = date.getHours();

hora_subida_imagenes = hour+':'+minutes+':'+seconds;


$('#hora_gasolina').val(hora_subida_imagenes);
$('#hora_horas').val(hora_subida_imagenes);

// console.log('horas',hour,':,minutes,':',hour);


$("#addimg").click(function(){
    if($('#addimg').prop('checked') ) {
    
  

        $('#formAddImagenIncidencias').css('display','block');  

    }
  else{
        $('#formAddImagenIncidencias').css('display','none');  


  }

});


 Dropzone.options.formAddImagenContadorHoras = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 20, // Tamaño máximo en MB
        maxFiles:1,
    };
    Dropzone.options.formAddImagenDepositoGasolina = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 20, // Tamaño máximo en MB
        maxFiles:1,
    };
    Dropzone.options.formAddImagenIncidencias = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 20, // Tamaño máximo en MB
        maxFiles:20,
    };

    $("#si").click(function(){
        // alert('funciona');
        // console.log("form");
        // console.log($("#form_albaran_entrada"));
        // console.log("finform");

        $('.seleccion').css('display','none');
        $('#formAddImagenContadorHoras').css('display','block');  
        $('#formAddImagenDepositoGasolina').css('display','block');    
        $('#himagenes').css('display','block'); 
        $('#hinicial').css('display','none');  
        $('#hforms').css('display','block');  
        $('#finalizar-checking').css('display','block');  





        var ruta = '{{ route("checking.usuario",$reservas->id) }}';
            // ruta = ruta.replace(':factura_id', factura_id);

        $.ajax({
            type: "GET",
            url: ruta, // This is what I have updated
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'Barco creado correctamente') {
                   

                    // $("#errores").css('display','none');
                    // alert(ultimo_barco);
                    

                }

                //errores

                if (data.errors != "" && data.errors != null) {
                    // console.log("error");
                    // console.log(data.errors);
                   
                    
                    // $("#errores").empty();
                    // var erroresAlertas = "<br><div class=\"alert alert-danger \" style=\"float:left;width:100%\" >";
                    // jQuery.each(data.errors, function(key, value) {
                    //     erroresAlertas = erroresAlertas + '<p>' + value + '</p>';
                    // });
                    // erroresAlertas += '</div>';
                    // $("#errores").append(erroresAlertas);
                }
            }
        })
    });

    $("#no").click(function(){
        // alert('funciona');
        // console.log("form");
        // console.log($("#form_albaran_entrada"));
        // console.log("finform");

        $('.seleccion').css('display','none');
        $('#hinicial').css('display','none');  


        $('#incidencias').css('display','block');  




        var ruta = '{{ route("checking.incidencias.usuario",$reservas->id) }}';


        $.ajax({
            type: "GET",
            url: ruta, // This is what I have updated
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'Barco creado correctamente') {
                   

                    // $("#errores").css('display','none');
                    // alert(ultimo_barco);
                    

                }

                //errores

                if (data.errors != "" && data.errors != null) {
                    // console.log("error");
                    // console.log(data.errors);
                   
                    
                    // $("#errores").empty();
                    // var erroresAlertas = "<br><div class=\"alert alert-danger \" style=\"float:left;width:100%\" >";
                    // jQuery.each(data.errors, function(key, value) {
                    //     erroresAlertas = erroresAlertas + '<p>' + value + '</p>';
                    // });
                    // erroresAlertas += '</div>';
                    // $("#errores").append(erroresAlertas);
                }
            }
        })
    });

    $("#enviar_incidencia").click(function(){
    // alert('funciona');

    var ruta = '{{ route("incidencias.checking") }}';
            // ruta = ruta.replace(':factura_id', factura_id);

        $.ajax({
            type: "POST",
            url: ruta, // This is what I have updated
            data: $("#formIncidenciasChecking").serialize(),
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'incidencias hecho') {
                   

                    // alert('hechos');
                    history.go(-1); return false;


                }

                //errores

                if (data.errors != "" && data.errors != null) {
                
                }
            }
        })
});

</script>

@endsection