@extends('layouts.app')


@section('content')
<div class="container" style="height:1000px">
    <div class="row">
        <div class="col-lg-12">

            
            @if($reservas->checkout!=1 && $reservas->checkout!=2)
            <div class="col-lg-12 text-center mt-5" id="botones">
            @if($reservas->imagen_gasolina_checkout=="" || $reservas->imagen_horas_checkout=="" )
                <h3 id="hinicial" class="text-center">Suba las imágenes del contador de hora y del depósito de gasolina</h3>
                <div class="row mt-4"> 
            @endif
        
            @if($reservas->imagen_gasolina_checkout=="")
            <form id="formAddImagenDepositoGasolina"   action="{{ asset('/reservas/imagenGasolinaCheckout') }}" class="dropzone col-lg-6" >
                
                <h2 class="col-lg-12 text-center">Imagen Depósito de gasolina y  ticket de gasolinera</h2>
                {{ csrf_field() }}
                <input hidden id="reservas_id" name="reservas_id" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                <input hidden id="hora_gasolina_checkout" name="hora_gasolina_checkout" type="text" class="form-control mt-2" placeholder="hora_gasolina_checkout" value="" >

                <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>
            
            </form>
            @endif

            @if($reservas->imagen_horas_checkout=="")
            <form id="formAddImagenContadorHoras"   action="{{ asset('/reservas/imagenHorasCheckout') }}" class="dropzone col-lg-6" >
                
                    @if($reservas->barco_id!=5)
                    <h2 class="col-lg-12 text-center">Imagen contador horas</h2>
                    @endif
                    @if($reservas->barco_id==5)
                    <h2 class="col-lg-12 text-center">Imagen contador de litros tras salida</h2>
                    @endif
                    
                    {{ csrf_field() }}
                    <input hidden id="reservas_id2" name="reservas_id2" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                    <input hidden id="hora_horas_checkout" name="hora_horas_checkout" type="text" class="form-control mt-2" placeholder="hora_horas_checkout" value="" >

                    <div class="dz-message" data-dz-message><span>Suba la imagen aquí</span></div>

            </form>
            @endif

            @if($reservas->imagen_bateria=="")
            <form id="formAddImagenBateria"   action="{{ asset('/reservas/imagenBateriaCheckout') }}" class="dropzone col-lg-6" >
                
                    <h2 class="col-lg-12 text-center">Imagen Batería</h2>
                    <input hidden id="reservas_id3" name="reservas_id3" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                    <input hidden id="hora_bateria_checkout" name="hora_bateria_checkout" type="text" class="form-control mt-2" placeholder="hora_horas_checkout" value="" >

                    <div class="dz-message" data-dz-message><span>Suba la imagen aquí</span></div>
                    {{ csrf_field() }}

            </form>
            @endif

            <div class="col-lg-12 mt-5" id="finalizar-checking">
                <!-- <a href="{{route('detalles.reservas',$reservas->id)}}"class="btn btn-primary"> Finalizar </a> -->
                @if($reservas->imagen_gasolina_checkout=="" || $reservas->imagen_horas_checkout=="" || $reservas->imagen_bateria=="" )
                <div class="btn btn-primary "onClick="location.reload(); return false;">Finalizar</div> 
                @endif
        </div>
     
        
    </div>
                 @if($reservas->imagen_gasolina_checkout!="" && $reservas->imagen_horas_checkout!=""  && $reservas->imagen_bateria!="")
                 <div class="container text-center">
                    <h3 id="hinicial" class="text-center">¿Se ha ocasionado alguna incidencia durante el viaje?</h3>
                    <div class="col-lg-12 ">
                        <div class="col-lg-12" style="margin:auto; text-left;">
                            <ul class="text-left list-group" style="list-style:none;margin:auto;text-align: left;  display:inline-block" >
                                <li> <i class="fas fa-check" style="color:green"></i> Bateria apagada</li>
                                <li > <i class="fas fa-check" style="color:green"></i> Barco limpio y lona consola puesta</li>
                                <li> <i class="fas fa-check" style="color:green"></i> 6 defensas y bichero</li>
                                <li> <i class="fas fa-check" style="color:green"></i> Estado exterior</li>
                                <li> <i class="fas fa-check" style="color:green"></i> Combustible</li>
                            </ul>
                        </div>
                    </div>
        
                    <button class="btn btn-primary seleccion mt-5" id="no" style="font-size:50px; height:150px; width:150px ">No </button>
                    <button id="si" class="btn btn-danger seleccion mt-5 " style="font-size:50px; height:150px; width:150px" >Sí</button>
                </div>
                @endif
               
           
            @elseif($reservas->checkout==2 && $reservas->incidencias_checkout=="")
                <div class="col-lg-12 text-center">
                    <h3 id="hinicial" class="text-center"> <i class="fas fa-times"></i>A un no has teminado de rellenar el formulario de incidencias</h3>

                    <button id="si" class="btn btn-danger seleccion botonFinalizar " style="font-size:50px; height:150px; width:220px" >Finalizar</button>

                </div>

            @elseif($reservas->checkout==2 && $reservas->incidencias_checkout!="")
            <div class="col-lg-12 text-center">
                <h3 id="hinicial" style="background:#04607c !important;color:white !important" class="text-center"> <i style="color:green" class="fas fa-check"></i> Checkout realizado con éxito</h3>
                <a class="btn btn-primary mt-3" href="javascript: history.go(-1);">Volver atrás</a>
            </div>


            @elseif($reservas->checkout==1)
                <div class="col-lg-12 text-center">
                    <h3 id="hinicial" class="text-center"> <i class="fas fa-check"></i>Checkout realizado con éxito</h3>
                
                    <a class="btn btn-primary mt-3" href="javascript: history.go(-1)">Volver atrás</a>
                </div>

            @endif
            </div>
            <div class="col-lg-12" id="incidencias">

              
                <h3 id="hincidencias" class="text-center">Infórmenos de las incidencias mediante el siguiente formulario de envío</h3>
                <div class="col-lg-12" id="incidencias">
                    <form method="POST" id="formIncidenciasCheckout">
                        <label>Incidencias:</label>
                        <textarea  style="height:200px" name="incidencias"  class="form-control"></textarea>
                        <input hidden type="text" name="reserva_id" value="{{$reservas->id}}">
                        <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>

                        {{ csrf_field() }}
                    </form>
                    <label><input id="addimg" type="checkbox" class="mt-3"> Añadir imágenes de las incidencias</label>
                    <form id="formAddImagenIncidencias"   action="{{ asset('/reservas/imagenes/incidencias/checkout') }}" class="dropzone col-lg-6" >
                        <h2 class="col-lg-12 text-center">Imágenes incidencias</h2>
                        {{ csrf_field() }}
                        <input hidden id="reservas_id" name="reservas_id" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                        <div class="dz-message" data-dz-message><span>Suba la imágen aquí</span></div>

                    </form>
                    <div class="col-lg-12 text-center mt-3">
                        <a id="enviar_incidencia" class="btn btn-primary">Enviar</a>
                    </div>
                    <p class="h5 col-lg-12 text-center mt-3" >O bíen puede contactar con nosotros vía telefónica o enviandonos un whathsapp</p>
                    <div class="row">
                        <div class="col-lg-6 text-center border-right mt-3 " style="height:200px" >
                        <i class="fas fa-phone-square-alt"></i> +34 66666666
                        </div>
                        <div class="col-lg-6 text-center mt-3" style="height:200px" >
                    
                        <a id="urlwhat" href="https://api.whatsapp.com/send?phone=34656825982">
                        <i  for="urlwhat" style="color:#25d366 !important" class="fab fa-whatsapp-square fa-3x mb-3 text-muted"></i> 
                        Via Whatshapp</a>
                        </div>
                    </div>
                </div>
            </div>
   
        </div>
        
    </div>
</div>
@include('footer')

<script type="text/javascript">
$('#incidencias').css('display','none');  
$('#hincidencias').css('display','none');  
$('#formAddImagenIncidencias').css('display','none');  

// Poner hora de subida de las imagenes de gasolina y horas

var date = new Date();
var seconds = date.getSeconds();
var minutes = date.getMinutes();
var hour = date.getHours();

hora_subida_imagenes = hour+':'+minutes+':'+seconds;


$('#hora_gasolina_checkout').val(hora_subida_imagenes);
$('#hora_horas_checkout').val(hora_subida_imagenes);
$('#hora_bateria_checkout').val(hora_subida_imagenes);



Dropzone.options.formAddImagenContadorHoras = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 20, // Tamaño máximo en MB
        maxFiles:1,
    };
    Dropzone.options.formAddImagenDepositoGasolina = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 20, // Tamaño máximo en MB
        maxFiles:1,
    };

Dropzone.options.formAddImagenIncidencias = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 20, // Tamaño máximo en MB
        maxFiles:20,
    };


    Dropzone.options.formAddImagenBateria = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 20, // Tamaño máximo en MB
        maxFiles:1,
    };
    



$("#addimg").click(function(){
    if($('#addimg').prop('checked') ) {
    
  

        $('#formAddImagenIncidencias').css('display','block');  

    }
  else{
        $('#formAddImagenIncidencias').css('display','none');  


  }

});

$("#enviar_incidencia").click(function(){
    // alert('funciona');

    var ruta = '{{ route("incidencias.checkout") }}';
            // ruta = ruta.replace(':factura_id', factura_id);

        $.ajax({
            type: "POST",
            url: ruta, // This is what I have updated
            data: $("#formIncidenciasCheckout").serialize(),
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'incidencias hecho') {
                   

                    // alert('hechos');
                    history.go(-1); return false;


                }

                //errores

                if (data.errors != "" && data.errors != null) {
                
                }
            }
        })
});
$("#si").click(function(){
        // alert('funciona');
        // console.log("form");
        // console.log($("#form_albaran_entrada"));
        // console.log("finform");

    

        var ruta = '{{ route("checkout.incidencias.usuario",$reservas->id) }}';
         

        $.ajax({
            type: "GET",
            url: ruta, // This is what I have updated
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'checkout hecho') {
                   

                    // $("#errores").css('display','none');
                    // alert(ultimo_barco);
                    // alert('Muchisimas gracias por confiar en nosotros, hasta la próxima');
                    $('#incidencias').css('display','block');  
                    $('#hincidencias').css('display','block');
                    $('#botones').css('display','none');   
                    $('#hinicial').css('display','none');  
                    $('.botonFinalizar').css('display','none');  


                }

                //errores

                if (data.errors != "" && data.errors != null) {
                    // console.log("error");
                    // console.log(data.errors);
                   
                    
                    // $("#errores").empty();
                    // var erroresAlertas = "<br><div class=\"alert alert-danger \" style=\"float:left;width:100%\" >";
                    // jQuery.each(data.errors, function(key, value) {
                    //     erroresAlertas = erroresAlertas + '<p>' + value + '</p>';
                    // });
                    // erroresAlertas += '</div>';
                    // $("#errores").append(erroresAlertas);
                }
            }
        })
    });

$("#no").click(function(){
        // alert('funciona');
        // console.log("form");
        // console.log($("#form_albaran_entrada"));
        // console.log("finform");

        var ruta = '{{ route("checkout.usuario",$reservas->id) }}';
            // ruta = ruta.replace(':factura_id', factura_id);

        $.ajax({
            type: "GET",
            url: ruta, // This is what I have updated
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'checkout hecho') {
                   
                    // $("#errores").css('display','none');
                    // alert(ultimo_barco);
                    alert('Esperamos que hayas disfrutado de la salida, recuerda depositar la llave en el lugar habilitado');
                    history.go(-1); return false;


                    

                }

                //errores

                if (data.errors != "" && data.errors != null) {
                    // console.log("error");
                    // console.log(data.errors);
                   
                    
                    // $("#errores").empty();
                    // var erroresAlertas = "<br><div class=\"alert alert-danger \" style=\"float:left;width:100%\" >";
                    // jQuery.each(data.errors, function(key, value) {
                    //     erroresAlertas = erroresAlertas + '<p>' + value + '</p>';
                    // });
                    // erroresAlertas += '</div>';
                    // $("#errores").append(erroresAlertas);
                }
            }
        })
    });





</script>

@endsection