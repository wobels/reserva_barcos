@extends('layouts.app')


@section('content')
<div class="col-lg-12" id="incidencias">

              
                <h3 id="hincidencias" class="text-center">Infórmenos de las incidencias mediante el siguiente formulario de envío</h3>
                <div class="col-lg-12" id="incidencias">
                    <form method="POST" id="formIncidenciasCheckout">
                        <label>Incidencias:</label>
                        <textarea  style="height:200px" name="incidencia"  class="form-control"></textarea>
                        <input hidden type="text" name="reserva_id" value="{{$reservas->id}}">;
                    </form>
                    <label><input id="addimg" type="checkbox" class="mt-3">  Añadir imágenes de las incidencias</label>
                    <form id="formAddImagenIncidencias"   action="{{ asset('/reservas/imagenGasolinaCheking') }}" class="dropzone col-lg-6" >
                        <h2 class="col-lg-12 text-center">Imágenes incidencias</h2>
                        {{ csrf_field() }}
                        <input hidden id="reservas_id" name="reservas_id" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$reservas->id}}">
                    </form>
                    <div class="col-lg-12 text-center mt-3">
                        <a id="enviar_incidencia" class="btn btn-primary">Enviar</a>
                    </div>
                    <p class="h5 col-lg-12 text-center mt-3" >O bíen puede contactar con nosotros vía telefónica o enviandonos un whathsapp</p>
                    <div class="row">
                        <div class="col-lg-6 text-center border-right mt-3 " style="height:200px" >
                        <i class="fas fa-phone-square-alt"></i> +34 66666666
                        </div>
                        <div class="col-lg-6 text-center mt-3" style="height:200px" >
                    
                        <a id="urlwhat" href="https://api.whatsapp.com/send?phone=34656825982">
                        <i  for="urlwhat" style="color:#25d366 !important" class="fab fa-whatsapp-square fa-3x mb-3 text-muted"></i> 
                        Via Whatshapp</a>
                        </div>
                    </div>
                </div>
            </div>
@endsection