@extends('layouts.app')
<style>
@media only screen and (min-width:0px) and (max-width: 991px) {
		
    #contenedor{
      width:100% !important;
    }
    #div_barco{
      width:100% !important;
    }
  
}
@media only screen and (min-width:992px) and (max-width: 3000px) {
		
    #contenedor{
      width:90% !important;
      padding: 0px 0px;
  border: 1px solid #ccc;
  border-top: none;
    }
    #div_barco{
      width:33% !important;
    }
  
}
#boton_modal{
  height:auto !important;
  font-size:15px !important;
} 
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
.tabcontent1 {

  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}


</style>

@section('content')



  <div id="contenedor" class="container-fluid" style="width:80%">

    <div class="tab col-lg-12" style="background: #04607c;">
    <button class="tablinks"><a style="color:white"  href="{{route('configuracion.index')}}">Usuarios</a></button>
  <button class="tablinks" ><a  style="color:white" href="{{route('configuracion.index.reserva')}}">Reservas</a></button>
  <button class="tablinks" ><a  style="color:white" href="{{route('configuracion.index.calendario')}}">Calendario</a></button>
  <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.bonos')}}">Bonos Extras @if($count_bonos_extras!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>

  <button class="tablinks"> <a style="color:white"  href="{{route('configuracion.index.descontar')}}">Bonos a descontar</a></button>

  <?php

$count_reservas_incidencias = 0;
  foreach($reservas_checking as $RES)
  {

    $count_reservas_incidencias++;
  }
?>

  <button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.incidencias.checking')}}">Incidencias check-in  @if($count_reservas_incidencias!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button>
  <?php
  $count_reservas_incidencias2 = 0;
    foreach($reservas_checkout as $RES)
    {

      $count_reservas_incidencias2++;
    }
  ?>

<button class="tablinks" ><a style="color:white"  href="{{route('configuracion.index.incidencias.checkout')}}">Incidencias checkout @if($count_reservas_incidencias2!=0)<i class="fas fa-circle text-danger"></i>@endif</a></button> 
<button class="tablinks" ><a  style="color:white"  href="{{route('configuracion.index.renovaciones')}}"> @if($count_renovaciones!=0)<i class="fas fa-circle text-danger"></i>@endif Renovaciones</a></button> 

<button class="tablinks" style="background:white" ><a  style="color:#04607c"  href="{{route('configuracion.index.barcos')}}">Barcos</a></button> 


    </div>
    <div  id="add_barco" style="" class="btn">
        
      
        @include('modal_add_barco')
       
     

    </div>
    <hr>

    <div class="row" style="padding:20px">

      <br>
      <br>
      <h1 class="h1 col-lg-12 text-center">Nuestros barcos<h1>
     
      @foreach($barcos as $barco)
        <?php
          $id = $barco->id;

          $url = "https://marnific.com/reserva_barcos/public/barcos/".$id."\\edit/";
        ?>
      
         
          <a href="<?php echo $url ?>" id="div_barco" style="margin-top:3% !important; float:left !important; width:25%" class="btn">
          <h4 >{{$barco->nombre}}</h4>
            <div class="border">
              <div  style=";padding:0px "  class="col-lg-12" >

                <img width="100%" height="300px" src=" {{ URL::to('/') }}/imagenes/barcos/{{$barco->imagen_principal}}">

              </div>

              <div  class="col-lg-12 border p-0 " >
                <div class="col-lg-4 btn border-right p-0">
                <strong>Nº personas:</strong>{{$barco->numero_personas}}
                </div>

                <div class="col-lg-7 btn">
                {{$barco->ciudad}} ,  {{$barco->puerto}}
                </div>
              </div>

              <div   class="col-lg-12 border" >
                {{$barco->titulo}}
              </div>

              <div    class="col-lg-12 border" >
       
              
                    {{ substr ($barco->descripcion,0,190)}}...
                   
                    
                </p>
              </div>
            </div>

          </a>
        
      @endforeach
    </div>

  </div>
 
  
  @include('footer')
  <script type="text/javascript">
    $('#add_barco').click(function(){

      $( "#boton_modal  " ).trigger( "click" );

    });

  </script>






@endsection

