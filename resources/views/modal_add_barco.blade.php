<!-- Button trigger modal -->
<button id="boton_modal" style="width:100%; height:418px;margin-top:11%;font-size:20px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
 
  <i class="fa fa-plus" aria-hidden="true">   </i>  Añadir barco
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 id="titulo_modal" class="modal-title" id="exampleModalLabel">Añadir barco al catálogo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formAddBarcos" enctype="multipart/form-data" method="POST" >
          <div class="row">

            <div class="col-lg-6">

            
              <label class="col-lg-12 text-left mt-2 tituloEdit">Nombre:</label>
              <input name="nombre" id="nombre" type="text" class="form-control mt-1" placeholder="Nombre">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Titulo:</label>
              <input name="titulo" id="titulo" type="text" class="form-control  mt-1" placeholder="Titulo">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Numero personas:</label>
              <input name="numero_personas" id="numero_personas" type="number " class="form-control mt-1" placeholder="Nº personas a bordo">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Eslora:</label>
              <input name="eslora" type="number" id="eslora" class="form-control  mt-1" placeholder="Eslora">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Manga:</label>
              <input name="manga" type="number" id="manga" class="form-control  mt-1" placeholder="Manga">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Calado:</label>
              <input name="calado" type="number" id="calado" class="form-control  mt-1" placeholder="Calado">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Motor:</label>
              <input name="motor" type="text" id="motor" class="form-control  mt-1" placeholder="Motor">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Velocidad máxima:</label>
              <input name="velocidad_maxima" id="velocidad_maxima" type="number" class="form-control  mt-1" placeholder="Velocidad máxima">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Tipo de barco</label>
              <select  name="tipo_barco" id="tipo_barco" class="form-control mt-1">
                  <option value="sdsad">Tipo de barco</option>
              </select>
              
        
            </div>

            <div class="col-lg-6">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Velocidad crucero:</label>
              <input name="velocidad_crucero" id="velocidad_crucero" type="number" class="form-control  mt-1" placeholder="Velocidad crucero">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Camarotes:</label>
              <input name="camarotes" type="number" id="camarotes" class="form-control  mt-1" placeholder="Camarotes">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Baños:</label>
              <input name="baños" type="number" id="baños" class="form-control  mt-1" placeholder="Baños">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Capacidad agua dulce:</label>
              <input name="capacidad_agua_dulce" id="capacidad_agua_dulce" type="number" class="form-control  mt-1" placeholder="Capacidad agua dulce">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Capacidad combustible:</label>
              <input name="capacidad_combustible" id="capacidad_combustible" type="number" class="form-control  mt-1" placeholder="Capacidad combustible">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Desplazamiento:</label>
              <input name="desplazamiento" id="desplazamiento" type="number" class="form-control  mt-1" placeholder="Desplazamiento">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Ciudad</label>
              <input name="ciudad" type="text" id="ciudad" class="form-control  mt-1" placeholder="Ciudad">
              <label  class="col-lg-12 text-left mt-2 tituloEdit">Puerto</label>
              <input name="puerto" type="text" id="puerto" class="form-control  mt-1" placeholder="Puerto">
            
            
          
            </div>

            <div class="col-lg-12">

              <label class=" mt-3">Descripcripción</label>
              <br>
              <textarea id="descripcion" name="descripcion"  class="form-control"></textarea>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            </div>

            <div class="col-lg-12">
              <div class="row">
                <h4 class="mt-4 col-lg-12">Configuración de bonos:</h4>
                
                <div class="col-lg-4">
                  <label  class="col-lg-12 text-left mt-2 tituloEdit">Precio de bono entre semana :</label>
                  <input name="precio_bono" id="precio_bono" type="number" class="form-control  mt-1" placeholder="Precio de bono">

                </div>

                <div class="col-lg-4">
                  <label  class="col-lg-12 text-left mt-2 tituloEdit">Precio de bono fin de semana  :</label>
                  <input name="precio_bono_findesemana" id="precio_bono_findesemana" type="number" class="form-control  mt-1" placeholder="Precio de bono">

                </div>

                <div class="col-lg-4">
                  <label  class="col-lg-12 text-left mt-2 tituloEdit">Código :</label>
                  <input name="codigo_bono" id="codigo_bono" type="number" class="form-control  mt-1" placeholder="Código bono">

                </div>


              </div>
            </div>

          </div>



      
       


          {{ csrf_field() }}
        </form>

        <div style="display:none" id="formAddImagenesBarco" >
           
            <form id="formAddImagenPrincipalBarcos"  action="{{ asset('/barcos/imagenPrincipal') }}" class="dropzone" >
              <h2 class="col-lg-12 text-center">Subir la imágen principal</h2>
              {{ csrf_field() }}
              <input hidden id="id_barco" name="id_barco" type="text" class="form-control mt-2" placeholder="Nombre">
            </form>


            <form id="formAddImagenesBarcos"  action="{{ asset('/barcos/imagenes') }}" class="dropzone" >
              <h2 class="col-lg-12 text-center">Subir imágenes secundarias </h2>
              {{ csrf_field() }}
              <input hidden class="id_barco" name="id_barco" type="text" class="form-control mt-2" placeholder="Nombre">
            </form>
        </div>


        <div class="col-lg-12" id="errores">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a id="crear_barco"  class="btn btn-primary">Siguiente</a>
        <a id="editar_barco"  class="btn btn-primary">Editar</a>
        <a  id="finalizar_subida_imagen_principal"  class="btn btn-primary">Siguiente</a>

        <a  data-dismiss="modal" id="finalizar_subida_imagenes"  class="btn btn-primary">Finalizar</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $('#finalizar_subida_imagen_principal').css('display','none');
    $('#finalizar_subida_imagenes').css('display','none');
    $('#formAddImagenesBarcos').css('display','none');
 


    $('#editar_barco').css('display','none');

    Dropzone.options.formAddImagenPrincipalBarcos = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 2, // Tamaño máximo en MB
        maxFiles:1,
    };

    Dropzone.options.formEditImagenPrincipalBarcos = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 2, // Tamaño máximo en MB
        maxFiles:1,
    };

    Dropzone.options.formAddImagenesBarcos = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 2, // Tamaño máximo en MB
        maxFiles:9,
    };

    $('#finalizar_subida_imagen_principal').click(function(){

      $('#formAddImagenPrincipalBarcos').css('display','none');

      $('#finalizar_subida_imagenes').css('display','block');
      $('#finalizar_subida_imagen_principal').css('display','none');
      
      $('#formAddImagenesBarcos').css('display','block');

      id_barco=$('#id_barco').val();

      $('.id_barco').val(id_barco);
      // alert( $('.id_barco').val())

    });

    $("#crear_barco").click(function(){
        // alert('funciona');
        // console.log("form");
        // console.log($("#form_albaran_entrada"));
        // console.log("finform");



        var ruta = '{{ route("barcos.store") }}';
            // ruta = ruta.replace(':factura_id', factura_id);

        $.ajax({
            type: "POST",
            url: ruta, // This is what I have updated
            data: $("#formAddBarcos").serialize(),
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'Barco creado correctamente') {
                   

                    $("#errores").css('display','none');
                    $('#formAddBarcos').css('display','none');
                    $('#formAddImagenesBarco').css('display','block');
                    $('.dz-default.dz-message').html('Introduce tus imagenes aquí');
                    $('#finalizar_subida_imagen_principal').css('display','block');
                    $('#crear_barco').css('display','none');

                    ultimo_barco = data.ultimo_barco;
                    $('#id_barco').val(ultimo_barco);
                    // alert(ultimo_barco);
                    

                }

                //errores

                if (data.errors != "" && data.errors != null) {
                    // console.log("error");
                    // console.log(data.errors);
                   
                    
                    $("#errores").empty();
                    var erroresAlertas = "<br><div class=\"alert alert-danger \" style=\"float:left;width:100%\" >";
                    jQuery.each(data.errors, function(key, value) {
                        erroresAlertas = erroresAlertas + '<p>' + value + '</p>';
                    });
                    erroresAlertas += '</div>';
                    $("#errores").append(erroresAlertas);
                }
            }
        })
    });

  




</script>