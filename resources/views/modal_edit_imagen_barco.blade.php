<!-- Button trigger modal -->
<button id="boton_modal_edit_image" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_imagen">
  Añadir barco
</button>

<button hidden id="boton_modal_edit_imagenes" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_imagen">
  Añadir barco
</button>

<!-- Modal -->
<div class="modal fade" id="modal_imagen" tabindex="-1" role="dialog" aria-labelledby="modal_imagenLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 id="titulo_modal" class="modal-title" id="modal_imagenLabel">Añadir barco al catálogo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
  
        <div id="formEditImagenPrincipalBarco" >
           
            <form id="formAddImagenPrincipalBarcos"  action="{{ asset('/barcos/edit/imagenPrincipal') }}" class="dropzone" >
              <h2 class="col-lg-12 text-center">Subir la imágen principal</h2>
              {{ csrf_field() }}
              <input hidden  id="id_barco" name="id_barco" type="text" class="form-control mt-2" placeholder="Nombre" value="{{$id}}">
            </form>

        </div>

        
        <div style="display:none" id="formEditImagenesBarco" >
      
            <form id="formEditImagenesBarcos"  action="{{ asset('/barcos/edit/imagenes') }}" class="dropzone" >
              <h2 class="col-lg-12 text-center">Subir imágenes secundarias </h2>
              {{ csrf_field() }}
              <input hidden  class="id_barco" name="id_barco" type="text" class="form-control mt-2" placeholder="Nombre"  value="{{$id}}">

              <input hidden id="numero_imagenes" name="numero_imagenes" type="text" class="form-control mt-2" placeholder="Nombre">

            </form>
        </div>
        

        <div class="col-lg-12" id="errores">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close" data-dismiss="modal">Cerrar</button>
        <button id="editar_imagen_principal"type="button" class="btn btn-primary" data-dismiss="modal">Editar</button>


      </div>
    </div>
  </div>
</div>

<script type="text/javascript">


    var maximo_imagen_principal=1;
    var maximo_imagen_secundarias=50;
    $('#numero_imagenes').val(maximo_imagen_secundarias);



    Dropzone.options.formAddImagenPrincipalBarcos = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 2, // Tamaño máximo en MB
        maxFiles: maximo_imagen_principal,
    };

    Dropzone.options.formEditImagenPrincipalBarcos = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 2, // Tamaño máximo en MB
        maxFiles: maximo_imagen_principal,
    };


    Dropzone.options.formEditImagenesBarco = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 2, // Tamaño máximo en MB
        maxFiles: maximo_imagen_principal,
    };
    Dropzone.options.formAddImagenesBarcos = {
        paramName: "file", // Las imágenes se van a usar bajo este nombre de parámetro
        maxFilesize: 2, // Tamaño máximo en MB
        maxFiles:maximo_imagen_secundarias,
    };

  

    

  




</script>