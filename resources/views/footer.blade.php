<style>
 @media only screen and (min-width:992px) and (max-width: 1600px) {
  /* #container-footer{
        width:135% !important;

      } */
 }
</style>
<div class="container-fluid text-center text-md-left mt-5 p-5" id="container-footer" style="opacity:1; background:#04607c !important">

<!-- Grid row -->
<div class="row">

  <!-- Grid column -->
  <div class="col-md-6 mt-md-0 mt-3">

    <!-- Content -->
    <img id="img_logo_footer"style="" src=" {{ URL::to('/') }}/imagenes/reservas/logo-horizontal-blanco.png"/>

    <!-- <p>Here you can use rows and columns to organize your footer content.</p> -->

  </div>
  <!-- Grid column -->

  <hr class="clearfix w-100 d-md-none pb-3">

  <!-- Grid column -->
  <div class="col-md-3 mb-md-0 mb-3">

    <!-- Links -->
    <h5 class="text-uppercase text-light">Contactar</h5>

    <ul class="list-unstyled">
      <li>
        <a style="font-size:18px; color:#E2E2E2" href="tel:+346666666666"><i class="fas fa-phone-alt"></i> +34 627834930  /  +34 680782250</a><br>
        <a style="font-size:18px; color:#E2E2E2" href="mailto:web@marnific.com"><i class="fas fa-at"></i> web@marnific.com</a><br>
        <a target="_blank" style="font-size:18px; color:#E2E2E2" href="https://api.whatsapp.com/send?phone=34627834930&amp;text="><i style="color:#25d366" class="fab fa-whatsapp-square"></i> Whatshapp 1</a>
        <br>
        <a target="_blank" style="font-size:18px; color:#E2E2E2" href="https://api.whatsapp.com/send?phone=34680782250&amp;text="><i style="color:#25d366" class="fab fa-whatsapp-square"></i> Whatshapp 2</a>


      </li>
   
    </ul>

  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-md-3 mb-md-0 mb-3">

    <!-- Links -->
    <h5 class="text-uppercase text-light">Preguntas frecuentes</h5>

    <ul class="list-unstyled text-light">
      <li>
        <a style="color:#E2E2E2" href="#!">¿Cómo puedo reservar?</a>
      </li>
      <li>
        <a style="color:#E2E2E2" href="#!">¿Cual es el método de pago?</a>
      </li>
      <li>
        <a  style="color:#E2E2E2" href="#!">¿Cuantas veces puedo reservar por mes?</a>
      </li>
      <li>
        <a style="color:#E2E2E2" href="#!">¿Como funciona las cancelaciones?</a>
      </li>
    </ul>

  </div>