@extends('layouts.app')

@section('content')
<!-- <link rel="stylesheet"  src="{{ asset('css/movil-home-cliente.css') }}" /> -->

@if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod'))


    <link rel="stylesheet"  href="{{ asset('css/pc-home-iphone.css') }}"/>
@else
    <link rel="stylesheet"  href="{{ asset('css/pc-home-android.css') }}"/>


@endif

<style>
      /* Moviles */
    



    #calendar{
        
        height:1300px;
      }


    #iframe_windy{
      width:110% !important;
      margin-top:-3%;
    }
    #div_leyenda_pc{
      margin-top:-3%;

    }

  #texto_ver_cam{
    position:absolute; 
    margin-top:35%; 
    width:100%; text-align:center;
    color:white ;
    margin-left:-2%; 
  }

  .fc-MiBoton-button{
    border:1px solid red !important;
    margin-top:-1000% !important;
    }

    .fc-day-sat{
      background: #DDFFDC !important;
    }
    .fc-day-sun{
      background: #DDFFDC !important;

    }

    .fc-daygrid-event img {


        margin:auto;
    }
    #table_leyenda_tablet{
      display:none;
    }

    .fc-daygrid-day-frame.fc-scrollgrid-sync-inner{
      
      height:40px !important;
    }

  .fc-day-today {
    background:#FFEEEE !important;

  }

  .fc-today-button{
      background:#FEDDDD !important;
      color:#04607c !important;
      font-weight:bold !important;
      border-color:#FEDDDD !important;

  }
  .fc-day-fri {
  
    background: rgb(221,255,220);
    background: linear-gradient(0deg, rgba(221,255,220,1) 45%, rgba(255,255,255,1) 46%, rgba(255,255,255,1) 100%);
  }
  .fc-col-header-cell.fc-day.fc-day-fri{
    background:white !important;
  }







  .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/tarde.png"] {
        position:absolute !important;
     
      }
    
      .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana.png"] {
        position:absolute !important;
        ;
      }
    
      .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_mañana.png"] {
        position:absolute !important;
       
      }
    
    
      .fc-day-fri img[src="https://marnific.com/reserva_barcos/public/imagenes/reservas/mantenimiento_tarde.png"] {
        position:absolute !important;
       
      }  
 


  
 
  
</style>

      <!-- <button class="btn btn-primary" >Añadir barco a catalogo</button> -->
<div id="contenedor" class="container-fluid" style="width:90%" >
  <div class="row ">
    <div class="col-lg-6">
      <div class="row pl-0">
        <div  class="col-lg-4 p-4 border text-left  p-0"> 
          <h5  class=" p-2 col-lg-12 text-center" style="background:#04607c !important;color:white">{{ Auth::user()->name }} </h5>   
          <ul class="p-0" style="list-style:none">
           
            <li><h6 class="text-primary">Bonos fin de semana:<span class="h3  text-secondary"> {{ Auth::user()->numero_bonos_findesemana}} </span></h6>     </li>
            <li><h6  class="text-primary">Bonos entre semana:<span class="h3  text-secondary"> {{ Auth::user()->numero_bonos_entresemana}}</span> </h6>    </li>
            <li><h6  class="text-primary">Bonos última hora:<span class="h3  text-secondary"> {{ Auth::user()->numero_bonos_ultima_hora}}</span> </h6>    </li>
           
            <li><h6  class="text-primary">Reservas actuales contratadas:<span class="h3  text-secondary"> {{$numero_reservas}}</span> </h6>    </li>
         

          </ul>
         
        </div>

        <div  class="col-lg-8"> 
          <div class="row border p-4 ">
            <div  class="col-lg-12" > 
              <div class="row">
                <div class="col-lg-4 p-0">
                  <span  style="color:#04607c " ><strong>{{$nombre_barco}}</strong> </span> <br>
                  <strong>Puerto:</strong><span class=" text-secondary">{{$puerto}}</span> <br>
                  <strong>Ciudad:</strong><span class=" text-secondary">{{$ciudad}}</span><br>

                  <a id="boton_barco" style="width:100%; background:#04607c !important; color:white " href="{{route('barcos.edit',$barco_identificacion)}}" class="btn y mt-1" ><i class="fas fa-ship"></i>  Barco</a>
                  <a  style="width:100%;  background:#04607c !important; color:white"  href="{{route('detalles.reservas',$user_id)}}" class="btn btn-primary mt-1" > <i class="fas fa-calendar-alt"></i> Mis reservas</a>
                  <button style="width:100%; background:#04607c !important; color:white"  type="button" class="btn btn-primary mt-1" data-toggle="modal" data-target="#exampleModal3">
                    <i class="fas fa-euro-sign"></i> Comprar Bonos
                  </button>

                </div>
               
                <?php
                  $id = Auth::user()->barco_id;

                  // $url = "http://localhost/reserva_barcos/public/bonos/index/".'#'.$id
                  $url = "https://marnific.com/reserva_barcos/public/bonos/index/".'#'.$id

                ?>

                <div class="col-lg-8 p-0" style="padding-left:10px !important;">
                  <a   target="_blank" href="https://www.skylinewebcams.com/es/webcam/espana/comunidad-valenciana/alicante/alicante-playa-almadraba.html">
                    <span id="texto_ver_cam">Ver web cam</span>
                    <img style="width:100%;" class="d-none d-sm-none d-md-block " id="imagen_cam_tablet_pc" src=" {{ URL::to('/') }}/imagenes/reservas/imagen-cam.png"/>
                    <img style="width:100%;" class="d-sm-block d-md-none mt-5" src=" {{ URL::to('/') }}/imagenes/reservas/imagen-cam.png"/>

                  </a>
                 
              
                  @if($proxima_reserva !="")
                    <div class="col-lg-12 mt-5">
                      <h4  style="background:#04607c !important;color:white" class="p-2">Próxima reserva:</h4>
                      <br>
                      <?php

                        $fecha_start = substr($proxima_reserva->start, 0, 10);
                        $porciones = explode("-", $fecha_start);


                        // echo ($porciones[2].'-'.$porciones[1].'-'.$porciones[0]);
                      ?>
                      <h5 class=" h5 text-secondary"  >
                      <?php
                        echo ($porciones[2].'-'.$porciones[1].'-'.$porciones[0]);
                      ?>

                        @if($proxima_reserva->mañana==1 && $proxima_reserva->tarde==0 )
                          (Mañana)
                        @elseif($proxima_reserva->mañana==0 && $proxima_reserva->tarde==1 )
                          (Tarde)
                        @elseif($proxima_reserva->mañana==1 && $proxima_reserva->tarde==1 )
                          (Mañana Tarde)
                        @endif
                      </h5>

                


                    </div>
              
                   

                  @endif
               
                  <div class="col-lg-12 mt-3">
                      <h4  class="p-2 col-lg-12 text-left" style="background:#04607c !important;color:white;">Horarios : </h4>   
                      <h6  class="text-primary" >Mañana:  <span class="h5  text-secondary">07:00 a 14:00 <span></h6>
                      <h6  class="text-primary" >Tarde:  <span class="h5  text-secondary">15:00 a 21:00 <span></h6>

                    </div>
                </div>
              
              </div>
           </div> 
          </div>
         </div> 
      </div>     
    </div>
        <!-- Button trigger modal -->
  

    <!-- Modal comprar bonos des de boton -->
    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel3">Comprar Bonos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="POST" id="formComprarBonos" autocomplete="off">
              <label>Elgir tipo de bono:</label>
             
              <label>Barco</label>
              <input hidden class="form-control" type="text" name="txtBarcoID" id="txtBarcoID4" value="{{$barco_identificacion}}" readonly>
              <input class="form-control" type="text" name="txtBarcoNombre" id="txtBarcoNombre4" value="{{$nombre_barco}}" readonly>
              <label>Usuario</label>
              <input hidden class="form-control"  type="text" name="txtUsuarioID" id="txtUsuarioID4" value="{{Auth::user()->id}}" readonly >

              <input class="form-control"  type="text" name="txtUsuario" id="txtUsuario4" value="{{Auth::user()->name}}" readonly >
              <label>Fecha</label>
              <input class="form-control"  type="date" name="txtFecha" id="txtFecha4">
              <h5 class="mt-3" id="titulo_tipo_bono4">Al tratarse de una reserva de última hora podrás elegir los bonos a comprar:  </h5>
              <select name ="tipo_bono" id="tipo_bono4" class="form-control">
              
             
                  <option value="1">Entre semana </option>
                  <option value="2"> Fin de semana  </option>



              </select>

              <h5 class="mt-3">Chequea un horario:  </h5>
              <label class="form-control mt-3" > 
                Mañana
              <input  type="checkbox" name="txtMañana" id="txtMañana4" value="0">
              &nbsp Horario de 07:00 a 14:00
              </label> 
              <label class="form-control mt-3" >
                Tarde 
                <input type="checkbox" name="txtTarde" id="txtTarde4" value="0">
                &nbsp Horario de 15:00 a 21:00
              </label>
            
              <input hidden type="text" id="dia_semana4" name="dia_semana" >
              <input hidden type="text" value="0" name="ultimo_dia" id="ultimo_dia4">
            
              <input hidden type="text" value="0" name="ultimo_dia_add_bono" id="ultimo_dia_add_bono4">

              <br>
              <span class="adquiriBonosT mt-3" style="color:#04607c"><strong style="color:#3c3c3b">Precio del bono entre semana:</strong> {{$precio_bono}}€</span> <br>
              <span class="adquiriBonosT mt-1" style="color:#04607c "><strong style="color:#3c3c3b">Precio del bono fin de semana :</strong> {{$precio_bono_findesemana}}€</span>
              <input hidden type="text" name="fecha_actual_control"  id="fecha_actual_control3">

              {{ csrf_field() }}

              <div id="errores4" class="bg-danger col-lg-12 text-light">
            
              </div>
              
              <div id="correcto4" class="bg-success col-lg-12 text-light mt-2">
            
              </div>

              <div id="errores4R" class="bg-danger col-lg-12 text-light">
            
              </div>
            
              <div id="correcto4R" class="bg-success col-lg-12 text-light mt-2">
          
             </div>
             

            
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <a id="comprar_bonos" type="button" class="btn btn-primary">Comprar</a>
            
          </div>
        </div>
      </div>
    </div>

      <div class="col-lg-6 d-block d-sm-block d-md-none mt-3 mb-3" id="eltiempoMovil">
        
        <h5 class="h5 border p-0 text-center mt-5" style="background:#04607c;color:white">Leyenda</h5>
      
        <table class="col-lg-12">
          <tr style="border-bottom:1px solid #D8D8D8">
            <td style="padding:10px">  
              <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mañana.png"/>
            </td>
            <td  style="padding:10px; border-right:1px solid #D8D8D8 ">  
              Reservado mañana  
            </td>

            <td  style="padding:10px">  
              <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/tarde.png"/>
            </td>
            <td  style="padding:10px;  ">  
              Reservado Tarde 
            </td>
            
            
          <tr>


          <tr style="border-bottom:1px solid #D8D8D8">
            <td  style="padding:10px">  
              <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mañana-tarde.png"/>
            </td>
            <td  style="padding:10px; border-right:1px solid #D8D8D8 ">  
              Reservado todo el dia
            </td>

            <td  style="padding:10px">  
            <img style="width:50px;" src=" {{ URL::to('/') }}/imagenes/reservas/festivo-leyenda.png"/>

            </td>
            <td style="padding:10px">  
               Día festivo
            </td>
            
          <tr>

          
          <tr style="border-bottom:1px solid #D8D8D8;">
            <td  style="padding:10px">  
              <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mantenimiento_leyenda.png"/>
            </td>
            <td style="border-bottom:1px solid #D8D8D8; border-right:1px solid #D8D8D8">  
              Mantenimiento
              
            </td>

            
            <td  style="padding:10px">  
                <div style="width:50%;background:#DDFFDC;height:66px">
                
                </div>
                </td>
                <td>  
                  Fin de semana 
                </td>
              </td>
              

            
          <tr>

          <tr style="border-bottom:1px solid #D8D8D8;" >
            
            
          <tr>

        </table>
        

      </div>

      <div class="col-lg-6 d-none d-sm-none d-md-block" id="div_tiempo_pc">
        <!-- <div id="c_19c7436efb59e63dd842176c5c13355c" class="ancho"></div><script type="text/javascript" src="https://www.eltiempo.es/widget/widget_loader/19c7436efb59e63dd842176c5c13355c"></script> -->
        <iframe  sandbox="allow-same-origin allow-scripts" src="https://wisuki.com/widget-details?spot=2586&lang=es&spotinfo=1" style="border: 0; width: 100%; height:400px;" frameborder="0"></iframe>
      </div>
 

    <!-- <iframe id="inlineFrameExample"
    title="Inline Frame Example"
    width="1000"
    height="1000"
    src="https://www.camaramar.com/spots/campello">
  </iframe>
    -->
  </div>
  <div id="div-calendario" class="col-lg-12 " style="overflow:hidden">
    <button type="button" style="margin-top:-100%" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
    </button>

    <button type="button" style="margin-top:-100%" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
    Launch demo modal
    </button>

    <!-- Modal reserva normal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reservar</h5>
            <button type="button" class="close cerrar-modal"  aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-lg-12">
            <form method="POST" autocomplete="off">
              <label>Barco</label>
              <input hidden class="form-control" type="text" name="txtBarcoID" id="txtBarcoID" value="{{$barco_identificacion}}" readonly>
              <input class="form-control" type="text" name="txtBarcoNombre" id="txtBarcoNombre" value="{{$nombre_barco}}" readonly>
              <label>Usuario</label>
              <input hidden class="form-control"  type="text" name="txtUsuarioID" id="txtUsuarioID" value="{{Auth::user()->id}}" readonly >

              <input class="form-control"  type="text" name="txtUsuario" id="txtUsuario" value="{{Auth::user()->name}}" readonly >
              <label>Fecha</label>
              <input class="form-control"  type="text" name="txtFecha" id="txtFecha" readonly>
              <h5 class="mt-3">Chequea un horario:  </h5>
              <label class="form-control mt-3" > 
                Mañana
              <input  type="checkbox" name="txtMañana" id="txtMañana" value="0">
              &nbsp Horario de 07:00 a 14:00
              </label> 
              <label class="form-control mt-3" >
                Tarde 
                <input type="checkbox" name="txtTarde" id="txtTarde" value="0">
                &nbsp Horario de 15:00 a 21:00
              </label>
            
              <input hidden type="text" value="0" name="ultimo_dia" id="ultimo_dia">
              <input hidden type="text" value="0" name="tipo_bono" id="tipo_bono">


              <div id="errores" class="bg-danger col-lg-12 text-light">
            
              </div>
              
              
              

              <span class="adquiriBonos mt-3">Precio del bono entre semana: {{$precio_bono}}€</span>
              <span class="adquiriBonos mt-1">Precio del bono fin de semana : {{$precio_bono_findesemana}}€</span>


              
              <input hidden type="text" name="fecha_actual_control" id="fecha_actual_control">
            

            </form>

            <form method="POST" id="formAddBono" autocomplete="off">
              <input hidden type="text" id="fecha_add_bono" name="fecha_add_bono">
              <input hidden type="text" id="dia_semana" name="dia_semana" >
              <input hidden type="text" id="bonos_barco_id" name="bonos_barco_id" value="{{$barco_identificacion}}" >
              <input hidden  type="text" id="bono_tarde" name="bono_tarde" >
              <input hidden type="text" id="bono_mañana" name="bono_mañana">
              <input hidden type="text" value="0" name="ultimo_dia_add_bono" id="ultimo_dia_add_bono">





          {{ csrf_field() }}

            </form>
            <button id="addbono" class="btn btn-primary mt-5 adquiriBonos">Adquirir bono</button>

            <div id="correcto" class="bg-success col-lg-12 text-light mt-2">
            
            </div>
         
          </div>
          <div class="modal-footer">
          <buttton id="btnAgregar" class="btn btn-primary">Reservar</buttton>
          
            <buttton id="btnCancelar"  class="btn btn-danger cerrar-modal">Cancelar</buttton>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal reserva ultimo momento -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel2">Reservar</h5>
            <button type="button" class="close cerrar-modal"  aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-lg-12">
            <form method="POST" autocomplete="off">
              <label>Barco</label>
              <input hidden class="form-control" type="text" name="txtBarcoID" id="txtBarcoID2" value="{{$barco_identificacion}}" readonly>
              <input class="form-control" type="text" name="txtBarcoNombre" id="txtBarcoNombre2" value="{{$nombre_barco}}" readonly>
              <label>Usuario</label>
              <input hidden class="form-control"  type="text" name="txtUsuarioID" id="txtUsuarioID2" value="{{Auth::user()->id}}" readonly >

              <input class="form-control"  type="text" name="txtUsuario" id="txtUsuario2" value="{{Auth::user()->name}}" readonly >
              <label>Fecha</label>
              <input class="form-control"  type="text" name="txtFecha" id="txtFecha2" readonly>
              <h5 class="mt-3">Chequea un horario:  </h5>
              <label class="form-control mt-3" > 
                Mañana
              <input  type="checkbox" name="txtMañana" id="txtMañana2" value="0">
              &nbsp Horario de 07:00 a 14:00
              </label> 
              <label class="form-control mt-3" >
                Tarde 
                <input type="checkbox" name="txtTarde" id="txtTarde2" value="0">
                &nbsp Horario de 15:00 a 21:00
              </label>
              <label class="col-md-12 mt-3">Seleccióna tipo de bono a usar</label>
              <select class="form-control" name="tipo_bono" id="tipo_bono2">
                <option value="1"> Bono de entre semana </option>
                <option value="2"> Bono de fin de semana </option>

              </select>
            
              <input hidden type="text" value="1" name="ultimo_dia" id="ultimo_dia2">

              <div id="errores2" class="bg-danger col-lg-12 text-light mt-2">
            
              </div>
              
              
              

              <span class="adquiriBonos mt-3">Precio del bono entre semana: {{$precio_bono}}€</span>
              <span class="adquiriBonos mt-1">Precio del bono fin de semana : {{$precio_bono_findesemana}}€</span>


              
              <input hidden type="text" name="fecha_actual_control"  id="fecha_actual_control2">
            

            </form>

            <form method="POST" id="formAddBono2" autocomplete="off">
              <input hidden type="text" id="fecha_add_bono2" name="fecha_add_bono">
              <input hidden type="text" id="dia_semana2" name="dia_semana" >
              <input hidden type="text" id="bonos_barco_id2" name="bonos_barco_id" value="{{$barco_identificacion}}" >
              <input hidden  type="text" id="bono_tarde2" name="bono_tarde" >
              <input hidden type="text" id="bono_mañana2" name="bono_mañana">
              <input hidden type="text" id="tipo_bono_add_bono2" name="tipo_bono_add_bono">

              <input hidden type="text" value="1" name="ultimo_dia_add_bono" id="ultimo_dia_add_bono2">
              <input hidden type="text"  name="ruta_imagen" id="ruta_imagen">






          {{ csrf_field() }}

            </form>
              <div id="contenedor-botones-ultima-hora2" style="display:none" class="row border">

                <div  id="contenedor-boton-addbono2" class="col-lg-6">
                    <button id="addbono2" style="width:100%;height:100%" class="btn btn-primary mt-5 adquiriBonos"><i class="fas fa-euro-sign"></i> Adquirir bono</button>
                </div>

                <div id="contenedor-boton-bono-ultima2" class="col-lg-6">
                  <button style="width:100%;height:100%" id="reserva_con_bono_ultima_hora" class="btn btn-primary mt-5 adquiriBonos"><i class="fas fa-stopwatch"></i> Reservar con bonos de última hora </button>
                </div>
                
           
              </div>
           
            <br>
  


            <div id="correcto2" class="bg-success col-lg-12 text-light mt-2">
            
            </div>
         
          </div>
          <div class="modal-footer mt-5">
            <buttton id="btnAgregar2" class="btn btn-primary btnAgregar2">Reservar</buttton>
          
            <buttton id="btnCancelar"  class="btn btn-danger cerrar-modal">Cancelar</buttton>
          </div>
        </div>
      </div>
    </div>


   <div class="row p-0">
   
      <div class="col-lg-10" id='calendar'>
      
      </div>

      <div id="div_windy"class="col-lg-2 p-0  " style="margin-top:4%;" id="c_5a9a9afcd3a29711ec9d37ed56f411a4"> 
      
        <h5 class="h5 border p-0 text-center" style="background:#04607c;color:white">Estado del mar</h5>
        <iframe id="iframe_windy"  width="100%" height="340" src="https://embed.windy.com/embed2.html?lat=38.308&lon=-0.444&detailLat=38.350&detailLon=-0.490&width=650&height=450&zoom=10&level=surface&overlay=wind&product=ecmwf&menu=&message=&marker=&calendar=now&pressure=&type=map&location=coordinates&detail=&metricWind=default&metricTemp=default&radarRange=-1" frameborder="0"></iframe>
          <p id="texto_windy" class="text-primary">Aquí podrás consultar el estado del mar y
          filtrar en diferentes opciones de viento,
          oleaje...Activa por horas, muévete por el
          mapa, o haz click para ampliar la imagen.</p>
        <h5 class="h5 border p-0 text-center mt-5 d-none d-sm-none d-md-block " style="background:#04607c;color:white">Leyenda</h5>
        
        <div id="div_leyenda_pc" class="border d-none d-sm-none d-md-block  " style="">

          <table id="table_leyenda_pc">
            <tr style="border-bottom:1px solid #D8D8D8">
              <td style="padding:10px">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mañana.png"/>
              </td>
              <td>  
                Reservado mañana  
              </td>
              
            <tr>

            <tr style="border-bottom:1px solid #D8D8D8">
              <td  style="padding:10px">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/tarde.png"/>
              </td>
              <td>  
                Reservado Tarde 
              </td>
              
            <tr>

            <tr style="border-bottom:1px solid #D8D8D8">
              <td  style="padding:10px">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mañana-tarde.png"/>
              </td>
              <td>  
                Reservado todo el dia
              </td>
              
            <tr>
            <tr style="border-bottom:1px solid #D8D8D8">
              <td  style="padding:10px">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mantenimiento_leyenda.png"/>
              </td>
              <td>  
                Mantenimiento
              </td>

              
            <tr>

            
            <td  style="padding:10px;border-bottom:1px solid #D8D8D8">  
                <div style="width:50%;background:#DDFFDC;height:66px">
                
                </div>
                </td>
                <td style="padding:10px;border-bottom:1px solid #D8D8D8">  
                  Fin de semana 
                </td>
              </td>
              

            <tr style="border-bottom:1px solid #D8D8D8;" >
              <td  style="padding:10px">  
              <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/festivo-leyenda.png"/>

              </td>
              <td>  
                Día festivo
              </td>
              
            <tr>

          </table>

          <table class="col-lg-12" id="table_leyenda_tablet" >
            <tr style="border-bottom:1px solid #D8D8D8">
              <td style="padding:10px;">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mañana.png"/>
              </td>
              <td style="padding:10px; border-right:1px solid #D8D8D8 ">  
                Reservado mañana  
              </td>

              <td  style="padding:10px">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/tarde.png"/>
              </td>
              <td>  
                Reservado Tarde 
              </td>
              
              
            <tr>


            <tr style="border-bottom:1px solid #D8D8D8">
              <td  style="padding:10px">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mañana-tarde.png"/>
              </td>
              <td  style="padding:10px; border-right:1px solid #D8D8D8 ">  
                Reservado todo el dia
              </td>

              <td  style="padding:10px">
              <img style="width:100px;" src=" {{ URL::to('/') }}/imagenes/reservas/festivo-leyenda.png"/>

              <!-- <div style="width:50%;background:#DDFFDC;height:66px">
              
              </div> -->
              </td>
              <td >  
                Día festivo 
              </td>
              
            <tr>

            <tr style="border-bottom:1px solid #D8D8D8; ">
              <td  style="padding:10px">  
                <img style="width:50%;" src=" {{ URL::to('/') }}/imagenes/reservas/mantenimiento_leyenda.png"/>
              </td>
              <td  style="padding:10px; border-right:1px solid #D8D8D8 ">  
                Mantenimiento
              </td>


              <td  style="padding:10px">  
                <div style="width:50%;background:#DDFFDC;height:66px">
                
                </div>
                </td>
                <td>  
                  Fin de semana 
                </td>
              </td>
              

            <tr>
          
          </table>
          
        
        </div>
        <div class="col-lg-12 d-sm-block d-md-none ">

          <!-- <div id="c_4a570143f6df52e95f8dbe4ad11504eb" class="normal"></div><script type="text/javascript" src="https://www.eltiempo.es/widget/widget_loader/4a570143f6df52e95f8dbe4ad11504eb"></script> -->
        </div>

      </div>
      
    </div>
  </div>

  </div>

</div>
@include('footer')


<script>


$('#tipo_bono4').css('display','none');
$('#titulo_tipo_bono4').css('display','none');


var textoTarde="";
var textoMañana="";
const events=[];

//sacar todos lops eventos que existen en reservas


fetch("{{route('dias_festivos.show')}}", { 
  headers: {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    "X-CSRF-Token": $('input[name="_token"]').val()
  },
  method: "post",
  credentials: "same-origin",
  body: JSON.stringify({
    key: "value"
  })
})
.then(response => response.json())
.then(data => {
  fechas_formateadas = [];
  for (var b=0; b<data.length; b++)
  {
    var element = data[b]['start'].split(' ');
    var fecha = element[0].split('-');
    fecha_formateada = fecha[0]+'-'+fecha[1]+'-'+fecha[2]
    nuevo_registro_fecha = fechas_formateadas.push(fecha_formateada);
  }
  imageUrl= "{{ URL::to('/') }}/imagenes/reservas/festivo.png ";
  for (var i=0; i<fechas_formateadas.length; i++) 
  { 
    $('[data-date="'+fechas_formateadas[i]+'"]').prepend('<img id="theImg" style="position:absolute !important"src="' + imageUrl + '" />');
  }
});





$('#txtFecha4').change(function(){

  hoy = new Date();
     año = hoy.getFullYear();
        dia= hoy.getDate();
        count =  dia.toString().length;
        if(count==1)
        {
          dia="0"+dia;
        }
      mes= hoy.getMonth()+1;
      
      h= año +"-" + mes+ '-'+dia;

      fecha = $('#txtFecha4').val();
      fecha_dif=moment(fecha,"YYYY-MM-DD");
      h_dif=moment(h), "YYYY-MM-DD";

      // alert(h);
      // alert(fecha);

      diferencias_dias =  fecha_dif.diff(h_dif, 'days');

     
      if(diferencias_dias<=1)
      {
        $('#tipo_bono4').css('display','block');
        $('#titulo_tipo_bono4').css('display','block');
       


      }
      else if(diferencias_dias>1){
        $('#tipo_bono4').css('display','none');

        $('#titulo_tipo_bono4').css('display','none');
      }
  
});
//comprar bonos des de botton
$('#comprar_bonos').click(function(){
    var confirmacion = confirm("¿Estas seguro de querer pedir un nuevo bono?");



 



    if (confirmacion == true) 
    {
      //comprobar fecha de cliente
        hoy_cliente = new Date();
        año_cliente = hoy_cliente.getFullYear();
        dia_cliente= hoy_cliente.getDate();
        count_cliente_dia =  dia_cliente.toString().length;
        if(count_cliente_dia==1)
        {
          dia_cliente="0"+dia_cliente;
        }
        mes_cliente= hoy_cliente.getMonth()+1;
   
        count_cliente_mes =  mes_cliente.toString().length;
        
        if(count_cliente_mes==1)
        {
          mes_cliente="0"+mes_cliente;
        }
        fecha_cliente= año_cliente +"-" + mes_cliente+ '-'+dia_cliente;

      

        $('#fecha_actual_control3').val(fecha_cliente); 

        alert( $('#fecha_actual_control3').val());

      //comprobar fecha para mosttrar distintos forms 

      hoy = new Date();
      año = hoy.getFullYear();
        dia= hoy.getDate();
        count =  dia.toString().length;
        if(count==1)
        {
          dia="0"+dia;
        }
      mes= hoy.getMonth()+1;
      
      h= año +"-" + mes+ '-'+dia;

      fecha = $('#txtFecha4').val();
      fecha_dif=moment(fecha, "YYYY-MM-DD");
      h_dif=moment(h, "YYYY-MM-DD");

      // alert(h);
      // alert(fecha);

      diferencias_dias =  fecha_dif.diff(h_dif, 'days');

     
      if(diferencias_dias<=1)
      {
        $('#ultimo_dia4').val(1);
        $('#ultimo_dia_add_bono4').val(1);
       


      }
      else if(diferencias_dias>1){
        $('#ultimo_dia4').val(0);
        $('#ultimo_dia_add_bono4').val(0);

    
      }
      // alert(diferencias_dias);

      if($('#txtTarde4').val()==0 && $('#txtMañana4').val()==0  )
      {
        alert('Selecciona al menos una franja horaria ');
        
      }
      else
      {
        if(diferencias_dias<0 || $('#txtFecha4').val()=="" )
        {
          alert('No se puede hacer una reserva con fecha antigua ni dejar en blanco la fecha');
        }

        else
        {
          const fechaComoCadena = fecha; // día lunes
          const numeroDia = new Date(fechaComoCadena).getDay();
          // dia="";
          // alert(numeroDia);
          switch (numeroDia) 
          {
            case 1:
                dia="lunes"
              break;
            case 2:
              dia="martes"

              break;
            case 3:
              dia="miercoles"

              break;
            case 4:
              dia="jueves"

              break;
            case 5:
              dia="viernes"

              break;
            case 6:
              dia="sabado"

              break;
            

            case 0:
              dia="domingo"
              break;

            default:
            dia="ninguno"
          }
          fecha = $('#dia_semana4').val(dia);
          var ruta = '{{ route("boton.bono.extra") }}';
        


          $.ajax({
            type: "POST",
            url: ruta, // This is what I have updated
            data: $("#formComprarBonos").serialize(),
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'bono pedido') 
                {

                  $('#errores4').css('display','none');
                  $('#correcto4').html('Su bono ha sido pedido, en recibir el aviso , procederomos a la carga del bono en su cuenta');

                
                  fecha_reservas =$('#txtFecha4').val();
                  tarde_reservas =$('#txtTarde4').val();
                  mañana_reservas =$('#txtMañana4').val();
                  tipo_bono =$('#tipo_bono4').val();
                  tipo_bono =$('#tipo_bono4').val();
                  fecha_control = $('#fecha_actual_control3').val();
                  
                  $('#txtFecha').val(fecha_reservas);
                  $('#txtTarde').val(tarde_reservas);
                  $('#txtMañana').val(mañana_reservas);
                  $('#ultimo_dia').val($('#ultimo_dia4').val());
                  $('#tipo_bono').val(tipo_bono);
                  $('#fecha_actual_control').val(fecha_control);




                
                  $( "#btnAgregar" ).trigger( "click" );
                }
                if (data.success == 'Reservado Mañana') 
                {
                    $('#errores4').css('display','block');
                    $('#errores4').html("");
                    $('#errores4').html("Reservado Mañana");
                }

                if (data.success == 'Reservado Tarde') 
                {
                    $('#errores4').css('display','block');

                    $('#errores4').html("");
                    $('#errores4').html("Reservado Tarde");
                }
                if (data.success == 'pedido diferente trimestre') 
                {
                    $('#errores4').css('display','block');

                    $('#errores4').html("");
                    $('#errores4').html("Si se trata de una reserva de diferente trimestre se tiene que hacer de manera normal");
                }


                if (data.success == 'Reservado todo el dia') 
                {
                    $('#errores4').css('display','block');

                    $('#errores4').html("");
                    $('#errores4').html("Reservado todo el dia");
                }
                
                if (data.success == 'Hay bonos del findesemana') 
                {
                    $('#errores4').css('display','block');

                    $('#errores4').html("");
                    $('#errores4').html("Hay bonos para realizar la reserva");
                }
                 
                if (data.success == 'Hay bonos del entresemana') 
                {
                    $('#errores4').css('display','block');

                    $('#errores4').html("");
                    $('#errores4').html("Hay bonos para realizar la reserva");
                }

                if (data.success == 'Hay bonos del entresemana y de findesemana') 
                {
                    $('#errores4').css('display','block');

                    $('#errores4').html("");
                    $('#errores4').html("Hay bonos para realizar la reserva");
                }
                if (data.success == 'Dos reservas pendientes') 
                {
                    $('#errores4').css('display','block');

                    $('#errores4').html("");
                    $('#errores4').html("No es posible, ya tienes dos reservas pendientes");
                }
             
            

               
                if (data.success == 'pedido en curso') 
                {
                    
                  $('#errores4').css('display','block');
                  $('#errores4').html("");
                    $('#errores4').html("Ya has pedido un bono, espera a que confirmemos el anterior");
                
                    $('#correcto4').css('display','none');

                  }

                //errores

                if (data.errors != "" && data.errors != null) {
                  
                }
            }
          })
        }
      }
      
    



     }
      

     
        
  });




//referente del modal normal de reservas

$("input[id=txtTarde]").click(function(){
	
  if($('#txtTarde').prop('checked') ) {
    $('#txtTarde').val(1);
    $('#bono_tarde').val(1);

    textoTarde="Tarde";

  }
  else{
    $('#txtTarde').val(0);
    $('#bono_tarde').val(0);

    textoTarde="";
  }
  img_reservas()
});

$("input[id=txtMañana]").click(function(){
	
  if($('#txtMañana').prop('checked') ) {
    $('#txtMañana').val(1);
    $('#bono_mañana').val(1);

    textoMañana="Mañana";
  }
  else{
    $('#txtMañana').val(0);
    $('#bono_mañana').val(0);

    textoMañana="";

  }
  img_reservas();
});
url_reservas="";
function img_reservas() {
  if($('#txtTarde').val()==1 && $('#txtMañana').val()==0)
  {
    url_reservas="{{ asset('imagenes/reservas/tarde.png') }}";
  }
  else if($('#txtTarde').val()==0 && $('#txtMañana').val()==1)
  {
    url_reservas="{{ asset('imagenes/reservas/mañana.png') }}";
  }
  else if($('#txtTarde').val()==1 && $('#txtMañana').val()==1)
  {
    url_reservas="{{ asset('imagenes/reservas/mañana-tarde.png') }}";
  }

}




//referernte al modal de ultimo dia

$("input[id=txtTarde2]").click(function(){
	
  if($('#txtTarde2').prop('checked') ) {
    $('#txtTarde2').val(1);
    $('#bono_tarde2').val(1);

    textoTarde="Tarde";

  }
  else{
    $('#txtTarde2').val(0);
    $('#bono_tarde2').val(0);

    textoTarde="";
  }
  img_reservas2();
});

$("input[id=txtMañana2]").click(function(){
	
  if($('#txtMañana2').prop('checked') ) {
    $('#txtMañana2').val(1);
    $('#bono_mañana2').val(1);

    textoMañana="Mañana";
  }
  else{
    $('#txtMañana2').val(0);
    $('#bono_mañana2').val(0);

    textoMañana="";

  }
  img_reservas2();
});

function img_reservas2() {
  if($('#txtTarde2').val()==1 && $('#txtMañana2').val()==0)
  {
    url_reservas="{{ asset('imagenes/reservas/tarde.png') }}";
  }
  else if($('#txtTarde2').val()==0 && $('#txtMañana2').val()==1)
  {
    url_reservas="{{ asset('imagenes/reservas/mañana.png') }}";
  }
  else if($('#txtTarde2').val()==1 && $('#txtMañana2').val()==1)
  {
    url_reservas="{{ asset('imagenes/reservas/mañana-tarde.png') }}";
  }

}

$('#reserva_con_bono_ultima_hora').click(function(){
  var ruta = '{{ route("bono.ultima.hora") }}';

  $('#ruta_imagen').val(url_reservas);
  // alert($('#ruta_imagen').val());
  $.ajax({
    type: "POST",
    url: ruta, // This is what I have updated
    data: $("#formAddBono2").serialize(),
    success:function(data) 
    {
        console.log("en el jquery");
        
        if (data.success == 'Reserva completada') 
        {
          alert('Reserva confirmada con éxito');

          location.reload();
        
        }
        if (data.success == 'Hay bonos de findesemana') 
        {
          $('#errores2').css('display','block');

          $('#errores2').html("Hay bonos disponibles de fin de semana para realizar la reserva");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }
        if (data.success == 'Hay bonos de entresemana') 
        {
          $('#errores2').css('display','block');

          $('#errores2').html("Hay bonos disponibles de entre semana para realizar la reserva");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }
        if (data.success == 'No hay bonos de ultima hora')
        {
          $('#errores2').css('display','block');

          $('#errores2').html("No hay bonos disponibles de ultima hora, por favor compre un bono para poder realizar la reserva");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }
        if (data.success == 'No hay bonos suficientes de ultima hora')
        {
          $('#errores2').css('display','block');

          $('#errores2').html("No hay bonos suficientes de ultima hora, por favor compre un bono para poder realizar la reserva");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }

        if (data.success == 'Consume bono entresemana')
        {
          $('#errores2').css('display','block');

          $('#errores2').html("Por favor consume el bono de entre semana primero y despues haga otra reserva con el bono de ultima hora");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }

        if (data.success == 'Consume bono findesemana')
        {
          $('#errores2').css('display','block');

          $('#errores2').html("Por favor consume el bono de fin de semana primero y despues haga otra reserva con el bono de ultima hora");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }

        if (data.success == 'reservado de tarde') 
        {
          $('#errores2').css('display','block');

          $('#errores2').html("Ya existe una reserva para la tarde");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }
        
        if (data.success == 'reservado de mañana') 
        {
          $('#errores2').css('display','block');

          $('#errores2').html("Ya existe una reserva para la mañana");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }

        if (data.success == 'reserva dia completo') 
        {
          $('#errores2').css('display','block');
          $('#errores2').html("Dia completo reservado");
          $('.adquiriBonos').css('display','none');
          $('#correcto2').css('display','none');
        }
        //errores
        
        if (data.errors != "" && data.errors != null) {
          
        }
    }
  })
});


//referente al modal del boton de comprar un bono
$("input[id=txtTarde4]").click(function(){
	
  if($('#txtTarde4').prop('checked') ) {
    $('#txtTarde4').val(1);
  

    textoTarde="Tarde";
   

  }
  else{
    $('#txtTarde4').val(0);

    textoTarde="";
  }
  img_reservas4();
});

$("input[id=txtMañana4]").click(function(){

  if($('#txtMañana4').prop('checked') ) {
    $('#txtMañana4').val(1);

    textoMañana="Mañana";
  }
  else{
    $('#txtMañana4').val(0);

    textoMañana="";

  }
  img_reservas4();
});

function img_reservas4() {
  if($('#txtTarde4').val()==1 && $('#txtMañana4').val()==0)
  {
    url_reservas="{{ asset('imagenes/reservas/tarde.png') }}";
  }
  else if($('#txtTarde4').val()==0 && $('#txtMañana4').val()==1)
  {
    url_reservas="{{ asset('imagenes/reservas/mañana.png') }}";
  }
  else if($('#txtTarde4').val()==1 && $('#txtMañana4').val()==1)
  {
    url_reservas="{{ asset('imagenes/reservas/mañana-tarde.png') }}";
  }

}


//calendario

$('.fc-dayGridMonth-button').html('value');
$('.adquiriBonos').css('display','none');


$('.cerrar-modal').click(function(){
  location.reload();

});

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
      schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
      // initialView: 'timeGridDay',
      initialView: 'dayGridMonth',
      headerToolbar: {
          left: 'prev,next today MiBoton',
          center: 'title',
          right:'',
          
      },
      firstDay: 1,
      customButtons:{
          prev:{
            click:function(){
              const monthActual = new Date().getMonth()+1;
              const monthCalendar = new Date(calendar.currentData.currentDate).getMonth()+1;
              const yearActual = new Date().getFullYear()
              const yearCalendar = new Date(calendar.currentData.currentDate).getFullYear();
              const diff = monthActual - monthCalendar;
              enablePreivous = false;
              if(diff <= 2 && yearActual === yearCalendar || yearActual < yearCalendar ||  yearActual > yearCalendar && monthCalendar=== 12 ){
                // console.log('previous');
                calendar.prev();
                fetch("{{route('dias_festivos.show')}}", { 
                  headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": $('input[name="_token"]').val()
                  },
                  method: "post",
                  credentials: "same-origin",
                  body: JSON.stringify({
                    key: "value"
                  })
                })
                .then(response => response.json())
                .then(data => {
                  fechas_formateadas = [];
                  for (var b=0; b<data.length; b++)
                  {
                    var element = data[b]['start'].split(' ');
                    var fecha = element[0].split('-');
                    fecha_formateada = fecha[0]+'-'+fecha[1]+'-'+fecha[2]
                    nuevo_registro_fecha = fechas_formateadas.push(fecha_formateada);
                  }
                  imageUrl= "{{ URL::to('/') }}/imagenes/reservas/festivo.png ";
                  for (var i=0; i<fechas_formateadas.length; i++) 
                  { 
                    $('[data-date="'+fechas_formateadas[i]+'"]').prepend('<img id="theImg" style="position:absolute !important"src="' + imageUrl + '" />');
                  }
                });
              }
            }
          },
          next:{
            click:function(){

              calendar.next();

                fetch("{{route('dias_festivos.show')}}", { 
                  headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": $('input[name="_token"]').val()
                  },
                  method: "post",
                  credentials: "same-origin",
                  body: JSON.stringify({
                    key: "value"
                  })
                })
                .then(response => response.json())
                .then(data => {
                  fechas_formateadas = [];
                  for (var b=0; b<data.length; b++)
                  {
                    var element = data[b]['start'].split(' ');
                    var fecha = element[0].split('-');
                    fecha_formateada = fecha[0]+'-'+fecha[1]+'-'+fecha[2]
                    nuevo_registro_fecha = fechas_formateadas.push(fecha_formateada);
                  }
                  imageUrl= "{{ URL::to('/') }}/imagenes/reservas/festivo.png ";
                  for (var i=0; i<fechas_formateadas.length; i++) 
                  { 
                    $('[data-date="'+fechas_formateadas[i]+'"]').prepend('<img id="theImg" style="position:absolute !important"src="' + imageUrl + '" />');
                  }
                });

            }

          },
          MiBoton:{
              text:"Reservar",
              click:function(){
              

                  $('#exampleModal').modal('toggle');
              } 
          }
      },
      buttonText:{
      
        today:'Hoy'
      },

      dateClick:function(info){
        //rellenar datos con fecha del cliente para comprobarla con la del servidor
        hoy_cliente = new Date();
        año_cliente = hoy_cliente.getFullYear();
        dia_cliente= hoy_cliente.getDate();
        count_cliente_dia =  dia_cliente.toString().length;
        if(count_cliente_dia==1)
        {
          dia_cliente="0"+dia_cliente;
        }
        mes_cliente= hoy_cliente.getMonth()+1;
   
        count_cliente_mes =  mes_cliente.toString().length;
        
        if(count_cliente_mes==1)
        {
          mes_cliente="0"+mes_cliente;
        }
        fecha_cliente= año_cliente +"-" + mes_cliente+ '-'+dia_cliente;

      

        $('#fecha_actual_control2').val(fecha_cliente);
        $('#fecha_actual_control').val(fecha_cliente);

        // alert($('#fecha_actual_control2').val());
        // alert($('#fecha_actual_control').val());

        //pasar datos
        $('#txtFecha').val(info.dateStr);
        $('#fecha_add_bono').val(info.dateStr);
        $('#fecha_add_bono2').val(info.dateStr);

        $('#txtFecha2').val(info.dateStr);


        hoy = new Date();
        año = hoy.getFullYear();
        dia= hoy.getDate();
        count =  dia.toString().length;
        if(count==1)
        {
          dia="0"+dia;
        }
        mes= hoy.getMonth()+1;
        
        h= año +"-" + mes+ '-'+dia;
        // alert(count);
        fecha_dif=moment(info.dateStr,"YYYY-MM-DD");
        h_dif=moment(h,"YYYY-MM-DD");

        console.log('fecha_diferencia: '+ fecha_dif);

        // alert(info.dateStr);
        // alert(h);
        diferencias_dias =  fecha_dif.diff(h_dif, 'days');
      
        if(diferencias_dias<=1)
        {
          $('#exampleModal2').modal('toggle');

        }
        else if(diferencias_dias>1){
          $('#exampleModal').modal('toggle');
      
        }
        
        // calendar.addEvent({title:"evento x", date:info.dateStr});
      },
      
  
      eventClick: function(info) {
       

      },



        eventContent: function(arg, createElement) 
        {
          if(arg.event.extendedProps.tarde==1 && arg.event.extendedProps.mañana==1  )
          {

           
            if(arg.event.extendedProps.imageUrl)
            {

              return createElement('img', {src: arg.event.extendedProps.imageUrl, width: 50, height: 150, id:"imagen_calendario"});
            }

          }
          else{

            if(arg.event.extendedProps.imageUrl)
            {

              return createElement('img', {src: arg.event.extendedProps.imageUrl, width: 50, height: 75, id:"imagen_calendario"});
            }

          }

       
          return arg.event.title;
        },
        eventOrder: "-title",
        events:"{{url('/reservas/show')}}",
      //  events: events,      

  });

  calendar.setOption('locale','Es');
  calendar.render();

  $('#btnAgregar').click(function(){
    ObjEvento=recolectarDatosGUI("POST");
    EnviarInformacion('',ObjEvento);
 

  });

  $('#btnAgregar2').click(function(){
   
    ObjEvento=recolectarDatosGUI2("POST");
    EnviarInformacion2('',ObjEvento);
 

  });


  
  function recolectarDatosGUI(method) {
    nuevoEvento = {
      barco_id:$('#txtBarcoID').val(),
      title:"ocupado:"+' '+textoMañana+' '+textoTarde,
      user_id:$('#txtUsuarioID').val(),
      user_name:$('#txtUsuario').val(),

      start:$('#txtFecha').val(),
      end:$('#txtFecha').val(),
      mañana:$('#txtMañana').val(),
      tarde:$('#txtTarde').val(),
      ultimo_dia:$('#ultimo_dia').val(),
      tipo_bono : $('#tipo_bono').val(),
      fecha_actual_control : $('#fecha_actual_control').val(),

      image_url:url_reservas,

      '_token':$("meta[name='csrf-token']").attr("content"),
      '_method':method,
    }
   return (nuevoEvento);
  }

  function recolectarDatosGUI2(method) {
    nuevoEvento = {
      barco_id:$('#txtBarcoID2').val(),
      title:"ocupado:"+' '+textoMañana+' '+textoTarde,
      user_id:$('#txtUsuarioID2').val(),
      user_name:$('#txtUsuario2').val(),

      start:$('#txtFecha2').val(),
      end:$('#txtFecha2').val(),
      mañana:$('#txtMañana2').val(),
      tarde:$('#txtTarde2').val(),
      ultimo_dia:$('#ultimo_dia2').val(),
      tipo_bono : $('#tipo_bono2').val(),
      fecha_actual_control : $('#fecha_actual_control2').val(),

      image_url:url_reservas,
      


      '_token':$("meta[name='csrf-token']").attr("content"),
      '_method':method,
    }
   return (nuevoEvento);
  }

  function EnviarInformacion(accion,objEvento){
    $.ajax(
      {
        type:"POST",
        url:"{{url('/reservas')}}"+accion,
        data:objEvento,
        success:function(data){

          if (data.success == 'fechas diferentes') {
             alert('Servicio en mantenimiento disculpen las molestias ');
             location.reload();
            // alert(data.success );

          }
        
          if (data.success == 'Reserva realizada correctamente') {
             alert('Reserva confirmada con éxito');
             location.reload();
            // alert(data.success );

          }
          if (data.success == 'No tienes bonos') {
            
              $('#errores').html('No hay bonos para realizar la reserva ');
              $('#errore4R').html('No hay bonos para realizar la reserva ');

              $('.adquiriBonos').css('display','block');



          }
          if (data.success == 'Ocupado por la mañana') {

              $('#errores').html('No es posible, ya hay una reserva confirmada para ese día');
              $('#errores4R').html('No es posible, ya hay una reserva confirmada para ese día');


          }

          if (data.success == 'Reserva ok diferente trimestre') {

              alert('Esta reserva al tratarse de diferente trimestre se descontrán los bonos del próximo trimestre')
              location.reload();


          }

          if (data.success == 'Ocupado por la tarde') 
          {

            $('#errores').html('No es posible, ya hay una reserva confirmada para ese día');
            $('#errores4R').html('No es posible, ya hay una reserva confirmada para ese día');


          }
          if (data.success == 'dia completo') 
          {

            $('#errores').html('No es posible, ya hay dos reservas confirmadas para ese día');
            $('#errores4R').html('No es posible, ya hay dos reservas confirmadas para ese día');



          }

          if (data.success == 'Hay una reserva como minimo') {

            $('#errores').html('No puedes reservar el dica completo ya que el barco tiene una reserva');
            $('#errores4R').html('No puedes reservar el dica completo ya que el barco tiene una reserva');


          }

          if (data.success == 'no check') {

            $('#errores').html('Hay que seleccionar al menos un a franja horaria');
            $('#errores4R').html('Hay que seleccionar al menos un a franja horaria');




          }

          if (data.success == 'reserva atras') {
            
            $('#errores').html('No puedes seleccionar una fecha antigua');
            $('#errores4R').html('No puedes seleccionar una fecha antigua');



          }

          if (data.success == 'dias corridos') {

              $('#errores').html('No puedes reservar una fecha con más de 90 dias de antelación');
              $('#errores4R').html('No puedes reservar una fecha con más de 90 dias de antelación');

              


          }

          if (data.success == 'simultaneo') {

              $('#errores').html('No es posible, ya tienes dos reservas pendientes');
              $('#errores4R').html('No es posible, ya tienes dos reservas pendientes');



          }

          if (data.success == 'error hora') {

              $('#errores').html('Se ha pasado la hora de reserva');
              $('#errores4R').html('Se ha pasado la hora de reserva');



              }

              if (data.success == 'encontrado') {

                  $('#errores').html('No es posible, ya hay dos reservas confirmadas para ese día');
                  $('#errores4R').html('No es posible, ya hay dos reservas confirmadas para ese día');


              }



            
          
          // alert('correcto');
          //     alert(data.success );
       
        
        },

        
        error:function(data){
        
        alert('Las franjas horarias que has marcadas de este barco se encuentra reservadas intenta otras franjas o fecha');
        // alert(data.errors);
      },



      }   
    );

  }

  function EnviarInformacion2(accion,objEvento){
    $.ajax(
      {
        type:"POST",
        url:"{{url('/reservas')}}"+accion,
        data:objEvento,
        success:function(data){
          
          if (data.success == 'fechas diferentes') {
             alert('Servicio en mantenimiento disculpen las molestias ');
             location.reload();
            // alert(data.success );

          }
        
          if (data.success == 'Reserva realizada correctamente') {
             alert('Reserva confirmada con éxito');
             location.reload();
            // alert(data.success );

          }
          if (data.success == 'No tienes bonos') {

              $('#contenedor-botones-ultima-hora2').css('display','block');
              $('#errores2').html('No hay bonos para realizar la reserva ');
              $('.adquiriBonos').css('display','block');
              $('#contenedor-boton-addbono2').css('width','50%');
              $('#contenedor-boton-addbono2').css('display','inline-block');
              $('#contenedor-boton-addbono2').css('float','left');
              $('#contenedor-boton-addbono2').css('height','100px');


              $('#contenedor-boton-bono-ultima2').css('width','50%');
              $('#contenedor-boton-bono-ultima2').css('display','inline-block');
              $('#contenedor-boton-bono-ultima2').css('float','left');
              $('#contenedor-boton-bono-ultima2').css('height','100px');




  



          }
          if (data.success == 'Ocupado por la mañana') {

              $('#errores2').html('No es posible, ya hay una reserva confirmada para ese día');


          }
          if (data.success == 'Reserva ok diferente trimestre') {

            alert('Esta reserva al tratarse de diferente trimestre se descontrán los bonos del próximo trimestre')
            location.reload();
         

          }

          if (data.success == 'Ocupado por la tarde') 
          {

            $('#errores2').html('No es posible, ya hay una reserva confirmada para ese día');


          }
          if (data.success == 'dia completo') 
          {

            $('#errores2').html('No es posible, ya hay dos reservas confirmadas para ese día');


          }

          if (data.success == 'Hay una reserva como minimo') {

            $('#errores2').html('No puedes reservar el dica completo ya que el barco tiene una reserva');


          }

          if (data.success == 'no check') {

            $('#errores2').html('Hay que seleccionar al menos un a franja horaria');



          }

          if (data.success == 'reserva atras') {

            $('#errores2').html('No puedes seleccionar una fecha antigua');


          }

          if (data.success == 'dias corridos') {

              $('#errores2').html('No puedes reservar una fecha con más de 90 dias de antelación');


          }

          if (data.success == 'simultaneo') {

              $('#errores2').html('No es posible, ya tienes dos reservas pendientes');


          }

          if (data.success == 'error hora') {

              $('#errores2').html('Se ha pasado la hora de reserva');


              }

              if (data.success == 'encontrado') {

                  $('#errores2').html('No es posible, ya hay dos reservas confirmadas para ese día');


              }



            
          
          // alert('correcto');
          //     alert(data.success );
       
        
        },

        
        error:function(data){
        
        alert('Las franjas horarias que has marcadas de este barco se encuentra reservadas intenta otras franjas o fecha');
        // alert(data.errors);
      },



      }   
    );

  }

  $('#addbono').click(function(){
    var confirmacion = confirm("¿Estas seguro de querer pedir un nuevo bono?");


    if (confirmacion == true) 
    {
      fecha = $('#fecha_add_bono').val();

      const fechaComoCadena = fecha; // día lunes
      const numeroDia = new Date(fechaComoCadena).getDay();
      // dia="";
      // alert(numeroDia);
      switch (numeroDia) {
        case 1:
            dia="lunes"
          break;
        case 2:
          dia="martes"

          break;
        case 3:
          dia="miercoles"

          break;
        case 4:
          dia="jueves"

          break;
        case 5:
          dia="viernes"

          break;
        case 6:
          dia="sabado"

          break;
        

        case 0:
          dia="domingo"
          break;

        default:
        dia="ninguno"
      }
      fecha = $('#dia_semana').val(dia);
      var ruta = '{{ route("bono.extra") }}';

      $.ajax({
        type: "POST",
        url: ruta, // This is what I have updated
        data: $("#formAddBono").serialize(),
        success:function(data) {
            console.log("en el jquery");
            if (data.success == 'bono pedido') {

              $('#errores').css('display','none');
              $('#correcto').html('Su bono ha sido pedido, en recibir el aviso , procederomos a la carga del bono en su cuenta');

              $('.adquiriBonos').css('display','none');
            
              $( "#btnAgregar" ).trigger( "click" );
            }
            if (data.success == 'pedido en curso') {
                
              $('#errores').css('display','block');

                $('#errores').html("Ya has pedido un bono, espera a que confirmemos el anterior");
                $('.adquiriBonos').css('display','none');
                $('#correcto').css('display','none');

                // $('#correcto').html('Su bono ha sido pedido, en recibir el aviso , procederomos a la carga del bono en su cuenta');
    
              
    
              }

              if (data.success == 'pedido diferente trimestre') 
                {
                    $('#errores').css('display','block');

                    $('#errores').html("");
                    $('#errores').html("Si se trata de una reserva de diferente trimestre se tiene que hacer de manera normal");
                }

            //errores

            if (data.errors != "" && data.errors != null) {
              
            }
        }
      })
    }

        
  });

  $('#addbono2').click(function(){
    var confirmacion = confirm("¿Estas seguro de querer pedir un nuevo bono?");

    fecha = $('#fecha_add_bono2').val();
    tipo_bono2 = $('#tipo_bono2').val();

    $('#tipo_bono_add_bono2').val(tipo_bono2);

    // alert($('#tipo_bono_add_bono2').val());

    if (confirmacion == true) 
    {
  
      const fechaComoCadena = fecha; // día lunes
      const numeroDia = new Date(fechaComoCadena).getDay();
      // dia="";
      switch (numeroDia) {
        case 1:
            dia="lunes"
          break;
        case 2:
          dia="martes"

          break;
        case 3:
          dia="miercoles"

          break;
        case 4:
          dia="jueves"

          break;
        case 5:
          dia="viernes"

          break;
        case 6:
          dia="sabado"

          break;
        

        case 0:
          dia="domingo"
          break;

        default:
        dia="ninguno"
      }
      fecha = $('#dia_semana2').val(dia);
      var ruta = '{{ route("bono.extra") }}';

      $.ajax({
        type: "POST",
        url: ruta, // This is what I have updated
        data: $("#formAddBono2").serialize(),
        success:function(data) {
            console.log("en el jquery");
            if (data.success == 'bono pedido') {

              $('#errores2').css('display','none');
              $('#correcto2').html('Su bono ha sido pedido, en recibir el aviso , procederomos a la carga del bono en su cuenta');

              $('.adquiriBonos').css('display','none');
            
              $( ".btnAgregar2" ).trigger( "click" );

            }
            if (data.success == 'pedido en curso') {
                
              $('#errores2').css('display','block');

                $('#errores2').html("Ya has pedido un bono, espera a que confirmemos el anterior");
                $('.adquiriBonos').css('display','none');
                $('#correcto2').css('display','none');

                // $('#correcto').html('Su bono ha sido pedido, en recibir el aviso , procederomos a la carga del bono en su cuenta');
    
              
    
              }
              if (data.success == 'pedido diferente trimestre') 
                {
                    $('#errores').css('display','block');

                    $('#errores').html("");
                    $('#errores').html("Si se trata de una reserva de diferente trimestre se tiene que hacer de manera normal");
                }


            //errores

            if (data.errors != "" && data.errors != null) {
              
            }
        }
      })
    }

        
  });

});



</script>





@endsection

