@extends('layouts.app')
<style>

input[type="radio"] {
  display: none;
}

.estrella_valorarion {
  color: grey;
  font-size:50px;
  cursor:pointer;
}

.clasificacion {
  direction: rtl;
  unicode-bidi: bidi-override;
}

.estrella_valorarion:hover,
.estrella_valorarion:hover ~ .estrella_valorarion {
  color: orange;
}

input[type="radio"]:checked ~ .estrella_valorarion {
  color: orange;
}

@media only screen and (min-width:0px) and (max-width: 600px) {
			
    #contenedor{
        width:100% !important;
      }
      #imagen_principal{
          height:300px !important;
          width:100% !important;
      }
      #detalles_tecnicos{
          margin-left:0% !important;
          margin-top:5% !important;
          /* border:1px solid red !important; */
      }

      #detalles_barco{
          /* margin-top:90%;    */
          /* border:1px solid red !important; */
      }

      #imagenes_secundarias{
          /* border:1px solid red !important; */
          /* position:absolute !important; */
          /* margin-top:90% !important; */
      }
      #nombre_barco{
          font-size:20px !important;
          padding:0px !important;
      }
      #imagenes_secundarias img{
         height:100px !important;
          width:40% !important;
          display:inline-block !important;
      }
      #div_titulo_descriptivo{
          margin-left:0% !important;
      }
      #precios_bonos{
          margin-top:10% !important;
      }
}
@media only screen and (min-width:768px) and (max-width: 991px) 
{
    #detalles_tecnicos{
        margin-top:5% !important;
    }
    #precios_bonos{
        margin-top:10% !important;
    }

}
@media only screen and (min-width:992px) and (max-width: 1600px) 
{
      /* #contenedor{
        width:130% !important;
      }
      #div-calendario{
        padding:0px !important;

      }
      #calendar{
        
        height:1000px;
      }
   
      #containder-header{
        width:135% !important;

      } */

}

</style>

@section('content')

<div id="contenedor" class="container-fluid p-3 rounded" style=" width:90%">
    <h1 id="nombre_barco" style="color:#04607c" class="col-lg-6">{{$nombre}}</h1>
<!-- 
    <div class="col-lg-6 text-right pb-2 pr-0">
        <i id="editar_imagen_principal" class="fa fa-edit btn btn-primary ">Editar Imágen principal</i>
    </div> -->

    <div id="boton_detalles"class="col-lg-6 text-right">
                @if(Auth::user()->admin == 1)
                    @include('modal_edit_imagen_barco') 
               @endif
            </div>
    
    <div class="row">

        <div class="col-lg-7">
            <img id="imagen_principal" width="90%" height="500px" src=" {{ URL::to('/') }}/imagenes/barcos/{{$imagen_principal}}">
        </div>

        <div id="detalles_barco" class="col-lg-5 p-0">

            <div id="boton_detalles"class="col-lg-12 text-right">
            @if(Auth::user()->admin == 1)
               @include('modal_add_barco') 
               @endif
            </div>

            
            <div id="detalles_tecnicos" class="col-lg-12 " style="margin-left:-5%;margin-bottom:10px">
                <h4  style="color:#04607c" class="h4   ">Detalles técnicos</h4>
                <strong class="">Marnific Nautic Club S.L.</strong>      

            </div>
         
                <div class="row border p-3">
              
                    <ul style="list-style:none" class="col-lg-6 ">
                        <li><strong  style="color:#04607c">Puerto:</strong> <span class="text-secondary">{{$puerto}}</span></li>
                        <li><strong  style="color:#04607c">Año:</strong> <span class="text-secondary">2021</span></li>
                        <li><strong  style="color:#04607c">Eslora:</strong> <span class="text-secondary">{{$eslora}} m</span></li>
                        <li><strong  style="color:#04607c">Manga:</strong> <span class="text-secondary">{{$manga}} m </span></li>
                        <li><strong  style="color:#04607c">Tipo:</strong> <span class="text-secondary">Motor</span></li>
                        <li><strong  style="color:#04607c">Calado:</strong> <span class="text-secondary">{{$calado}} m</span></li>
                        
                        <!-- <li><strong style="color:#04607c" >Velocidad Máxima:</strong><span class="text-secondary"> {{$velocidad_maxima}}</span></li> -->
                    </ul>
                       
                    <ul  style="list-style:none" class="col-lg-6">
                        <li><strong style="color:#04607c">Combustible:</strong> <span class="text-secondary">Gsolina</span></li>
                        <li><strong style="color:#04607c">Depósito Combustible:</strong> <span class="text-secondary">{{$capacidad_combustible}} l</span></li>
                        <li><strong  style="color:#04607c" >Motor:</strong> <span class="text-secondary">{{$motor}} HP MERCURY F115</span></li>
                        <!-- <li><strong  style="color:#04607c">Desplazamiento:</strong> <span class="text-secondary">{{$desplazamiento}} t</span></li> -->
                        <!-- <li><strong style="color:#04607c">Velocidad de crucero: <span class="text-secondary">{{$velocidad_crucero}}</span></li> -->
                        <li><strong style="color:#04607c">Nº personas a bordo:</strong> <span class="text-secondary">{{$numero_personas}}</span></li>
                        <!-- <li><strong style="color:#04607c">Camarotes:</strong> <span class="text-secondary">{{$camarotes}}</span></li> -->
                        <!-- <li><strong style="color:#04607c">Baños:</strong> <span class="text-secondary">{{$baños}}</span></li> -->
                        <li><strong style="color:#04607c">Depósito Agua :</strong><span class="text-secondary"> {{$capacidad_agua_dulce}} l</span></li>
                    </ul>
                    
                </div>
                <div id="div_titulo_descriptivo"class="col-lg-12 mt-3" style="margin-left:-5%">
                    <strong style="color:#04607c">Titulo descriptivo:</strong>
                    <br>
                    {{$titulo}}

                </div>
           
        </div>
        

        <div id="imagenes_secundarias" class="col-lg-7 p-0">
            <h4 style="color:#04607c" class="col-lg-12 mt-5">Imágenes </h4>

                <?php $contador_imagenes = 0; ?>

                @foreach($imagenes_barco as $imagen)

                <?php $contador_imagenes++ ?>
                
                <a target="_blank"  href=" {{ URL::to('/') }}/imagenes/barcos/{{$imagen->nombre_archivo}}">
                    <img  style="margin-top:1% !important"width="15%" height="150px" src=" {{ URL::to('/') }}/imagenes/barcos/{{$imagen->nombre_archivo}}">  
                </a>
                @if(Auth::user()->admin == 1)
                <a href="{{route('eliminar.imagenes', $imagen->id)}}"><i type="button" class="fas fa-window-close"></i></a>
                @endif
                @endforeach
                @if(Auth::user()->admin == 1)
                <a  id="añadir_imagenes"  width="20%" height="200px" style="height:200px !important; width:15% !important;padding-top:5% !important;" class="btn btn-primary ml-3">
                   <p> Añadir más imágenes</p>
                </a>
                @endif
                
          
        </div>


        <div id="precios_bonos" class="col-lg-5 border rounded " style="background:#04607c;color:#AEAEAE">
            <div class="row">
                <div class="col-lg-6"> 
                    <br>
                    <br>
                    <h3 style="color:#FAD429"class="h2">Precio bonos extra:</h3><br>
                    <h5 style="color:white;">Bono extra  = <span style="background:white;color:#04607c;padding:4px;border-radius:4px;"><strong>{{$precio_entresemana}}€</strong></span></h5><br>
                    <!-- <h5 style="color:white;">Bono fin de semana = <span style="background:white;color:#04607c;padding:4px;border-radius:4px;"><strong>{{$precio_findesemana}}€</strong></span></h5> -->
                    
                </div>
                <div class="col-lg-6"> 
                <!-- <h2 style="color:#FAD429" class="h2 mt-3 text-center">Tipo de barco</h2> -->
                    <img class=" ml-1 mt-4"  width="100%" height="150px" src=" {{ URL::to('/') }}/imagenes/iconos_tipo_barco/yate.png">  
                    <h4 y  class="col-lg-12 text-center    " style="color:#FAD429">Bono salida extra</h4>
                </div>


            </div>
            
        </div>


    </div>

    <div class="col-lg-12 border mt-5 pt-3">
        <!-- <button id="boton-descripcion" class="btn btn-secondary">Descripción larga</button> -->
        <!-- <button id="boton-valoraciones" class="btn btn-secondary">Valoracion</button> -->
        <!-- <button id="boton-mensaje" class="btn btn-secondary">Mensaje</button> -->
       

        <div id="div_descripcion_larga" class="col-lg-12">
        
            <h3 style="color:#04607c">Descripción:</h3>
            <hr>
            <br>
            {{$descripcion}}

        </div>

        <div style="" id="div_valoraciones" class="col-lg-12">
            
            <form>
                Puntuación:
                <p class="clasificacion col-lg-12">
                    <input class="input_valoracion" id="radio1" type="radio" name="estrellas" value="5"><!--
                    --><label class="estrella_valorarion" for="radio1">★</label><!--
                    --><input class="input_valoracion" id="radio2" type="radio" name="estrellas" value="4"><!--
                    --><label class="estrella_valorarion"  for="radio2">★</label><!--
                    --><input class="input_valoracion" id="radio3" type="radio" name="estrellas" value="3"><!--
                    --><label class="estrella_valorarion"  for="radio3">★</label><!--
                    --><input class="input_valoracion" id="radio4" type="radio" name="estrellas" value="2"><!--
                    --><label class="estrella_valorarion"  for="radio4">★</label><!--
                    --><input class="input_valoracion" id="radio5" type="radio" name="estrellas" value="1"><!--
                    --><label class="estrella_valorarion"  for="radio5">★</label>
             
                </p>

                Deja un comenatario:
                <textarea style="height:200px"   class="form-control"></textarea>
            </form>
          
        </div>

        <div id="div_mensaje" class="col-lg-12">
            
            <form class="col-lg-12 mt-4 mb-4">
                <input  class="form-control" type="text" placeholder="Asunto">
                <label class="col-lg-12 p-0 mt-3">Mensaje:</label>
                <textarea style="height:200px"   class="form-control"></textarea>
            </form>
            <div class="col-lg-12 text-center">
                <a class="btn btn-primary col-lg-2 mb-3 ">Enviar</a>
            </div>
 
          
        </div>
     

    </div>

</div>
@include('footer')
<script type="text/javascript">


    $( document ).ready(function() {

        $('#finalizar_subida_imagen_principal').css('display','none');
        $('#finalizar_subida_imagenes').css('display','none');
        $('#formAddImagenesBarcos').css('display','none');
        $('#editar_barco').css('display','block');
        $('#crear_barco').css('display','none');
        $('#div_valoraciones').css('display','none');
        $('#div_mensaje').css('display','none');
        $('#boton_modal').html('Editar detalles del barco');
        $('#boton_modal_edit_image').html('Editar imagen principal');

        
        $('#titulo_modal').html('Editar detalles');

        //rellenar el form de editar con los datos
        $('#nombre').val('<?php echo $nombre?>');
        $('#titulo').val('<?php echo $titulo?>');
        $('#numero_personas').val('<?php echo $numero_personas?>');
        $('#eslora').val('<?php echo $eslora?>');
        $('#manga').val('<?php echo $manga?>');
        $('#calado').val('<?php echo $calado?>');
        $('#motor').val('<?php echo $motor?>');
        $('#velocidad_maxima').val('<?php echo $velocidad_maxima?>');
        $('#velocidad_crucero').val('<?php echo $velocidad_crucero?>');
        $('#camarotes').val('<?php echo $camarotes?>');
        $('#baños').val('<?php echo $baños?>');
        $('#capacidad_agua_dulce').val('<?php echo $capacidad_agua_dulce?>');
        $('#capacidad_combustible').val('<?php echo $capacidad_combustible?>');
        $('#desplazamiento').val('<?php echo $desplazamiento?>');
        $('#ciudad').val('<?php echo $ciudad?>');
        $('#puerto').val('<?php echo $puerto?>');
        $('#descripcion').html('<?php echo preg_replace("/\r\n|\r|\n/",'\n',$descripcion);?>');
        $('#precio_bono_findesemana').val('<?php echo $precio_findesemana?>');
        $('#precio_bono').val('<?php echo $precio_entresemana?>');
        $('#codigo_bono').val('<?php echo $codigo_bono?>');

        $('#boton_modal').css('height','60px');
        $('#boton_modal').css('width','50%');
        $('#boton_modal').css('font-size','15px');
        $('#añadir_imagenes').css('height','80px');
        $('#añadir_imagenes').css('width','20%');
        $('#boton_modal_edit_image').css('margin-bottom','5%');

    
        
    
    });

    $("#editar_imagen_principal").click(function(){
        location.reload();
    });

    document.onkeydown = function(evt) { evt = evt || window.event; if (evt.keyCode == 27) { 
        location.reload();
        
    } };


    $(".close").click(function(){
        location.reload();
    });

    $("#boton-descripcion").click(function(){
        $('#div_valoraciones').css('display','none');
        $('#div_mensaje').css('display','none');
        $('#div_descripcion_larga').css('display','block');
    });

    $("#boton-valoraciones").click(function(){
        $('#div_valoraciones').css('display','block');
        $('#div_mensaje').css('display','none');
        $('#div_descripcion_larga').css('display','none');

    }); 

    $("#boton-mensaje").click(function(){
        $('#div_valoraciones').css('display','none');
        $('#div_mensaje').css('display','block');
        $('#div_descripcion_larga').css('display','none');
    });

    $('#boton_modal_edit_imagenes').click(function(){
        $('#formEditImagenPrincipalBarco').css('display','block');
        $('#formEditImagenesBarco').css('display', 'none');
    });

    $("#añadir_imagenes").click(function(){
        $("#boton_modal_edit_image").trigger( "click" );
        $('#formEditImagenPrincipalBarco').css('display','none');
        $('#formEditImagenesBarco').css('display', 'block');
    });

    $(".xzoom").xzoom({
        position: 'left',
        Xoffset: 0,
      
    });
    

    $("#editar_barco").click(function(){


        
        var ruta = '{{route("barcos.update", $id)}}';
            // ruta = ruta.replace(':factura_id', factura_id);
      
        $.ajax({
            type: "POST",
            url: ruta, // This is what I have updated
            data: $("#formAddBarcos").serialize(),
            success:function(data) {
                console.log("en el jquery");
                if (data.success == 'Barco creado correctamente') {
                  
                    location.reload();
                    // $("#errores").css('display','none');
                    // $('#formAddBarcos').css('display','none');
                    // $('#formAddImagenesBarco').css('display','block');
                    // $('.dz-default.dz-message').html('Introduce tus imagenes aquí');
                    // $('#finalizar_subida_imagen_principal').css('display','block');
                    // $('#crear_barco').css('display','none');

                    // ultimo_barco = data.ultimo_barco;
                    // $('#id_barco').val(ultimo_barco);
                    // alert(ultimo_barco);
                    

                }

                //errores

                if (data.errors != "" && data.errors != null) {
                    // console.log("error");
                    // console.log(data.errors);
                   
                    
                    $("#errores").empty();
                    var erroresAlertas = "<br><div class=\"alert alert-danger \" style=\"float:left;width:100%\" >";
                    jQuery.each(data.errors, function(key, value) {
                        erroresAlertas = erroresAlertas + '<p>' + value + '</p>';
                    });
                    erroresAlertas += '</div>';
                    $("#errores").append(erroresAlertas);
                }
            }
        })
    });




</script>

@endsection