<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Barco;
use App\Models\ImagenBarco;
use App\Models\Bono;
use App\Http\Requests\BarcoRequest;

class BarcosController extends Controller
{
    public function store(Request $request)
    {
        $input = $request->all();
        // Log::info($input['imagen_principal']);
     
        // Log::info('entra albaran entrada create');
        // Log::info($request);
        $validator = Validator::make(
            $request->all(),
          
            $this->reglasValidacion(),
            $this->mensajesValidacion(),
        );
        if ($validator->fails()) {
            // Log::info($request);
         
            return response()->json(['errors' =>  $validator->errors()->all()]);
        } else {
            // Log::info($request);
            $this->save($request);

           
            $ultimo_barco= $request->session()->get('barco_metido');

            // Log::info($ultimo_barco);

            return response()->json(['success' => 'Barco creado correctamente','ultimo_barco'=>$ultimo_barco]);
        }
        
      
        
    }
    public function save($requestDatos)
    {

        // Log::info($requestDatos);
        Log::info('entra save barco');
        //Albaran
        $barco = new Barco();
        $barco->nombre = $requestDatos['nombre'];
        $barco->tipo = $requestDatos['tipo_barco'];
        $barco->titulo = $requestDatos['titulo'];
        $barco->descripcion = $requestDatos['descripcion'];
        $barco->numero_personas = $requestDatos['numero_personas'];
        $barco->ciudad = $requestDatos['ciudad'];
        $barco->puerto = $requestDatos['puerto'];
        $barco->eslora = $requestDatos['eslora'];
        $barco->manga = $requestDatos['manga'];
        $barco->calado = $requestDatos['calado'];
        $barco->motor = $requestDatos['motor'];
        $barco->desplazamiento = $requestDatos['desplazamiento'];
        $barco->velocidad_maxima = $requestDatos['velocidad_maxima'];
        $barco->velocidad_crucero = $requestDatos['velocidad_crucero'];
        $barco->camarotes = $requestDatos['camarotes'];
        $barco->baños = $requestDatos['baños'];
        $barco->capacidad_agua_dulce = $requestDatos['capacidad_agua_dulce'];
        $barco->capacidad_combustible = $requestDatos['capacidad_combustible'];   
        $barco->save();

        $ultimo_id = Barco::latest('id')->first();

        $ultimo_id = $ultimo_id->id;

        $requestDatos->session()->put('barco_metido', $ultimo_id);

        //Creamos un nuevo bono referente al barco

        $bono = new Bono();

        $bono->barco_id = $ultimo_id;
        $bono->precio =  $requestDatos['precio_bono'];
        $bono->precio_findesemana =  $requestDatos['precio_bono_findesemana'];

  
        $bono->codigo =  $requestDatos['codigo_bono'];

        $bono->save();





    }

    public function upload_imagen( Request $request)
    {

        $file = $request->file('file');
        $id_barco = $request->id_barco;
        $path = public_path() . '/imagenes/barcos';
        $fileName = "PRINCIPAL".'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $imagen_barco = new ImagenBarco();
        $imagen_barco->barco_id= $id_barco;
        $imagen_barco->nombre=$file;
        $imagen_barco ->nombre_archivo = $fileName;  
        $imagen_barco ->principal = 1;  


        $imagen_barco->save();

        //añadimos la imagen principal a barco

        $barco = Barco::where('id', $id_barco)->first();

        $barco->imagen_principal= $fileName;

        $barco->save();

        //añadimos la imagen principal al bono creado

        $bono= Bono::where('barco_id', $id_barco)->first();

        $bono->imagen_principal= $fileName;

        $bono->save();
            
        // Log::info($id_barco);

    }

    public function upload_imagenes( Request $request)
    {

        $file = $request->file('file');
        $id_barco = $request->id_barco;
        $path = public_path() . '/imagenes/barcos';
        $fileName = uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $imagen_barco = new ImagenBarco();
        $imagen_barco->barco_id= $id_barco;
        $imagen_barco->nombre=$file;
        $imagen_barco ->nombre_archivo = $fileName;  
        $imagen_barco ->principal = 0;  
        


        $imagen_barco->save();
            
        // Log::info('imagenes');

    }
    public function update(Request $request, $id)
    {


        Log::info('entra en update');
        $input = $request->all();
        // Log::info($input['imagen_principal']);
     
        // Log::info('entra albaran entrada create');
        // Log::info($request);
        $validator = Validator::make(
            $request->all(),
          
            $this->reglasValidacion(),
            $this->mensajesValidacion(),
        );
        if ($validator->fails()) {
            // Log::info($request);
         
            return response()->json(['errors' =>  $validator->errors()->all()]);
        } else {
            // Log::info($request);
            $this->editar($request, $id);

           
            $ultimo_barco= $request->session()->get('barco_metido');

            // Log::info($ultimo_barco);

            return response()->json(['success' => 'Barco creado correctamente','ultimo_barco'=>$ultimo_barco]);
        }
        
      
        
    }
    public function editar($requestDatos, $id)
    {

        // Log::info($requestDatos);
        Log::info('entra save barco');
        //Albaran
        $barco = Barco::where('id',$id)->first();
        $bono = Bono::where('barco_id',$id)->first();



        $barco->nombre = $requestDatos['nombre'];
        $barco->tipo = $requestDatos['tipo_barco'];
        $barco->titulo = $requestDatos['titulo'];
        $barco->descripcion = $requestDatos['descripcion'];
        $barco->numero_personas = $requestDatos['numero_personas'];
        $barco->ciudad = $requestDatos['ciudad'];
        $barco->puerto = $requestDatos['puerto'];
        $barco->eslora = $requestDatos['eslora'];
        $barco->manga = $requestDatos['manga'];
        $barco->calado = $requestDatos['calado'];
        $barco->motor = $requestDatos['motor'];
        $barco->desplazamiento = $requestDatos['desplazamiento'];
        $barco->velocidad_maxima = $requestDatos['velocidad_maxima'];
        $barco->velocidad_crucero = $requestDatos['velocidad_crucero'];
        $barco->camarotes = $requestDatos['camarotes'];
        $barco->baños = $requestDatos['baños'];
        $barco->capacidad_agua_dulce = $requestDatos['capacidad_agua_dulce'];
        $barco->capacidad_combustible = $requestDatos['capacidad_combustible'];   
        $barco->save();

        $bono->precio = $requestDatos['precio_bono'];  
        $bono->precio_findesemana = $requestDatos['precio_bono_findesemana'];   
        $bono->codigo = $requestDatos['codigo_bono'];   


        $bono->save();

        
        // $ultimo_id = Barco::latest('id')->first();

        // $ultimo_id = $ultimo_id->id;

        // $requestDatos->session()->put('barco_metido', $ultimo_id);


    }


    public function edit($id)
    {

        
        // Log::info('id barco');
        // Log::info($id);

        $barco = Barco::where('id', $id)->first();
        $imagenes_barco = ImagenBarco::where('barco_id', $id)->get();

        $bonos = Bono::where('id',$barco->id)->first();
        $precio_entresemana = $bonos->precio;
        $precio_findesemana= $bonos->precio_findesemana;
        $codigo_bono= $bonos->codigo;



  

        return view('barcos/edit-barco', [
          
            'id'=> $barco->id,
            'titulo'=>$barco->titulo,
            'nombre'=> $barco->nombre,
            'imagen_principal'=>$barco->imagen_principal,
            'imagenes_barco'=>$imagenes_barco,
            'descripcion'=>$barco->descripcion,
            'eslora'=>$barco->eslora,
            'manga'=>$barco->manga,
            'calado'=>$barco->calado,
            'motor'=>$barco->motor,
            'desplazamiento'=>$barco->desplazamiento,
            'velocidad_maxima'=>$barco->velocidad_maxima,
            'velocidad_crucero'=>$barco->velocidad_crucero,
            'camarotes'=>$barco->camarotes,
            'baños'=>$barco->baños,
            'ciudad'=>$barco->ciudad,
            'puerto'=>$barco->puerto,
            'capacidad_agua_dulce'=>$barco->capacidad_agua_dulce,
            'capacidad_combustible'=>$barco->capacidad_combustible,
            'numero_personas'=>$barco->numero_personas,
            'precio_entresemana'=>$precio_entresemana,
            'precio_findesemana'=>$precio_findesemana,
            'codigo_bono'=>$codigo_bono,





        ]);

    }

    public function edit_imagen( Request $request)
    {
        Log::info('entra en editar imagen principal');
        $id_barco=$request->id_barco;
        $barco = Barco::where('id',$id_barco)->first();

        $imagen_principal = $barco->imagen_principal;

        Log::info( $imagen_principal );

        $image_path = public_path()."/imagenes/barcos/{$imagen_principal}";
       
        if($imagen_principal != "")
        {
            unlink($image_path);
        }
        

        Log::info($imagen_principal);

        $file = $request->file('file');
        $id_barco = $request->id_barco;
        $path = public_path() . '/imagenes/barcos';
        $fileName = "PRINCIPAL".'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);
        if($imagen_principal != "")
        {
            $imagen_barco = ImagenBarco::where('barco_id',$id_barco)->first();
        }
        else
        {
            $imagen_barco = new ImagenBarco();
        }
       
        $imagen_barco->barco_id= $id_barco;
        $imagen_barco->nombre=$file;
        $imagen_barco ->nombre_archivo = $fileName;  
        $imagen_barco ->principal = 1;  
        $imagen_barco->save();

        $barco = Barco::where('id', $id_barco)->first();

        $barco->imagen_principal= $fileName;

        $barco->save();
            
        Log::info($id_barco);

    }

    public function edit_imagenes(Request $request)
    {  
        
        //  $n_imagenes = $request->numero_imagenes;

        // Log::info($n_imagenes);
        $file = $request->file('file');
        $id_barco = $request->id_barco;
        $path = public_path() . '/imagenes/barcos';
        $fileName = uniqid() . $file->getClientOriginalName();


    
        $file->move($path, $fileName);

        $imagen_barco = new ImagenBarco();
        $imagen_barco->barco_id= $id_barco;
        $imagen_barco->nombre=$file;
        $imagen_barco ->nombre_archivo = $fileName;  
        $imagen_barco ->principal = 0;  
        


        $imagen_barco->save();
            
    }

  
    public function eliminar_imagenes($id){
        Log::info('eliminar imagenes');
       

        $imagen = ImagenBarco::where('id',$id)->first();

        $nombre_imagen = $imagen->nombre_archivo;
        $barco_id = $imagen->barco_id;
    

       $pos  = strrpos($nombre_imagen, 'PRINCIPAL');


        $image_path = public_path()."/imagenes/barcos/{$nombre_imagen}";
        unlink($image_path);

        ImagenBarco::where('id',$id)->delete();
        Log::info('POS');
        log::info($pos);

        if($pos===false)
        {
         
        }
        else{

            $barco =Barco::where('id',$barco_id)->first();
            // Log::info($barco);
            $barco->imagen_principal = null;
            $barco->save();
            // Log::info($barco);   

        }
       
        

        return back();

    }
 

    function reglasValidacion()
    {
        return [
            //Reglas
            //Datos básicos
            // 'imagen_principal' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'nombre' => 'required|string',
            'numero_personas' => 'nullable',
            'titulo' => 'required|string',
            'tipo_barco' => 'required|string',
            'precio_bono' => 'required|numeric',
            'precio_bono_findesemana' => 'required|numeric',

            'codigo_bono' => 'required|numeric',
            'descripcion' => 'required|string',
        ];
    }

    function mensajesValidacion()
    {
        return [
            //Mensajes de error
            //Datos básicos
            // 'imagen_pricipal.required' => 'Imagen obligatoria',
            'nombre.required' => 'El Nombre es obligatorio',
            'codigo_bono.required' => 'El Codigo de bono es obligatorio',
            'precio_bono.required' => 'El Precio de bono es obligatorio',
            'precio_bono_findesemana.required' => 'El Precio de bono es obligatorio',

            'titulo.required' => 'El  Titulo es obligatorio',
            'tipo_barco.required' => 'El  Tipo de barco es obligatorio',
            'descripcion.required' => 'El  Descripcion es obligatorio',
         
        ];
    }


}
