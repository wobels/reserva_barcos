<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Barco;
use App\Models\ImagenBarco;
use App\Models\Bono;
use App\Http\Requests\BarcoRequest;

class BonosController extends Controller
{
    public function index()
    {
        $bonos=Bono::all();

        // $nombre_barco = $bonos->barco->nombre;

        return view('bonos.bonos',[

            'bonos'=>$bonos,

        ]);
    }
}
