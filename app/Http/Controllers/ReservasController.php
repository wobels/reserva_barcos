<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Reserva;
use App\Models\User;
use App\Models\Bono;
use App\Models\Barco;

use App\Models\DiaFestivo;

use App\Models\BonoExtra;
use App\Models\DescontarBono;
use Mail;


use Auth;


use App\DataTables\ReservasDataTable;

use App\Models\ImagenIncidencia;



class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ReservasDataTable $dataTable, $id)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas = Reserva::where('user_id',$id)->whereBetween('start', [$hoy, '2100-12-12'])->orderBy('start', 'ASC')->get();

        Log::info($reservas);
    
        
        if($reservas==[])
        {
            $reservas=="";
        }
        
       return $dataTable->render('/reservas/detalles_reservas',[
                'reservas' => $reservas,
                'error'=>"",
                
       ]);

        // return view('/reservas/detalles_reservas',[
        //     'reservas' => $reservas,
        //     'error'=>"",
        //     'dataTable'=> $dataTable->render('reservas/detalles_reservas'),
        // ]);
      
    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo 'hola';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

 
    // Guardar reservas
    public function store(Request $request)
    {
        Log::info('ENNNNNTRA EN STORE');
        $datosEvento=request()->except(['_token','_method']);
        $barco_id =$datosEvento['barco_id'];
        $title =$datosEvento['title'];
        $user_id =$datosEvento['user_id'];

        $usuario_datos = User::where('id',$user_id)->first();
        $user_name= $usuario_datos->name;
        Log::info('barco_id');
        
        Log::info($barco_id);
        $start =$datosEvento['start'];
        $end =$datosEvento['end'];
        $mañana =$datosEvento['mañana'];
        $tarde =$datosEvento['tarde'];
        $ultimo_dia = $datosEvento['ultimo_dia'];
        $tipo_bono = $datosEvento['tipo_bono'];
        $url_imag_url = $datosEvento['image_url'];
        // Log::info('url_imagen_reserva');
        // Log::info($url_imag_url);

        




        $dia_completo = false;
        $ningun_dia= false;
        $reserva_atras = false;
        $reserva_findesemana = false;
        $reserva_entresemana = false;
        $reserva_dias_corridos = false;
        $hora_mañana = false;
        $hora_tarde=false;
        $bono_finde = false;
        $bono_entre = false;

        $fecha_cliente = $datosEvento['fecha_actual_control'];
        $hoy = date("Y-m-d");
        $hoy_hora = getdate();

        //COMPROBAR QUE LA FECHA CUADRA TANTO EL CLIENTE COMO EN EL SERVIDOR
        Log::info('FECHA ACTUAL SERVIDOR = '.$hoy);
        Log::info('FECHA ACTUAL CLIENTE = '.$fecha_cliente);
        $comprobacion_fecha_cliente_servidor = false;
        if($hoy==$fecha_cliente)
        {
            $comprobacion_fecha_cliente_servidor = false;

        }
        else
        {
            $comprobacion_fecha_cliente_servidor = true;

        }

        //buscar barco nombre y puerto 

        $barco_mail = Barco::where('id',$barco_id)->first();

        $barco_mail_nombre = $barco_mail->nombre;
        $barco_mail_puerto = $barco_mail->puerto;
        $barco_mail_ciudad = $barco_mail->ciudad;


        //reservas existentes en este barco el mismo dia que se pidela reserva
        $reservas = Reserva::where('barco_id',$barco_id)->where('start', $start)->get();
        
        $count = 0;
        foreach($reservas as $res)
        {
            $count++;
            $res_mañana = $res->mañana;
            $res_tarde = $res->tarde;


        }

        //reservas existentes del usuario para el tema de que no puede tener mas de dos reservas simultaneas
        $reservas_usuarios = Reserva::where('user_id',$user_id)->whereBetween('start', [$hoy, '2100-12-12'])->get();

        $count_res_usuario = 0;
        foreach($reservas_usuarios as $res_usuario)
        {
            
            $count_res_usuario++;
            $usuario_tarde = $res_usuario->tarde;
            $usuario_mañana = $res_usuario->mañana;

        
            if($usuario_tarde==1 && $usuario_mañana==1)
            {
                
                $count_res_usuario++;

                

            }

        }

        
       


        //comprobar bonos usuario

        $usuarios = User::where('id',$user_id)->get();

        foreach($usuarios as $user)
        {
            $bonos_entresemana = $user->numero_bonos_entresemana;
            $bonos_findesemana = $user->numero_bonos_findesemana;
         
        }
        //comprobar si esta a 0 los bonos o existen 1 solo
        //finde
        if($bonos_findesemana==0)
        {
            $bono_finde = true;
        }
        elseif($bonos_findesemana==1)
        {
            if($tarde==1 && $mañana==0 || $tarde==0 && $mañana==1)
            {
                 $bono_finde = false;

            }
            if($tarde==1 && $mañana==1 || $tarde==0 && $mañana==0)
            {
                 $bono_finde = true;

            }
        }
        elseif($bonos_findesemana>=2)
        {
            $bono_finde = false;

        }
        
        //entre

        if($bonos_entresemana==0)
        {
            $bono_entre = true;
        }
        elseif($bonos_entresemana==1)
        {
            if($tarde==1 && $mañana==0 || $tarde==0 && $mañana==1)
            {
                 $bono_entre = false;

            }
            if($tarde==1 && $mañana==1 || $tarde==0 && $mañana==0)
            {
                 $bono_entre = true;

            }
        }
        elseif($bonos_entresemana>=2)
        {
            $bono_entre = false;

        }
    

  
       //Sacar todo lo relacionado con los dias de fecha actual, fecha de reserva etc.....

        //FECHA RESERVA
        date_default_timezone_set('Europe/Madrid');

        $fecha_start = substr($start, 0, 10);
        $numeroDia = date('d', strtotime($fecha_start));
        $dia = date('l', strtotime($fecha_start));
        $mes = date('F', strtotime($fecha_start));
        $anio = date('Y', strtotime($fecha_start));

        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $nombremes = str_replace($mes_EN, $mes_ES, $mes);
        

        //FECHA ACTUAL
        $hoy = substr($hoy, 0, 10);
        $numeroDia_actual = date('d', strtotime($hoy));
        $dia_actual = date('l', strtotime($hoy));
        $mes_actual = date('F', strtotime($hoy));
        $anio_actual = date('Y', strtotime($hoy));

        
        $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);

       


        //comprobar que en que trimestre Se hace la reserva
      
       $trimestre="";
        switch ($nombremes) {
            case "Enero":
                $trimestre="Primero";
                break;
            case "Febrero":
                $trimestre="Primero";
                break;
            case "Marzo":
                $trimestre="Primero";
                break;
            case "Abril":
                $trimestre="Segundo";
                break;
            case "Mayo":
                $trimestre="Segundo";
                break;
            case "Junio":
                $trimestre="Segundo";
                break; 
            case "Julio":
                $trimestre="Tercero";
                break; 
            case "Agosto":
                $trimestre="Tercero";
                break; 
            case "Septiembre":
                $trimestre="Tercero";
                break; 
            case "Octubre":
                $trimestre="Cuarto";
                break;
            case "Noviembre":
                $trimestre="Cuarto";
                break;
            case "Diciembre":
                $trimestre="Cuarto";
                break;
                      
        }

        //comprobar en que trimestre estamos actualmente

        $trimestre_actual="";
        switch ($nombremes_actual) {
            case "Enero":
                $trimestre_actual="Primero";
                break;
            case "Febrero":
                $trimestre_actual="Primero";
                break;
            case "Marzo":
                $trimestre_actual="Primero";
                break;
            case "Abril":
                $trimestre_actual="Segundo";
                break;
            case "Mayo":
                $trimestre_actual="Segundo";
                break;
            case "Junio":
                $trimestre_actual="Segundo";
                break; 
            case "Julio":
                $trimestre_actual="Tercero";
                break; 
            case "Agosto":
                $trimestre_actual="Tercero";
                break; 
            case "Septiembre":
                $trimestre_actual="Tercero";
                break; 
            case "Octubre":
                $trimestre_actual="Cuarto";
                break;
            case "Noviembre":
                $trimestre_actual="Cuarto";
                break;
            case "Diciembre":
                $trimestre_actual="Cuarto";
                break;
                      
        }

        $diferente_trimestre = false;

        // Log::info('TRIMESTRE ACTUAL');
        // Log::info($trimestre_actual);
        // Log::info('TRIMESTRE RESERVA ');
        // Log::info($trimestre);

        if($trimestre_actual == $trimestre)
        {
          $diferente_trimestre = false;
        }
        elseif($trimestre_actual != $trimestre)
        {
            $diferente_trimestre = true;

        }
       
      


     


        //Saber a que hora se realiza la reserva
       
      
        if($fecha_start ==$hoy)
        {
            
            if($mañana==1 && $tarde==0)
            {
                if($hoy_hora['hours']+1>=14)
                {
                    $hora_mañana = true;

                }

                
            }
            elseif($mañana==0 && $tarde==1)
            {
                if($hoy_hora['hours']+1>=21)
                {
                    $hora_tarde = true;

                }
            }
            elseif($mañana==1 && $tarde==1)
            {
                if($hoy_hora['hours']+1>=21)
                {
                    $hora_tarde = true;

                }
            }
            
        }

        


        //Saber si es finde semana o no

        
        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
        {
           

            if($nombredia=="Viernes" && $tarde==1 && $mañana==0)
            {
                  $reserva_findesemana=true;
                 $reserva_entresemana=false;
            }
            elseif($nombredia=="Viernes" && $tarde==0 && $mañana==1)
            {
                $reserva_findesemana=false;
                $reserva_entresemana=true;
            }
            elseif($nombredia=="Viernes" && $tarde==1 && $mañana==1)
            {
                $reserva_findesemana=false;
                $reserva_entresemana=false;
            }
            elseif($nombredia=="Sábado" || $nombredia=="Domingo" )
            {
                $reserva_findesemana=true;
                 $reserva_entresemana=false;
            }
                
              
             
 


            
        }
        else{
            $reserva_findesemana=false;
            $reserva_entresemana=true;
        }

        //calcular dias festivos

        $dias_festivos = DiaFestivo::all();
        $dias_festivos_count = 0;

        foreach($dias_festivos as $dia_festivo)
        {
            $dia_festivo_sin_horas = substr($dia_festivo->start, 0, 10);

            // Log::info('dia festivo');
            // Log::info($dia_festivo_sin_horas);
            
            if($fecha_start == $dia_festivo_sin_horas)
            {
                // Log::info('concuerda');
                $dias_festivos_count++;
            }
            
        }

        if($dias_festivos_count>0)
        {
            $reserva_findesemana=true;
            $reserva_entresemana=false;
        }

        //90 dias corridos 


        $dStart = new \DateTime($hoy);
        $dEnd  = new \DateTime($start);
        $dias_corridos = $dStart->diff($dEnd)->format('%r%a');
   

        if($dias_corridos>"90")
        {
            
         $reserva_dias_corridos = true;


        }


      

        //comparar lña fecha de hoy con la que se va hacer de reserva para comprobar que no se hace una reserva de un dia anterior al de hoy

        if($start<$hoy)
        {

            $reserva_atras = true;
    
        }
        
        //comprobar si se ha marcado dia completo en los inputs para hacer todas las funcions despues

        if($tarde==1 && $mañana==1)
        {
            $count_res_usuario++;

            $dia_completo = true;
        }
         //comprobar si no se ha chequeado ningun input en la reserva

        if($tarde==0 && $mañana==0)
        {

            $ningun_dia = true;
        }

        // Log::info('ultimo dia');
        // Log::info($ultimo_dia);
        $bool_ultimo_dia=false;

        // $url_imagen_tarde_mañana = "http://localhost/reserva_barcos/public/imagenes/reservas/mañana-tarde.png";
        $url_imagen_tarde_mañana = "https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana-tarde.png";


        //toda la programacion pedida por el cliente
        if($comprobacion_fecha_cliente_servidor==false)
        {
            // if($hora_tarde==false && $hora_mañana==false)
            // {
               
                if($reserva_dias_corridos==false)
                {
                    if($reserva_atras==false)
                    {
                        if($ningun_dia==false)
                        {
                            if($count <2)
                            {
                                if($count==0)
                                {
                                    // $reserva_findesemana=true;
                                    // $reserva_entresemana=false;
                                    if($count_res_usuario<2)
                                    {
                                       
                                        if($reserva_findesemana==true && $reserva_entresemana==false && $ultimo_dia=="0")
                                        {
                                            if($diferente_trimestre==false)
                                            {
                                                if($bono_finde==false && $diferente_trimestre==false)
                                                {
    
                                                
                                                    Log::info('No hay reservas ni de tarde ni de mañana');
                                                    $bonos = User::where('id',$user_id)->first();
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $resta_bono = $bonos->numero_bonos_findesemana-2;
                                        
                                                    }
                                                    else
                                                    {
                                                        $resta_bono = $bonos->numero_bonos_findesemana-1;
                                        
                                                    }
                                        
                                                    $bonos->numero_bonos_findesemana = $resta_bono;
                                                    $bonos->save();
    
                                                    
    
    
    
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl =     $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
    
                                                    if($tarde==0 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4M";
    
                                                    }
                                                    elseif($tarde==1 && $mañana==0)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                    }
                                                    elseif($tarde==1 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4MT";
    
                                                    }
    
    
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
                                                    }
    
                                                }
                                                else
                                                {
                                                    
                                                    return response()->json(['success' => 'No tienes bonos',]);
                                        
                                                
                                                }
                                            }
    
                                            elseif($diferente_trimestre==true)
                                            {
                                                $nueva_reserva =new  Reserva();
                                                $nueva_reserva->title= $title;
                                                $nueva_reserva->barco_id = $barco_id;
                                                $nueva_reserva->user_id = $user_id;
                                                $nueva_reserva->user_name = $user_name;
                                                $nueva_reserva->start = $start;
                                                $nueva_reserva->end = $end;
                                                $nueva_reserva->mañana = $mañana;
                                                $nueva_reserva->tarde = $tarde;
                                                $nueva_reserva->imageUrl = $url_imag_url;
                                                if($dias_festivos_count>0)
                                                {
                                                    $nueva_reserva->dia_festivo = 1;
                                                }
                                                elseif($dias_festivos_count==0)
                                                {
                                                    $nueva_reserva->dia_festivo = 0;
    
                                                }
    
                                                if($tarde==0 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4M";
    
                                                }
                                                elseif($tarde==1 && $mañana==0)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                }
                                                elseif($tarde==1 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4MT";
    
                                                }
    
            
                                                $nueva_reserva->save();
    
                                                $ultima_reserva_para_bonos = Reserva::all()->last();
                                                $ultimo_bono_extra = BonoExtra::all()->last();
                                                if($ultimo_bono_extra!="")
                                                {   
                                                    if($ultimo_bono_extra->reserva_id =="")
                                                    {
                                                        $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                        $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                        $ultimo_bono_extra->save();
    
                                                        $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                    }
                                                }
    
                                                $ultima_reserva= Reserva::all()->last();
                                                $id_ultima_reserva = $ultima_reserva->id;
    
                                                // Log::info('ULTIMA RESERVA');
                                                // Log::info($id_ultima_reserva);
    
                                         
                                                $bonos_descontar = new DescontarBono();
                                                if($tarde==1 && $mañana==1)
                                                {
                                                    $resta_bono = $bonos_descontar->cantidad_findesemana+2;
                                       
            
                                    
                                                }
                                                else
                                                {
                                                    $resta_bono = $bonos_descontar->cantidad_findesemana+1;
                                    
                                                }
                                               
                                                $bonos_descontar->barco_id = $barco_id;
                                                $bonos_descontar->user_id = $user_id;
                                                $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                $bonos_descontar->trimestre = $trimestre;
                                                $bonos_descontar->cantidad_findesemana= $resta_bono;
                                            
            
                                                $bonos_descontar->save();
            
                                                // //envio de correo al cliente
                                                $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                // $name="david";
                                                // $msg = "todo el mensaje";
                                                $subject = "Nueva reserva realizada";
                                                $for = Auth::user()->email;
                                                Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                    $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                    $msj->subject($subject);
                                                    $msj->to($for);
                                                });

                                                   //  //envio de correoA
                                                   $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                    // $name="david";
                                                    // $msg = "todo el mensaje";
                                                    $subject = "Nueva reserva realizada";
                                                    $for = "marnificwebcontrol@gmail.com";
                                                    Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                        $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                        $msj->subject($subject);
                                                        $msj->to($for);
                                                    });

                                                
                                                return response()->json(['success' => 'Reserva ok diferente trimestre',]);
    
            
                                            }
                                            
                                            
                                            // log::info($resta_bono);
                                        }
                                        else if($reserva_findesemana==false && $reserva_entresemana==true && $ultimo_dia=="0")
                                        {
                                            if($diferente_trimestre==false)
                                            {
    
                                                if($bono_entre==false)
                                                {
                                                    Log::info('No hay reservas ni de tarde ni de mañana');
                                                    $bonos = User::where('id',$user_id)->first();
                                                    // $resta_bono = $bonos->numero_bonos_entresemana-1;
                                        
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $resta_bono = $bonos->numero_bonos_entresemana-2;
                                        
                                                    }
                                                    else
                                                    {
                                                        $resta_bono = $bonos->numero_bonos_entresemana-1;
                                        
                                                    }
                                                    
                                                    $bonos->numero_bonos_entresemana = $resta_bono;
                                                    $bonos->save();
    
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl =$url_imag_url;
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
    
                                                    if($tarde==0 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4M";
    
                                                    }
                                                    elseif($tarde==1 && $mañana==0)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                    }
                                                    elseif($tarde==1 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4MT";
    
                                                    }
    
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
                                                }
                                                else
                                                {
                                                    return response()->json(['success' => 'No tienes bonos',]);
    
                                                }
                                            }
    
                                            elseif($diferente_trimestre==true)
                                            {
                                                $nueva_reserva =new  Reserva();
                                                $nueva_reserva->title= $title;
                                                $nueva_reserva->barco_id = $barco_id;
                                                $nueva_reserva->user_id = $user_id;
                                                $nueva_reserva->user_name = $user_name;
                                                $nueva_reserva->start = $start;
                                                $nueva_reserva->end = $end;
                                                $nueva_reserva->mañana = $mañana;
                                                $nueva_reserva->tarde = $tarde;
                                                $nueva_reserva->imageUrl = $url_imag_url;
                                                if($dias_festivos_count>0)
                                                {
                                                    $nueva_reserva->dia_festivo = 1;
                                                }
                                                elseif($dias_festivos_count==0)
                                                {
                                                    $nueva_reserva->dia_festivo = 0;
    
                                                }
    
                                                if($tarde==0 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4M";
    
                                                }
                                                elseif($tarde==1 && $mañana==0)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                }
                                                elseif($tarde==1 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4MT";
    
                                                }
    
    
                                                $nueva_reserva->save();
    
                                                $ultima_reserva_para_bonos = Reserva::all()->last();
                                                $ultimo_bono_extra = BonoExtra::all()->last();
                                                if($ultimo_bono_extra!="")
                                                {   
                                                    if($ultimo_bono_extra->reserva_id =="")
                                                    {
                                                        $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                        $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                        $ultimo_bono_extra->save();
    
                                                        $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                    }
    
                                                    
                                                }
                                            
    
                                                
                                              
                                                $ultima_reserva= Reserva::all()->last();
                                                $id_ultima_reserva = $ultima_reserva->id;
    
                                                Log::info('DIFERENTE TRIMESTRE');
                                                $bonos_descontar = new DescontarBono();
                                                if($tarde==1 && $mañana==1)
                                                {
                                                    $resta_bono = $bonos_descontar->cantidad_entresemana+2;
                                       
            
                                    
                                                }
                                                else
                                                {
                                                    $resta_bono = $bonos_descontar->cantidad_entresemana+1;
                                    
                                                }
                                               
                                                $bonos_descontar->barco_id = $barco_id;
                                                $bonos_descontar->user_id = $user_id;
                                                $bonos_descontar->trimestre = $trimestre;
                                                $bonos_descontar->cantidad_entresemana= $resta_bono;
                                                $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                            
            
                                                $bonos_descontar->save();
                                                // //envio de correo al cliente
                                                $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                // $name="david";
                                                // $msg = "todo el mensaje";
                                                $subject = "Nueva reserva realizada";
                                                $for = Auth::user()->email;
                                                Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                    $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                    $msj->subject($subject);
                                                    $msj->to($for);
                                                });
                                               
                                                     //  //envio de correoA
                                                     $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                     // $name="david";
                                                     // $msg = "todo el mensaje";
                                                     $subject = "Nueva reserva realizada";
                                                     $for = "marnificwebcontrol@gmail.com";
                                                     Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                         $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                         $msj->subject($subject);
                                                         $msj->to($for);
                                                     });

                                                     
                                                return response()->json(['success' => 'Reserva ok diferente trimestre',]);
    
            
                                            }
                                            
                                            
                                            // log::info($resta_bono);
                                        }
                                        else if($reserva_findesemana==false && $reserva_entresemana==false && $ultimo_dia=="0")
                                        {
                                            if($diferente_trimestre==false)
                                            {
                                                if($bonos_entresemana!=0 && $bonos_findesemana!=0)
                                                {
                                                    
                                                    $bonos = User::where('id',$user_id)->first();
    
                                                    $resta_bono_entresemana = $bonos->numero_bonos_entresemana-1;
                                                    $resta_bono_findesemana = $bonos->numero_bonos_findesemana-1;
                                                    
                                                    $bonos->numero_bonos_entresemana = $resta_bono_entresemana;
                                                    $bonos->numero_bonos_findesemana = $resta_bono_findesemana;
    
                                                    $bonos->save();
    
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl =  $url_imag_url;  
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
    
                                                    if($tarde==0 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4M";
    
                                                    }
                                                    elseif($tarde==1 && $mañana==0)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                    }
                                                    elseif($tarde==1 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia ="4MT";
    
                                                    }
    
                                                
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
    
                                                    }
                                                   
    
                                                }
                                                else
                                                {
                                                    return response()->json(['success' => 'No tienes bonos',]);
    
                                                }
                                            }
                                            elseif($diferente_trimestre==true)
                                            {
                                                $nueva_reserva =new  Reserva();
                                                $nueva_reserva->title= $title;
                                                $nueva_reserva->barco_id = $barco_id;
                                                $nueva_reserva->user_id = $user_id;
                                                $nueva_reserva->user_name = $user_name;
                                                $nueva_reserva->start = $start;
                                                $nueva_reserva->end = $end;
                                                $nueva_reserva->mañana = $mañana;
                                                $nueva_reserva->tarde = $tarde;
                                                $nueva_reserva->imageUrl = $url_imag_url;
                                                if($dias_festivos_count>0)
                                                {
                                                    $nueva_reserva->dia_festivo = 1;
                                                }
                                                elseif($dias_festivos_count==0)
                                                {
                                                    $nueva_reserva->dia_festivo = 0;
    
                                                }
                                       
                                                if($tarde==0 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4M";
    
                                                }
                                                elseif($tarde==1 && $mañana==0)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                }
                                                elseif($tarde==1 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia ="4MT";
    
                                                }
    
    
                                                $nueva_reserva->save();
    
                                                $ultima_reserva_para_bonos = Reserva::all()->last();
                                                $ultimo_bono_extra = BonoExtra::all()->last();
                                                if($ultimo_bono_extra!="")
                                                {   
                                                    if($ultimo_bono_extra->reserva_id =="")
                                                    {
                                                        $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                        $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                        $ultimo_bono_extra->save();
    
                                                        $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                    }
    
                                                    
                                                }
                                            
    
                                                $ultima_reserva= Reserva::all()->last();
                                                $id_ultima_reserva = $ultima_reserva->id;
    
                                                Log::info('DIFERENTE TRIMESTRE');
                                                $bonos_descontar = new DescontarBono();
                                                
                                              
                                                    $resta_bono_finde = $bonos_descontar->cantidad_entresemana+1;
                                                    $resta_bono_entre = $bonos_descontar->cantidad_findesemana+1;
    
                                       
            
                                    
                                               
                                               
                                                $bonos_descontar->barco_id = $barco_id;
                                                $bonos_descontar->user_id = $user_id;
                                                $bonos_descontar->trimestre = $trimestre;
                                                $bonos_descontar->cantidad_entresemana= $resta_bono_entre;
                                                $bonos_descontar->cantidad_findesemana= $resta_bono_finde;
                                                $bonos_descontar->reserva_id = $id_ultima_reserva;
    
    
                                            
            
                                                $bonos_descontar->save();
            
                                             
                                                //   //envio de correo al cliente
                                                  $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                  // $name="david";
                                                  // $msg = "todo el mensaje";
                                                  $subject = "Nueva reserva realizada";
                                                  $for = Auth::user()->email;
                                                  Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                      $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                      $msj->subject($subject);
                                                      $msj->to($for);
                                                  });
                                                       //  //envio de correoA
                                                       $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                       // $name="david";
                                                       // $msg = "todo el mensaje";
                                                       $subject = "Nueva reserva realizada";
                                                       $for = "marnificwebcontrol@gmail.com";
                                                       Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                           $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                           $msj->subject($subject);
                                                           $msj->to($for);
                                                       });
   
                                                return response()->json(['success' => 'Reserva ok diferente trimestre',]);
    
            
                                            }
    
                                                
                                        }
    
                                    
                                    }
                                    else{
                                        $bool_ultimo_dia=true;
                                    }
    
                                   
                            
                                    if($ultimo_dia=="1")
                                    {
                                        $bool_ultimo_dia=false;
                                        if($tipo_bono=="1") //entre
                                        {
                                            if($diferente_trimestre==false)
                                            {
                                                if($bonos_entresemana>0)
                                                {
                                                    $bonos = User::where('id',$user_id)->first();
    
                                                    if($tarde==1 && $mañana==1 && $bonos_entresemana>1 )
                                                    {
                                                        $resta_bono_entresemana = $bonos->numero_bonos_entresemana-2;
                                                        $bonos->numero_bonos_entresemana = $resta_bono_entresemana;
                                                
            
                                                        $bonos->save();
                                                    }
                                                    elseif($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                    {
                                                        $resta_bono_entresemana = $bonos->numero_bonos_entresemana-1;
                                                        $bonos->numero_bonos_entresemana = $resta_bono_entresemana;
                                                
            
                                                        $bonos->save();
    
                                                    }
                                                    else{
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
    
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl =  $url_imag_url;
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    
                                                    // añaadir el ultimo dia y su tipo
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $nueva_reserva->cantidad_ultimo_dia=2;
                                                    }
                                                    else
                                                    {
                                                        $nueva_reserva->cantidad_ultimo_dia=1;
                                                    }
    
                                                    if($tarde==0 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
    
    
                                                    }
                                                    elseif($tarde==1 && $mañana==0)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
    
    
                                                    }
                                                    elseif($tarde==1 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'MT';
    
    
                                                    }
    
    
                                                    $nueva_reserva->ultimo_dia=1;
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
    
                                                }
                                                else{
                                                    return response()->json(['success' => 'No tienes bonos',]);
    
                                                }
                                            }
                                            elseif($diferente_trimestre==true)
                                            {
                                                $nueva_reserva =new  Reserva();
                                                $nueva_reserva->title= $title;
                                                $nueva_reserva->barco_id = $barco_id;
                                                $nueva_reserva->user_id = $user_id;
                                                $nueva_reserva->user_name = $user_name;
                                                $nueva_reserva->start = $start;
                                                $nueva_reserva->end = $end;
                                                $nueva_reserva->mañana = $mañana;
                                                $nueva_reserva->tarde = $tarde;
                                                $nueva_reserva->imageUrl = $url_imag_url;
    
                                                if($dias_festivos_count>0)
                                                {
                                                    $nueva_reserva->dia_festivo = 1;
                                                }
                                                elseif($dias_festivos_count==0)
                                                {
                                                    $nueva_reserva->dia_festivo = 0;
    
                                                }
    
                                                if($tarde==0 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
    
    
                                                }
                                                elseif($tarde==1 && $mañana==0)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
    
    
                                                }
                                                elseif($tarde==1 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'MT';
    
    
                                                }
                                                $nueva_reserva->ultimo_dia=1;
    
                                                $nueva_reserva->save();
    
                                                $ultima_reserva_para_bonos = Reserva::all()->last();
                                                $ultimo_bono_extra = BonoExtra::all()->last();
                                                if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
    
    
                                                $ultima_reserva= Reserva::all()->last();
                                                $id_ultima_reserva = $ultima_reserva->id;
    
                                                Log::info('DIFERENTE TRIMESTRE');
                                                $bonos_descontar = new DescontarBono();
                                                
                                              
                                                if($tarde==1 && $mañana==1)
                                                {
                                                    $resta_bono_entre = $bonos_descontar->cantidad_entresemana+2;
                                                    
                                                }
                                                elseif($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                {
                                                    $resta_bono_entre= $bonos_descontar->cantidad_entresemana+1;
    
                                                }
    
                                            
                                                $bonos_descontar->barco_id = $barco_id;
                                                $bonos_descontar->user_id = $user_id;
                                                $bonos_descontar->trimestre = $trimestre;
                                                $bonos_descontar->cantidad_entresemana= $resta_bono_entre;
                                                $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                
    
                                            
            
                                                $bonos_descontar->save();
            
                                               
                                                //   //envio de correo al cliente
                                                  $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                  // $name="david";
                                                  // $msg = "todo el mensaje";
                                                  $subject = "Nueva reserva realizada";
                                                  $for = Auth::user()->email;
                                                  Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                      $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                      $msj->subject($subject);
                                                      $msj->to($for);
                                                  });
                                                       //  //envio de correoA
                                                       $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                       // $name="david";
                                                       // $msg = "todo el mensaje";
                                                       $subject = "Nueva reserva realizada";
                                                       $for = "marnificwebcontrol@gmail.com";
                                                       Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                           $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                           $msj->subject($subject);
                                                           $msj->to($for);
                                                       });
   
                                                return response()->json(['success' => 'Reserva ok diferente trimestre',]);
                                                
                                            }
                                        }
                                        elseif($tipo_bono=="2") // finde
                                        {
                                            if($diferente_trimestre==false)
                                            {
                                                if($bonos_findesemana>0)
                                                {
                                                    
                                                    $bonos = User::where('id',$user_id)->first();
                                                    if($tarde==1 && $mañana==1 && $bonos_findesemana>1 )
                                                    {
    
                                                        $resta_bono_findesemana = $bonos->numero_bonos_findesemana-2;
                                                        $bonos->numero_bonos_findesemana = $resta_bono_findesemana;
                                                
            
                                                        $bonos->save();
                                                
                                                    }
                                                    elseif($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                    {
                                                            $resta_bono_findesemana = $bonos->numero_bonos_findesemana-1;
                                                            $bonos->numero_bonos_findesemana = $resta_bono_findesemana;
                                                
            
                                                            $bonos->save();
    
                                                    }
                                                    else{
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
    
                                                    
    
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
    
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $nueva_reserva->cantidad_ultimo_dia=2;
                                                    }
                                                    else
                                                    {
                                                        $nueva_reserva->cantidad_ultimo_dia=1;
                                                    }
    
                                                    if($tarde==0 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
        
        
                                                    }
                                                    elseif($tarde==1 && $mañana==0)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
        
        
                                                    }
                                                    elseif($tarde==1 && $mañana==1)
                                                    {
                                                        $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'MT';
        
        
                                                    }
                                                    $nueva_reserva->ultimo_dia=1;
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
                                                }
                                                else{
                                                    return response()->json(['success' => 'No tienes bonos',]);
    
                                                }
                                            }
                                            elseif($diferente_trimestre==true)
                                            {
                                                $nueva_reserva =new  Reserva();
                                                $nueva_reserva->title= $title;
                                                $nueva_reserva->barco_id = $barco_id;
                                                $nueva_reserva->user_id = $user_id;
                                                $nueva_reserva->user_name = $user_name;
                                                $nueva_reserva->start = $start;
                                                $nueva_reserva->end = $end;
                                                $nueva_reserva->mañana = $mañana;
                                                $nueva_reserva->tarde = $tarde;
                                                $nueva_reserva->imageUrl = $url_imag_url;
                                                if($dias_festivos_count>0)
                                                {
                                                    $nueva_reserva->dia_festivo = 1;
                                                }
                                                elseif($dias_festivos_count==0)
                                                {
                                                    $nueva_reserva->dia_festivo = 0;
    
                                                }
                                                if($tarde==0 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
    
    
                                                }
                                                elseif($tarde==1 && $mañana==0)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
    
    
                                                }
                                                elseif($tarde==1 && $mañana==1)
                                                {
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'MT';
    
    
                                                }
                                                $nueva_reserva->ultimo_dia=1;
                                                
                                                $nueva_reserva->save();
    
                                                $ultima_reserva_para_bonos = Reserva::all()->last();
                                                $ultimo_bono_extra = BonoExtra::all()->last();
                                                if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
    
                                                $ultima_reserva= Reserva::all()->last();
                                                $id_ultima_reserva = $ultima_reserva->id;
    
                                                Log::info('DIFERENTE TRIMESTRE');
                                                $bonos_descontar = new DescontarBono();
                                                
                                              
                                                if($tarde==1 && $mañana==1)
                                                {
                                                    $resta_bono_finde = $bonos_descontar->cantidad_findesemana+2;
                                                    
                                                }
                                                elseif($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                {
                                                    $resta_bono_finde= $bonos_descontar->cantidad_findesemana+1;
    
                                                }
    
                                            
                                                $bonos_descontar->barco_id = $barco_id;
                                                $bonos_descontar->user_id = $user_id;
                                                $bonos_descontar->trimestre = $trimestre;
                                                $bonos_descontar->cantidad_findesemana= $resta_bono_finde;
                                                $bonos_descontar->reserva_id = $id_ultima_reserva;
                                                
    
                                                
    
                                            
            
                                                $bonos_descontar->save();
            
                                            //    //envio de correo al cliente
                                               $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                               // $name="david";
                                               // $msg = "todo el mensaje";
                                               $subject = "Nueva reserva realizada";
                                               $for = Auth::user()->email;
                                               Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                   $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                   $msj->subject($subject);
                                                   $msj->to($for);
                                               });

                                                    //  //envio de correoA
                                                    $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                    // $name="david";
                                                    // $msg = "todo el mensaje";
                                                    $subject = "Nueva reserva realizada";
                                                    $for = "marnificwebcontrol@gmail.com";
                                                    Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                        $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                        $msj->subject($subject);
                                                        $msj->to($for);
                                                    });

                                                
                                                return response()->json(['success' => 'Reserva ok diferente trimestre',]);
                                                
                                            }
    
                                        }
                                        
                                        Log::info($tipo_bono);
                                    }
                    
                                     Log::info($bool_ultimo_dia);
    
                                    if($bool_ultimo_dia==false)
                                    {
                                        Log::info('entraa aqyui y no deberia');
                                        
                                        // //envio de correo al cliente
                                        $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                        // $name="david";
                                        // $msg = "todo el mensaje";
                                        $subject = "Nueva reserva realizada";
                                        $for = Auth::user()->email;
                                        Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                            $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                            $msj->subject($subject);
                                            $msj->to($for);
                                        });

                                             //  //envio de correoA
                                             $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                             // $name="david";
                                             // $msg = "todo el mensaje";
                                             $subject = "Nueva reserva realizada";
                                             $for = "marnificwebcontrol@gmail.com";
                                             Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                 $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                 $msj->subject($subject);
                                                 $msj->to($for);
                                             });

    
    
                                        return response()->json(['success' => 'Reserva realizada correctamente',]);
                                    }
                                    elseif($bool_ultimo_dia==true){
                                        return response()->json(['success' => 'simultaneo',]);
    
                                    }
                
                                    
                                        
                                }
                                else if($count==1 && $dia_completo==false)
                                {
                                
                                    Log::info('entra');
                                    
                                    if($res_mañana==0 && $mañana==1)
                                    {
                                    
                                        if($count_res_usuario<2)
                                        {
                                           
                                            if($reserva_findesemana==true && $reserva_entresemana==false && $ultimo_dia=="0")
                                            {
                                                if($diferente_trimestre==false)
                                                {
                                                    if($bono_finde==false)
                                                    {
                                                        Log::info('No hay reservas ni de tarde ni de mañana');
                                                        $bonos = User::where('id',$user_id)->first();
                                                        if($tarde==1 && $mañana==1)
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_findesemana-2;
                                            
                                                        }
                                                        else
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_findesemana-1;
                                            
                                                        }
                                            
                                                        $bonos->numero_bonos_findesemana = $resta_bono;
                                                        $bonos->save();
    
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =  $url_imag_url;
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
                                                            $nueva_reserva->tipo_ultimo_dia ="4M";
    
    
                                                            $nueva_reserva->save();
                                                            
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                                                                log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
    
                                                            
                                                            $reservas_comprobacion->tipo_ultimo_dia ="4MT";
    
    
                                                            $reservas_comprobacion->save();
    
                                                             $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                
                                                            
    
    
    
    
                                                        }
                                                    }
                                                    
                                                    
                                                    // log::info($resta_bono);
                                
                                                    else
                                                    {
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    $nueva_reserva->tipo_ultimo_dia ="4M";
    
                
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
    
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_findesemana+2;
                                        
                
                                        
                                                    }
                                                    else
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_findesemana+1;
                                        
                                                    }
                                               
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_findesemana= $resta_bono;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                    
                                                    //   //envio de correo al cliente
                                                      $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                      // $name="david";
                                                      // $msg = "todo el mensaje";
                                                      $subject = "Nueva reserva realizada";
                                                      $for = Auth::user()->email;
                                                      Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                          $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                          $msj->subject($subject);
                                                          $msj->to($for);
                                                      });

                                                           //  //envio de correoA
                                                            $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                            // $name="david";
                                                            // $msg = "todo el mensaje";
                                                            $subject = "Nueva reserva realizada";
                                                            $for = "marnificwebcontrol@gmail.com";
                                                            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                                $msj->subject($subject);
                                                                $msj->to($for);
                                                            });


                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
    
            
                                                }
                                            }
    
                                            else if($reserva_findesemana==false && $reserva_entresemana==true && $ultimo_dia=="0")
                                            {
                                                if($diferente_trimestre==false)
                                                {
                                                    if($bono_entre==false)
                                                    {
    
                                                        Log::info('No hay reservas ni de tarde ni de mañana');
                                                        $bonos = User::where('id',$user_id)->first();
                                                        // $resta_bono = $bonos->numero_bonos_entresemana-1;
                                            
                                                        if($tarde==1 && $mañana==1)
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_entresemana-2;
                                            
                                                        }
                                                        else
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_entresemana-1;
                                            
                                                        }
                                                        
                                                        $bonos->numero_bonos_entresemana = $resta_bono;
                                                        $bonos->save();
    
    
                                                        
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =  $url_imag_url;
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
                                                            
                                                            $nueva_reserva->tipo_ultimo_dia ="4M";
    
    
                                                            $nueva_reserva->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                                                                log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
    
                                                            $reservas_comprobacion->tipo_ultimo_dia ="4MT";
    
    
                                                            $reservas_comprobacion->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
    
    
                                                        }
                                                        
                                                        // log::info($resta_bono);
                                                    }
                                                    else
                                                    {
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    $nueva_reserva->tipo_ultimo_dia ="4M";
    
                
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_entresemana+2;
                                        
                
                                        
                                                    }
                                                    else
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_entresemana+1;
                                        
                                                    }
                                               
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_entresemana= $resta_bono;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                    //  //envio de correo al cliente
                                                     $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                     // $name="david";
                                                     // $msg = "todo el mensaje";
                                                     $subject = "Nueva reserva realizada";
                                                     $for = Auth::user()->email;
                                                     Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                         $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                         $msj->subject($subject);
                                                         $msj->to($for);
                                                     });

                                                          //  //envio de correoA
                                                        $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                        // $name="david";
                                                        // $msg = "todo el mensaje";
                                                        $subject = "Nueva reserva realizada";
                                                        $for = "marnificwebcontrol@gmail.com";
                                                        Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                            $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                            $msj->subject($subject);
                                                            $msj->to($for);
                                                        });

                                                    
                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
    
            
                                                }
                                            }
                                        }
                                        else{
                                            $bool_ultimo_dia=true;
                                        }
    
                                        if($ultimo_dia=="1")
                                        {
                                            $bool_ultimo_dia=false;
    
                                            if($tipo_bono=="1") //entre
                                            {
                                                if($diferente_trimestre==false)
                                                {
                                                    if($bonos_entresemana>0)
                                                    {
                                                        $bonos = User::where('id',$user_id)->first();
    
                                                        if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                        {
                                                            $resta_bono_entresemana = $bonos->numero_bonos_entresemana-1;
    
                                                        }
    
                                                    
                                                    
                                                        $bonos->numero_bonos_entresemana = $resta_bono_entresemana;
                                                    
                
                                                        $bonos->save();
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =  $url_imag_url;
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
                                                            // añaadir el ultimo dia y su tipo
                                                            
                                                            $nueva_reserva->cantidad_ultimo_dia=1;
                                                            $nueva_reserva->ultimo_dia=1;
                                                            
                                                          
                                                            $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
            
            
                                                            
    
                                                            $nueva_reserva->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                                                            log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
                                                               
                                                            $reservas_comprobacion->cantidad_ultimo_dia=2;
                                                            $reservas_comprobacion->ultimo_dia=1;
                                                   
    
                                                            $tipo_anyadir= $reservas_comprobacion->tipo_ultimo_dia;
                                                            $reservas_comprobacion->tipo_ultimo_dia= $tipo_bono.'M'.','.$tipo_anyadir;
    
                                                            $reservas_comprobacion->save();
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                          
    
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                                    $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
    
    
    
                                                        }
                                                        
    
                                                    }
                                                    else{
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    $nueva_reserva->ultimo_dia=1;
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
    
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
    
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    
                                                    if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                    {
                                                        $resta_bono_entre = $bonos_descontar->cantidad_entresemana+1;
    
                                                    }
    
                                                
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_entresemana= $resta_bono_entre;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                    
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                    
                                                
                                                    //   //envio de correo al cliente
                                                      $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                      // $name="david";
                                                      // $msg = "todo el mensaje";
                                                      $subject = "Nueva reserva realizada";
                                                      $for = Auth::user()->email;
                                                      Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                          $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                          $msj->subject($subject);
                                                          $msj->to($for);
                                                      });
                                                           //  //envio de correoA
                                                            $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                            // $name="david";
                                                            // $msg = "todo el mensaje";
                                                            $subject = "Nueva reserva realizada";
                                                            $for = "marnificwebcontrol@gmail.com";
                                                            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                                $msj->subject($subject);
                                                                $msj->to($for);
                                                            });

                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
                                                    
                                                }
                                            }
                                            elseif($tipo_bono=="2") // finde
                                            {
                                                if($diferente_trimestre==false)
                                                {
                                                    if($bonos_findesemana>0)
                                                    {
                                                        $bonos = User::where('id',$user_id)->first();
                                                        if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                        {
                                                                $resta_bono_findesemana = $bonos->numero_bonos_findesemana-1;
    
                                                        }
    
                                                        $bonos->numero_bonos_findesemana = $resta_bono_findesemana;
                                                    
                
                                                        $bonos->save();
    
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =  $url_image_url;
    
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
                                                            $nueva_reserva->cantidad_ultimo_dia=1;
                                                            $nueva_reserva->ultimo_dia=1;
                                                            $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
    
                                                            $nueva_reserva->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"."Mañana Tarde";
                                                            log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl = $url_imagen_tarde_mañana;
    
                                                            $reservas_comprobacion->cantidad_ultimo_dia=2;
                                                            $reservas_comprobacion->ultimo_dia=1;
                                                            $tipo_anyadir= $reservas_comprobacion->tipo_ultimo_dia;
                                                            $reservas_comprobacion->tipo_ultimo_dia= $tipo_bono.'M'.','.$tipo_anyadir;
    
                                                            $reservas_comprobacion->save();
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
    
    
    
                                                        }
                                                    
                                                    }
                                                    else{
                                                        return response()->json(['success' => 'No tienes bonos',]);
            
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                
                                                    $nueva_reserva->ultimo_dia=1;
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'M';
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    
                                                    if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                    {
                                                        $resta_bono_finde = $bonos_descontar->cantidad_findesemana+1;
    
                                                    }
    
                                                
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_findesemana= $resta_bono_finde;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                    
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                  
                                                    //   //envio de correo al cliente
                                                      $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                      // $name="david";
                                                      // $msg = "todo el mensaje";
                                                      $subject = "Nueva reserva realizada";
                                                      $for = Auth::user()->email;
                                                      Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                          $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                          $msj->subject($subject);
                                                          $msj->to($for);
                                                      });
                                                           //  //envio de correoA
                                                            $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                            // $name="david";
                                                            // $msg = "todo el mensaje";
                                                            $subject = "Nueva reserva realizada";
                                                            $for = "marnificwebcontrol@gmail.com";
                                                            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                                $msj->subject($subject);
                                                                $msj->to($for);
                                                            });

                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
                                                    
                                                }
    
                                            }
                                            
                                        }
                
                                        if( $bool_ultimo_dia==false)
                                        {
                                            //   //envio de correo al cliente
                                              $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                            // $name="david";
                                            // $msg = "todo el mensaje";
                                            $subject = "Nueva reserva realizada";
                                            $for = Auth::user()->email;
                                            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                $msj->subject($subject);
                                                $msj->to($for);
                                            });

                                                 //  //envio de correoA
                                                 $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                 // $name="david";
                                                 // $msg = "todo el mensaje";
                                                 $subject = "Nueva reserva realizada";
                                                 $for = "marnificwebcontrol@gmail.com";
                                                 Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                     $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                     $msj->subject($subject);
                                                     $msj->to($for);
                                                 });


                                            return response()->json(['success' => 'Reserva realizada correctamente',]);
                                        }
                                        elseif( $bool_ultimo_dia==true){
                                            return response()->json(['success' => 'simultaneo',]);
        
                                        }
                                    }
    
                                    else if($res_mañana==1 && $mañana==1)
                                    {
                                        return response()->json(['success' => 'Ocupado por la mañana',]);
    
                                    }
    
                                    if($res_tarde==0 && $tarde==1)
                                    {
                                        
                                        if($count_res_usuario<2)
                                        {
                                                
                                            if($reserva_findesemana==true && $reserva_entresemana==false && $ultimo_dia=="0")
                                            {
                                                if($diferente_trimestre==false)
                                                {
                                                
                                                    if($bono_finde==false)
                                                    {
                                                        
                                                    
    
                                                        Log::info('No hay reservas ni de tarde ni de mañana');
                                                        $bonos = User::where('id',$user_id)->first();
    
                                                        if($tarde==1 && $mañana==1)
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_findesemana-2;
                                            
                                                        }
                                                        else
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_findesemana-1;
                                            
                                                        }
                                            
                                                        $bonos->numero_bonos_findesemana = $resta_bono;
                                                        $bonos->save();
    
                                                        
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =  $url_imag_url;
    
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
                                                            $nueva_reserva->tipo_ultimo_dia ="4T";
    
    
                                                            $nueva_reserva->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                                                                log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
                                                            $reservas_comprobacion->tipo_ultimo_dia ="4MT";
    
    
                                                            $reservas_comprobacion->save();
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
    
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
    
    
                                                        }
    
                                                    
                                                        log::info($resta_bono);
                                                    }
                                                    else
                                                    {
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
    
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_findesemana+2;
                                        
                
                                        
                                                    }
                                                    else
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_findesemana+1;
                                        
                                                    }
                                               
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_findesemana= $resta_bono;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                        //   //envio de correo al cliente
                                                          $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                            // $name="david";
                                                            // $msg = "todo el mensaje";
                                                            $subject = "Nueva reserva realizada";
                                                            $for = Auth::user()->email;
                                                            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                                $msj->subject($subject);
                                                                $msj->to($for);
                                                            });

                                                                 //  //envio de correoA
                                                   $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                   // $name="david";
                                                   // $msg = "todo el mensaje";
                                                   $subject = "Nueva reserva realizada";
                                                   $for = "marnificwebcontrol@gmail.com";
                                                   Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                       $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                       $msj->subject($subject);
                                                       $msj->to($for);
                                                   });

                                                    
                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
    
            
                                                }
                                            }
                                            else if($reserva_findesemana==false && $reserva_entresemana==true && $ultimo_dia=="0")
                                            {
                                                
                                                if($diferente_trimestre==false)
                                                {
                                                    if($bono_entre==false)
                                                    {
                                                    
                                                        Log::info('No hay reservas ni de tarde ni de mañana');
                                                        $bonos = User::where('id',$user_id)->first();
                                                        // $resta_bono = $bonos->numero_bonos_entresemana-1;
                                            
                                                        if($tarde==1 && $mañana==1)
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_entresemana-2;
                                            
                                                        }
                                                        else
                                                        {
                                                            $resta_bono = $bonos->numero_bonos_entresemana-1;
                                            
                                                        }
                                                        
                                                        $bonos->numero_bonos_entresemana = $resta_bono;
                                                        $bonos->save();
    
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =$url_imag_url;
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
                                                            $nueva_reserva->tipo_ultimo_dia ="4T";
    
    
                                                            $nueva_reserva->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                                                                log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
                                                            $reservas_comprobacion->tipo_ultimo_dia ="4MT";
    
    
                                                            $reservas_comprobacion->save();
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
    
    
                                                        }
                                                        
                                                        // log::info($resta_bono);
                                                    }
                                                    else
                                                    {
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    $nueva_reserva->tipo_ultimo_dia ="4T";
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
    
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    if($tarde==1 && $mañana==1)
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_entresemana+2;
                                        
                
                                        
                                                    }
                                                    else
                                                    {
                                                        $resta_bono = $bonos_descontar->cantidad_entresemana+1;
                                        
                                                    }
                                               
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_entresemana= $resta_bono;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                   
                                                    //   //envio de correo al cliente
                                                      $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                      // $name="david";
                                                      // $msg = "todo el mensaje";
                                                      $subject = "Nueva reserva realizada";
                                                      $for = Auth::user()->email;
                                                      Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                          $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                          $msj->subject($subject);
                                                          $msj->to($for);
                                                      });

                                                           //  //envio de correoA
                                                   $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                   // $name="david";
                                                   // $msg = "todo el mensaje";
                                                   $subject = "Nueva reserva realizada";
                                                   $for = "marnificwebcontrol@gmail.com";
                                                   Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                       $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                       $msj->subject($subject);
                                                       $msj->to($for);
                                                   });

                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
    
            
                                                }
                                                
                                            }
                                        }
                                        else{
                                            $bool_ultimo_dia=true;
                                        }
                                     
    
                                        if($ultimo_dia=="1")
                                        {
                                            $bool_ultimo_dia=false;
    
                                            if($tipo_bono=="1") //entre
                                            {
                                                if($diferente_trimestre==false)
                                                {
                                                    if($bonos_entresemana>0)
                                                    {
                                                        $bonos = User::where('id',$user_id)->first();
    
                                                        if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                        {
                                                            $resta_bono_entresemana = $bonos->numero_bonos_entresemana-1;
    
                                                        }
    
                                                    
                                                    
                                                        $bonos->numero_bonos_entresemana = $resta_bono_entresemana;
                                                    
                
                                                        $bonos->save();
    
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =  $url_imag_url;
    
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
    
                                                            $nueva_reserva->cantidad_ultimo_dia=1;
                                                            $nueva_reserva->ultimo_dia=1;
                                                            $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
    
    
                                                            $nueva_reserva->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                                                            log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
    
                                                            $reservas_comprobacion->cantidad_ultimo_dia=2;
                                                            $reservas_comprobacion->ultimo_dia=1;
                                                            $tipo_anyadir= $reservas_comprobacion->tipo_ultimo_dia;
                                                            $reservas_comprobacion->tipo_ultimo_dia= $tipo_bono.'T'.','.$tipo_anyadir;
    
    
                                                            $reservas_comprobacion->save();
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
    
    
                                                        }
                                                    
    
                                                    }
                                                    else{
                                                        return response()->json(['success' => 'No tienes bonos',]);
    
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    $nueva_reserva->ultimo_dia=1;
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
                
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    
                                                    if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                    {
                                                        $resta_bono_entre = $bonos_descontar->cantidad_entresemana+1;
    
                                                    }
    
                                                
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_entresemana= $resta_bono_entre;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                    
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                    //   //envio de correo al cliente
                                                      $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                      // $name="david";
                                                      // $msg = "todo el mensaje";
                                                      $subject = "Nueva reserva realizada";
                                                      $for = Auth::user()->email;
                                                      Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                          $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                          $msj->subject($subject);
                                                          $msj->to($for);
                                                      });

                                                           //  //envio de correoA
                                                   $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                   // $name="david";
                                                   // $msg = "todo el mensaje";
                                                   $subject = "Nueva reserva realizada";
                                                   $for = "marnificwebcontrol@gmail.com";
                                                   Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                       $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                       $msj->subject($subject);
                                                       $msj->to($for);
                                                   });

                                                    
                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
                                                    
                                                }
                                            }
                                            elseif($tipo_bono=="2") // finde
                                            {
                                                if($diferente_trimestre==false)
                                                {
                                                    if($bonos_findesemana>0)
                                                    {
                                                        $bonos = User::where('id',$user_id)->first();
                                                        if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                        {
                                                                $resta_bono_findesemana = $bonos->numero_bonos_findesemana-1;
    
                                                        }
    
                                                        $bonos->numero_bonos_findesemana = $resta_bono_findesemana;
                                                    
                
                                                        $bonos->save();
    
                                                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();
    
                                                        Log::info($reservas_comprobacion);
    
                                                        if($reservas_comprobacion==[])
                                                        {
                                                            $nueva_reserva =new  Reserva();
                                                            $nueva_reserva->title= $title;
                                                            $nueva_reserva->barco_id = $barco_id;
                                                            $nueva_reserva->user_id = $user_id;
                                                            $nueva_reserva->user_name = $user_name;
                                                            $nueva_reserva->start = $start;
                                                            $nueva_reserva->end = $end;
                                                            $nueva_reserva->mañana = $mañana;
                                                            $nueva_reserva->tarde = $tarde;
                                                            $nueva_reserva->imageUrl =  $url_imag_url;
                                                            if($dias_festivos_count>0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 1;
                                                            }
                                                            elseif($dias_festivos_count==0)
                                                            {
                                                                $nueva_reserva->dia_festivo = 0;
            
                                                            }
    
                                                            $nueva_reserva->cantidad_ultimo_dia=1;
                                                            $nueva_reserva->ultimo_dia=1;
                                                            $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
    
                                                            $nueva_reserva->save();
    
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
                                                            
    
                                                        }
                                                        else{
                                                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                                                            $porciones = explode(":", $titulo_reservas_comprobacion);
                                                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                                                                log::info($titulo_reservas_comprobacion);
    
                                                            $reservas_comprobacion->title=$titulo_final;
                                                            $reservas_comprobacion->tarde=1;
                                                            $reservas_comprobacion->mañana=1;
                                                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
    
                                                            $reservas_comprobacion->cantidad_ultimo_dia=2;
                                                            $reservas_comprobacion->ultimo_dia=1;
                                                            $tipo_anyadir= $reservas_comprobacion->tipo_ultimo_dia;
                                                            $reservas_comprobacion->tipo_ultimo_dia= $tipo_bono.'T'.','.$tipo_anyadir;
    
                                                            $reservas_comprobacion->save();
                                                            $ultima_reserva_para_bonos = Reserva::all()->last();
                                                            $ultimo_bono_extra = BonoExtra::all()->last();
                                                            if($ultimo_bono_extra!="")
                                                            {   
                                                                if($ultimo_bono_extra->reserva_id =="")
                                                                {
                                                                    $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                                    $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                                    $ultimo_bono_extra->save();
                                                                    $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                                }
                
                                                                
                                                            }
                                                        
    
    
    
                                                        }
                                                        
                                                    }
                                                    else{
                                                        return response()->json(['success' => 'No tienes bonos',]);
            
                                                    }
                                                }
                                                elseif($diferente_trimestre==true)
                                                {
                                                    $nueva_reserva =new  Reserva();
                                                    $nueva_reserva->title= $title;
                                                    $nueva_reserva->barco_id = $barco_id;
                                                    $nueva_reserva->user_id = $user_id;
                                                    $nueva_reserva->user_name = $user_name;
                                                    $nueva_reserva->start = $start;
                                                    $nueva_reserva->end = $end;
                                                    $nueva_reserva->mañana = $mañana;
                                                    $nueva_reserva->tarde = $tarde;
                                                    $nueva_reserva->imageUrl = $url_imag_url;
    
                                                    if($dias_festivos_count>0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 1;
                                                    }
                                                    elseif($dias_festivos_count==0)
                                                    {
                                                        $nueva_reserva->dia_festivo = 0;
    
                                                    }
                                                    
                                                    $nueva_reserva->ultimo_dia=1;
                                                    $nueva_reserva->tipo_ultimo_dia= $tipo_bono.'T';
    
                                                    $nueva_reserva->save();
    
                                                    $ultima_reserva_para_bonos = Reserva::all()->last();
                                                    $ultimo_bono_extra = BonoExtra::all()->last();
                                                    if($ultimo_bono_extra!="")
                                                    {   
                                                        if($ultimo_bono_extra->reserva_id =="")
                                                        {
                                                            $id_ultima_reserva_para_bonos =   $ultima_reserva_para_bonos->id;
                                                            $ultimo_bono_extra->reserva_id = $id_ultima_reserva_para_bonos;
                                                            $ultimo_bono_extra->save();
                                                            $ultima_reserva_para_bonos->reserva_bono_extra = 1;
                                                            $ultima_reserva_para_bonos->save();
                                                        }
        
                                                        
                                                    }
                                                
    
                                                    $ultima_reserva= Reserva::all()->last();
                                                    $id_ultima_reserva = $ultima_reserva->id;
    
                                                    Log::info('DIFERENTE TRIMESTRE');
                                                    $bonos_descontar = new DescontarBono();
                                                    
                                                    if($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                                                    {
                                                        $resta_bono_finde = $bonos_descontar->cantidad_findesemana+1;
    
                                                    }
    
                                                
                                                    $bonos_descontar->barco_id = $barco_id;
                                                    $bonos_descontar->user_id = $user_id;
                                                    $bonos_descontar->trimestre = $trimestre;
                                                    $bonos_descontar->cantidad_findesemana= $resta_bono_finde;
                                                    $bonos_descontar->reserva_id = $id_ultima_reserva;
    
                                                    
    
                                                
                
                                                    $bonos_descontar->save();
                
                                                    //  //envio de correo al cliente
                                                     $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                     // $name="david";
                                                     // $msg = "todo el mensaje";
                                                     $subject = "Nueva reserva realizada";
                                                     $for = Auth::user()->email;
                                                     Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                         $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                         $msj->subject($subject);
                                                         $msj->to($for);
                                                     });

                                                          //  //envio de correoA
                                                   $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                   // $name="david";
                                                   // $msg = "todo el mensaje";
                                                   $subject = "Nueva reserva realizada";
                                                   $for = "marnificwebcontrol@gmail.com";
                                                   Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                       $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                       $msj->subject($subject);
                                                       $msj->to($for);
                                                   });

                                                    return response()->json(['success' => 'Reserva ok diferente trimestre',]);
                                                    
                                                }
    
    
                                            }
                                            
                                        }
                
                                        if( $bool_ultimo_dia==false)
                                        {
                                            //   //envio de correo al cliente
                                              $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                            // $name="david";
                                            // $msg = "todo el mensaje";
                                            $subject = "Nueva reserva realizada";
                                            $for = Auth::user()->email;
                                            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                $msj->subject($subject);
                                                $msj->to($for);
                                            });

                                                 //  //envio de correoA
                                                 $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                                                 // $name="david";
                                                 // $msg = "todo el mensaje";
                                                 $subject = "Nueva reserva realizada";
                                                 $for = "marnificwebcontrol@gmail.com";
                                                 Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                                                     $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                                     $msj->subject($subject);
                                                     $msj->to($for);
                                                 });

                                            return response()->json(['success' => 'Reserva realizada correctamente',]);
                                        }
                                        elseif( $bool_ultimo_dia==true){
                                            return response()->json(['success' => 'simultaneo',]);
        
                                        }
                                    }
    
                                    else if($res_tarde==1 && $tarde==1)
                                    {
                                        return response()->json(['success' => 'Ocupado por la tarde',]);
    
                                    }
    
                                }
    
                                else{
                                    return response()->json(['success' => 'dia completo',]);
    
                
                                }
    
                            }
    
                            else if($count>=2)
                            {
                                // Log::info('reservas completas');
                                // echo'errors:Dia completo';
                
                                return response()->json(['success' => 'encontrado',]);
    
                
                        
                                
                
                            }
                    
                        }
    
                        else if($tarde==0 && $mañana==0){
                            return response()->json(['success' => 'no check',]);
    
                
                        }
                        
                
                    
                    }
    
                    else if($reserva_atras==true){
                        return response()->json(['success' => 'reserva atras',]);
    
    
                    }
                }
    
                else if($reserva_dias_corridos==true)
                {
                    return response()->json(['success' => 'dias corridos',]);
    
    
                }
                
            
            
    
    
            // }
            // else{
            //     return response()->json(['success' => 'error hora',]);
            // }  

        }
        elseif($comprobacion_fecha_cliente_servidor==true)
        {
            return response()->json(['success' => 'fechas diferentes',]);
            
        }
      



        


        // Reserva::insert($datosEvento);
    }
    public function store_bono_ultima_hora(Request $request)
    {
        Log::info('entra en store bono ultima hora');
        $hoy = getdate();

        $datosEvento=request()->except(['_token','_method']);
        $barco_id =$datosEvento['bonos_barco_id'];
        $start =$datosEvento['fecha_add_bono'];
        $end =$datosEvento['fecha_add_bono'];
        $mañana =$datosEvento['bono_mañana'];
        $tarde =$datosEvento['bono_tarde'];
        $ultimo_dia = $datosEvento['ultimo_dia_add_bono'];
        $tipo_bono = $datosEvento['tipo_bono_add_bono'];

        $url_imagen_tarde_mañana = "https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana-tarde.png";
        $url_imag_url = $datosEvento['ruta_imagen'];

        Log::info('URL DEL FORM= '.$url_imag_url);

        //datos usuario

        $user_id =Auth::user()->id;
        $user_name =Auth::user()->name;
        $numero_bonos_findesemana = Auth::user()->numero_bonos_findesemana;
        $numero_bonos_entresemana = Auth::user()->numero_bonos_entresemana;
        $numero_bonos_ultima_hora = Auth::user()->numero_bonos_ultima_hora;

        //datos Barco

        $barco=Barco::where('id',$barco_id)->first();

        $barco_mail_puerto = $barco->puerto;
        $barco_mail_ciudad = $barco->ciudad;
        $barco_mail_nombre = $barco->nombre;



        $bonos = User::where('id',Auth::user()->id)->first();

        //reservas existentes en este barco el mismo dia que se pidela reserva
        $reservas = Reserva::where('barco_id',$barco_id)->where('start', $start)->get();

        $count = 0;
        $res_tarde = null;
        $res_mañana = null;
        foreach($reservas as $res)
        {
            $count++;
            $res_mañana = $res->mañana;
            $res_tarde = $res->tarde;
        }
    

        //Comprobar mes actual para comprobar trimestre mas abajo
        date_default_timezone_set('Europe/Madrid');

        $fecha_start = substr($start, 0, 10);
        $numeroDia = date('d', strtotime($fecha_start));
        $dia = date('l', strtotime($fecha_start));
        $mes = date('F', strtotime($fecha_start));
        $anio = date('Y', strtotime($fecha_start));

        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $nombremes = str_replace($mes_EN, $mes_ES, $mes);
        

        //FECHA ACTUAL
        $hoy = substr($start, 0, 10);
        $numeroDia_actual = date('d', strtotime($hoy));
        $dia_actual = date('l', strtotime($hoy));
        $mes_actual = date('F', strtotime($hoy));
        $anio_actual = date('Y', strtotime($hoy));

        
        $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);

        
 
 
         //comprobar que en que trimestre Se hace la reserva
       
        $trimestre="";
        switch ($nombremes) {
            case "Enero":
                $trimestre="Primero";
                break;
            case "Febrero":
                $trimestre="Primero";
                break;
            case "Marzo":
                $trimestre="Primero";
                break;
            case "Abril":
                $trimestre="Segundo";
                break;
            case "Mayo":
                $trimestre="Segundo";
                break;
            case "Junio":
                $trimestre="Segundo";
                break; 
            case "Julio":
                $trimestre="Tercero";
                break; 
            case "Agosto":
                $trimestre="Tercero";
                break; 
            case "Septiembre":
                $trimestre="Tercero";
                break; 
            case "Octubre":
                $trimestre="Cuarto";
                break;
            case "Noviembre":
                $trimestre="Cuarto";
                break;
            case "Diciembre":
                $trimestre="Cuarto";
                break;
                    
        }

         //comprobar en que trimestre estamos actualmente
 
        $trimestre_actual="";
        switch ($nombremes_actual) {
            case "Enero":
                $trimestre_actual="Primero";
                break;
            case "Febrero":
                $trimestre_actual="Primero";
                break;
            case "Marzo":
                $trimestre_actual="Primero";
                break;
            case "Abril":
                $trimestre_actual="Segundo";
                break;
            case "Mayo":
                $trimestre_actual="Segundo";
                break;
            case "Junio":
                $trimestre_actual="Segundo";
                break; 
            case "Julio":
                $trimestre_actual="Tercero";
                break; 
            case "Agosto":
                $trimestre_actual="Tercero";
                break; 
            case "Septiembre":
                $trimestre_actual="Tercero";
                break; 
            case "Octubre":
                $trimestre_actual="Cuarto";
                break;
            case "Noviembre":
                $trimestre_actual="Cuarto";
                break;
            case "Diciembre":
                $trimestre_actual="Cuarto";
                break;
                    
        }
 
        $diferente_trimestre = false;

        Log::info('TRIMESTRE ACTUAL');
        Log::info($trimestre_actual);
        Log::info('TRIMESTRE RESERVA ');
        Log::info($trimestre);

        if($trimestre_actual == $trimestre)
        {
        $diferente_trimestre = false;
        }
        elseif($trimestre_actual != $trimestre)
        {
            $diferente_trimestre = true;

        }

        // Toda la programacion
    

        if($diferente_trimestre==false)
        {
            $envio_email=false;
            //reserva de dos franjas
            if($tarde==1 && $mañana==1 && $count==0)
            {
                    if($numero_bonos_findesemana>=1 || $numero_bonos_entresemana>=1)
                    {
                        if($numero_bonos_findesemana<=0 || $numero_bonos_entresemana>=1)
                        {
                            return response()->json(['success' => 'Consume bono entresemana',]);

                        }
                        elseif($numero_bonos_findesemana>=1 || $numero_bonos_entresemana<=0)
                        {
                            return response()->json(['success' => 'Consume bono findesemana',]);

                        }


                    }
                    elseif($numero_bonos_findesemana>=2 && $numero_bonos_entresemana<=1)
                    {
                        Log::info('hay bonos de finde');
                        return response()->json(['success' => 'Hay bonos de findesemana',]);

                    }
                    elseif($numero_bonos_findesemana<=1 && $numero_bonos_entresemana>=2)
                    {
                        Log::info('hay bonos de entre');
                        return response()->json(['success' => 'Hay bonos de entresemana',]);
                    }
                    elseif($numero_bonos_findesemana<=0 && $numero_bonos_entresemana<=0)
                    {
                        
                        
                        if($numero_bonos_ultima_hora>=2)
                        {
                            Log::info('consumir bonos ultima hora dos franjas');
                            
                            $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();

                            Log::info($reservas_comprobacion);

                            if($reservas_comprobacion==[])
                            {
                                Log::info('Consumir bono ultima hora una franja NUEVA RESERVA');
                                $bonos->numero_bonos_ultima_hora = $bonos->numero_bonos_ultima_hora-2;
                                $bonos->save();
                        
                                $nueva_reserva =new  Reserva();
                            
                                $title="Ocupado: Mañana Tarde";
                                
                                
                                $nueva_reserva->title= $title;
                                $nueva_reserva->barco_id = $barco_id;
                                $nueva_reserva->user_id = $user_id;
                                $nueva_reserva->user_name = $user_name;
                                $nueva_reserva->start = $start;
                                $nueva_reserva->end = $end;
                                $nueva_reserva->mañana = $mañana;
                                $nueva_reserva->tarde = $tarde;
                                $nueva_reserva->imageUrl =  $url_imag_url;

                                $nueva_reserva->ultimo_dia =1;
                                $nueva_reserva->bono_ultimo_dia =1;
                                $nueva_reserva->tipo_ultimo_dia ="4U";


                                $nueva_reserva->save();
                                $envio_email=true;

                                

                            }
                        
                        }
                        elseif($numero_bonos_ultima_hora<=1)
                        {
                            return response()->json(['success' => 'No hay bonos suficientes de ultima hora',]);

                            
                        }
                            
                    }
                

            
            }
                //reserva de solo una franja
            elseif($tarde==1 && $mañana==0 || $tarde==0 && $mañana==1 && $count < 2)
            {
                if($numero_bonos_findesemana>=1 && $numero_bonos_entresemana<=0)
                {
                    // Log::info('hay bonos de finde');
                    return response()->json(['success' => 'Hay bonos de findesemana',]);

                }
                elseif($numero_bonos_findesemana<=0 && $numero_bonos_entresemana>=1)
                {
                    // Log::info('hay bonos de entre');
                    return response()->json(['success' => 'Hay bonos de entresemana',]);

                }
                elseif($numero_bonos_findesemana<=0 && $numero_bonos_entresemana<=0)
                {

                    if($numero_bonos_ultima_hora>=1)
                    {
                        
                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();

                        Log::info($reservas_comprobacion);

                        if($reservas_comprobacion==[])
                        {

                            if($tarde==1 && $res_tarde ==1)
                            {
                                return response()->json(['success' => 'reservado de tarde',]);
                            }
                            elseif($mañana==1 && $res_mañana ==1){
                                 return response()->json(['success' => 'reservado de mañana',]);
                            }


                            Log::info('Consumir bono ultima hora una franja NUEVA RESERVA');
                            $bonos->numero_bonos_ultima_hora = $bonos->numero_bonos_ultima_hora-1;
                            $bonos->save();
                    
                            $nueva_reserva =new  Reserva();
                            if($tarde==1)
                            {
                                $title="Ocupado: Tarde";
                                $mañana=0;
                                $tarde=1;
                            }
                            elseif($mañana==1)
                            {
                                $title="Ocupado: Mañana";
                                $mañana=1;
                                $tarde=0;
                            }
                            $nueva_reserva->title= $title;
                            $nueva_reserva->barco_id = $barco_id;
                            $nueva_reserva->user_id = $user_id;
                            $nueva_reserva->user_name = $user_name;
                            $nueva_reserva->start = $start;
                            $nueva_reserva->end = $end;
                            $nueva_reserva->mañana = $mañana;
                            $nueva_reserva->tarde = $tarde;
                            $nueva_reserva->imageUrl =  $url_imag_url;
                        
                            $nueva_reserva->ultimo_dia =1;
                            $nueva_reserva->bono_ultimo_dia =1;
                            $nueva_reserva->tipo_ultimo_dia ="4U";


                            $nueva_reserva->save();
                            $envio_email=true;

                            

                        }
                        else
                        {
                            if($tarde==1 && $res_tarde ==1)
                            {
                                return response()->json(['success' => 'reservado de tarde',]);
                            }
                            elseif($mañana==1 && $res_mañana ==1){
                                 return response()->json(['success' => 'reservado de mañana',]);
                            }

                            $bonos->numero_bonos_ultima_hora = $bonos->numero_bonos_ultima_hora-1;
                            $bonos->save();
                    

                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                            $porciones = explode(":", $titulo_reservas_comprobacion);
                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                            $reservas_comprobacion->title=$titulo_final;
                            $reservas_comprobacion->tarde=1;
                            $reservas_comprobacion->mañana=1;
                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
                            $reservas_comprobacion->ultimo_dia =1;

                            $reservas_comprobacion->bono_ultimo_dia =1;
                            $tipo_anyadir= $reservas_comprobacion->tipo_ultimo_dia;
                            $reservas_comprobacion->tipo_ultimo_dia='4U'.','.$tipo_anyadir;

                            

                            $reservas_comprobacion->save();
                            $envio_email=true;


                        }


                    }
                    elseif($numero_bonos_ultima_hora<=0)
                    {
                        return response()->json(['success' => 'No hay bonos de ultima hora',]);

                        Log::info('No hay bonos de ultima hora');
                        
                    }


                }

                


            }

            elseif($count==2)
            {
                return response()->json(['success' => 'reserva dia completo',]);

            }

            //Envio de email en caso de que se haya hecho correctamente la reserva
            if($envio_email==true)
            {
                    // //envio de correo al cliente
                    $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                    // $name="david";
                    // $msg = "todo el mensaje";
                    $subject = "Nueva reserva realizada";
                    $for = Auth::user()->email;
                    Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                        $msj->from("marnificwebcontrol@gmail.com","Marnific");
                        $msj->subject($subject);
                        $msj->to($for);
                    });

                         //  //envio de correoA
                         $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                         // $name="david";
                         // $msg = "todo el mensaje";
                         $subject = "Nueva reserva realizada";
                         $for = "marnificwebcontrol@gmail.com";
                         Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                             $msj->from("marnificwebcontrol@gmail.com","Marnific");
                             $msj->subject($subject);
                             $msj->to($for);
                         });


                    return response()->json(['success' => 'Reserva completada',]);

                
            }
        }
        elseif($diferente_trimestre==true)
        {
            if($tarde==0 && $mañana==1 && $count <2)
            {
                if($mañana==1 && $res_mañana ==1)
                {
                    return response()->json(['success' => 'reservado de mañana',]);
                }
               
                $tarde=0;
                $mañana=1;
                $title = "Ocupado: Mañana";

            }
            elseif($tarde==1 && $mañana==0 && $count <2)
            {
                if($tarde==1 && $res_tarde ==1)
                {
                    return response()->json(['success' => 'reservado de tarde',]);
                }
                $tarde=1;
                $mañana=0;
                $title = "Ocupado: Tarde";

            }
            elseif($tarde==1 && $mañana==1 && $count == 0)
            {
                $tarde=1;
                $mañana=1;
                $title = "Ocupado: Mañana Tarde";


            }
            elseif($count == 2)
            {
                return response()->json(['success' => 'reserva dia completo',]);

            }

            $nueva_reserva =new  Reserva();
            $nueva_reserva->title= $title;
            $nueva_reserva->barco_id = $barco_id;
            $nueva_reserva->user_id = $user_id;
            $nueva_reserva->user_name = $user_name;
            $nueva_reserva->start = $start;
            $nueva_reserva->end = $end;
            $nueva_reserva->mañana = $mañana;
            $nueva_reserva->tarde = $tarde;
            $nueva_reserva->imageUrl = $url_imag_url;
            $nueva_reserva->tipo_ultimo_dia ="4U";
    


            $nueva_reserva->save();

            //añadir nuevo bono diferente trimestre

            $ultima_reserva= Reserva::all()->last();
            $id_ultima_reserva = $ultima_reserva->id;

            // Log::info('ULTIMA RESERVA');
            // Log::info($id_ultima_reserva);

    
            $bonos_descontar = new DescontarBono();
            if($tarde==1 && $mañana==1)
            {
                $resta_bono = $bonos_descontar->cantidad_findesemana+2;



            }
            else
            {
                $resta_bono = $bonos_descontar->cantidad_findesemana+1;

            }
        
            $bonos_descontar->barco_id = $barco_id;
            $bonos_descontar->user_id = $user_id;
            $bonos_descontar->reserva_id = $id_ultima_reserva;

            $bonos_descontar->trimestre = $trimestre;
            $bonos_descontar->cantidad_findesemana= $resta_bono;
        

            $bonos_descontar->save();

            // //envio de correo al cliente
            $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
            // $name="david";
            // $msg = "todo el mensaje";
            $subject = "Nueva reserva realizada";
            $for = Auth::user()->email;
            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                $msj->subject($subject);
                $msj->to($for);
            });

                 //  //envio de correoA
                 $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                 // $name="david";
                 // $msg = "todo el mensaje";
                 $subject = "Nueva reserva realizada";
                 $for = "marnificwebcontrol@gmail.com";
                 Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                     $msj->from("marnificwebcontrol@gmail.com","Marnific");
                     $msj->subject($subject);
                     $msj->to($for);
                 });

            
            return response()->json(['success' => 'Reserva ok diferente trimestre',]);


        }

      

    }
  

    public function cancelacion(ReservasDataTable $dataTable,$id,$fecha,$user_id)
    {
        $hoy = date("Y-m-d");

        $fecha_start = substr($fecha, 0, 10);
        
        $dStart = new \DateTime($hoy);
        $dEnd  = new \DateTime($fecha_start);
        
        //comprobar trimestres

        date_default_timezone_set('Europe/Madrid');

        $fecha_start = substr($fecha, 0, 10);
        $numeroDia = date('d', strtotime($fecha_start));
        $dia = date('l', strtotime($fecha_start));
        $mes = date('F', strtotime($fecha_start));
        $anio = date('Y', strtotime($fecha_start));

        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $nombremes = str_replace($mes_EN, $mes_ES, $mes);
        

        //FECHA ACTUAL
        $hoy = substr($hoy, 0, 10);
        $numeroDia_actual = date('d', strtotime($hoy));
        $dia_actual = date('l', strtotime($hoy));
        $mes_actual = date('F', strtotime($hoy));
        $anio_actual = date('Y', strtotime($hoy));

        
        $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);

       


        //comprobar que en que trimestre Se hace la reserva
      
       $trimestre="";
        switch ($nombremes) {
            case "Enero":
                $trimestre="Primero";
                break;
            case "Febrero":
                $trimestre="Primero";
                break;
            case "Marzo":
                $trimestre="Primero";
                break;
            case "Abril":
                $trimestre="Segundo";
                break;
            case "Mayo":
                $trimestre="Segundo";
                break;
            case "Junio":
                $trimestre="Segundo";
                break; 
            case "Julio":
                $trimestre="Tercero";
                break; 
            case "Agosto":
                $trimestre="Tercero";
                break; 
            case "Septiembre":
                $trimestre="Tercero";
                break; 
            case "Octubre":
                $trimestre="Cuarto";
                break;
            case "Noviembre":
                $trimestre="Cuarto";
                break;
            case "Diciembre":
                $trimestre="Cuarto";
                break;
                      
        }

        //comprobar en que trimestre estamos actualmente

        $trimestre_actual="";
        switch ($nombremes_actual) {
            case "Enero":
                $trimestre_actual="Primero";
                break;
            case "Febrero":
                $trimestre_actual="Primero";
                break;
            case "Marzo":
                $trimestre_actual="Primero";
                break;
            case "Abril":
                $trimestre_actual="Segundo";
                break;
            case "Mayo":
                $trimestre_actual="Segundo";
                break;
            case "Junio":
                $trimestre_actual="Segundo";
                break; 
            case "Julio":
                $trimestre_actual="Tercero";
                break; 
            case "Agosto":
                $trimestre_actual="Tercero";
                break; 
            case "Septiembre":
                $trimestre_actual="Tercero";
                break; 
            case "Octubre":
                $trimestre_actual="Cuarto";
                break;
            case "Noviembre":
                $trimestre_actual="Cuarto";
                break;
            case "Diciembre":
                $trimestre_actual="Cuarto";
                break;
                      
        }

        $diferente_trimestre = false;

        Log::info('TRIMESTRE ACTUAL');
        Log::info($trimestre_actual);
        Log::info('TRIMESTRE RESERVA ');
        Log::info($trimestre);

        if($trimestre_actual == $trimestre)
        {
          $diferente_trimestre = false;
        }
        elseif($trimestre_actual != $trimestre)
        {
            $diferente_trimestre = true;

        }
       
        Log::info('diferente trimestre');
        Log::info($diferente_trimestre);
      

        //comprobar diar corridos

        $dias_corridos = $dStart->diff($dEnd)->format('%r%a');
        $reservas_bonos_extras=Reserva::where('id',$id)->first();

        //sacar nombre de barco  puerto y ciudad
        
        $barco_nombre = Barco::where('id', $reservas_bonos_extras->barco_id)->first();

        $nombre_barco = $barco_nombre->nombre;
        $puerto = $barco_nombre->puerto;

        $ciudad= $barco_nombre->ciudad;

        $reservas=Reserva::where('id',$id)->first();
        $tarde = $reservas->tarde;
        $mañana = $reservas->mañana;
        
        if($reservas_bonos_extras->reserva_bono_extra==0)
        {
            if($diferente_trimestre==false)
            {
                if($dias_corridos>="2")
                {
                   

                    if($reservas->dia_festivo==0)
                    {
                       

                      
                        $bool_reserva= false;
                        if($tarde==1 && $mañana==1)
                        {
                            $bool_reserva =true;
                        }

                        Log::info('SI se puede cancelar');
                        $fecha_start = substr($fecha, 0, 10);
                        $numeroDia = date('d', strtotime($fecha_start));
                        $dia = date('l', strtotime($fecha_start));
                        $mes = date('F', strtotime($fecha_start));
                        $anio = date('Y', strtotime($fecha_start));
                        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
                        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
                    

                        $bonos = User::where('id',$user_id)->first();

                        if($nombredia == "Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes" )
                        {
                            //suammos 1 bono

                            if($nombredia=="Viernes" && $tarde==1 && $mañana==0)
                            {   
                                if($bool_reserva==false)
                                {
                                    $suma_bono = $bonos->numero_bonos_findesemana+1;
                                }
                                //SUMAMOSos dos bonos
                                elseif($bool_reserva==true)
                                {
                                    $suma_bono = $bonos->numero_bonos_findesemana+2;


                                }

                                
                                $bonos->numero_bonos_findesemana = $suma_bono;
                                $bonos->save();
                            }
                            elseif($nombredia=="Viernes" && $tarde==0 && $mañana==1)
                            {
                                if($bool_reserva==false)
                                {
                                    $suma_bono = $bonos->numero_bonos_entresemana+1;
                                }
                                //SUMAMOSos dos bonos
                                elseif($bool_reserva==true)
                                {
                                    $suma_bono = $bonos->numero_bonos_entresemana+2;


                                }
            
                                $bonos->numero_bonos_entresemana = $suma_bono;
                                $bonos->save();

                            }
                            elseif($nombredia=="Viernes" && $tarde==1 && $mañana==1)
                            {
                                
                                    $suma_bono_entresemana = $bonos->numero_bonos_entresemana+1;
                            
                                    $suma_bono_findesemana = $bonos->numero_bonos_findesemana+1;


                                $bonos->numero_bonos_entresemana = $suma_bono_entresemana;
                                $bonos->numero_bonos_findesemana = $suma_bono_findesemana;

                                $bonos->save();

                            }
                            elseif($nombredia=="Sábado" || $nombredia=="Domingo" )
                            {
                                Log::info('cancela sabado o domingo');
                                if($bool_reserva==false)
                                {
                                    $suma_bono = $bonos->numero_bonos_findesemana+1;
                                }
                                //SUMAMOSos dos bonos
                                elseif($bool_reserva==true)
                                {
                                    $suma_bono = $bonos->numero_bonos_findesemana+2;


                                }

                                $bonos->numero_bonos_findesemana= $suma_bono;
                                $bonos->save();
                            }

                        }
                        else{
                            Log::info('Nombre dia');
                            Log::info($nombredia);
                            if($bool_reserva==false)
                            {
                                $suma_bono = $bonos->numero_bonos_entresemana+1;
                            }
                            //SUMAMOS dos bonos
                            elseif($bool_reserva==true)
                            {
                                $suma_bono = $bonos->numero_bonos_entresemana+2;


                            }

                        
                            $bonos->numero_bonos_entresemana = $suma_bono;
                            $bonos->save();
                        }
                            Reserva::where('id',$id)->delete();
                            // //envio de correo al cliente
                            $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];
                            // $name="david";
                            // $msg = "todo el mensaje";
                            $subject = "Cancelacion de reserva";
                            $for = Auth::user()->email;
                            Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                $msj->subject($subject);
                                $msj->to($for);
                            });

                            //  //envio de correoA
                            $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];

                            // $name="david";
                            // $msg = "todo el mensaje";
                            $subject = "Cancelacion de reserva";
                            $for = "marnificwebcontrol@gmail.com";
                            Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                                $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                $msj->subject($subject);
                                $msj->to($for);
                            });

                        
            
                        return back();

                    }
                    elseif($reservas->dia_festivo==1)
                    {
                        $bonos2 = User::where('id',Auth::user()->id)->first();

                        if($reservas->tarde==1 && $reservas->mañana==1)
                        {
                            Log::info('ENTRA AQUISAFDAD');
                            $suma_bono2 = $bonos2->numero_bonos_findesemana+2;
                            $bonos2->numero_bonos_findesemana = $suma_bono2;

                            $bonos2->save();

                        }
                        elseif($reservas->tarde==0 && $reservas->mañana==1)
                        {
                            $suma_bono2 = $bonos2->numero_bonos_findesemana+1;
                            $bonos2->numero_bonos_findesemana = $suma_bono2;

                            $bonos2->save();

                        }
                        elseif($reservas->tarde==1 && $reservas->mañana==0)
                        {
                            $suma_bono2 = $bonos2->numero_bonos_findesemana+1;
                            $bonos2->numero_bonos_findesemana = $suma_bono2;

                            $bonos2->save();

                        }


                        Reserva::where('id',$id)->delete();

                        // //envio de correo al cliente
                        $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];
                        // $name="david";
                        // $msg = "todo el mensaje";
                        $subject = "Cancelacion de reserva";
                        $for = Auth::user()->email;
                        Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                            $msj->from("marnificwebcontrol@gmail.com","Marnific");
                            $msj->subject($subject);
                            $msj->to($for);
                        });

                              //  //envio de correoA
                              $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];

                              // $name="david";
                              // $msg = "todo el mensaje";
                              $subject = "Cancelacion de reserva";
                              $for = "marnificwebcontrol@gmail.com";
                              Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                                  $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                  $msj->subject($subject);
                                  $msj->to($for);
                              });

            
                        return back();


                    }
                    

                   
                }
                elseif($dias_corridos<"2")
                {

                    $hoy = date("Y-m-d");
                    Log::info($id);

                         Reserva::where('id',$id)->delete();
                    
                        // //envio de correo al cliente
                        $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];
                        // $name="david";
                        // $msg = "todo el mensaje";
                        $subject = "Cancelacion de reserva";
                        $for = Auth::user()->email;
                        Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                            $msj->from("marnificwebcontrol@gmail.com","Marnific");
                            $msj->subject($subject);
                            $msj->to($for);
                        });
                             //  //envio de correoA
                             $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];

                             // $name="david";
                             // $msg = "todo el mensaje";
                             $subject = "Cancelacion de reserva";
                             $for = "marnificwebcontrol@gmail.com";
                             Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                                 $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                 $msj->subject($subject);
                                 $msj->to($for);
                             });


                    $reservas = Reserva::where('user_id',$user_id)->whereBetween('start', [$hoy, '2100-12-12'])->get();
                    return redirect()->back()->with('alert', 'Se ha eliminado la reserva pero no recuperarás los bonos ya que ha sido una cancelación dentro de los 48h establecidas para recuperar los bonos en una cancelacion!');

                    
                    // return $dataTable->render('/reservas/detalles_reservas',[
                    //     'reservas' => $reservas,
                    //     'error'=>"Se ha eliminado la reserva pero no recuperarás los bonos ya que ha sido una cancelación dentro de los 48h establecidas para recuperar los bonos en una cancelacion",
                    //     ]);
                    
                
                    
                }
            }
            elseif($diferente_trimestre==true)
            {
                 Reserva::where('id',$id)->delete();

                DescontarBono::where('reserva_id',$id)->delete();

                // //envio de correo al cliente
                $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = Auth::user()->email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("marnificwebcontrol@gmail.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                  //  //envio de correoA
                  $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad,'tarde'=>$tarde, 'mañana'=>$mañana];

                  // $name="david";
                  // $msg = "todo el mensaje";
                  $subject = "Cancelacion de reserva";
                  $for = "marnificwebcontrol@gmail.com";
                  Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                      $msj->from("marnificwebcontrol@gmail.com","Marnific");
                      $msj->subject($subject);
                      $msj->to($for);
                  });
                
                
                $reservas = Reserva::where('user_id',$user_id)->whereBetween('start', [$hoy, '2100-12-12'])->get();
                return redirect()->back()->with('alert', 'Se ha eliminado la reserva pero no recuperarás los bonos ya que ha sido una cancelación de un trimestre distinto, tampoco se te descontarán en el próximo trimestre dicha reserva!');
                // return $dataTable->render('/reservas/detalles_reservas',[
                //     'reservas' => $reservas,
                //     'error'=>"Se ha eliminado la reserva pero no recuperarás los bonos ya que ha sido una cancelación de un trimestre distinto, tampoco se te descontarán en el próximo trimestre dicha reserva",
                //     ]);

            }

        }
        elseif($reservas_bonos_extras->reserva_bono_extra==1){
            $reservas = Reserva::where('user_id',$user_id)->whereBetween('start', [$hoy, '2100-12-12'])->get();

            return $dataTable->render('/reservas/detalles_reservas',[
                'reservas' => $reservas,
                'error'=>"No se puede eliminar la reserva ya que esta reserva esta relacionada con un bono extra, si quieres denegar el bono pongase en contacto con nosotros",
                ]);

        }
        



    }

    public function checking_index(ReservasDataTable $dataTable,$id)   
    {
        $reservas = Reserva::where('id',$id)->first();

        $tarde = $reservas->tarde;
        $mañana = $reservas->mañana;

        $hoy = date("Y-m-d");
        $fecha_start = substr($reservas->start, 0, 10);


        Log::info($fecha_start);
        Log::info($hoy);

        
        if($fecha_start!=$hoy)
        {

           
            $reservas2 = Reserva::where('user_id',$reservas->user_id)->whereBetween('start', [$hoy, '2100-12-12'])->get();

            return $dataTable->render('/reservas/detalles_reservas',[
                'reservas' => $reservas2,
                'error'=>"No se puede hacer el check no es el momento",
                ]);
        }
        elseif($fecha_start==$hoy){
            
            return view('reservas.checking',[
                'reservas'=>$reservas,
            ]);
        }
    }
    public function checkout_index(ReservasDataTable $dataTable,$id)
    {
        $reservas = Reserva::where('id',$id)->first();

        $tarde = $reservas->tarde;
        $mañana = $reservas->mañana;

        $hoy = date("Y-m-d");
        $fecha_start = substr($reservas->start, 0, 10);


        Log::info($fecha_start);
        Log::info($hoy);

        
        if($fecha_start!=$hoy)
        {

           
            $reservas2 = Reserva::where('user_id',$reservas->user_id)->whereBetween('start', [$hoy, '2100-12-12'])->get();

            return $dataTable->render('/reservas/detalles_reservas',[
                'reservas' => $reservas2,
                'error'=>"No se puede hacer el check no es el momento",
                ]);
        }
        elseif($fecha_start==$hoy){
            
            return view('reservas.checkout',[
                'reservas'=>$reservas,
            ]);
        }
    }

    public function upload_imagen_gasolina(Request $request)
    {
        $file = $request->file('file');
        $reservas_id = $request->reservas_id;
        $hora_gasolina = $request->hora_gasolina;

        $path = public_path() . '/imagenes/checkings/gasolina';
        $fileName = "DepositoGasolina".'('.$reservas_id .')'.'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $reservas = Reserva::where('id',$reservas_id)->first();

        $reservas->imagen_gasolina = $fileName;
        $reservas->hora_gasolina = $hora_gasolina;


        $reservas->save();

      
            
    }
    public function upload_imagen_gasolina_checkout(Request $request)
    {
        $file = $request->file('file');
        $reservas_id = $request->reservas_id;
        $hora_gasolina_checkout = $request->hora_gasolina_checkout;

        $path = public_path() . '/imagenes/checkouts/gasolina';
        $fileName = "DepositoGasolinaCheckout".'('.$reservas_id .')'.'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $reservas = Reserva::where('id',$reservas_id)->first();

        $reservas->imagen_gasolina_checkout = $fileName;
        $reservas->hora_gasolina_checkout = $hora_gasolina_checkout;

        $reservas->save();

      
            
    }
    public function upload_imagen_horas(Request $request)
    {
        $file = $request->file('file');
        $hora_horas = $request->hora_horas;

        $reservas_id = $request->reservas_id2;
        $path = public_path() . '/imagenes/checkings/horas';
        $fileName = "ContadorHoras".'('.$reservas_id .')'.'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $reservas = Reserva::where('id',$reservas_id)->first();

        $reservas->imagen_horas = $fileName;
        $reservas->hora_horas = $hora_horas;


        $reservas->save();

      
            
    }
    public function upload_imagen_horas_checkout(Request $request)
    {
        Log::info('GUARDA LAS IMAGENES DE HORAS DEL CHECKOUT');
        $file = $request->file('file');
        $hora_horas_checkout = $request->hora_horas_checkout;

        $reservas_id = $request->reservas_id2;
        $path = public_path() . '/imagenes/checkouts/horas';
        $fileName = "ContadorHorasCheckout".'('.$reservas_id .')'.'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $reservas = Reserva::where('id',$reservas_id)->first();

        $reservas->imagen_horas_checkout = $fileName;
        $reservas->hora_horas_checkout = $hora_horas_checkout;

        $reservas->save();

      
            
    }
    public function upload_imagen_bateria_checkout(Request $request)
    {
        Log::info('GUARDA LAS IMAGENES DE BATERIA DEL CHECKOUT');
        $file = $request->file('file');
        $hora_bateria_checkout = $request->hora_bateria_checkout;
        $reservas_id = $request->reservas_id3;
        $path = public_path() . '/imagenes/checkouts/bateria';
        $fileName = "BateriaCheckout".'('.$reservas_id .')'.'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $reservas = Reserva::where('id',$reservas_id)->first();

        $reservas->imagen_bateria = $fileName;
        $reservas->hora_bateria_checkout = $hora_bateria_checkout;

        $reservas->save();

      
            
    }
    public function upload_imagen_incidencias_checkout(Request $request)
    {
        $file = $request->file('file');
        $reservas_id = $request->reservas_id;
        $path = public_path() . '/imagenes/checkings/incidencias_checkout';
        $fileName = "Incidencias_checkout".'('.$reservas_id .')'.'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);

        $imagen = new ImagenIncidencia();

        $imagen->reserva_id = $request->reservas_id;
        $imagen->nombre = $file;
        $imagen->nombre_archivo = $fileName;


        $imagen->save();
    }
    public function upload_imagen_incidencias_checking(Request $request)
    {
        $file = $request->file('file');
        $reservas_id = $request->reservas_id;
        $path = public_path() . '/imagenes/checkings/incidencias_checking';
        $fileName = "Incidencias_checking".'('.$reservas_id .')'.'-'.uniqid() . $file->getClientOriginalName();
    
        $file->move($path, $fileName);
        Log::info(' img incidencias checking');

        $imagen = new ImagenIncidencia();

        $imagen->reserva_id = $request->reservas_id;
        $imagen->nombre = $file;
        $imagen->nombre_archivo = $fileName;


        $imagen->save();

        // $reservas = Reserva::where('id',$reservas_id)->first();

        // $reservas->imagen_horas = $fileName;

        // $reservas->save();
    }

    public function checking_usuario($id)
    {
        Log::info('entra a checking usuarios');
        Log::info($id);

        $reservas = Reserva::where('id',$id)->first();

        Log::info('reserva');
        Log::info($reservas);
        $reservas->checking = 1;
        $reservas->checking_verificado = 2;



        $reservas->save();


    }
    public function checking_usuario_incidencias($id)
    {
        Log::info('entra a checking incidencias usuarios');
        Log::info($id);

        $reservas = Reserva::where('id',$id)->first();

        Log::info('reserva');
        Log::info($reservas);
        $reservas->checking = 2;
        $reservas->checking_verificado = 3;
        $reservas->save();

        return response()->json(['success' => 'checkout hecho',]);



    }

    
    public function checkout_usuario($id)
    {
        Log::info('entra a checkout usuarios');
        Log::info($id);

        $reservas = Reserva::where('id',$id)->first();

        Log::info('reserva');
        Log::info($reservas);
        $reservas->checkout = 1;
        $reservas->checkout_verificado = 2;
        $reservas->save();

        return response()->json(['success' => 'checkout hecho',]);



    }
  
    

    public function checkout_usuario_incidencias($id)
    {
        Log::info('entra a checkout incidencias usuarios');
        Log::info($id);

        $reservas = Reserva::where('id',$id)->first();

        Log::info('reserva');
        Log::info($reservas);
        $reservas->checkout = 2;
        $reservas->checkout_verificado = 3;
  
        $reservas->save();

        return response()->json(['success' => 'checkout hecho',]);



    }


    public function checkout_incidencias(Request $request)
    {
        Log::info('incidencias');
        Log::info($request->incidencias);

        
        $reservas = Reserva::where('id',$request->reserva_id)->first();
        $reservas->incidencias_checkout = $request->incidencias; //cambiar a checkout

        $reservas->save();

        return response()->json(['success' => 'incidencias hecho',]);




    }
    public function checking_incidencias(Request $request)
    {
        Log::info('incidencias');
        Log::info($request->incidencias);

        
        $reservas = Reserva::where('id',$request->reserva_id)->first();
        $reservas->incidencias_checking = $request->incidencias; //cambiar a checkout

        $reservas->save();

        return response()->json(['success' => 'incidencias hecho',]);




    }

    public function bono_extra(Request $request)
    {
        Log::info('enta en bono  extra');

        $dia=$request->dia_semana;
        $fecha_start = $request->fecha_add_bono;
        $barco_id=$request->bonos_barco_id;
        $bono_tarde=$request->bono_tarde;
        $bono_mañana=$request->bono_mañana;
        $user_id =Auth::user()->id;
        Log::info('DIA');
        Log::info($dia);
    

        $barco=Bono::where('barco_id',$barco_id)->first();
        $precio_entresemana =  $barco->precio;
        $precio_findesemana = $barco->precio_findesemana;
        $tipo="";
        $hoy = date("Y-m-d");

        //sacamos informacion del barco 
        $barco_barco = Barco::where('id', $barco_id)->first();
        $nombre_barco = $barco_barco->nombre;
        $puerto = $barco_barco->puerto;
        $ciudad = $barco_barco->ciudad;


        //comprobamos diass festivos

        $dias_festivos = DiaFestivo::all();
        $dias_festivos_count = 0;

        foreach($dias_festivos as $dia_festivo)
        {
            $dia_festivo_sin_horas = substr($dia_festivo->start, 0, 10);

            Log::info('dia festivo');
            Log::info($dia_festivo_sin_horas);
            
            if($fecha_start == $dia_festivo_sin_horas)
            {
                Log::info('concuerda');
                $dias_festivos_count++;
            }
            
        }

        log::info('fecha inicio');
        log::info($fecha_start);

        log::info('count dias festivos');
        log::info($dias_festivos_count);

        

      //comprobamos que el usuario no tenga pedido más de 1 bono extra sin confirmar

        $bonos_extra=BonoExtra::where('user_id',Auth::user()->id)->get();

        $count_bonos=0;
        foreach($bonos_extra as $bx)
        {

            $confirmados = $bx->confirmar;

            if($confirmados=="no")
            {
                $count_bonos++;
            }
        }

        
      //comprobamos que tipo de dia es si entresemana o finde semana 
        if($count_bonos==0)
        {
          
            if($dia=="sabado" || $dia=="domingo" || $dia=="viernes" )
            {
                if($dia=="viernes" && $bono_tarde==1 && $bono_mañana==0)
                {
                    $tipo="Finde semana";
                    $cantidad = 1;

                }
                elseif($dia=="viernes" && $bono_tarde==0 && $bono_mañana==1)
                {
                    $tipo="Entre semana";
                    $cantidad = 1;

                }
                elseif($dia=="viernes" && $bono_tarde==1 && $bono_mañana==1)
                {
                    $tipo="Entre semana y Finde semana";
                    $cantidad = 2;

                }
                elseif($dia=="sabado" || $dia=="domingo" )
                {
                    $tipo="Finde semana";

                    if( $bono_tarde==1 && $bono_mañana==1)
                    {
                        $tipo="Finde semana dos franjas";
                         $cantidad = 2;



                    }
                    else{
                        $tipo="Finde semana";
                        $cantidad = 1;

                    }
                    

                }

                
            }
            else
            {
                if( $bono_tarde==1 && $bono_mañana==1)
                {
                    $tipo="Entre semana dos franjas";
                    $cantidad = 2;


                }
                else{
                    $tipo="Entre semana";
                    $cantidad = 1;

                }

                

            }

            Log::info('barco id ');
            Log::info( $barco_id);
            Log::info('user id ');
            Log::info($user_id);
            $precio=0;

            if($tipo=="Finde semana")
            {
                $precio= $precio_findesemana;
               
            }
            elseif($tipo=="Entre semana")
            {
                $precio= $precio_entresemana ;
            }

            elseif($tipo=="Entre semana y Finde semana")
            {
                $precio= $precio_entresemana + $precio_findesemana ;
            }
            elseif($tipo=="Entre semana dos franjas")
            {
                $precio= $precio_entresemana + $precio_entresemana ;
            }

            elseif($tipo=="Finde semana dos franjas")
            {
                $precio= $precio_findesemana + $precio_findesemana ;
            }
            
            
            //Nueva funcionalidad
                        
            //Sacar todo lo relacionado con los dias de fecha actual, fecha de reserva etc.....

            //FECHA RESERVA
            date_default_timezone_set('Europe/Madrid');

            $fecha_start = substr($fecha_start, 0, 10);
            $numeroDia = date('d', strtotime($fecha_start));
            $dia = date('l', strtotime($fecha_start));
            $mes = date('F', strtotime($fecha_start));
            $anio = date('Y', strtotime($fecha_start));

            $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
            $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $nombredia = str_replace($dias_EN, $dias_ES, $dia);
            $nombremes = str_replace($mes_EN, $mes_ES, $mes);
            

            //FECHA ACTUAL
            $hoy = substr($hoy, 0, 10);
            $numeroDia_actual = date('d', strtotime($hoy));
            $dia_actual = date('l', strtotime($hoy));
            $mes_actual = date('F', strtotime($hoy));
            $anio_actual = date('Y', strtotime($hoy));

            
            $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);

        


            //comprobar que en que trimestre Se hace la reserva
        
        $trimestre="";
            switch ($nombremes) {
                case "Enero":
                    $trimestre="Primero";
                    break;
                case "Febrero":
                    $trimestre="Primero";
                    break;
                case "Marzo":
                    $trimestre="Primero";
                    break;
                case "Abril":
                    $trimestre="Segundo";
                    break;
                case "Mayo":
                    $trimestre="Segundo";
                    break;
                case "Junio":
                    $trimestre="Segundo";
                    break; 
                case "Julio":
                    $trimestre="Tercero";
                    break; 
                case "Agosto":
                    $trimestre="Tercero";
                    break; 
                case "Septiembre":
                    $trimestre="Tercero";
                    break; 
                case "Octubre":
                    $trimestre="Cuarto";
                    break;
                case "Noviembre":
                    $trimestre="Cuarto";
                    break;
                case "Diciembre":
                    $trimestre="Cuarto";
                    break;
                        
            }

            //comprobar en que trimestre estamos actualmente

            $trimestre_actual="";
            switch ($nombremes_actual) {
                case "Enero":
                    $trimestre_actual="Primero";
                    break;
                case "Febrero":
                    $trimestre_actual="Primero";
                    break;
                case "Marzo":
                    $trimestre_actual="Primero";
                    break;
                case "Abril":
                    $trimestre_actual="Segundo";
                    break;
                case "Mayo":
                    $trimestre_actual="Segundo";
                    break;
                case "Junio":
                    $trimestre_actual="Segundo";
                    break; 
                case "Julio":
                    $trimestre_actual="Tercero";
                    break; 
                case "Agosto":
                    $trimestre_actual="Tercero";
                    break; 
                case "Septiembre":
                    $trimestre_actual="Tercero";
                    break; 
                case "Octubre":
                    $trimestre_actual="Cuarto";
                    break;
                case "Noviembre":
                    $trimestre_actual="Cuarto";
                    break;
                case "Diciembre":
                    $trimestre_actual="Cuarto";
                    break;
                        
            }

            $diferente_trimestre = false;

            Log::info('TRIMESTRE ACTUAL');
            Log::info($trimestre_actual);
            Log::info('TRIMESTRE RESERVA ');
            Log::info($trimestre);

            if($trimestre_actual == $trimestre)
            {
            $diferente_trimestre = false;
            }
            elseif($trimestre_actual != $trimestre)
            {
                $diferente_trimestre = true;

            }
        
            Log::info('TRIMESTRE');
            Log::info($diferente_trimestre);

        
            //hastas aqui
            //Nueva funcionalidad
            $ultimo_dia  =$request->ultimo_dia_add_bono;
    
            $usuario_bono = User::where('id', Auth::user()->id)->first();
            $numero_bonos_findesemana = $usuario_bono->numero_bonos_findesemana;
            $numero_bonos_entresemana = $usuario_bono->numero_bonos_entresemana;
  
            $verificacion_ultimo_dia=false;

            if($dias_festivos_count==0 && $diferente_trimestre ==false)
            {
                if($ultimo_dia == 0)
                {
                    $verificacion_ultimo_dia=false;

                    Log::info('ULLLLLLTIMO DIAAAAAAAA NOOOOO ');
                    Log::info($ultimo_dia);
                   
                    Log::info('tipo de bonos a consumir ');
                    Log::info($tipo);
    
                    if($tipo=="Finde semana")
                    {
                        if($numero_bonos_findesemana==0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                            $precio = $precio_findesemana;
                            $cantidad = 1;
                        }
                   
                        
                    
    
                    }
                    elseif($tipo=="Entre semana")
                    {
                        if($numero_bonos_entresemana==0)
                        {
                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                            $precio = $precio_entresemana;
                            $cantidad = 1;
                            
                           
                        }
                        
                    }
                    elseif($tipo=="Entre semana y Finde semana")
                    {
                      
    
                        if($numero_bonos_entresemana==0 && $numero_bonos_findesemana>0 )
                        {
                         $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                         $precio = $precio_entresemana;
                         $cantidad = 1;
                         $tipo="Entre semana";
    
                        
                        }
                        elseif($numero_bonos_findesemana==0 && $numero_bonos_entresemana>0  )
                        {
                         $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                         $precio = $precio_findesemana;
                         $cantidad = 1;
                         $tipo="Finde semana";
    
                        }
                        elseif($numero_bonos_findesemana==0 && $numero_bonos_entresemana==0  )
                        {
                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                            $precio = $precio_findesemana + $precio_entresemana;
                            $cantidad = 2;
                            $tipo="Entre semana y Finde semana";
                        }
                        
    
                        
    
    
    
                    }
                    elseif($tipo=="Entre semana dos franjas")
                    {
                        $tipo="Entre semana";
                        if($numero_bonos_entresemana==0)
                        {
                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 2;
    
                            $precio = $precio_entresemana + $precio_entresemana ;
                            $cantidad = 2;
                           
    
    
                        }
                        elseif($numero_bonos_entresemana>0)
                        {
                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
    
                            $precio = $precio_entresemana;
                            $cantidad = 1;
                        }
                    }
    
                    elseif($tipo=="Finde semana dos franjas")
                    {
                        $tipo="Finde semana";
                        if($numero_bonos_findesemana==0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                            $precio = $precio_findesemana + $precio_findesemana ;
                            $cantidad = 2;
                        }
                        elseif($numero_bonos_findesemana>0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                            $precio = $precio_findesemana;
                            $cantidad = 1;
    
                        }
                    }
                }
                elseif($ultimo_dia == 1)
                {
                    $verificacion_ultimo_dia=true;

                    Log::info('ULLLLLLTIMO DIAAAAAAAA siiiiiiiii');
                    Log::info($ultimo_dia);
    
                    $tipo_de_bono   = $request->tipo_bono_add_bono;
                    Log::info('tipo de bono ');
                    Log::info($tipo_de_bono );
    
                    if($tipo_de_bono ==1)
                    {
                        $tipo="Entre semana";
    
                      
    
                        if( $bono_tarde==1 && $bono_mañana==0)
                        {
                          
    
                            if($numero_bonos_entresemana==0)
                            {
                           
                                $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                $precio= $precio_entresemana;
                                $cantidad = 1;
                            }
    
                        }
                        elseif( $bono_tarde==0 && $bono_mañana>0)
                        {
                            
    
                            if($numero_bonos_entresemana==0)
                            {
                                $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                $precio= $precio_entresemana ;
                                $cantidad = 1;
                            }
                        }
                        elseif( $bono_tarde==1 && $bono_mañana>0)
                        {
                           
    
                            if($numero_bonos_entresemana==0)
                            {
                             $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 2;
                             $precio= $precio_entresemana + $precio_entresemana ;
                             $cantidad = 2;
     
                            }
                            if($numero_bonos_entresemana>0)
                            {
                             $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                             $precio= $precio_entresemana;
                             $cantidad = 1;
                            }
                        }
    
                    }
                    elseif($tipo_de_bono ==2)
                    {
                        $tipo="Finde semana";
    
                  
    
    
    
    
                        if( $bono_tarde==1 && $bono_mañana==0)
                        {
    
                            if($numero_bonos_findesemana==0)
                            {
                                $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                $precio= $precio_findesemana ;
                                $cantidad = 1;
    
                            }
    
                        }
                        elseif( $bono_tarde==0 && $bono_mañana>0)
                        {
                        
                            if($numero_bonos_findesemana==0)
                            {
                             $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                             $precio= $precio_findesemana ;
                             $cantidad = 1;
                            }
                        }
                        elseif( $bono_tarde==1 && $bono_mañana>0)
                        {
                            $precio= $precio_findesemana + $precio_findesemana  ;
    
                            if($numero_bonos_findesemana==0)
                            {
                                $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                $precio= $precio_findesemana + $precio_findesemana   ;
                                $cantidad = 2;
                            }
                            if($numero_bonos_findesemana>0)
                            {
                                $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                $precio= $precio_findesemana ;
                                $cantidad = 1;
                            }
                        }
    
                    }
                    
    
                }

                $usuario_bono->save();

            
                $nuevo_bono_extra = new BonoExtra();
            
                $nuevo_bono_extra->barco_id= $barco_id;
                $nuevo_bono_extra->user_id= Auth::user()->id;
                $nuevo_bono_extra->tipo=$tipo;
                $nuevo_bono_extra->precio=$precio;
                $nuevo_bono_extra->cantidad=$cantidad;

               
                if($verificacion_ultimo_dia==false)
                {
                    $nuevo_bono_extra->ultimo_dia=0;

                }
                elseif($verificacion_ultimo_dia==true)
                {
                    $nuevo_bono_extra->ultimo_dia=1;
                    $nuevo_bono_extra->tipo_ultimo_dia=$tipo;

                }



    
    
                $nuevo_bono_extra->save();


                //  //envio de correo al administrador
                 $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                 // $name="david";
                 // $msg = "todo el mensaje";
                 $subject = "Nuevo bono extra pedido";
                 $for = "marnificwebcontrol@gmail.com";
                 Mail::send('mails/mail_bono',$datos, function($msj) use($subject,$for){
                     $msj->from("marnificwebcontrol@gmail.com","Marnific");
                     $msj->subject($subject);
                     $msj->to($for);
                 });

                 

                //    //envio de correo al cliente
                   $datos2 = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                   // $name="david";
                   // $msg = "todo el mensaje";
                   $subject = "Nuevo bono extra pedido";
                   $for = Auth::user()->email;
                   Mail::send('mails/mail_bono',$datos2, function($msj) use($subject,$for){
                       $msj->from("marnificwebcontrol@gmail.com","Marnific");
                       $msj->subject($subject);
                       $msj->to($for);
                   });
      
    
    
    
                
                return response()->json(['success' => 'bono pedido',]);
              
            }
            //dias festivos
            elseif($dias_festivos_count>0 && $diferente_trimestre ==false)
            {
                if($ultimo_dia == 0)
                {
                    if($tipo=="Finde semana")
                    {
                        if($numero_bonos_findesemana==0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                            $precio = $precio_findesemana;
                            $cantidad = 1;
                            $tipo="Finde semana";
                        }
                    
                        
                    
    
                    }
                    elseif($tipo=="Entre semana")
                    {
                        if($numero_bonos_findesemana==0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                            $precio = $precio_findesemana;
                            $cantidad = 1;
                            $tipo="Finde semana";
                            
                            
                        }
                        
                    }
                    elseif($tipo=="Entre semana y Finde semana")
                    {
                        
    
                        if( $numero_bonos_findesemana==0 )
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                            $precio = $precio_findesemana + $precio_findesemana;
                            $cantidad = 2;
                            $tipo="Finde semana";
    
                        
                        }
    
                        
                        if( $numero_bonos_findesemana>0 )
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                            $precio = $precio_findesemana;
                            $cantidad = 1;
                            $tipo="Finde semana";
    
                        
                        }
                      
    
                        
    
    
    
                    }
    
                    elseif($tipo=="Entre semana dos franjas")
                    {
                       
                        if($numero_bonos_findesemana==0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
    
                            $precio = $precio_findesemana + $precio_findesemana ;
                            $cantidad = 2;
                            $tipo="Finde semana";
    
                            
    
    
                        }
                        elseif($numero_bonos_findesemana>0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
    
                            $precio = $precio_findesemana;
                            $cantidad = 1;
                            $tipo="Finde semana";
    
                        }
                    }
        
                    elseif($tipo=="Finde semana dos franjas")
                    {
                        $tipo="Finde semana";
                        if($numero_bonos_findesemana==0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                            $precio = $precio_findesemana + $precio_findesemana ;
                            $cantidad = 2;
                            $tipo="Finde semana";
    
                        }
                        elseif($numero_bonos_findesemana>0)
                        {
                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                            $precio = $precio_findesemana;
                            $cantidad = 1;
                            $tipo="Finde semana";
    
    
                        }
                    }

                }
                elseif($ultimo_dia == 1)
                {
                    $verificacion_ultimo_dia=true;

                    $tipo_de_bono   = $request->tipo_bono_add_bono;
                    Log::info('tipo de bono ');
                    Log::info($tipo_de_bono );
    
                    if($tipo_de_bono ==1)
                    {
                        $tipo="Entre semana";
    
                      
    
                        if( $bono_tarde==1 && $bono_mañana==0)
                        {
                          
    
                            if($numero_bonos_entresemana==0)
                            {
                           
                                $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                $precio= $precio_entresemana;
                                $cantidad = 1;
                            }
    
                        }
                        elseif( $bono_tarde==0 && $bono_mañana>0)
                        {
                            
    
                            if($numero_bonos_entresemana==0)
                            {
                                $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                $precio= $precio_entresemana ;
                                $cantidad = 1;
                            }
                        }
                        elseif( $bono_tarde==1 && $bono_mañana>0)
                        {
                           
    
                            if($numero_bonos_entresemana==0)
                            {
                             $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 2;
                             $precio= $precio_entresemana + $precio_entresemana ;
                             $cantidad = 2;
     
                            }
                            if($numero_bonos_entresemana>0)
                            {
                             $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                             $precio= $precio_entresemana;
                             $cantidad = 1;
                            }
                        }

                    }
                    elseif($tipo_de_bono ==2)
                    {
                        $tipo="Finde semana";
    
                  
    
    
    
    
                        if( $bono_tarde==1 && $bono_mañana==0)
                        {
    
                            if($numero_bonos_findesemana==0)
                            {
                                $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                $precio= $precio_findesemana ;
                                $cantidad = 1;
    
                            }
    
                        }
                        elseif( $bono_tarde==0 && $bono_mañana>0)
                        {
                        
                            if($numero_bonos_findesemana==0)
                            {
                             $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                             $precio= $precio_findesemana ;
                             $cantidad = 1;
                            }
                        }
                        elseif( $bono_tarde==1 && $bono_mañana>0)
                        {
                            $precio= $precio_findesemana + $precio_findesemana  ;
    
                            if($numero_bonos_findesemana==0)
                            {
                                $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                $precio= $precio_findesemana + $precio_findesemana   ;
                                $cantidad = 2;
                            }
                            if($numero_bonos_findesemana>0)
                            {
                                $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                $precio= $precio_findesemana ;
                                $cantidad = 1;
                            }
                        }
                        
                    }

                }
                 
               
                $usuario_bono->save();

            
                $nuevo_bono_extra = new BonoExtra();
            
                $nuevo_bono_extra->barco_id= $barco_id;
                $nuevo_bono_extra->user_id= Auth::user()->id;
                $nuevo_bono_extra->tipo=$tipo;
                $nuevo_bono_extra->precio=$precio;
                $nuevo_bono_extra->cantidad=$cantidad;

              
                if($verificacion_ultimo_dia==false)
                {
                    $nuevo_bono_extra->ultimo_dia=0;

                }
                elseif($verificacion_ultimo_dia==true)
                {
                    $nuevo_bono_extra->ultimo_dia=1;
                    $nuevo_bono_extra->tipo_ultimo_dia=$tipo;

                }

    
    
                $nuevo_bono_extra->save();

                    // //envio de correo al administrador
                    $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                    // $name="david";
                    // $msg = "todo el mensaje";
                    $subject = "Nuevo bono extra pedido";
                    $for = "marnificwebcontrol@gmail.com";
                    Mail::send('mails/mail_bono',$datos, function($msj) use($subject,$for){
                        $msj->from("marnificwebcontrol@gmail.com","Marnific");
                        $msj->subject($subject);
                        $msj->to($for);
                    });

                    // //envio de correo al cliente
                    $datos2 = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                    // $name="david";
                    // $msg = "todo el mensaje";
                    $subject = "Nuevo bono extra pedido";
                    $for = Auth::user()->email;
                    Mail::send('mails/mail_bono',$datos2, function($msj) use($subject,$for){
                        $msj->from("marnificwebcontrol@gmail.com","Marnific");
                        $msj->subject($subject);
                        $msj->to($for);
                    });
        

    
    
                
                return response()->json(['success' => 'bono pedido',]);
            }
            elseif($diferente_trimestre ==true)
            {
                return response()->json(['success' => 'pedido diferente trimestre',]);

            }

        


           
        }
        else
        {
            return response()->json(['success' => 'pedido en curso',]);
        }

    }
    public function boton_bono_extra(Request $request)
    {
        Log::info('EN EL BOTON BONO EXTRA');
        Log::info('enta en bono  extra');
        $dia=$request->dia_semana;
        $barco_id=$request->txtBarcoID;
        $bono_tarde=$request->txtTarde;
        $bono_mañana=$request->txtMañana;
        $user_id =Auth::user()->id;
        $fecha_reserva = $request->txtFecha;
        $ultimo_dia  =$request->ultimo_dia;

      
        Log::info('DIA');
        Log::info($dia);
    

        $barco=Bono::where('barco_id',$barco_id)->first();
        $precio_entresemana =  $barco->precio;
        $precio_findesemana = $barco->precio_findesemana;
        $tipo="";
        $dia_completo=false;
        $guardar_bono =true;

        //sacamos informacion del barco 
        $barco_barco = Barco::where('id', $barco_id)->first();
        $nombre_barco = $barco_barco->nombre;
        $puerto = $barco_barco->puerto;
        $ciudad = $barco_barco->ciudad;
        

      //comprobamos que el usuario no tenga pedido más de 1 bono extra sin confirmar

        $bonos_extra=BonoExtra::where('user_id',Auth::user()->id)->get();

        $count_bonos=0;
        foreach($bonos_extra as $bx)
        {

            $confirmados = $bx->confirmar;

            if($confirmados=="no")
            {
                $count_bonos++;
            }
        }

        $reservas = Reserva::where('barco_id',$barco_id)->where('start', $fecha_reserva)->get();
        $count_reserva = 0;
        $res_mañana="";
        $res_tarde="";
        foreach($reservas as $res)
        {
            $count_reserva++;
            $res_mañana = $res->mañana;
            $res_tarde = $res->tarde;
  
  
        }
        if($count_reserva==2)
        {
            $dia_completo=true;
        }

        //comprobar numero de reservas del usuario
        $hoy = date("Y-m-d");

        $reservas_usuarios = Reserva::where('user_id',$user_id)->whereBetween('start', [$hoy, '2100-12-12'])->get();
        $bol_res_usuario = false;
        $count_res_usuario = 0;
        foreach($reservas_usuarios as $res_usuario)
        {
            
            $count_res_usuario++;
            $usuario_tarde = $res_usuario->tarde;
            $usuario_mañana = $res_usuario->mañana;

        
            if($usuario_tarde==1 && $usuario_mañana==1)
            {
                
                $count_res_usuario++;

                

            }

        }

        if($ultimo_dia==0 && $count_res_usuario>1)
        {
            $bol_res_usuario = true;

        }
        elseif($ultimo_dia==1)
        {
            $bol_res_usuario = false;

        }

        //comprobamos diass festivos

        $dias_festivos = DiaFestivo::all();
        $dias_festivos_count = 0;

        foreach($dias_festivos as $dia_festivo)
        {
            $dia_festivo_sin_horas = substr($dia_festivo->start, 0, 10);

            Log::info('dia festivo');
            Log::info($dia_festivo_sin_horas);
            
            if($fecha_reserva == $dia_festivo_sin_horas)
            {
                Log::info('concuerda');
                $dias_festivos_count++;
            }
            
        }

        log::info('fecha inicio');
        log::info($fecha_reserva);

        log::info('count dias festivos');
        log::info($dias_festivos_count);




      //toda la programacion
   
        if($bol_res_usuario == false)
        {
            if($count_reserva==0 || $count_reserva==1)
            {
            
                if($res_mañana==1 && $bono_mañana==1)
                {
                    return response()->json(['success' => 'Reservado Mañana',]);

                }
                elseif($res_tarde==1 && $bono_tarde==1)
                {
                    return response()->json(['success' => 'Reservado Tarde',]);

                }
                else
                {
                    if($count_bonos==0)
                    {
                    
                        if($dia=="sabado" || $dia=="domingo" || $dia=="viernes" )
                        {
                            if($dia=="viernes" && $bono_tarde==1 && $bono_mañana==0)
                            {
                                $tipo="Finde semana";
                                $cantidad = 1;
        
                            }
                            elseif($dia=="viernes" && $bono_tarde==0 && $bono_mañana==1)
                            {
                                $tipo="Entre semana";
                                $cantidad = 1;
        
                            }
                            elseif($dia=="viernes" && $bono_tarde==1 && $bono_mañana==1)
                            {
                                $tipo="Entre semana y Finde semana";
                                $cantidad = 2;
        
                            }
                            elseif($dia=="sabado" || $dia=="domingo" )
                            {
                                $tipo="Finde semana";
        
                                if( $bono_tarde==1 && $bono_mañana==1)
                                {
                                    $tipo="Finde semana dos franjas";
                                    $cantidad = 2;
        
        
        
                                }
                                else{
                                    $tipo="Finde semana";
                                    $cantidad = 1;
        
                                }
                                
        
                            }
        
                            
                        }
                        else
                        {
                            if( $bono_tarde==1 && $bono_mañana==1)
                            {
                                $tipo="Entre semana dos franjas";
                                $cantidad = 2;
        
        
                            }
                            else{
                                $tipo="Entre semana";
                                $cantidad = 1;
        
                            }
        
                            
        
                        }
        
                        Log::info('barco id ');
                        Log::info( $barco_id);
                        Log::info('user id ');
                        Log::info($user_id);
                        $precio=0;
        
                        if($tipo=="Finde semana")
                        {
                            $precio= $precio_findesemana;
                        }
                        elseif($tipo=="Entre semana")
                        {
                            $precio= $precio_entresemana ;
                        }
        
                        elseif($tipo=="Entre semana y Finde semana")
                        {
                            $precio= $precio_entresemana + $precio_findesemana ;
                        }
                        elseif($tipo=="Entre semana dos franjas")
                        {
                            $precio= $precio_entresemana + $precio_entresemana ;
                        }
        
                        elseif($tipo=="Finde semana dos franjas")
                        {
                            $precio= $precio_findesemana + $precio_findesemana ;
                        }
                        
                        
        
        
                        //Nueva funcionalidad
                        
                        //Sacar todo lo relacionado con los dias de fecha actual, fecha de reserva etc.....

                        //FECHA RESERVA
                        date_default_timezone_set('Europe/Madrid');

                        $fecha_start = substr($fecha_reserva, 0, 10);
                        $numeroDia = date('d', strtotime($fecha_start));
                        $dia = date('l', strtotime($fecha_start));
                        $mes = date('F', strtotime($fecha_start));
                        $anio = date('Y', strtotime($fecha_start));

                        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
                        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                        $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                        $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
                        $nombremes = str_replace($mes_EN, $mes_ES, $mes);
                        

                        //FECHA ACTUAL
                        $hoy = substr($hoy, 0, 10);
                        $numeroDia_actual = date('d', strtotime($hoy));
                        $dia_actual = date('l', strtotime($hoy));
                        $mes_actual = date('F', strtotime($hoy));
                        $anio_actual = date('Y', strtotime($hoy));

                        
                        $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);

                    


                        //comprobar que en que trimestre Se hace la reserva
                    
                    $trimestre="";
                        switch ($nombremes) {
                            case "Enero":
                                $trimestre="Primero";
                                break;
                            case "Febrero":
                                $trimestre="Primero";
                                break;
                            case "Marzo":
                                $trimestre="Primero";
                                break;
                            case "Abril":
                                $trimestre="Segundo";
                                break;
                            case "Mayo":
                                $trimestre="Segundo";
                                break;
                            case "Junio":
                                $trimestre="Segundo";
                                break; 
                            case "Julio":
                                $trimestre="Tercero";
                                break; 
                            case "Agosto":
                                $trimestre="Tercero";
                                break; 
                            case "Septiembre":
                                $trimestre="Tercero";
                                break; 
                            case "Octubre":
                                $trimestre="Cuarto";
                                break;
                            case "Noviembre":
                                $trimestre="Cuarto";
                                break;
                            case "Diciembre":
                                $trimestre="Cuarto";
                                break;
                                    
                        }

                        //comprobar en que trimestre estamos actualmente

                        $trimestre_actual="";
                        switch ($nombremes_actual) {
                            case "Enero":
                                $trimestre_actual="Primero";
                                break;
                            case "Febrero":
                                $trimestre_actual="Primero";
                                break;
                            case "Marzo":
                                $trimestre_actual="Primero";
                                break;
                            case "Abril":
                                $trimestre_actual="Segundo";
                                break;
                            case "Mayo":
                                $trimestre_actual="Segundo";
                                break;
                            case "Junio":
                                $trimestre_actual="Segundo";
                                break; 
                            case "Julio":
                                $trimestre_actual="Tercero";
                                break; 
                            case "Agosto":
                                $trimestre_actual="Tercero";
                                break; 
                            case "Septiembre":
                                $trimestre_actual="Tercero";
                                break; 
                            case "Octubre":
                                $trimestre_actual="Cuarto";
                                break;
                            case "Noviembre":
                                $trimestre_actual="Cuarto";
                                break;
                            case "Diciembre":
                                $trimestre_actual="Cuarto";
                                break;
                                    
                        }

                        $diferente_trimestre = false;

                        Log::info('TRIMESTRE ACTUAL');
                        Log::info($trimestre_actual);
                        Log::info('TRIMESTRE RESERVA ');
                        Log::info($trimestre);

                        if($trimestre_actual == $trimestre)
                        {
                        $diferente_trimestre = false;
                        }
                        elseif($trimestre_actual != $trimestre)
                        {
                            $diferente_trimestre = true;

                        }
                    
                        Log::info('TRIMESTRE');
                        Log::info($diferente_trimestre);

                    
                        //hastas aqui
                
                
                    
                        
                    
                
                    
        
        
                        $usuario_bono = User::where('id', Auth::user()->id)->first();
                        $numero_bonos_findesemana = $usuario_bono->numero_bonos_findesemana;
                        $numero_bonos_entresemana = $usuario_bono->numero_bonos_entresemana;

                        $verificacion_ultimo_dia=false;
                        
                        if($dias_festivos_count==0 && $diferente_trimestre==false )
                        {
                            if($ultimo_dia == 0)
                            {
                                $verificacion_ultimo_dia=false;

                                Log::info('ULLLLLLTIMO DIAAAAAAAA NOOOOO ');
                                Log::info($ultimo_dia);
                            
                                Log::info('tipo de bonos a consumir ');
                                Log::info($tipo);
            
                                if($tipo=="Finde semana")
                                {
                                    if($numero_bonos_findesemana==0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
    
                                    }
                                    if($numero_bonos_findesemana>0)
                                    {
                                        return response()->json(['success' => 'Hay bonos del findesemana',]);
    
                                        $guardar_bono = false;
                                    }
                                
            
                                }
                                elseif($tipo=="Entre semana")
                                {
                                    if($numero_bonos_entresemana==0)
                                    {
                                        $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                        $precio = $precio_entresemana;
                                        $cantidad = 1;
                                        $tipo="Entre semana";
    
                                    }
    
                                    if($numero_bonos_entresemana>0)
                                    {
                                        return response()->json(['success' => 'Hay bonos del entresemana',]);
                                        $guardar_bono = false;
    
                                    }
                                    
                                }
                                elseif($tipo=="Entre semana y Finde semana")
                                {
                                    // if($numero_bonos_entresemana==0)
                                    // {
                                    // $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                    // }
                                    // if($numero_bonos_findesemana==0)
                                    // {
                                    // $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                    // }
    
                                    if($numero_bonos_findesemana>0 && $numero_bonos_entresemana==0 )
                                    {
                                        $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                        $precio = $precio_entresemana;
                                        $cantidad = 1;
                                        $tipo="Entre semana";
    
                                    }
                                    elseif($numero_bonos_findesemana==0 && $numero_bonos_entresemana>0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
    
                                    }
                                    elseif($numero_bonos_findesemana==0 && $numero_bonos_entresemana==0 )
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                        $precio = $precio_findesemana + $precio_entresemana;
                                        $cantidad = 2;
                                        $tipo="Entre semana y finde semana";
    
    
                                    }
                                    elseif($numero_bonos_findesemana>0 && $numero_bonos_entresemana>0)
                                    {
                                        return response()->json(['success' => 'Hay bonos del entresemana y de findesemana',]);
                                        $guardar_bono = false;
    
    
                                    }
            
            
                                }
                                elseif($tipo=="Entre semana dos franjas")
                                {
                                    if($numero_bonos_entresemana==0)
                                    {
                                        $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 2;
    
                                        $precio = $precio_entresemana + $precio_entresemana;
                                        $cantidad = 2;
                                        $tipo="Entre semana";
    
                                    }
                                    elseif($numero_bonos_entresemana==1)
                                    {
                                        $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                        $precio = $precio_entresemana;
                                        $cantidad = 1;
                                        $tipo="Entre semana";
                                    }
                                    elseif($numero_bonos_entresemana>1)
                                    {
                                        return response()->json(['success' => 'Hay bonos del entresemana',]);
                                        $guardar_bono = false;
    
                                    }
    
    
                                }
            
                                elseif($tipo=="Finde semana dos franjas")
                                {
                                    if($numero_bonos_findesemana==0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                        $precio = $precio_findesemana + $precio_findesemana;
                                        $cantidad = 2;
                                        $tipo="Finde semana";
                                    }
                                    elseif($numero_bonos_findesemana==1)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
    
                                    }
                                    elseif($numero_bonos_findesemana>1)
                                    {
                                        return response()->json(['success' => 'Hay bonos del findesemana',]);
                                        $guardar_bono = false;
    
                                    }
                                }
                            }
                            elseif($ultimo_dia == 1)
                            {
                                $verificacion_ultimo_dia=true;

                                Log::info('ULLLLLLTIMO DIAAAAAAAA en bonos es 1');
                        
                        
    
                                $tipo_de_bono   = $request->tipo_bono;
                                Log::info('TIPO DE BONO A USAR ');
                                Log::info($tipo_de_bono);
    
                                if($tipo_de_bono ==1)
                                {
                                    $tipo="Entre semana";
    
                                    if( $bono_tarde==1 && $bono_mañana==0)
                                    {
                                          
                                        if($numero_bonos_entresemana==0)
                                        {
                                    
                                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                            $precio= $precio_entresemana;
                                            $cantidad = 1;
                                            $tipo="Entre semana";
    
    
    
                                        }
            
                                    }
                                    elseif( $bono_tarde==0 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_entresemana==0)
                                        {
                                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                            $precio= $precio_entresemana;
                                            $cantidad = 1;
                                            $tipo="Entre semana";
    
                                        }
                                    }
                                    elseif( $bono_tarde==1 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_entresemana==0)
                                        {
                                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 2;
                                            $precio= $precio_entresemana + $precio_entresemana ;
                                            $cantidad = 2;
                                            $tipo="Entre semana";
    
                                        }
                                        if($numero_bonos_entresemana>0)
                                        {
                                             $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                             $precio= $precio_entresemana;
                                             $cantidad = 1;
                                             $tipo="Entre semana";
                                        }
                                    }
            
                                }
                                elseif($tipo_de_bono ==2)
                                {
                                    $tipo="Finde semana";
                                    if( $bono_tarde==1 && $bono_mañana==0)
                                    {
    
                                        if($numero_bonos_findesemana==0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                            $precio= $precio_findesemana;
                                            $cantidad = 1;
                                            $tipo="Finde semana";
    
                                        }
            
                                    }
                                    elseif( $bono_tarde==0 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_findesemana==0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                            $precio= $precio_findesemana;
                                            $cantidad = 1;
                                            $tipo="Finde semana";
    
                                        }
                                    }
                                    elseif( $bono_tarde==1 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_findesemana==0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                            $precio= $precio_findesemana + $precio_findesemana ;
                                            $cantidad = 2;
                                            $tipo="Finde semana";
    
                                        }
                                        if($numero_bonos_findesemana>0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                            $precio= $precio_findesemana;
                                            $cantidad = 1;
                                            $tipo="Finde semana";
    
                                        }
                                    }
            
                                }
                                
            
                            }
                          
                            
                            if($guardar_bono==true)
                            {
                                
                                $nuevo_bono_extra = new BonoExtra();
                            
                                $nuevo_bono_extra->barco_id= $barco_id;
                                $nuevo_bono_extra->user_id= Auth::user()->id;
                                $nuevo_bono_extra->tipo=$tipo;
                                $nuevo_bono_extra->precio=$precio;
                                $nuevo_bono_extra->cantidad=$cantidad;

                            
                                if($verificacion_ultimo_dia==false)
                                {
                                    $nuevo_bono_extra->ultimo_dia=0;
                
                                }
                                elseif($verificacion_ultimo_dia==true)
                                {
                                    $nuevo_bono_extra->ultimo_dia=1;
                                    $nuevo_bono_extra->tipo_ultimo_dia=$tipo;
                
                                }
                
                
                
                                $nuevo_bono_extra->save();
    
                                $usuario_bono->save();

                                // //envio de correo al administrador
                                $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                                // $name="david";
                                // $msg = "todo el mensaje";
                                $subject = "Nuevo bono extra pedido";
                                $for = "marnificwebcontrol@gmail.com";
                                Mail::send('mails/mail_bono',$datos, function($msj) use($subject,$for){
                                    $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                    $msj->subject($subject);
                                    $msj->to($for);
                                });

                                //   //envio de correo al cliente
                                  $datos2 = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                                  // $name="david";
                                  // $msg = "todo el mensaje";
                                  $subject = "Nuevo bono extra pedido";
                                  $for = Auth::user()->email;
                                  Mail::send('mails/mail_bono',$datos2, function($msj) use($subject,$for){
                                      $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                      $msj->subject($subject);
                                      $msj->to($for);
                                  });
                            }

                        }
                        //dias festivos
                        elseif($dias_festivos_count>0 && $diferente_trimestre==false)
                        {
                            if($ultimo_dia == 0)
                            {
                                if($tipo=="Finde semana")
                                {
                                    if($numero_bonos_findesemana==0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
    
                                    }
                                    if($numero_bonos_findesemana>0)
                                    {
                                        return response()->json(['success' => 'Hay bonos del findesemana',]);
    
                                        $guardar_bono = false;
                                    }
                                
            
                                }
                                elseif($tipo=="Entre semana")
                                {
                                    if($numero_bonos_findesemana==0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
                                    }
    
                                    if($numero_bonos_findesemana>0)
                                    {
                                        return response()->json(['success' => 'Hay bonos del entresemana',]);
                                        $guardar_bono = false;
    
                                    }
                                    
                                }
                                elseif($tipo=="Entre semana y Finde semana")
                                {
    
                                    if($numero_bonos_findesemana==1)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
                                    }
    
                                    
                                 
                                    elseif($numero_bonos_findesemana==0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                        $precio = $precio_findesemana + $precio_findesemana;
                                        $cantidad = 2;
                                        $tipo="Finde semana";
    
    
                                    }
                                    elseif($numero_bonos_findesemana>1)
                                    {
                                        return response()->json(['success' => 'Hay bonos del entresemana y de findesemana',]);
                                        $guardar_bono = false;
    
    
                                    }
            
            
                                }
                                elseif($tipo=="Entre semana dos franjas")
                                {
                                    if($numero_bonos_findesemana==0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                        $precio = $precio_findesemana + $precio_findesemana;
                                        $cantidad = 2;
                                        $tipo="Finde semana";
    
    
                                    }
                                    elseif($numero_bonos_findesemana==1)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
                                    }
                                    elseif($numero_bonos_findesemana>1)
                                    {
                                        return response()->json(['success' => 'Hay bonos del entresemana',]);
                                        $guardar_bono = false;
    
                                    }
    
    
                                }
            
                                elseif($tipo=="Finde semana dos franjas")
                                {
                                    if($numero_bonos_findesemana==0)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                        $precio = $precio_findesemana + $precio_findesemana;
                                        $cantidad = 2;
                                        $tipo="Finde semana";
                                    }
                                    elseif($numero_bonos_findesemana==1)
                                    {
                                        $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                        $precio = $precio_findesemana;
                                        $cantidad = 1;
                                        $tipo="Finde semana";
    
                                    }
                                    elseif($numero_bonos_findesemana>1)
                                    {
                                        return response()->json(['success' => 'Hay bonos del findesemana',]);
                                        $guardar_bono = false;
    
                                    }
                                }
                            }
                            if($ultimo_dia == 1)
                            {
                                $verificacion_ultimo_dia=true;

                                Log::info('ULLLLLLTIMO DIAAAAAAAA en bonos es 1');
                        
                        
    
                                $tipo_de_bono   = $request->tipo_bono;
                                Log::info('TIPO DE BONO A USAR ');
                                Log::info($tipo_de_bono);
    
                                if($tipo_de_bono ==1)
                                {
                                    $tipo="Entre semana";
    
                                    if( $bono_tarde==1 && $bono_mañana==0)
                                    {
                                          
                                        if($numero_bonos_entresemana==0)
                                        {
                                    
                                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                            $precio= $precio_entresemana;
                                            $cantidad = 1;
                                            $tipo="Entre semana";
    
    
    
                                        }
            
                                    }
                                    elseif( $bono_tarde==0 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_entresemana==0)
                                        {
                                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                            $precio= $precio_entresemana;
                                            $cantidad = 1;
                                            $tipo="Entre semana";
    
                                        }
                                    }
                                    elseif( $bono_tarde==1 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_entresemana==0)
                                        {
                                            $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 2;
                                            $precio= $precio_entresemana + $precio_entresemana ;
                                            $cantidad = 2;
                                            $tipo="Entre semana";
    
                                        }
                                        if($numero_bonos_entresemana>0)
                                        {
                                             $usuario_bono->numero_bonos_entresemana = $numero_bonos_entresemana + 1;
                                             $precio= $precio_entresemana;
                                             $cantidad = 1;
                                             $tipo="Entre semana";
                                        }
                                    }
            
                                }
                                elseif($tipo_de_bono ==2)
                                {
                                    $tipo="Finde semana";
                                    if( $bono_tarde==1 && $bono_mañana==0)
                                    {
    
                                        if($numero_bonos_findesemana==0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                            $precio= $precio_findesemana;
                                            $cantidad = 1;
                                            $tipo="Finde semana";
    
                                        }
            
                                    }
                                    elseif( $bono_tarde==0 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_findesemana==0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                            $precio= $precio_findesemana;
                                            $cantidad = 1;
                                            $tipo="Finde semana";
    
                                        }
                                    }
                                    elseif( $bono_tarde==1 && $bono_mañana==1)
                                    {
                                        
    
                                        if($numero_bonos_findesemana==0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 2;
                                            $precio= $precio_findesemana + $precio_findesemana ;
                                            $cantidad = 2;
                                            $tipo="Finde semana";
    
                                        }
                                        if($numero_bonos_findesemana>0)
                                        {
                                            $usuario_bono->numero_bonos_findesemana = $numero_bonos_findesemana + 1;
                                            $precio= $precio_findesemana;
                                            $cantidad = 1;
                                            $tipo="Finde semana";
    
                                        }
                                    }
            
                                }
                                
            
                            }

                            if($guardar_bono==true)
                            {
                                
                                $nuevo_bono_extra = new BonoExtra();
                            
                                $nuevo_bono_extra->barco_id= $barco_id;
                                $nuevo_bono_extra->user_id= Auth::user()->id;
                                $nuevo_bono_extra->tipo=$tipo;
                                $nuevo_bono_extra->precio=$precio;
                                $nuevo_bono_extra->cantidad=$cantidad;

                            
                                if($verificacion_ultimo_dia==false)
                                {
                                    $nuevo_bono_extra->ultimo_dia=0;
                
                                }
                                elseif($verificacion_ultimo_dia==true)
                                {
                                    $nuevo_bono_extra->ultimo_dia=1;
                                    $nuevo_bono_extra->tipo_ultimo_dia=$tipo;
                
                                }
                
                
                
                                $nuevo_bono_extra->save();
    
                                $usuario_bono->save();

                                // //envio de correo al adminsitrador
                                $datos = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                                // $name="david";
                                // $msg = "todo el mensaje";
                                $subject = "Nuevo bono extra pedido";
                                $for = "marnificwebcontrol@gmail.com";
                                Mail::send('mails/mail_bono',$datos, function($msj) use($subject,$for){
                                    $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                    $msj->subject($subject);
                                    $msj->to($for);
                                });

                                //   //envio de correo al cliente
                                  $datos2 = ['name'=>Auth::user()->name, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$bono_tarde,'mañana'=>$bono_mañana];
                                  // $name="david";
                                  // $msg = "todo el mensaje";
                                  $subject = "Nuevo bono extra pedido";
                                  $for = Auth::user()->email;
                                  Mail::send('mails/mail_bono',$datos2, function($msj) use($subject,$for){
                                      $msj->from("marnificwebcontrol@gmail.com","Marnific");
                                      $msj->subject($subject);
                                      $msj->to($for);
                                  });


                            }
                            

                        }

                        elseif($diferente_trimestre==true)
                        {
                            return response()->json(['success' => 'pedido diferente trimestre',]);
                        }
        
                    

                       
                                        
        
        
                        
                        return response()->json(['success' => 'bono pedido',]);
                    }
                    else
                    {
                        return response()->json(['success' => 'pedido en curso',]);
                    }
            

                }
            

            }
            elseif($count_reserva == 2)
            {
            

                return response()->json(['success' => 'Reservado todo el dia',]);
                

            }

        }
        elseif($bol_res_usuario==true)
        {
            return response()->json(['success' => 'Dos reservas pendientes',]);

        }
        
          

    }
      
     


    

    public function editar_datos_personales(Request $request, $id)
    {
        Log::info("en editar datos personales");

        $nombre = $request->nombre;
        $apellidos = $request->apellidos;
        $email = $request->email;
        $dni = $request->dni;
        $telefono = $request->telefono;

        $usuario = User::where('id',$id)->first();

        $usuario->name = $nombre;
        $usuario->apellidos = $apellidos;
        $usuario->dni = $dni;
        $usuario->telefono = $telefono;

        $usuario->save();

        return response()->json(['success' => 'usuario editado',]);

        




        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // Log::info('show reservas por barco');
        $data['reservas']= Reserva::where('barco_id',Auth::user()->barco_id )->get();
        // Log::info($data['reservas']);
        return response()->json($data['reservas']);
    }
    public function dias_festivos_show()
    {
        Log::info('entraaaaaaa en showwwww');
        $data['dias_festivos']= DiaFestivo::all();
        return response()->json($data['dias_festivos']);
        // print_r($data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
