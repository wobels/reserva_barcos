<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Reserva;
use App\Models\User;
use App\Models\Bono;
use App\Models\Barco;

use App\Models\DiaFestivo;

use App\Models\DescontarBono;

use App\Models\BonoExtra;
use Auth;
use Mail;



use App\DataTables\UsuariosAdminDataTable;
use App\DataTables\UsuarioIndividualDataTable;
use App\DataTables\ReservasAdminDataTable;
use App\DataTables\ReservasAdminAntiguasDataTable;

use App\DataTables\BonoExtraAdminDataTable;
use App\DataTables\BonoExtraAdminAceptadoDataTable;
use App\DataTables\BonoExtraAdminDenegadoDataTable;
use App\DataTables\BonoDescontarAdminDataTable;
use App\DataTables\IncidenciasCheckoutDataTable;
use App\DataTables\IncidenciasCheckingDataTable;
use App\DataTables\IncidenciasCheckingAntiguoDataTable;
use App\DataTables\IncidenciasCheckoutAntiguoDataTable;
use App\DataTables\SuscripcionesDataTable;


use App\Models\ImagenIncidencia;

class Obj
{
    public $name;
    public $path;
    public $column;

}

class ConfiguracionController extends Controller
{
    public function automatizacion_eliminar_imagenes(){
        if(Auth::user()->id === 1)
        {
            $mock_inicio = '2023'.'-'.'06'.'-'.'01';
            $mock_fin = '2023'.'-'.'12'.'-'.'31';
            $reservas = Reserva::whereBetween('start', [ $mock_inicio, $mock_fin])->orderBy('start', 'ASC')->get();
             foreach($reservas as $reserva)
            {
                $gasolina  = new Obj();
                $gasolina ->name = $reserva->imagen_gasolina;
                $gasolina ->path =  public_path()."/imagenes/checkings/gasolina/".$reserva->imagen_gasolina;  //ALERT CAMBIAR DIRECCION DE BARRA A /
                $gasolina ->column = 'imagen_gasolina'; 
                
                $horas = new Obj();
                $horas->name = $reserva->imagen_horas;
                $horas->path =  public_path()."/imagenes/checkings/horas/".$reserva->imagen_horas;
                $horas ->column = 'imagen_horas'; 
               
                $bateria = new Obj();
                $bateria->name = $reserva->imagen_bateria;
                $bateria->path = public_path()."/imagenes/checkouts/bateria/".$reserva->imagen_bateria;
                $bateria ->column = 'imagen_bateria'; 
    
               
                $gasolina_checkout  = new Obj();
                $gasolina_checkout  ->name = $reserva->imagen_gasolina_checkout;
                $gasolina_checkout  ->path = public_path()."/imagenes/checkouts/gasolina/".$reserva->imagen_gasolina_checkout;
                $gasolina_checkout ->column = 'imagen_gasolina_checkout'; 
               
                $horas_checkout = new Obj();
                $horas_checkout->name = $reserva->imagen_horas_checkout;
                $horas_checkout->path =  public_path()."/imagenes/checkouts/horas/".$reserva->imagen_horas_checkout;
                $horas_checkout ->column = 'imagen_horas_checkout'; 
                
                $arrayDelete = array($gasolina, $horas, $bateria, $gasolina_checkout, $horas_checkout);
    
                foreach($arrayDelete as $delete){
                    
                    if($delete->name != '' && $delete->name != null ){
                        unlink($delete->path);
                        $reserva[$delete->column] =  null;
                        $reserva->save();
                        $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $reserva->id)->get();
                        foreach($imagenes_incidencias as $imagen){
                            $image_checking = strrpos($imagen->nombre_archivo, "Incidencias_checking");
                            $image_checkout = strrpos($imagen->nombre_archivo, "Incidencias_checkout");
                            if ($image_checking === 0) { 
                                $path = public_path()."/imagenes/checkings/incidencias_checking/".$imagen->nombre_archivo;
                                unlink($path);
                            }
                            if ($image_checkout === 0) { 
                                $path = public_path()."/imagenes/checkings/incidencias_checkout/".$imagen->nombre_archivo;
                                unlink($path);
                            }
                            $imagen->delete();
                        }
                        
                    }
                }
            }
        }
       
    }

    public function automatizacion(){

        $usuarios = User::all();

        if(Auth::user()->id == 1)
        {
        Log::info('CUSTOMIZATION');

            foreach($usuarios as $usuario)
            {
                //cargamos los bonos dependiendo el tipo de usuario

                //normal
                if($usuario->premium == 1)
                {     
                    $usuario->numero_bonos_entresemana = 4;
                    $usuario->numero_bonos_findesemana = 3;
                    $usuario->numero_bonos_ultima_hora = 2;

    
                    $usuario->save();
                }//premium
                elseif($usuario->premium==2)
                {
                    $usuario->numero_bonos_entresemana = 9;
                    $usuario->numero_bonos_findesemana = 6;
                    $usuario->numero_bonos_ultima_hora = 4;

    
                    $usuario->save();
                }
         
                $descontar_bono = DescontarBono::all();

                //descontamos los bonos en el caso que haya que descontar
                foreach($descontar_bono as $descontar)
                {
      
    

                    if($usuario->id == $descontar->user_id)
                    {
                        $cantidad_findesemana = $descontar->cantidad_findesemana;
                        $cantidad_entresemana = $descontar->cantidad_entresemana;

                        

                        if($cantidad_findesemana!=null)
                        {
                           Log::info('numeros entresenama');
                           Log::info($usuario->numero_bonos_entresemana);
                           Log::info('numeros findesemana');
                           Log::info($usuario->numero_bonos_findesemana);
                           Log::info('cantidad a descontar entresmena');
                           Log::info( $cantidad_entresemana);
                           Log::info(' cantidad a descontar findesemana');
                           Log::info( $cantidad_findesemana);

                            $usuario->numero_bonos_findesemana = $usuario->numero_bonos_findesemana - $cantidad_findesemana;
            
                            $usuario->save();
                        }
                        if($cantidad_entresemana!=null)
                        {
                            Log::info('numeros entresenama');
                            Log::info($usuario->numero_bonos_entreseman);
                            Log::info('numeros findesemana');
                            Log::info($usuario->numero_bonos_findesemana);
                            Log::info('cantidad a descontar entresmena');
                            Log::info( $cantidad_entresemana);
                            Log::info(' cantidad a descontar findesemana');
                            Log::info( $cantidad_findesemana);

                            $usuario->numero_bonos_entresemana = $usuario->numero_bonos_entresemana - $cantidad_entresemana;
            
                            $usuario->save();
                        }

                        $descontar->delete();
                    }
                  

                }
            }
        }
    }
    public function index(UsuariosAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }

        $error="hola";

        return $dataTable->render('/configuracion/index',[
                'reservas_checking' => $reservas_checking,
                'reservas_checkout' => $reservas_checkout,
                'count_bonos_extras' => $count_bonos_extras,
                'count_renovaciones'=>$count_renovaciones,
          
                
       ]);

  
      
    }
    public function index_renovaciones(SuscripcionesDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }


        $error="hola";

        return $dataTable->render('/configuracion/index_renovaciones',[
                'reservas_checking' => $reservas_checking,
                'reservas_checkout' => $reservas_checkout,
                'count_bonos_extras' => $count_bonos_extras,
                'count_renovaciones'=>$count_renovaciones,


          
                
       ]);

  
      
    }
    public function index_reserva(ReservasAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }

      

        return $dataTable->render('/configuracion/index_reservas',[
            'reservas_checking' => $reservas_checking,
          
            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,

                
       ]);

  
      
    }
    public function index_reserva_antiguas(ReservasAdminAntiguasDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }

      

        return $dataTable->render('/configuracion/index_reservas_antiguas',[
            'reservas_checking' => $reservas_checking,

            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,

                
       ]);

  
      
    }
    public function index_bonos_descontar(BonoDescontarAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }
        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }

      

        return $dataTable->render('/configuracion/index_bonos_descontar',[
            'reservas_checking' => $reservas_checking,
            'reservas_checkout' => $reservas_checkout,

            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,

          
                
       ]);

  
      
    }
    public function index_bonos_extras(BonoExtraAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }



        return $dataTable->render('/configuracion/index_bonos',[
            'reservas_checking' => $reservas_checking,
            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,

          
                
       ]);

  
      
    }
    public function index_bonos_extras_aceptado(BonoExtraAdminAceptadoDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }



        return $dataTable->render('/configuracion/index_bonos_aceptado',[
            'reservas_checking' => $reservas_checking,

            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,

                
       ]);

  
      
    }

    public function index_bonos_extras_denegado(BonoExtraAdminDenegadoDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }
      
        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }


        return $dataTable->render('/configuracion/index_bonos_denegado',[
            'reservas_checking' => $reservas_checking,
            'reservas_checkout' => $reservas_checkout,

            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,

          
                
       ]);

  
      
    }

    public function index_barcos(BonoExtraAdminDenegadoDataTable $dataTable)
    {
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }
      
        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }




        $barcos['barcos']=Barco::all();
        $edit=true;
        $users['users']= User::all();
        
        return view('homeAdmin', [
            'users'=>$users['users']= User::all(), 
            'barcos'=>$barcos['barcos']=Barco::all(), 
            'edit'=>true,
            'reservas_checking' => $reservas_checking,
            'reservas_checkout' => $reservas_checkout,

            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,
            ]);

    }
    public function agregar_dia_festivo(Request $request)
    {
        Log::info('Agregar dia festivo');

        $dia = New DiaFestivo();
        $dia->start = $request->start;
        $dia->save();
        return response()->json(['success' => 'dia festivo agregado',]);


    }

    public function aceptar_bono_extra($id)
    {
        Log::info('EN ACEPTAR BONO EXTRA');
        //sacamos toda la información

        $bono_extra = BonoExtra::where('id', $id)->first();

        $usuario = $bono_extra->user_id;
        $tipo = $bono_extra->tipo;
        $cantidad = $bono_extra->cantidad;
        $confirmar = $bono_extra->confirmar;
        $precio = $bono_extra->precio;

        //sacar id de la reserva para cambiarle el tipo de reserva bono extra a 0 para que el usuario lo pueda eliminar
        
        

        $reserva = Reserva::where('id',$bono_extra->reserva_id)->first();

        $reserva->reserva_bono_extra=0;
        $reserva->save();

        //sacamos datos de la reserva

        $fecha_start = $reserva->start;
        $tarde=$reserva->tarde;
        $mañana=$reserva->mañana;

        //sacamos datos del barrco

        $barco = Barco::where('id', $reserva->barco_id)->first();

        $nombre_barco = $barco->nombre;
        $puerto = $barco->puerto;
        $ciudad = $barco->ciudad;

        //sacamos informacion del cliente

        $cliente = User::where('id', $reserva->user_id)->first();

        $name_cliente=$cliente->name;
        $email_cliente=$cliente->email;




        
        //Cargamos los nuevos bonos al usuario

        // $usuario = User::where('id', $usuario)->first();

        if($confirmar=="no")
        {
         

    
            //cambiamos las columnas que estaçn por defecto
    
            $bono_extra->estado = "Pagado";
            $bono_extra ->confirmar = "si";
    
            $bono_extra->save();


            // //envio de correo al cliente
            $datos = ['name'=>$name_cliente, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$tarde,'mañana'=>$mañana];
            // $name="david";
            // $msg = "todo el mensaje";
            $subject = "Bono extra confirmado";
            $for = $email_cliente;
            Mail::send('mails/mail_bono_confirmacion',$datos, function($msj) use($subject,$for){
                $msj->from("web@marnific.com","Marnific");
                $msj->subject($subject);
                $msj->to($for);
            });

               
  
    
            return redirect('/configuracion/bonos_extras');

            
    

        }
        elseif($confirmar=="si" || $confirmar=="denegado")
        {
            return redirect('/configuracion/bonos_extras');

        }

    //  //envio de correo al cliente
     $datos = ['name'=>$name_cliente, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$tarde,'mañana'=>$mañana];
     // $name="david";
     // $msg = "todo el mensaje";
     $subject = "Bono extra confirmado";
     $for = $email_cliente;
     Mail::send('mails/mail_bono_confirmacion',$datos, function($msj) use($subject,$for){
         $msj->from("web@marnific.com","Marnific");
         $msj->subject($subject);
         $msj->to($for);
     });
      
        return redirect('/configuracion/bonos_extras');
    }

    public function denegar_bono_extra($id)
    {
        Log::info('EN DENEGAR BONO EXTRA');

         //denegamos bono
         $bono_extra = BonoExtra::where('id', $id)->first();

         $bono_extra->estado = "No pagado";
         $bono_extra ->confirmar = "Denegado";
         
 
         $bono_extra->save();
         //sacamos informacion bono exta

         $precio= $bono_extra->precio;
         $cantidad = $bono_extra->cantidad;
         $tipo = $bono_extra->tipo;
         $bono_tarde = $bono_extra->tarde;
         $bono_mañana= $bono_extra->mañana;
         $bono_ultimo_dia= $bono_extra->ultimo_dia;
         $bono_tipo_ultimo_dia= $bono_extra->tipo_ultimo_dia;
         



        

        $reserva = Reserva::where('id', $bono_extra->reserva_id)->first();

        //sacamos datos de la reserva

        $fecha_start = $reserva->start;
        $tarde=$reserva->tarde;
        $mañana=$reserva->mañana;
        $imagen_gasolina = $reserva->imagen_gasolina;
        $imagen_horas = $reserva->imagen_horas;

        $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
        $imagen_horas_checkout = $reserva->imagen_horas_checkout;
        $imagen_bateria = $reserva->imagen_bateria;


        $dia_festivo = $reserva->dia_festivo;

        //sacamos datos del barrco

        $barco = Barco::where('id', $reserva->barco_id)->first();

        $nombre_barco = $barco->nombre;
        $puerto = $barco->puerto;
        $ciudad = $barco->ciudad;

        //sacamos informacion del cliente

        $cliente = User::where('id', $reserva->user_id)->first();

        $name_cliente=$cliente->name;
        $email_cliente=$cliente->email;

        //comprobamos que dia de la semana es la fecha

        $fecha_start = substr($fecha_start, 0, 10);
        $numeroDia = date('d', strtotime($fecha_start));
        $dia = date('l', strtotime($fecha_start));
        $mes = date('F', strtotime($fecha_start));
        $anio = date('Y', strtotime($fecha_start));

        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $nombremes = str_replace($mes_EN, $mes_ES, $mes);



      

        $borrar_reserva=false;
        
        //si la reserva esta complta por el mismo usuario, si el bono comprado es 1 de las dos reservas no es festivo y tampoco ultimahora
        if($tarde==1 && $mañana==1 && $cantidad==1 && $dia_festivo==0 && $bono_ultimo_dia==0 )
        {
            Log::info('aqui1');
     
           if($tipo=="Finde semana")
           {
       
                
                if($nombredia=="Viernes" )
                {

                    $cliente->numero_bonos_entresemana= $cliente->numero_bonos_entresemana+1;
                  
                }
            
                elseif($nombredia=="Sábado" || $nombredia=="Domingo" )
                {
                    $cliente->numero_bonos_findesemana= $cliente->numero_bonos_findesemana+1;
                    
                }
                else{
                    $cliente->numero_bonos_findesemana= $cliente->numero_bonos_findesemana+1;

                }
              
               


                $cliente->save();

                
           }
           elseif($tipo=="Entre semana")
           {
                if($nombredia=="Viernes")
                {
                    $cliente->numero_bonos_findesemana= $cliente->numero_bonos_findesemana+1;

                
                }
           
                else
                {
                    $cliente->numero_bonos_entresemana= $cliente->numero_bonos_entresemana+1;

                    
                }
           


                $cliente->save();

            
           }
           $borrar_reserva=true;

        }
        //es ultima hora
        elseif($tarde==1 && $mañana==1 && $cantidad==1 && $dia_festivo==0 && $bono_ultimo_dia==1)
        {
            Log::info('aqui2');

            if($bono_tipo_ultimo_dia=="Entre semana")
            {
                $cliente->numero_bonos_entresemana= $cliente->numero_bonos_entresemana+1;

                

            }
            elseif($bono_tipo_ultimo_dia=="Finde semana")
            {
                $cliente->numero_bonos_findesemana= $cliente->numero_bonos_findesemana+1;
                
            }

            $borrar_reserva=true;

            $cliente->save();

           

        }
        //es festivo y no es ultima hora
        elseif($tarde==1 && $mañana==1 && $cantidad==1 && $dia_festivo==1 && $bono_ultimo_dia==0)
        {
            Log::info('aqui3');

            $cliente->numero_bonos_findesemana= $cliente->numero_bonos_findesemana+1;
            $cliente->save();
           $borrar_reserva=true;

                
        }
        //si es festivo y ultima hora

        elseif($tarde==1 && $mañana==1 && $cantidad==1 && $dia_festivo==1 && $bono_ultimo_dia==1)
        {
            Log::info('aqui4');

            if($bono_tipo_ultimo_dia=="Entre semana")
            {
                $cliente->numero_bonos_entresemana= $cliente->numero_bonos_entresemana+1;

                

            }
            elseif($bono_tipo_ultimo_dia=="Finde semana")
            {
                $cliente->numero_bonos_findesemana= $cliente->numero_bonos_findesemana+1;

                
            }
            $cliente->save();
            $borrar_reserva=true;

        }
        //si la reserva completa es pedida por bonos

        elseif($tarde==1 && $mañana==1 && $cantidad==2)
        {
           $borrar_reserva=true;

        }
        //si la reserva solo tiene una franja y es compra de bonos
        elseif($tarde==1 && $mañana==0 && $cantidad==1)
        {
            
                $borrar_reserva=true;
            
            

        }

        elseif($tarde==0 && $mañana==1 && $cantidad==1)
        {
            
                $borrar_reserva=true;
            
            

        }

        //borrar reserva
        if($borrar_reserva==true)
        {
            //nueva funcionalidad

            $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $bono_extra->reserva_id)->get();

            foreach($imagenes_incidencias as $imgIn)
            {
                $nombre_archivo = $imgIn->nombre_archivo;
                $pos = strpos($nombre_archivo, 'checking');
                $pos2 = strpos($nombre_archivo, 'checkout');
    
    
    
                if($pos!=0)
                {
                    $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                    unlink($image_path);
                }
                if($pos2!=0)
                {
                    $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                    unlink($image_path);
                }
                
            }
    
            ImagenIncidencia::where('reserva_id', $bono_extra->reserva_id)->delete();

            
            if($imagen_gasolina !="")
            {

                $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";

        
                unlink($image_horas);
                unlink($image_gasolina);


        
            }


            if($imagen_gasolina_checkout !="")
            {

                $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                unlink($imagen_bateria);
                unlink($imagen_horas_checkout);
                unlink($imagen_gasolina_checkout);
           

        
            }


            $reserva_denegar = Reserva::where('id',$bono_extra->reserva_id)->delete();


        }
       
       
      

        //   //envio de correo al cliente
          $datos = ['name'=>$name_cliente, 'msg'=>'todo el mensage','fecha'=>$fecha_start,'precio'=>$precio, 'cantidad'=>$cantidad,'tipo'=>$tipo,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto, 'ciudad'=>$ciudad,'tarde'=>$tarde,'mañana'=>$mañana];
          // $name="david";
          // $msg = "todo el mensaje";
          $subject = "Bono extra denegado";
          $for = $email_cliente;
          Mail::send('mails/mail_bono_denegado',$datos, function($msj) use($subject,$for){
              $msj->from("web@marnific.com","Marnific");
              $msj->subject($subject);
              $msj->to($for);
          });



 
         return redirect('/configuracion/bonos_extras');

    }
    public function renovar_suscripcion($id)
    {
      Log::info('Renovar suscripcion');

     
      $user = User::where('id',$id)->first();
      $fecha_renovacion = $user->renovacion;
      $porciones = explode("-", $fecha_renovacion);

      if($user->recordatorio_renovacion==2 && $user->admin==0)
      {
        $suma_año = $porciones[0]+1;
        $fecha_renovacion_mas_anyo = $suma_año.'-'.$porciones[1].'-'.$porciones[2];
        $user->renovacion = $fecha_renovacion_mas_anyo;
        $user->recordatorio_renovacion = 0;
        $user->save();
        
        $expiracion = 4;
        // //envio de correo al cliente
        $datos = ['name'=>$user->name,'fecha_renovacion'=>$fecha_renovacion,'expiracion'=>$expiracion,'fecha_renovacion_mas_anyo'=> $fecha_renovacion_mas_anyo];
        // $name="david";
        // $msg = "todo el mensaje";
        $subject = "Renovación confirmada";
        $for = $user->email;
        Mail::send('mails/mail_renovacion',$datos, function($msj) use($subject,$for){
            $msj->from("web@marnific.com","Marnific");
            $msj->subject($subject);
            $msj->to($for);
        });

        $fecha_mostrar = $porciones[2].'-'.$porciones[1].'-'.$suma_año;

        return redirect()->back()->with('alert', 'Suscripción renovada, próxima renovación: '.$fecha_mostrar);

      }
      elseif($user->recordatorio_renovacion==1 || $user->recordatorio_renovacion==0 && $user->admin==0)
      {
            $fecha_mostrar = $porciones[2].'-'.$porciones[1].'-'.$porciones[0];
  
            return redirect()->back()->with('alert2', 'No se puede renovar hasta que no sea el día '.$fecha_mostrar);


      }
      elseif($user->admin==1)
      return redirect()->back()->with('alert2', 'Esta gestión no se puede realizar en un usuario administrador');
      

    }

    public function verificar_incidencia_checking($id)
    {
        Log::info('entra en verificar incidencia checking');

        $reserva= Reserva::where('id', $id)->first();

        $reserva->checking_verificado = 1;

        $reserva->save();

        return redirect('/configuracion/incidencias/checking');
    }

    public function verificar_incidencia_checkout($id)
    {
        Log::info('entra en verificar incidencia checking');

        $reserva= Reserva::where('id', $id)->first();

        $reserva->checkout_verificado = 1;

        $reserva->save();

        return redirect('/configuracion/incidencias/checkout');
    }


    public function index_calendario(BonoExtraAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();

        $dias_festivos = DiaFestivo::all();

        $barcos = Barco::all();
        $usuarios = User::all();
             
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }

        $barco_actual = Barco::where('id','1')->first();



        return view('/configuracion/index_calendario',[
                'reservas_checking' => $reservas_checking,
                'barcos'=>$barcos,
                'barco_actual' => $barco_actual,
                'usuarios'=>$usuarios,
                'dias_festivos'=>$dias_festivos,
            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,


                
                
       ]);

       
  
      
    }

    public function index_calendario2(BonoExtraAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();

        $dias_festivos = DiaFestivo::all();

        $barcos = Barco::all();
        $usuarios = User::all();

        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }

        $barco_actual = Barco::where('id','2')->first();


        return view('/configuracion/index_calendario2',[
                'reservas_checking' => $reservas_checking,
                'barcos'=>$barcos,
                'barco_actual' => $barco_actual,
                'usuarios'=>$usuarios,
                'dias_festivos'=>$dias_festivos,
            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,


                
                
       ]);

       
  
      
    }

    public function index_calendario3(BonoExtraAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();

        $dias_festivos = DiaFestivo::all();

        $barcos = Barco::all();
        $usuarios = User::all();
             
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }
        $barco_actual = Barco::where('id','3')->first();

        return view('/configuracion/index_calendario3',[
                'reservas_checking' => $reservas_checking,
                'barcos'=>$barcos,
                'barco_actual' => $barco_actual,
                'usuarios'=>$usuarios,
                'dias_festivos'=>$dias_festivos,
            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,


                
                
       ]);

       
  
      
    }

    public function index_calendario4(BonoExtraAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();

        $dias_festivos = DiaFestivo::all();

        $barcos = Barco::all();
        $usuarios = User::all();
             
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }
        $barco_actual = Barco::where('id','4')->first();

        return view('/configuracion/index_calendario4',[
                'reservas_checking' => $reservas_checking,
                'barcos'=>$barcos,
                'barco_actual' => $barco_actual,
                'usuarios'=>$usuarios,
                'dias_festivos'=>$dias_festivos,
            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,


                
                
       ]);

       
  
      
    }

    public function index_calendario5(BonoExtraAdminDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();

        $dias_festivos = DiaFestivo::all();

        $barcos = Barco::all();
        $usuarios = User::all();
             
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }
        $barco_actual = Barco::where('id','5')->first();

        return view('/configuracion/index_calendario5',[
                'reservas_checking' => $reservas_checking,
                'barcos'=>$barcos,
                'barco_actual' => $barco_actual,
                'usuarios'=>$usuarios,
                'dias_festivos'=>$dias_festivos,
            'reservas_checkout' => $reservas_checkout,
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,


                
                
       ]);

       
  
      
    }

    public function index_incidencias_checking(IncidenciasCheckingDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
      
        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }

   

       return $dataTable->render('/configuracion/index_incidencias_checking',[
            'reservas' => $reservas,
            'reservas_checkout' => $reservas_checkout,
  
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,
        
        ]);
  
      
    }
    public function index_incidencias_checking_antiguo(IncidenciasCheckingAntiguoDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();

        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();
      

        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }


       return $dataTable->render('/configuracion/index_incidencias_checking_antiguo',[
            'reservas_checking' => $reservas_checking,
            'reservas_checkout' => $reservas_checkout,
  
            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,
        
        ]);
  
      
    }

    public function index_incidencias_checkout(IncidenciasCheckoutDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();

        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }


        return $dataTable->render('/configuracion/index_incidencias_checkout',[
            'reservas_checking' => $reservas_checking,
            'reservas_checkout' => $reservas_checkout,

            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,
  
        
        ]);

  
      
    }

    public function index_incidencias_checkout_antiguo(IncidenciasCheckoutAntiguoDataTable $dataTable)
    {
        $hoy = date("Y-m-d");
        // Log::info($id);
        $reservas_checking = Reserva::where('checking_verificado','3')->get();
        $reservas_checkout = Reserva::where('checkout_verificado','3')->get();

        $bonos_extra = BonoExtra::all();
        $count_bonos_extras=0;
        foreach($bonos_extra as $bono)
        {

            if($bono->confirmar=="no")
            {
                $count_bonos_extras++;
            }
            
        }

        $renovaciones = User::all();
        $count_renovaciones=0;
        foreach($renovaciones as $renovacion)
        {

            if($renovacion->recordatorio_renovacion=="2")
            {
                $count_renovaciones++;
            }
            
        }


        return $dataTable->render('/configuracion/index_incidencias_checkout_antiguo',[
            'reservas_checking' => $reservas_checking,
            'reservas_checkout' => $reservas_checkout,

            'count_bonos_extras' => $count_bonos_extras,
            'count_renovaciones'=>$count_renovaciones,
  
        
        ]);

  
      
    }
    


    public function show()
    {
        $data['reservas']= Reserva::all();
        return response()->json($data['reservas']);
        // print_r($data);
    }

    public function index_edit_usuario(UsuarioIndividualDataTable $dataTable, $id)
    {
       Log::info('estoy en index edit usuario');
       Log::info($id);

        $usuario = User::where('id',$id)->first();
       
        $reservas = Reserva::where('user_id',$id)->get();
       
        
       return $dataTable->render('/configuracion/edit_usuario',[
                'usuario'=>$usuario,
                'reservas'=>$reservas,
                'error'=>'',
           
                
       ]);

       
      
    }
    public function editar_usuario(Request $request, $id)
    {
        Log::info('en eitar usuario');
        Log::info($request);

        $usuario = User::where('id',$id)->first();

        $usuario->name=$request->name;
        $usuario->email=$request->email;
        $usuario->apellidos=$request->apellidos;
        $usuario->telefono=$request->telefono;
        $usuario->dni=$request->dni;
        $usuario->tipo_pago=$request->tipo_pago;




        $usuario->numero_bonos_entresemana=$request->bonos_entresemana;
        $usuario->numero_bonos_findesemana=$request->bonos_findesemana;
        $usuario->numero_bonos_ultima_hora=$request->bonos_ultima_hora;



        $usuario->save();

        return redirect()->back()->with('alert', 'Usuario editado');

    }
    public function index_edit_reserva(UsuarioIndividualDataTable $dataTable, $id)
    {
       Log::info('estoy en index edit usuario');
       Log::info($id);
       $reserva = Reserva::where('id',$id)->first();
       $imagenes_incidencias_checking  =  ImagenIncidencia::where('reserva_id', $id)->where('nombre_archivo', 'NOT LIKE', 'Incidencias_checkout%')->get();
       $imagenes_incidencias_checkout  =  ImagenIncidencia::where('reserva_id', $id)->where('nombre_archivo', 'NOT LIKE', 'Incidencias_checking%')->get();
      
        $barco_id = $reserva->barco_id;
        $barcos = Barco::where('id',$barco_id)->first();

        $nombre_barco = $barcos->nombre;

        $usuario=User::where('id',$reserva->user_id)->first();

        $id_usuario = $usuario->id;

       return view('/configuracion/edit_reserva',[
           'reserva'=>$reserva,
           'id_usuario'=>$id_usuario,
           'nombre_barco'=>$nombre_barco,
           'imagenes_incidencias_checking'=>  $imagenes_incidencias_checking,
           'imagenes_incidencias_checkout'=>  $imagenes_incidencias_checkout,

                    
       ]);

       
      
    }

    public function destroy_reserva( ReservasAdminDataTable $dataTable, $id)
    {

        $hoy = date("Y-m-d");








        Log::info('en destroy reserva config');
        Log::info($id);

        $reserva = Reserva::where('id', $id)->first();

        //comprobar que la reserva no es anterior al dia actual ya que devolveria bonos
        $hoy_comprobacion = substr($hoy, 0, 10);

        $fecha_start_comprobacion = substr($reserva->start, 0, 10);
        $fechas_comprobacion = true;

        if($fecha_start_comprobacion < $hoy_comprobacion)
        {
            $fechas_comprobacion = false;

        }
        if($fechas_comprobacion == true)
        {
                //sacar nombre puertyo y ciudad dle barco

            $barco = Barco::where('id', $reserva->barco_id)->first();

            $nombre_barco = $barco->nombre;
            $puerto = $barco->puerto;
            $ciudad = $barco->ciudad;


    
            //Sacar todo lo relacionado con los dias de fecha actual, fecha de reserva etc.....

            //FECHA RESERVA
            date_default_timezone_set('Europe/Madrid');
            

            $fecha_start = substr($reserva->start, 0, 10);
            $numeroDia = date('d', strtotime($fecha_start));
            $dia = date('l', strtotime($fecha_start));
            $mes = date('F', strtotime($fecha_start));
            $anio = date('Y', strtotime($fecha_start));

            $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
            $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $nombredia = str_replace($dias_EN, $dias_ES, $dia);
            $nombremes = str_replace($mes_EN, $mes_ES, $mes);
            

            //FECHA ACTUAL
            $hoy = substr($hoy, 0, 10);
            $numeroDia_actual = date('d', strtotime($hoy));
            $dia_actual = date('l', strtotime($hoy));
            $mes_actual = date('F', strtotime($hoy));
            $anio_actual = date('Y', strtotime($hoy));

            
            $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);

        


            //comprobar que en que trimestre Se hace la reserva
        
            $trimestre="";
            switch ($nombremes) {
                case "Enero":
                    $trimestre="Primero";
                    break;
                case "Febrero":
                    $trimestre="Primero";
                    break;
                case "Marzo":
                    $trimestre="Primero";
                    break;
                case "Abril":
                    $trimestre="Segundo";
                    break;
                case "Mayo":
                    $trimestre="Segundo";
                    break;
                case "Junio":
                    $trimestre="Segundo";
                    break; 
                case "Julio":
                    $trimestre="Tercero";
                    break; 
                case "Agosto":
                    $trimestre="Tercero";
                    break; 
                case "Septiembre":
                    $trimestre="Tercero";
                    break; 
                case "Octubre":
                    $trimestre="Cuarto";
                    break;
                case "Noviembre":
                    $trimestre="Cuarto";
                    break;
                case "Diciembre":
                    $trimestre="Cuarto";
                    break;
                        
            }

            //comprobar en que trimestre estamos actualmente

            $trimestre_actual="";
            switch ($nombremes_actual) {
                case "Enero":
                    $trimestre_actual="Primero";
                    break;
                case "Febrero":
                    $trimestre_actual="Primero";
                    break;
                case "Marzo":
                    $trimestre_actual="Primero";
                    break;
                case "Abril":
                    $trimestre_actual="Segundo";
                    break;
                case "Mayo":
                    $trimestre_actual="Segundo";
                    break;
                case "Junio":
                    $trimestre_actual="Segundo";
                    break; 
                case "Julio":
                    $trimestre_actual="Tercero";
                    break; 
                case "Agosto":
                    $trimestre_actual="Tercero";
                    break; 
                case "Septiembre":
                    $trimestre_actual="Tercero";
                    break; 
                case "Octubre":
                    $trimestre_actual="Cuarto";
                    break;
                case "Noviembre":
                    $trimestre_actual="Cuarto";
                    break;
                case "Diciembre":
                    $trimestre_actual="Cuarto";
                    break;
                        
            }

            $diferente_trimestre = false;

            Log::info('TRIMESTRE ACTUAL');
            Log::info($trimestre_actual);
            Log::info('TRIMESTRE RESERVA ');
            Log::info($trimestre);

            if($trimestre_actual == $trimestre)
            {
            $diferente_trimestre = false;
            }
            elseif($trimestre_actual != $trimestre)
            {
                $diferente_trimestre = true;

            }

            if($reserva->dia_festivo==0 && $diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==0  && $reserva->bono_ultimo_dia==0)
            { 
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();

                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');



                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                
                }

                ImagenIncidencia::where('reserva_id', $id)->delete();

                $reserva = Reserva::where('id', $id)->first();

                $imagen_gasolina = $reserva->imagen_gasolina;
                $imagen_horas = $reserva->imagen_horas;

                $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                $imagen_bateria = $reserva->imagen_bateria;

                if($imagen_gasolina !="")
                {

                    $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                    $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
            
                    unlink($image_horas);
                    unlink($image_gasolina);
            
                }

                if($imagen_gasolina_checkout !="")
                {

                    $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                    $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                    $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                    unlink($imagen_bateria);
                    unlink($imagen_horas_checkout);
                    unlink($imagen_gasolina_checkout);
               

            
                }
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;

                

                $fecha_start = substr($reserva->start, 0, 10);
                $numeroDia = date('d', strtotime($fecha_start));
                $dia = date('l', strtotime($fecha_start));
                $mes = date('F', strtotime($fecha_start));
                $anio = date('Y', strtotime($fecha_start));

                $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
                $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                $nombredia = str_replace($dias_EN, $dias_ES, $dia);

                Log::info('FECHA');
                Log::info($nombredia);

                $usuario = User::where('id',$reserva->user_id)->first();

                if($nombredia=="Viernes" || $nombredia=="Sábado" || $nombredia=="Domingo")
                {
                    if($nombredia=="Viernes")
                    {
                        if($tarde==1 && $mañana==1)
                        {
                            
                            
                            $suma_entre = $usuario->numero_bonos_entresemana+1;
                            $suma_finde = $usuario->numero_bonos_findesemana+1;

                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->numero_bonos_entresemana =   $suma_entre;

                            $usuario->save();
                        }
                        elseif($tarde==1 && $mañana==0){
                            $suma_finde = $usuario->numero_bonos_findesemana+1;
                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->save();
                        

                        }
                        elseif($tarde==0 && $mañana==1){
                            $suma_entre = $usuario->numero_bonos_entresemana+1;
                            $usuario->numero_bonos_entresemana =   $suma_entre;
                            $usuario->save();



                        }

                


                    }
                    else
                    {
                        if($tarde==1 && $mañana==1)
                        {
                            
                            $suma_finde = $usuario->numero_bonos_findesemana+2;
                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->save();

                
                        }
                        else
                        {
                            $suma_finde = $usuario->numero_bonos_findesemana+1;
                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->save();

                        }
                    


                    }
                }
                else 
                {
                    if($tarde==1 && $mañana==1)
                    {
                        $suma_entre = $usuario->numero_bonos_entresemana+2;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->save();
                    
            
                    }
                    else
                    {
                        $suma_entre = $usuario->numero_bonos_entresemana+1;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->save();

                    }

                    
                }
                
                // //envio de correo al cliente

                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });
                
                Reserva::where('id', $id)->delete();
                DescontarBono::where('reserva_id',$id)->delete();


                return response()->json(['success' => 'reserva borrada',]);
            }
            elseif($reserva->dia_festivo==1 && $diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==0 && $reserva->bono_ultimo_dia==0 )
            {
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();

                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');



                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                
                }

                ImagenIncidencia::where('reserva_id', $id)->delete();

                $reserva = Reserva::where('id', $id)->first();

                $imagen_gasolina = $reserva->imagen_gasolina;
                $imagen_horas = $reserva->imagen_horas;
                $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                $imagen_bateria = $reserva->imagen_bateria;

                if($imagen_gasolina !="")
                {

                    $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                    $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
            
                    unlink($image_horas);
                    unlink($image_gasolina);
            
                }

                if($imagen_gasolina_checkout !="")
                {

                    $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                    $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                    $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                    unlink($imagen_bateria);
                    unlink($imagen_horas_checkout);
                    unlink($imagen_gasolina_checkout);
            
                }
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;

                $usuario = User::where('id',$reserva->user_id)->first();

                if($tarde==1 && $mañana==1)
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();
                }
                elseif($tarde==0 && $mañana==1)
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();
                }
                elseif($tarde==1 && $mañana==0)
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();
                }
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                Reserva::where('id', $id)->delete();
            
    
        
                // return redirect('/configuracion/index_calendario');
                return response()->json(['success' => 'reserva borrada',]);

                


            }
            elseif($diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==1 && $reserva->bono_ultimo_dia==0)
            {
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();

                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');



                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                
                }

                ImagenIncidencia::where('reserva_id', $id)->delete();

                $reserva = Reserva::where('id', $id)->first();

                $imagen_gasolina = $reserva->imagen_gasolina;
                $imagen_horas = $reserva->imagen_horas;

                $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                $imagen_bateria = $reserva->imagen_bateria;

                if($imagen_gasolina !="")
                {

                    $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                    $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
            
                    unlink($image_horas);
                    unlink($image_gasolina);
            
                }

                if($imagen_gasolina_checkout !="")
                {

                    $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                    $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                    $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                    unlink($imagen_bateria);
                    unlink($imagen_horas_checkout);
                    unlink($imagen_gasolina_checkout);
            
                }
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;

                $usuario = User::where('id',$reserva->user_id)->first();

                //nueva funcionalidad

            
                if($reserva->tipo_ultimo_dia=='2T,1M' ||$reserva->tipo_ultimo_dia=='1M,2T' || $reserva->tipo_ultimo_dia=='2M,1T'|| $reserva->tipo_ultimo_dia=='1T,2M'|| $reserva->tipo_ultimo_dia=='1M,2T'|| $reserva->tipo_ultimo_dia=='2T,1M'||  $reserva->tipo_ultimo_dia=='1T,2M' || $reserva->tipo_ultimo_dia=='2M,1T')
                {
                        $suma_entre = $usuario->numero_bonos_entresemana+1;
                        $suma_finde = $usuario->numero_bonos_findesemana+1;
                        $usuario->numero_bonos_findesemana =   $suma_finde;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->save();
                }
                elseif($reserva->tipo_ultimo_dia=='1T,4M' || $reserva->tipo_ultimo_dia=='2T,4M' || $reserva->tipo_ultimo_dia=='1M,4T' || $reserva->tipo_ultimo_dia=='2M,4T')
                {
                    if($reserva->tipo_ultimo_dia=='1T,4M')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                }
                                
                            }
                            else
                            {
                            
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                }

                        }
                            
                    }
                    elseif($reserva->tipo_ultimo_dia=='2T,4M')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    

                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();

                                }
                                
                            }
                            else
                            {
                            
                            
                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                    
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                            {
                                

                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();

                            }
                            elseif($reserva->dia_festivo==1)
                            {
                                $suma_finde = $usuario->numero_bonos_findesemana+2;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                
                            }

                        }
                            
                    }
                    elseif($reserva->tipo_ultimo_dia=='1M,4T')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    

                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        

                                }
                                
                            }
                            else
                            {
                            
                            
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                    
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                            {
                                
        
                                $suma_entre = $usuario->numero_bonos_entresemana+2;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $usuario->save();

                            }
                            elseif($reserva->dia_festivo==1)
                            {
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                            }

                        }
                            
                    }
                    elseif($reserva->tipo_ultimo_dia=='2M,4T')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        
                                    
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                

                                }
                                
                            }
                            else
                            {
                            
                            
                                $suma_finde = $usuario->numero_bonos_findesemana+2;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                    
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                            {
                                
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
        
                            }
                            elseif($reserva->dia_festivo==1)
                            {
                                $suma_finde = $usuario->numero_bonos_findesemana+2;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                    
                            
                            }

                        }
                            
                    }
                
                

                }
                
                //hasta aqui
                
                elseif($reserva->tipo_ultimo_dia=='2M' || $reserva->tipo_ultimo_dia=='2T' )
                {
                    
                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();

                }
            
                elseif($reserva->tipo_ultimo_dia=='2MT')
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();

                }
                elseif($reserva->tipo_ultimo_dia=='2M,2T' || $reserva->tipo_ultimo_dia=='2T,2M' )
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();

                }
                elseif($reserva->tipo_ultimo_dia=='1M' || $reserva->tipo_ultimo_dia=='1T')
                {
                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                    $usuario->numero_bonos_entresemana =   $suma_entre;
                    $usuario->save();
                }
            
                elseif($reserva->tipo_ultimo_dia=='1MT')
                {
                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                    $usuario->numero_bonos_entresemana =   $suma_entre;
                    $usuario->save();

                }

                elseif($reserva->tipo_ultimo_dia=='1M,1T' || $reserva->tipo_ultimo_dia=='1T,1M' )
                {
                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                    $usuario->numero_bonos_entresemana =   $suma_entre;
                    $usuario->save();

                }


            
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                Reserva::where('id', $id)->delete();
            
    
        
                // return redirect('/configuracion/index_calendario');
                return response()->json(['success' => 'reserva borrada',]);


            }
            //nueva funcionalidad
            elseif($diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==1 && $reserva->bono_ultimo_dia==1)
            {
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();

                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');



                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                
                }

                ImagenIncidencia::where('reserva_id', $id)->delete();

                $reserva = Reserva::where('id', $id)->first();

                $imagen_gasolina = $reserva->imagen_gasolina;
                $imagen_horas = $reserva->imagen_horas;

                $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                $imagen_bateria = $reserva->imagen_bateria;

                if($imagen_gasolina !="")
                {

                    $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                    $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
            
                    unlink($image_horas);
                    unlink($image_gasolina);
            
                }

                if($imagen_gasolina_checkout !="")
                {

                    $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                    $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                    $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                    unlink($imagen_bateria);
                    unlink($imagen_horas_checkout);
                    unlink($imagen_gasolina_checkout);
            
                }
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;

                $usuario = User::where('id',$reserva->user_id)->first();

            
            
                if($reserva->tipo_ultimo_dia=='4U,4M' || $reserva->tipo_ultimo_dia=='4U,4T' ||$reserva->tipo_ultimo_dia=='4M,4U' || $reserva->tipo_ultimo_dia=='4T,4U' || $reserva->tipo_ultimo_dia=='4U,1M' || $reserva->tipo_ultimo_dia=='4U,1T' || $reserva->tipo_ultimo_dia=='1M,4U' || $reserva->tipo_ultimo_dia=='1T,4U' ||  $reserva->tipo_ultimo_dia=='4U,2M' || $reserva->tipo_ultimo_dia=='4U,2T' ||  $reserva->tipo_ultimo_dia=='2M,4U' || $reserva->tipo_ultimo_dia=='2T,4U'  )
                {
                    if($reserva->tipo_ultimo_dia=='4U,4M' || $reserva->tipo_ultimo_dia=='4M,4U' )
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }
                                
                            }
                            else
                            {
                            
                                $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                $usuario->save();
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }

                        }


                    }
                    if($reserva->tipo_ultimo_dia=='4U,4T' || $reserva->tipo_ultimo_dia=='4T,4U')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }
                                
                            }
                            else
                            {
                            
                                $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                $usuario->save();
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }

                        }


                    }
                    if($reserva->tipo_ultimo_dia=='4U,1M' || $reserva->tipo_ultimo_dia=='4U,1T' || $reserva->tipo_ultimo_dia=='1M,4U' || $reserva->tipo_ultimo_dia=='1T,4U')
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                        $suma_entre = $usuario->numero_bonos_entresemana+1;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();


                    }
                    if($reserva->tipo_ultimo_dia=='4U,2M' || $reserva->tipo_ultimo_dia=='4U,2T' || $reserva->tipo_ultimo_dia=='2M,4U' || $reserva->tipo_ultimo_dia=='2T,4U' )
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                        $suma_finde = $usuario->numero_bonos_findesemana+1;
                        $usuario->numero_bonos_findesemana =   $suma_finde;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();


                    }
                    
                
                }

                if($reserva->tipo_ultimo_dia=='4U')
                {
                    if($tarde==1 && $mañana==1)
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+2;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();

                    }
                    elseif($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();
                    }

                }
                if($reserva->tipo_ultimo_dia=='4U,4U')
                {
                    $suma_ultima = $usuario->numero_bonos_ultima_hora+2;
                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                    $usuario->save();
                }


            
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                Reserva::where('id', $id)->delete();
            
    
        
                // return redirect('/configuracion/index_calendario');
                return response()->json(['success' => 'reserva borrada',]);


            }
            //hasta aqui
            
            elseif($reserva->reserva_bono_extra==1 && $diferente_trimestre ==false)
            {
                return response()->json(['success' => 'reserva bono extra',]);

            }
            elseif($diferente_trimestre ==true)
            {
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                DescontarBono::where('reserva_id',$id)->delete();
                Reserva::where('id', $id)->delete();
                return response()->json(['success' => 'reserva borrada',]);




            }
        

        }
        else{
            $a = new ConfiguracionController();
            $a->destroy_reserva_antigua($id);
            return response()->json(['success' => 'reserva borrada',]);
        }
    }

    function destroy_reserva_antigua($id)
    {
        $reserva = Reserva::where('id', $id)->first();
        $gasolina  = new Obj();
        $gasolina ->name = $reserva->imagen_gasolina;
        $gasolina ->path =  public_path()."/imagenes/checkings/gasolina/".$reserva->imagen_gasolina;  //ALERT CAMBIAR DIRECCION DE BARRA A /
        $gasolina ->column = 'imagen_gasolina'; 
        
        $horas = new Obj();
        $horas->name = $reserva->imagen_horas;
        $horas->path =  public_path()."/imagenes/checkings/horas/".$reserva->imagen_horas;
        $horas ->column = 'imagen_horas'; 
        
        $bateria = new Obj();
        $bateria->name = $reserva->imagen_bateria;
        $bateria->path = public_path()."/imagenes/checkouts/bateria/".$reserva->imagen_bateria;
        $bateria ->column = 'imagen_bateria'; 

        
        $gasolina_checkout  = new Obj();
        $gasolina_checkout  ->name = $reserva->imagen_gasolina_checkout;
        $gasolina_checkout  ->path = public_path()."/imagenes/checkouts/gasolina/".$reserva->imagen_gasolina_checkout;
        $gasolina_checkout ->column = 'imagen_gasolina_checkout'; 
        
        $horas_checkout = new Obj();
        $horas_checkout->name = $reserva->imagen_horas_checkout;
        $horas_checkout->path =  public_path()."/imagenes/checkouts/horas/".$reserva->imagen_horas_checkout;
        $horas_checkout ->column = 'imagen_horas_checkout'; 
        
        $arrayDelete = array($gasolina, $horas, $bateria, $gasolina_checkout, $horas_checkout);

        foreach($arrayDelete as $delete){
            
            if($delete->name != '' && $delete->name != null ){
                unlink($delete->path);
                $reserva[$delete->column] =  null;
                $reserva->save();
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $reserva->id)->get();
                foreach($imagenes_incidencias as $imagen){
                    $image_checking = strrpos($imagen->nombre_archivo, "Incidencias_checking");
                    $image_checkout = strrpos($imagen->nombre_archivo, "Incidencias_checkout");
                    if ($image_checking === 0) { 
                        $path = public_path()."/imagenes/checkings/incidencias_checking/".$imagen->nombre_archivo;
                        unlink($path);
                    }
                    if ($image_checkout === 0) { 
                        $path = public_path()."/imagenes/checkings/incidencias_checkout/".$imagen->nombre_archivo;
                        unlink($path);
                    }
                    $imagen->delete();
                }
                
            }
        }
        $reserva->delete();
    }

    public function destroy_reserva_form(Request $request)
    {
        $hoy = date("Y-m-d");

        $id=$request->IDInfo;
        $reserva = Reserva::where('id', $id)->first();

         //comprobar que la reserva no es anterior al dia actual ya que devolveria bonos
         $hoy_comprobacion = substr($hoy, 0, 10);

         $fecha_start_comprobacion = substr($reserva->start, 0, 10);
         $fechas_comprobacion = true;
 
         if($fecha_start_comprobacion < $hoy_comprobacion)
         {
             $fechas_comprobacion = false;
 
         }

         if($fechas_comprobacion == true)
         {
               //sacar nombre puertyo y ciudad dle barco

        

            $barco = Barco::where('id', $reserva->barco_id)->first();

            $nombre_barco = $barco->nombre;
            $puerto = $barco->puerto;
            $ciudad = $barco->ciudad;
            //Sacar todo lo relacionado con los dias de fecha actual, fecha de reserva etc.....

            //FECHA RESERVA
            date_default_timezone_set('Europe/Madrid');

            $fecha_start = substr($reserva->start, 0, 10);
            $numeroDia = date('d', strtotime($fecha_start));
            $dia = date('l', strtotime($fecha_start));
            $mes = date('F', strtotime($fecha_start));
            $anio = date('Y', strtotime($fecha_start));

            $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
            $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $nombredia = str_replace($dias_EN, $dias_ES, $dia);
            $nombremes = str_replace($mes_EN, $mes_ES, $mes);
            

            //FECHA ACTUAL
            $hoy = substr($hoy, 0, 10);
            $numeroDia_actual = date('d', strtotime($hoy));
            $dia_actual = date('l', strtotime($hoy));
            $mes_actual = date('F', strtotime($hoy));
            $anio_actual = date('Y', strtotime($hoy));

            
            $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);

        


            //comprobar que en que trimestre Se hace la reserva
        
            $trimestre="";
            switch ($nombremes) {
                case "Enero":
                    $trimestre="Primero";
                    break;
                case "Febrero":
                    $trimestre="Primero";
                    break;
                case "Marzo":
                    $trimestre="Primero";
                    break;
                case "Abril":
                    $trimestre="Segundo";
                    break;
                case "Mayo":
                    $trimestre="Segundo";
                    break;
                case "Junio":
                    $trimestre="Segundo";
                    break; 
                case "Julio":
                    $trimestre="Tercero";
                    break; 
                case "Agosto":
                    $trimestre="Tercero";
                    break; 
                case "Septiembre":
                    $trimestre="Tercero";
                    break; 
                case "Octubre":
                    $trimestre="Cuarto";
                    break;
                case "Noviembre":
                    $trimestre="Cuarto";
                    break;
                case "Diciembre":
                    $trimestre="Cuarto";
                    break;
                        
            }

            //comprobar en que trimestre estamos actualmente

            $trimestre_actual="";
            switch ($nombremes_actual) {
                case "Enero":
                    $trimestre_actual="Primero";
                    break;
                case "Febrero":
                    $trimestre_actual="Primero";
                    break;
                case "Marzo":
                    $trimestre_actual="Primero";
                    break;
                case "Abril":
                    $trimestre_actual="Segundo";
                    break;
                case "Mayo":
                    $trimestre_actual="Segundo";
                    break;
                case "Junio":
                    $trimestre_actual="Segundo";
                    break; 
                case "Julio":
                    $trimestre_actual="Tercero";
                    break; 
                case "Agosto":
                    $trimestre_actual="Tercero";
                    break; 
                case "Septiembre":
                    $trimestre_actual="Tercero";
                    break; 
                case "Octubre":
                    $trimestre_actual="Cuarto";
                    break;
                case "Noviembre":
                    $trimestre_actual="Cuarto";
                    break;
                case "Diciembre":
                    $trimestre_actual="Cuarto";
                    break;
                        
            }

            $diferente_trimestre = false;

            Log::info('TRIMESTRE ACTUAL');
            Log::info($trimestre_actual);
            Log::info('TRIMESTRE RESERVA ');
            Log::info($trimestre);

            if($trimestre_actual == $trimestre)
            {
            $diferente_trimestre = false;
            }
            elseif($trimestre_actual != $trimestre)
            {
                $diferente_trimestre = true;

            }
        

            if($reserva->dia_festivo==0 && $diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==0  && $reserva->bono_ultimo_dia==0)
            {
                Log::info('en destroy reserva Form');
            
                Log::info($id);
        
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();
        
                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');
        
        
        
                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
        
                
                }
                ImagenIncidencia::where('reserva_id', $id)->delete();
        
                
                Log::info($reserva);
                if($reserva!="[]")
                {
                    $imagen_gasolina = $reserva->imagen_gasolina;
                    $imagen_horas = $reserva->imagen_horas;
            
                    $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                    $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                    $imagen_bateria = $reserva->imagen_bateria;

                    if($imagen_gasolina !="")
                    {

                        $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                        $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
                
                        unlink($image_horas);
                        unlink($image_gasolina);
                
                    }

                    if($imagen_gasolina_checkout !="")
                    {

                        $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                        $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                        $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                        unlink($imagen_bateria);
                        unlink($imagen_horas_checkout);
                        unlink($imagen_gasolina_checkout);
                
                    }
                    $tarde = $reserva->tarde;
                    $mañana = $reserva->mañana;
                }
            
                $fecha_start = substr($reserva->start, 0, 10);
                $numeroDia = date('d', strtotime($fecha_start));
                $dia = date('l', strtotime($fecha_start));
                $mes = date('F', strtotime($fecha_start));
                $anio = date('Y', strtotime($fecha_start));
        
                $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
                $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        
                Log::info('FECHA');
                Log::info($nombredia);
        
                $usuario = User::where('id',$reserva->user_id)->first();
        
                if($nombredia=="Viernes" || $nombredia=="Sábado" || $nombredia=="Domingo")
                {
                    if($nombredia=="Viernes")
                    {
                        if($tarde==1 && $mañana==1)
                        {
                            
                            
                            $suma_entre = $usuario->numero_bonos_entresemana+1;
                            $suma_finde = $usuario->numero_bonos_findesemana+1;
        
                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->numero_bonos_entresemana =   $suma_entre;
        
                            $usuario->save();
                        }
                        elseif($tarde==1 && $mañana==0){
                            $suma_finde = $usuario->numero_bonos_findesemana+1;
                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->save();
                        
        
                        }
                        elseif($tarde==0 && $mañana==1){
                            $suma_entre = $usuario->numero_bonos_entresemana+1;
                            $usuario->numero_bonos_entresemana =   $suma_entre;
                            $usuario->save();
        
        
        
                        }
        
                
        
        
                    }
                    else
                    {
                        if($tarde==1 && $mañana==1)
                        {
                            
                            $suma_finde = $usuario->numero_bonos_findesemana+2;
                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->save();
        
                
                        }
                        else
                        {
                            $suma_finde = $usuario->numero_bonos_findesemana+1;
                            $usuario->numero_bonos_findesemana =   $suma_finde;
                            $usuario->save();
        
                        }
                    
        
        
                    }
                }
                else 
                {
                    if($tarde==1 && $mañana==1)
                    {
                        $suma_entre = $usuario->numero_bonos_entresemana+2;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->save();
                    
            
                    }
                    else
                    {
                        $suma_entre = $usuario->numero_bonos_entresemana+1;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->save();
        
                    }
        
                    
                }
                // //envio de correo al cliente
        
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });
                
                Reserva::where('id', $id)->delete();
            
        
        
                // return redirect('/configuracion/index_calendario');
                return response()->json(['success' => 'reserva borrada',]);

            }
            elseif($reserva->dia_festivo==1 && $diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==0  && $reserva->bono_ultimo_dia==0 )
            {
            
        
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();
        
                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');
        
        
        
                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
        
                
                }
                ImagenIncidencia::where('reserva_id', $id)->delete();
                Log::info($reserva);
                if($reserva!="[]")
                {
                    $imagen_gasolina = $reserva->imagen_gasolina;
                    $imagen_horas = $reserva->imagen_horas;
            
                    $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                    $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                    $imagen_bateria = $reserva->imagen_bateria;

                    if($imagen_gasolina !="")
                    {

                        $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                        $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
                
                        unlink($image_horas);
                        unlink($image_gasolina);
                
                    }

                    if($imagen_gasolina_checkout !="")
                    {

                        $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                        $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                        $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                        unlink($imagen_bateria);
                        unlink($imagen_horas_checkout);
                        unlink($imagen_gasolina_checkout);
                
                    }
                    $tarde = $reserva->tarde;
                    $mañana = $reserva->mañana;
                }

                $usuario = User::where('id',$reserva->user_id)->first();

                if($tarde==1 && $mañana==1)
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();
                }
                elseif($tarde==0 && $mañana==1)
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();
                }
                elseif($tarde==1 && $mañana==0)
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();
                }
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                Reserva::where('id', $id)->delete();
            
    
    
                // return redirect('/configuracion/index_calendario');
                return response()->json(['success' => 'reserva borrada',]);
            
            
            

            }
            elseif($diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==1 && $reserva->bono_ultimo_dia==0)
            {
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();

                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');



                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                
                }

                ImagenIncidencia::where('reserva_id', $id)->delete();

                $reserva = Reserva::where('id', $id)->first();

                $imagen_gasolina = $reserva->imagen_gasolina;
                $imagen_horas = $reserva->imagen_horas;

                $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                $imagen_bateria = $reserva->imagen_bateria;

                if($imagen_gasolina !="")
                {

                    $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                    $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
            
                    unlink($image_horas);
                    unlink($image_gasolina);
            
                }

                if($imagen_gasolina_checkout !="")
                {

                    $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                    $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                    $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                    unlink($imagen_bateria);
                    unlink($imagen_horas_checkout);
                    unlink($imagen_gasolina_checkout);
            
                }
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;

                $usuario = User::where('id',$reserva->user_id)->first();

                //nueva funcionalidad

            
                if($reserva->tipo_ultimo_dia=='2T,1M' ||$reserva->tipo_ultimo_dia=='1M,2T' || $reserva->tipo_ultimo_dia=='2M,1T'|| $reserva->tipo_ultimo_dia=='1T,2M'|| $reserva->tipo_ultimo_dia=='1M,2T'|| $reserva->tipo_ultimo_dia=='2T,1M'||  $reserva->tipo_ultimo_dia=='1T,2M' || $reserva->tipo_ultimo_dia=='2M,1T')
                {
                        $suma_entre = $usuario->numero_bonos_entresemana+1;
                        $suma_finde = $usuario->numero_bonos_findesemana+1;
                        $usuario->numero_bonos_findesemana =   $suma_finde;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->save();
                }
                elseif($reserva->tipo_ultimo_dia=='1T,4M' || $reserva->tipo_ultimo_dia=='2T,4M' || $reserva->tipo_ultimo_dia=='1M,4T' || $reserva->tipo_ultimo_dia=='2M,4T')
                {
                    if($reserva->tipo_ultimo_dia=='1T,4M')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                }
                                
                            }
                            else
                            {
                            
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                }

                        }
                            
                    }
                    elseif($reserva->tipo_ultimo_dia=='2T,4M')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    

                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();

                                }
                                
                            }
                            else
                            {
                            
                            
                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                    
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                            {
                                

                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();

                            }
                            elseif($reserva->dia_festivo==1)
                            {
                                $suma_finde = $usuario->numero_bonos_findesemana+2;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                
                            }

                        }
                            
                    }
                    elseif($reserva->tipo_ultimo_dia=='1M,4T')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    

                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        

                                }
                                
                            }
                            else
                            {
                            
                            
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                    
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                            {
                                
        
                                $suma_entre = $usuario->numero_bonos_entresemana+2;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $usuario->save();

                            }
                            elseif($reserva->dia_festivo==1)
                            {
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                            }

                        }
                            
                    }
                    elseif($reserva->tipo_ultimo_dia=='2M,4T')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
        
                                    
                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->save();
                                

                                }
                                
                            }
                            else
                            {
                            
                            
                                $suma_finde = $usuario->numero_bonos_findesemana+2;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                    
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                            {
                                
                                $suma_entre = $usuario->numero_bonos_entresemana+1;
                                $usuario->numero_bonos_entresemana =   $suma_entre;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
        
                            }
                            elseif($reserva->dia_festivo==1)
                            {
                                $suma_finde = $usuario->numero_bonos_findesemana+2;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->save();
                                    
                            
                            }

                        }
                            
                    }
                
                

                }
                
                //hasta aqui
                
                elseif($reserva->tipo_ultimo_dia=='2M' || $reserva->tipo_ultimo_dia=='2T' )
                {
                    
                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();

                }
            
                elseif($reserva->tipo_ultimo_dia=='2MT')
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();

                }
                elseif($reserva->tipo_ultimo_dia=='2M,2T' || $reserva->tipo_ultimo_dia=='2T,2M' )
                {
                    $suma_finde = $usuario->numero_bonos_findesemana+2;
                    $usuario->numero_bonos_findesemana =   $suma_finde;
                    $usuario->save();

                }
                elseif($reserva->tipo_ultimo_dia=='1M' || $reserva->tipo_ultimo_dia=='1T')
                {
                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                    $usuario->numero_bonos_entresemana =   $suma_entre;
                    $usuario->save();
                }
            
                elseif($reserva->tipo_ultimo_dia=='1MT')
                {
                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                    $usuario->numero_bonos_entresemana =   $suma_entre;
                    $usuario->save();

                }

                elseif($reserva->tipo_ultimo_dia=='1M,1T' || $reserva->tipo_ultimo_dia=='1T,1M' )
                {
                    $suma_entre = $usuario->numero_bonos_entresemana+2;
                    $usuario->numero_bonos_entresemana =   $suma_entre;
                    $usuario->save();

                }


            
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                Reserva::where('id', $id)->delete();
            
    
        
                // return redirect('/configuracion/index_calendario');
                return response()->json(['success' => 'reserva borrada',]);


            }
            //nueva funcionalidad
            elseif($diferente_trimestre ==false && $reserva->reserva_bono_extra==0 && $reserva->ultimo_dia==1 && $reserva->bono_ultimo_dia==1)
            {
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $id)->get();

                foreach($imagenes_incidencias as $imgIn)
                {
                    $nombre_archivo = $imgIn->nombre_archivo;
                    $pos = strpos($nombre_archivo, 'checking');
                    $pos2 = strpos($nombre_archivo, 'checkout');



                    if($pos!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checking"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                    if($pos2!=0)
                    {
                        $image_path = public_path()."/imagenes"."/checkings"."/incidencias_checkout"."/{$nombre_archivo}";
                        unlink($image_path);
                    }
                
                }

                ImagenIncidencia::where('reserva_id', $id)->delete();

                $reserva = Reserva::where('id', $id)->first();

                $imagen_gasolina = $reserva->imagen_gasolina;
                $imagen_horas = $reserva->imagen_horas;

                $imagen_gasolina_checkout = $reserva->imagen_gasolina_checkout;
                $imagen_horas_checkout = $reserva->imagen_horas_checkout;
                $imagen_bateria = $reserva->imagen_bateria;

                if($imagen_gasolina !="")
                {

                    $image_horas = public_path()."/imagenes"."/checkings"."/horas"."/{$imagen_horas}";
                    $image_gasolina = public_path()."/imagenes"."/checkings"."/gasolina"."/{$imagen_gasolina}";
            
                    unlink($image_horas);
                    unlink($image_gasolina);
            
                }

                if($imagen_gasolina_checkout !="")
                {

                    $imagen_horas_checkout = public_path()."/imagenes"."/checkouts"."/horas"."/{$imagen_horas_checkout }";
                    $imagen_gasolina_checkout = public_path()."/imagenes"."/checkouts"."/gasolina"."/{$imagen_gasolina_checkout}";
                    $imagen_bateria = public_path()."/imagenes"."/checkouts"."/bateria"."/{$imagen_bateria}";

                    unlink($imagen_bateria);
                    unlink($imagen_horas_checkout);
                    unlink($imagen_gasolina_checkout);
            
                }
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;

                $usuario = User::where('id',$reserva->user_id)->first();

                
            
                if($reserva->tipo_ultimo_dia=='4U,4M' || $reserva->tipo_ultimo_dia=='4U,4T' ||$reserva->tipo_ultimo_dia=='4M,4U' || $reserva->tipo_ultimo_dia=='4T,4U' || $reserva->tipo_ultimo_dia=='4U,1M' || $reserva->tipo_ultimo_dia=='4U,1T' || $reserva->tipo_ultimo_dia=='1M,4U' || $reserva->tipo_ultimo_dia=='1T,4U' ||  $reserva->tipo_ultimo_dia=='4U,2M' || $reserva->tipo_ultimo_dia=='4U,2T' ||  $reserva->tipo_ultimo_dia=='2M,4U' || $reserva->tipo_ultimo_dia=='2T,4U'  )
                {
                    if($reserva->tipo_ultimo_dia=='4U,4M' || $reserva->tipo_ultimo_dia=='4M,4U' )
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();

                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }
                                
                            }
                            else
                            {
                                
                                $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                $usuario->save();
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();

                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }

                        }


                    }
                    if($reserva->tipo_ultimo_dia=='4U,4T' || $reserva->tipo_ultimo_dia=='4T,4U')
                    {
                        if($nombredia=="Sábado" || $nombredia=="Domingo" || $nombredia=="Viernes")
                        {
                            if($nombredia=="Viernes")
                            {
                                if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();

                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }
                                
                            }
                            else
                            {
                                
                                $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                $suma_finde = $usuario->numero_bonos_findesemana+1;
                                $usuario->numero_bonos_findesemana =   $suma_finde;
                                $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                $usuario->save();
                            }
                        }
                        //entresemana
                        else
                        {
                            if($reserva->dia_festivo==0 )
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_entre = $usuario->numero_bonos_entresemana+1;
                                    $usuario->numero_bonos_entresemana =   $suma_entre;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();

                                }
                                elseif($reserva->dia_festivo==1)
                                {
                                    $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                                    $suma_finde = $usuario->numero_bonos_findesemana+1;
                                    $usuario->numero_bonos_findesemana =   $suma_finde;
                                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                                    $usuario->save();
                                }

                        }


                    }
                    if($reserva->tipo_ultimo_dia=='4U,1M' || $reserva->tipo_ultimo_dia=='4U,1T' || $reserva->tipo_ultimo_dia=='1M,4U' || $reserva->tipo_ultimo_dia=='1T,4U')
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                        $suma_entre = $usuario->numero_bonos_entresemana+1;
                        $usuario->numero_bonos_entresemana =   $suma_entre;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();


                    }
                    if($reserva->tipo_ultimo_dia=='4U,2M' || $reserva->tipo_ultimo_dia=='4U,2T' || $reserva->tipo_ultimo_dia=='2M,4U' || $reserva->tipo_ultimo_dia=='2T,4U' )
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                        $suma_finde = $usuario->numero_bonos_findesemana+1;
                        $usuario->numero_bonos_findesemana =   $suma_finde;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();


                    }
                    
                    
                }

                if($reserva->tipo_ultimo_dia=='4U')
                {
                    if($tarde==1 && $mañana==1)
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+2;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();

                    }
                    elseif($tarde==0 && $mañana==1 || $tarde==1 && $mañana==0)
                    {
                        $suma_ultima = $usuario->numero_bonos_ultima_hora+1;
                        $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                        $usuario->save();
                    }

                }
                if($reserva->tipo_ultimo_dia=='4U,4U')
                {
                    $suma_ultima = $usuario->numero_bonos_ultima_hora+2;
                    $usuario->numero_bonos_ultima_hora =   $suma_ultima;

                    $usuario->save();
                }


                
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                Reserva::where('id', $id)->delete();
            


                // return redirect('/configuracion/index_calendario');
                return response()->json(['success' => 'reserva borrada',]);


            }
            //hasta aqui
            elseif($reserva->reserva_bono_extra==1 && $diferente_trimestre ==false)
            {
                return response()->json(['success' => 'reserva bono extra',]);

            }

            elseif($diferente_trimestre ==true)
            {
                $tarde = $reserva->tarde;
                $mañana = $reserva->mañana;
                // //envio email
                $reserva_correo = Reserva::where('id', $id)->first();
                $usuario_correo = User::where('id', $reserva_correo->user_id)->first();
                $usuario_email = $usuario_correo->email;
                $usuario_nombre = $reserva_correo->user_name;
                $datos = ['name'=> $usuario_nombre, 'msg'=>'todo el mensage', 'fecha'=>$fecha_start,'nombre_barco'=>$nombre_barco,'puerto'=>$puerto,'ciudad'=>$ciudad, 'tarde'=>$tarde,'mañana'=>$mañana];
                // $name="david";
                // $msg = "todo el mensaje";
                $subject = "Cancelacion de reserva";
                $for = $usuario_email;
                Mail::send('mails/mail_cancelacion_reserva',$datos, function($msj) use($subject,$for){
                    $msj->from("web@marnific.com","Marnific");
                    $msj->subject($subject);
                    $msj->to($for);
                });

                DescontarBono::where('reserva_id',$id)->delete();
                Reserva::where('id', $id)->delete();
            
                return response()->json(['success' => 'reserva borrada',]);

            }
        
   
         }
         else{
            $a = new ConfiguracionController();
            $a->destroy_reserva_antigua($id);
            return response()->json(['success' => 'reserva borrada',]);
         }

      

    }
    public function checking_admin_index($id)
    {
        Log::info('checkinf admin');
        Log::info($id);

        $reservas2 = Reserva::where('id',$id)->first();

        return view('/configuracion/index_checking_admin',[
            'reservas' => $reservas2,
            'error'=>"No se puede hacer el check no es el momento",
            ]);


    }
    public function checking_admin($id)
    {
        Log::info('en checking admin correcto');
        $reserva = Reserva::where('id',$id)->first();

        $reserva->checking = 1;

        $reserva->save();

        return response()->json(['success' => 'checking correcto realizado']);

    }

    public function checking_incidencia_admin($id)
    {
        Log::info('en checking admin incidencia');
        $reserva = Reserva::where('id',$id)->first();

        $reserva->checking = 2;

        $reserva->save();

        return response()->json(['success' => 'checking incidencia realizado']);

    }
    public function checkout_admin_index($id)
    {
        Log::info('checkout admin');
        Log::info($id);

        $reservas2 = Reserva::where('id',$id)->first();

        return view('/configuracion/index_checkout_admin',[
            'reservas' => $reservas2,
            'error'=>"No se puede hacer el check no es el momento",
            ]);


    }

    public function checkout_admin($id)
    {
        Log::info('en checkout admin correcto');
        $reserva = Reserva::where('id',$id)->first();

        if($reserva->checking==0 )
        {   
            return response()->json(['success' => 'checkout incidencia NO realizado']);

        }
        elseif($reserva->checking==1 || $reserva->checking==2)
        {
            $reserva->checkout = 1;

            $reserva->save();
            return response()->json(['success' => 'checkout correcto realizado']);

        }
      

     

    }

    public function checkout_incidencia_admin($id)
    {
        Log::info('en checkout admin correcto');
        $reserva = Reserva::where('id',$id)->first();

        
        if($reserva->checking==0 )
        {   
            return response()->json(['success' => 'checkout incidencia NO realizado']);

        }
        elseif($reserva->checking==1 || $reserva->checking==2)
        {
            $reserva->checkout = 2;

            $reserva->save();
    
            return response()->json(['success' => 'checkout incidencia realizado']);
        }
     

    }
    public function destroy_dia_festivo($id)
    {
        Log::info('entra en eliminar dia_festivo');

        DiaFestivo::where('id',$id)->delete();
        
        return back();

    }

    public function destroy_usuario(UsuariosAdminDataTable $dataTable,$id)
    {
        $usuario= User::where('id',$id)->first();

        if($usuario->admin==0)
        {
            BonoExtra::where('user_id',$id)->delete();
            DescontarBono::where('user_id',$id)->delete();
            $reservas = Reserva::where('user_id',$id)->get();
   
            foreach($reservas as $reserva)
            {
                $imagenes_incidencias = ImagenIncidencia::where('reserva_id', $reserva->id)->get();
                foreach($imagenes_incidencias as $imgIn)
                {
                    $pos = strrpos($imgIn->nombre_archivo, "Incidencias_checking");
                    $pos2 = strrpos($imgIn->nombre_archivo, "Incidencias_checkout");
                    if($pos===0)
                    {
                        $image_path = public_path()."/imagenes/checkings/incidencias_checking/".$imgIn->nombre_archivo;
                        unlink($image_path);
                        ImagenIncidencia::where('reserva_id',$reserva->id)->delete();
                    }
                    if($pos2===0)
                    {
                        $image_path = public_path()."/imagenes/checkings/incidencias_checkout/".$imgIn->nombre_archivo;
                        unlink($image_path);
                        ImagenIncidencia::where('reserva_id',$reserva->id)->delete();
                    }
                }

               $reserva2 = Reserva::where('id', $reserva->id)->first();
               $imagen_gasolina = $reserva2->imagen_gasolina;
               $imagen_gasolina_checkout = $reserva2->imagen_gasolina_checkout;
               $imagen_horas = $reserva2->imagen_horas;
               $imagen_horas_checkout = $reserva2->imagen_horas_checkout;
               $imagen_bateria = $reserva2->imagen_bateria;

               if($imagen_gasolina !=""  && $imagen_gasolina !=null )
               {
                   $imagen_gasolina = public_path()."/imagenes/checkings/gasolina/".$imagen_gasolina;
                   unlink($imagen_gasolina);
                   $reserva2->imagen_gasolina = null;
               }
               if($imagen_horas !="" &&  $imagen_horas != null){
                    $imagen_horas = public_path()."/imagenes/checkings/horas/".$imagen_horas;
                    unlink($imagen_horas);
                    $reserva2->imagen_horas = null;

               }
               if($imagen_bateria !="" &&  $imagen_bateria != null){
                    $imagen_bateria = public_path()."/imagenes/checkouts/bateria/".$imagen_bateria;
                    unlink($imagen_bateria);
                     $reserva2->imagen_bateria = null;
                }
                if($imagen_gasolina_checkout !="" &&  $imagen_gasolina_checkout != null){
                    $imagen_gasolina_checkout = public_path()."/imagenes/checkouts/gasolina/".$imagen_gasolina_checkout;
                    unlink($imagen_gasolina_checkout);
                    $reserva2->imagen_gasolina_checkout = null;
                }
                if($imagen_horas_checkout !="" &&  $imagen_horas_checkout != null){
                    $imagen_horas_checkout = public_path()."/imagenes/checkouts/horas/".$imagen_horas_checkout;
                    unlink($imagen_horas_checkout);
                    $reserva2->imagen_horas_checkout = null;
                }
               Reserva::where('id',$reserva->id)->delete();   
            }
            User::where('id',$id)->delete();
            return response()->json(['success' => 'usuario borrado',]);
        }
        elseif($usuario->admin==1)
        {
            return response()->json(['success' => 'usuario admin',]);
        }
     
    }

    public function store_bono_ultima_hora_admin(Request $request)
    {
        Log::info('entra en store bono ultima hora admin');
        $hoy = getdate();

        $barco_id =$request['txtBarcoID'];
        $start =$request['txtFecha'];
        $end =$request['txtFecha'];
        $mañana =$request['txtMañana'];
        $tarde =$request['txtTarde'];
        $ultimo_dia = $request['ultimo_dia'];
        $tipo_bono = $request['tipo_bono'];

        $url_imagen_tarde_mañana = "https://marnific.com/reserva_barcos/public/imagenes/reservas/mañana-tarde.png";
        $url_imag_url = $request['ruta_imagen'];

        Log::info('URL DEL FORM= '.$url_imag_url);

        // datos usuario
        
        $user_id =$request['txtUsuarioID'];

        $user=User::where('id', $user_id)->first();

        $user_name =$user->name;
        $user_email = $user->email;
        $numero_bonos_findesemana = $user->numero_bonos_findesemana;
        $numero_bonos_entresemana = $user->numero_bonos_entresemana;
        $numero_bonos_ultima_hora = $user->numero_bonos_ultima_hora;

        //datos Barco

        $barco=Barco::where('id',$barco_id)->first();

        $barco_mail_puerto = $barco->puerto;
        $barco_mail_ciudad = $barco->ciudad;
        $barco_mail_nombre = $barco->nombre;



        $bonos = User::where('id',$user_id)->first();

         //Comprobar mes actual para comprobar trimestre mas abajo
         date_default_timezone_set('Europe/Madrid');

         $fecha_start = substr($start, 0, 10);
         $numeroDia = date('d', strtotime($fecha_start));
         $dia = date('l', strtotime($fecha_start));
         $mes = date('F', strtotime($fecha_start));
         $anio = date('Y', strtotime($fecha_start));
 
         $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
         $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
         $mes_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
         $mes_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
         $nombredia = str_replace($dias_EN, $dias_ES, $dia);
         $nombremes = str_replace($mes_EN, $mes_ES, $mes);
         
 
         //FECHA ACTUAL
         $hoy = substr($start, 0, 10);
         $numeroDia_actual = date('d', strtotime($hoy));
         $dia_actual = date('l', strtotime($hoy));
         $mes_actual = date('F', strtotime($hoy));
         $anio_actual = date('Y', strtotime($hoy));
 
         
         $nombremes_actual = str_replace($mes_EN, $mes_ES, $mes_actual);
 
        
 
 
         //comprobar que en que trimestre Se hace la reserva
       
        $trimestre="";
         switch ($nombremes) {
             case "Enero":
                 $trimestre="Primero";
                 break;
             case "Febrero":
                 $trimestre="Primero";
                 break;
             case "Marzo":
                 $trimestre="Primero";
                 break;
             case "Abril":
                 $trimestre="Segundo";
                 break;
             case "Mayo":
                 $trimestre="Segundo";
                 break;
             case "Junio":
                 $trimestre="Segundo";
                 break; 
             case "Julio":
                 $trimestre="Tercero";
                 break; 
             case "Agosto":
                 $trimestre="Tercero";
                 break; 
             case "Septiembre":
                 $trimestre="Tercero";
                 break; 
             case "Octubre":
                 $trimestre="Cuarto";
                 break;
             case "Noviembre":
                 $trimestre="Cuarto";
                 break;
             case "Diciembre":
                 $trimestre="Cuarto";
                 break;
                       
         }
 
         //comprobar en que trimestre estamos actualmente
 
         $trimestre_actual="";
         switch ($nombremes_actual) {
             case "Enero":
                 $trimestre_actual="Primero";
                 break;
             case "Febrero":
                 $trimestre_actual="Primero";
                 break;
             case "Marzo":
                 $trimestre_actual="Primero";
                 break;
             case "Abril":
                 $trimestre_actual="Segundo";
                 break;
             case "Mayo":
                 $trimestre_actual="Segundo";
                 break;
             case "Junio":
                 $trimestre_actual="Segundo";
                 break; 
             case "Julio":
                 $trimestre_actual="Tercero";
                 break; 
             case "Agosto":
                 $trimestre_actual="Tercero";
                 break; 
             case "Septiembre":
                 $trimestre_actual="Tercero";
                 break; 
             case "Octubre":
                 $trimestre_actual="Cuarto";
                 break;
             case "Noviembre":
                 $trimestre_actual="Cuarto";
                 break;
             case "Diciembre":
                 $trimestre_actual="Cuarto";
                 break;
                       
         }
 
         $diferente_trimestre = false;
 
         Log::info('TRIMESTRE ACTUAL');
         Log::info($trimestre_actual);
         Log::info('TRIMESTRE RESERVA ');
         Log::info($trimestre);
 
         if($trimestre_actual == $trimestre)
         {
           $diferente_trimestre = false;
         }
         elseif($trimestre_actual != $trimestre)
         {
             $diferente_trimestre = true;
 
         }
        

        if($diferente_trimestre==false)
        {
            $envio_email=false;
            //reserva de dos franjas
            if($tarde==1 && $mañana==1)
            {
                    if($numero_bonos_findesemana>=1 || $numero_bonos_entresemana>=1)
                    {
                        if($numero_bonos_findesemana<=0 || $numero_bonos_entresemana>=1)
                        {
                            return response()->json(['success' => 'Consume bono entresemana',]);

                        }
                        elseif($numero_bonos_findesemana>=1 || $numero_bonos_entresemana<=0)
                        {
                            return response()->json(['success' => 'Consume bono findesemana',]);

                        }


                    }
                    elseif($numero_bonos_findesemana>=2 && $numero_bonos_entresemana<=1)
                    {
                        Log::info('hay bonos de finde');
                        return response()->json(['success' => 'Hay bonos de findesemana',]);

                    }
                    elseif($numero_bonos_findesemana<=1 && $numero_bonos_entresemana>=2)
                    {
                        Log::info('hay bonos de entre');
                        return response()->json(['success' => 'Hay bonos de entresemana',]);
                    }
                    elseif($numero_bonos_findesemana<=0 && $numero_bonos_entresemana<=0)
                    {
                        
                        
                        if($numero_bonos_ultima_hora>=2)
                        {
                            Log::info('consumir bonos ultima hora dos franjas');
                            
                            $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();

                            Log::info($reservas_comprobacion);

                            if($reservas_comprobacion==[])
                            {
                                Log::info('Consumir bono ultima hora una franja NUEVA RESERVA');
                                $bonos->numero_bonos_ultima_hora = $bonos->numero_bonos_ultima_hora-2;
                                $bonos->save();
                        
                                $nueva_reserva =new  Reserva();
                            
                                $title="Ocupado: Mañana Tarde";
                                
                                
                                $nueva_reserva->title= $title;
                                $nueva_reserva->barco_id = $barco_id;
                                $nueva_reserva->user_id = $user_id;
                                $nueva_reserva->user_name = $user_name;
                                $nueva_reserva->start = $start;
                                $nueva_reserva->end = $end;
                                $nueva_reserva->mañana = $mañana;
                                $nueva_reserva->tarde = $tarde;
                                $nueva_reserva->imageUrl =  $url_imag_url;

                                $nueva_reserva->ultimo_dia =1;
                                $nueva_reserva->bono_ultimo_dia =1;
                                $nueva_reserva->tipo_ultimo_dia ="4U";


                                $nueva_reserva->save();
                                $envio_email=true;

                                

                            }
                        
                        }
                        elseif($numero_bonos_ultima_hora<=1)
                        {
                            return response()->json(['success' => 'No hay bonos suficientes de ultima hora',]);

                            
                        }
                            
                    }
                

            
            }
                //reserva de solo una franja
            elseif($tarde==1 && $mañana==0 || $tarde==0 && $mañana==1)
            {
                if($numero_bonos_findesemana>=1 && $numero_bonos_entresemana<=0)
                {
                    // Log::info('hay bonos de finde');
                    return response()->json(['success' => 'Hay bonos de findesemana',]);

                }
                elseif($numero_bonos_findesemana<=0 && $numero_bonos_entresemana>=1)
                {
                    // Log::info('hay bonos de entre');
                    return response()->json(['success' => 'Hay bonos de entresemana',]);

                }
                elseif($numero_bonos_findesemana<=0 && $numero_bonos_entresemana<=0)
                {

                    if($numero_bonos_ultima_hora>=1)
                    {
                        
                        $reservas_comprobacion = Reserva::where('user_id',$user_id)->where('start',$start)->where('barco_id',$barco_id)->first();

                        Log::info($reservas_comprobacion);

                        if($reservas_comprobacion==[])
                        {
                            Log::info('Consumir bono ultima hora una franja NUEVA RESERVA');
                            $bonos->numero_bonos_ultima_hora = $bonos->numero_bonos_ultima_hora-1;
                            $bonos->save();
                    
                            $nueva_reserva =new  Reserva();
                            if($tarde==1)
                            {
                                $title="Ocupado: Tarde";
                                $mañana=0;
                                $tarde=1;
                            }
                            elseif($mañana==1)
                            {
                                $title="Ocupado: Mañana";
                                $mañana=1;
                                $tarde=0;
                            }
                            $nueva_reserva->title= $title;
                            $nueva_reserva->barco_id = $barco_id;
                            $nueva_reserva->user_id = $user_id;
                            $nueva_reserva->user_name = $user_name;
                            $nueva_reserva->start = $start;
                            $nueva_reserva->end = $end;
                            $nueva_reserva->mañana = $mañana;
                            $nueva_reserva->tarde = $tarde;
                            $nueva_reserva->imageUrl =  $url_imag_url;
                        
                            $nueva_reserva->ultimo_dia =1;
                            $nueva_reserva->bono_ultimo_dia =1;
                            $nueva_reserva->tipo_ultimo_dia ="4U";


                            $nueva_reserva->save();
                            $envio_email=true;

                            

                        }
                        else
                        {
                            $bonos->numero_bonos_ultima_hora = $bonos->numero_bonos_ultima_hora-1;
                            $bonos->save();
                    

                            $titulo_reservas_comprobacion = $reservas_comprobacion->title;
                            $porciones = explode(":", $titulo_reservas_comprobacion);
                            $titulo_final = $porciones[0].":"." Mañana Tarde";
                            $reservas_comprobacion->title=$titulo_final;
                            $reservas_comprobacion->tarde=1;
                            $reservas_comprobacion->mañana=1;
                            $reservas_comprobacion->imageUrl =  $url_imagen_tarde_mañana;
                            $reservas_comprobacion->ultimo_dia =1;

                            $reservas_comprobacion->bono_ultimo_dia =1;
                            $tipo_anyadir= $reservas_comprobacion->tipo_ultimo_dia;
                            $reservas_comprobacion->tipo_ultimo_dia='4U'.','.$tipo_anyadir;

                            

                            $reservas_comprobacion->save();
                            $envio_email=true;


                        }


                    }
                    elseif($numero_bonos_ultima_hora<=0)
                    {
                        return response()->json(['success' => 'No hay bonos de ultima hora',]);

                        Log::info('No hay bonos de ultima hora');
                        
                    }


                }

                


            }

            //Envio de email en caso de que se haya hecho correctamente la reserva
            if($envio_email==true)
            {
                    // //envio de correo al cliente
                    $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
                    // $name="david";
                    // $msg = "todo el mensaje";
                    $subject = "Nueva reserva realizada";
                    $for = $user_email;
                    Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                        $msj->from("web@marnific.com","Marnific");
                        $msj->subject($subject);
                        $msj->to($for);
                    });

                    return response()->json(['success' => 'Reserva completada',]);

                
            }
        }
        elseif($diferente_trimestre==true)
        {
            if($tarde==0 && $mañana==1)
            {
                $tarde=0;
                $mañana=1;
                $title = "Ocupado: Mañana";

            }
            elseif($tarde==1 && $mañana==0)
            {
                $tarde=1;
                $mañana=0;
                $title = "Ocupado: Tarde";

            }
            elseif($tarde==1 && $mañana==1)
            {
                $tarde=1;
                $mañana=1;
                $title = "Ocupado: Mañana Tarde";


            }

            $nueva_reserva =new  Reserva();
            $nueva_reserva->title= $title;
            $nueva_reserva->barco_id = $barco_id;
            $nueva_reserva->user_id = $user_id;
            $nueva_reserva->user_name = $user_name;
            $nueva_reserva->start = $start;
            $nueva_reserva->end = $end;
            $nueva_reserva->mañana = $mañana;
            $nueva_reserva->tarde = $tarde;
            $nueva_reserva->imageUrl = $url_imag_url;
            $nueva_reserva->tipo_ultimo_dia ="4U";
    


            $nueva_reserva->save();

            //añadir nuevo bono diferente trimestre

            $ultima_reserva= Reserva::all()->last();
            $id_ultima_reserva = $ultima_reserva->id;

            // Log::info('ULTIMA RESERVA');
            // Log::info($id_ultima_reserva);

    
            $bonos_descontar = new DescontarBono();
            if($tarde==1 && $mañana==1)
            {
                $resta_bono = $bonos_descontar->cantidad_findesemana+2;



            }
            else
            {
                $resta_bono = $bonos_descontar->cantidad_findesemana+1;

            }
        
            $bonos_descontar->barco_id = $barco_id;
            $bonos_descontar->user_id = $user_id;
            $bonos_descontar->reserva_id = $id_ultima_reserva;

            $bonos_descontar->trimestre = $trimestre;
            $bonos_descontar->cantidad_findesemana= $resta_bono;
        

            $bonos_descontar->save();

            // //envio de correo al cliente
            $datos = ['name'=>$user_name, 'fecha'=>$fecha_start, 'tarde'=>$tarde, 'mañana'=>$mañana, 'puerto'=>$barco_mail_puerto, 'ciudad'=>$barco_mail_ciudad, 'nombre_barco'=>$barco_mail_nombre];
            // $name="david";
            // $msg = "todo el mensaje";
            $subject = "Nueva reserva realizada";
            $for = $user_email;
            Mail::send('mails/mail_nueva_reserva',$datos, function($msj) use($subject,$for){
                $msj->from("web@marnific.com","Marnific");
                $msj->subject($subject);
                $msj->to($for);
            });
            
            return response()->json(['success' => 'Reserva ok diferente trimestre',]);


        }

      

    }

    
}
