<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Reserva;
use App\Models\User;
use App\Models\Bono;
use App\Models\BonoExtra;
use App\Models\DescontarBono;

use Auth;


use App\DataTables\ReservasDataTable;

use App\Models\ImagenIncidencia;

class ReservasAdminController4 extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ReservasDataTable $dataTable, $id)
    {
       
    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo 'hola';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

 
    // Guardar reservas
    public function store(Request $request)
    {
      

        $datosEvento=request()->except(['_token','_method']);
        $barco_id =$datosEvento['barco_id'];
        $title =$datosEvento['title'];
        $user_id =$datosEvento['user_id'];
        $start =$datosEvento['start'];
        $end =$datosEvento['end'];
        $mañana =$datosEvento['mañana'];
        $tarde =$datosEvento['tarde'];
        $url_imag_url = $datosEvento['image_url'];
        // Log::info($datosEvento['title']);
        // Log::info('url_imagen_reserva');
        // Log::info($url_imag_url);
        // Log::info('mañana');
        // Log::info($mañana);
        // Log::info('tarde');
        // Log::info($tarde);
        $ningun_dia = false;
        $dia_completo = false;



        $reservas = Reserva::where('barco_id',$barco_id)->where('start', $start)->get();
        
        $count = 0;
        foreach($reservas as $res)
        {
            $count++;
            $res_mañana = $res->mañana;
            $res_tarde = $res->tarde;

            if($res_mañana==1 && $res_tarde==1)
            {
             $count++;

            }


        }
     
         

         //comprobar si se ha marcado dia completo en los inputs para hacer todas las funcions despues

         if($tarde==1 && $mañana==1)
         {
             $dia_completo = true;
         }

        //comprobar si no se ha chequeado ningun input en la reserva

        if($tarde==0 && $mañana==0)
        {

            $ningun_dia = true;
        }



        //toda la programacion
     
        if($ningun_dia==false)
        {
            if($count==0)
            {
                $nueva_reserva =new  Reserva();
                $nueva_reserva->title= $title;
                $nueva_reserva->barco_id = $barco_id;
                $nueva_reserva->user_id = $user_id;
                $nueva_reserva->start = $start;
                $nueva_reserva->end = $end;
                $nueva_reserva->mañana = $mañana;
                $nueva_reserva->tarde = $tarde;
                $nueva_reserva->imageUrl =  $url_imag_url;
                $nueva_reserva->save();

            }
            elseif($count>=2)
            {
                // Log::info('reservas completas');
                // echo'errors:Dia completo';
                Log::info('count');
                Log::info($count);
                return response()->json(['success' => 'dia completo',]);
    
    
        
                
    
            }
            elseif($count==1)
            {
                if($tarde==1 && $mañana==1)
                {
                     return response()->json(['success' => 'ocupado',]);
                    


                }
                else
                {
                    if($res_mañana==0 && $mañana==1)
                    {
                        $nueva_reserva =new  Reserva();
                        $nueva_reserva->title= $title;
                        $nueva_reserva->barco_id = $barco_id;
                        $nueva_reserva->user_id = $user_id;
                        $nueva_reserva->start = $start;
                        $nueva_reserva->end = $end;
                        $nueva_reserva->mañana = $mañana;
                        $nueva_reserva->tarde = $tarde;
                        $nueva_reserva->imageUrl =  $url_imag_url;
                        $nueva_reserva->save();
    
    
                    }
                    elseif($res_mañana==1 && $mañana==1 )
                    {
                        return response()->json(['success' => 'ocupado mañana',]);
                    }
    
                    if($res_tarde==0 && $tarde==1)
                    {
    
                        $nueva_reserva =new  Reserva();
                        $nueva_reserva->title= $title;
                        $nueva_reserva->barco_id = $barco_id;
                        $nueva_reserva->user_id = $user_id;
                        $nueva_reserva->start = $start;
                        $nueva_reserva->end = $end;
                        $nueva_reserva->mañana = $mañana;
                        $nueva_reserva->tarde = $tarde;
                        $nueva_reserva->imageUrl =  $url_imag_url;
                        $nueva_reserva->save();
    
                    }
                    elseif($res_tarde==1 && $tarde==1)
                    {
                        return response()->json(['success' => 'ocupado tarde',]);
                    }

                }
               

                

    
            }
            return response()->json(['success' => 'Reserva realizada correctamente',]);
            

        }
        elseif($tarde==0 && $mañana==0)
        {
            return response()->json(['success' => 'no check',]);


        }

            
        
       

       
       
        // Reserva::insert($datosEvento);
    }
  
  
    

 

    
  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data['reservas']= Reserva::where('barco_id', '4')->get();
        return response()->json($data['reservas']);
        // print_r($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
