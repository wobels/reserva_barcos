<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Ssheduardo\Redsys\Facades\Redsys;

class RedsysController extends Controller
{
    //
    public function index(Request $request)
    {
        try{
            $factura = $request->factura;
            $cantidad = $request->cantidad;
            $nombre = $request->nombre;
            $precio = $request->precio;
            
            $cantidad_por_precio = $cantidad*$precio;

            $key = config('redsys.key');

            Redsys::setAmount(rand(10,600));
            Redsys::setOrder(time());
            Redsys::setMerchantcode('999008881'); //Reemplazar por el código que proporciona el banco
            Redsys::setCurrency('978');
            Redsys::setTransactiontype('0');
            Redsys::setTerminal('1');
            Redsys::setMethod('T'); //Solo pago con tarjeta, no mostramos iupay
            Redsys::setNotification(config('redsys.url_notification')); //Url de notificacion
            Redsys::setUrlOk(config('redsys.url_ok')); //Url OK
            Redsys::setUrlKo(config('redsys.url_ko')); //Url KO
            Redsys::setVersion('HMAC_SHA256_V1');
            Redsys::setTradeName('Tienda S.L');
            Redsys::setTitular('Pedro Risco');
            Redsys::setProductDescription('Compras varias');
            Redsys::setEnviroment('test'); //Entorno test

            $signature = Redsys::generateMerchantSignature($key);
            Redsys::setMerchantSignature($signature);

            $form = Redsys::createForm();
        }
        catch(Exception $e){
            echo $e->getMessage();
        }
        // return $form;

        // $form=json_encode($form);
       
        return view('redsys.finalizar_pago', [
            'cantidad'=> $cantidad,
            'nombre'=>$nombre,
            'form'=>$form,
            'precio'=>$cantidad_por_precio,
            'precio_unidad'=>$precio,

            
            
        ]);

    }
}