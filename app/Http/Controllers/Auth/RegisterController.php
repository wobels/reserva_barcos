<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Barco;
use Mail;
use Illuminate\Support\Facades\Log;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin']);
        // $this->middleware('guest');

    }

    public function showRegistrationForm()
    {
        $barcos=Barco::all();
        return view('auth.register', compact('barcos'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'premium' => ['required'],
            'apellidos' => ['required','string', 'max:255'],
            'dni' => ['required','string', 'max:255'],
            'telefono' => ['required','numeric'],
            'tipo_de_pago' => ['required','numeric'],
            'barco' => ['nullable'],
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        Log::info('premium');

        $tipo_cliente = $data['premium'];

        if($tipo_cliente == 1) //normal
        {
            $numero_bonos_entresemana = 4;
            $numero_bonos_findesemana = 3;
            $numero_bonos_ultima_hora = 2;

        }
        else if($tipo_cliente == 2 ) //premium
        {
            $numero_bonos_entresemana = 9;
            $numero_bonos_findesemana = 6;
            $numero_bonos_ultima_hora = 4;

        }
        $barco = $data['barco'];

        if($barco==999)
        {
            $barco=null;
        }
        else{
            $barco=$data['barco'];
        }

       
        $barcos= Barco::where('id', $barco)->first();

        $nombre_barco = $barcos->nombre;
        

        //sumar un año para guardarlo en renovacion
        $hoy = date("Y-m-d");

        $hoy = substr($hoy, 0, 10);

        // Log::info('fecha de renovacion');

        $porciones = explode("-",$hoy);

        $suma_año = $porciones[0]+1;

        $renovacion = $suma_año.'-'.$porciones[1].'-'.$porciones[2];


        //envio de correo al cliente
        $datos = ['name'=>$data['name'], 'msg'=>'todo el mensage','email'=>$data['email'],'password'=>$data['password']];
        // $name="david";
        // $msg = "todo el mensaje";
        $subject = "Nuevo registro realizado";
        $for = $data['email'];
        Mail::send('mails/mail_registro',$datos, function($msj) use($subject,$for){
            $msj->from("marnificwebcontrol@gmail.com","Marnific");
            $msj->subject($subject);
            $msj->to($for);
        });
    
        
        
        //envio de correo al administrador
        $datos = ['name'=>$data['name'], 'msg'=>'todo el mensage','email'=>$data['email'],'password'=>$data['password']];
        // $name="david";
        // $msg = "todo el mensaje";
        $subject = "Nuevo registro realizado";
        $for = 'marnificwebcontrol@gmail.com';
        Mail::send('mails/mail_registro',$datos, function($msj) use($subject,$for){
            $msj->from("marnificwebcontrol@gmail.com","Marnific");
            $msj->subject($subject);
            $msj->to($for);
        });
    
        
        
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'premium' => $data['premium'],
            'numero_bonos_entresemana'=>$numero_bonos_entresemana,
            'numero_bonos_findesemana'=>$numero_bonos_findesemana,
            'numero_bonos_ultima_hora'=>$numero_bonos_ultima_hora,

            'barco_id'=>$barco,
            'apellidos' => $data['apellidos'],
            'dni' => $data['dni'],
            'telefono' => $data['telefono'],
            'tipo_pago' => $data['tipo_de_pago'],
            'renovacion'=>$renovacion,
            'nombre_barco'=>$nombre_barco,



        ]);


        
     
    }
}
