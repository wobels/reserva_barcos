<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonoExtra extends Model
{
    use HasFactory;

    protected $table = 'bonos_extras';

    protected $fillable = [
        'barco_id', 'user_id', 'estado','tipo','precio','confirmar',
    ];
}
