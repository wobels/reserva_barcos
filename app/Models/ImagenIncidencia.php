<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImagenIncidencia extends Model
{
    use HasFactory;

    protected $table = 'imagenes_incidencias';


    protected $fillable = [
        'nombre', 'reserva_id', 'nombre', 'nomrbe_archivo',
    ];



    public function reserva() {
        // Phone tiere la clave ajena 'user_id'
        return $this->belongsTo('App\Models\Reserva');
        }
}
