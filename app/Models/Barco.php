<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barco extends Model
{
    use HasFactory;

    protected $table = 'barcos';


    protected $fillable = [
        'nombre', 'titulo', 'tipo', 'descripcion','numero_personas','ciudad','puerto',
    ];

    public function imagenesBarco() {
         return $this->hasMany('App\Models\ImagenBarco');
    }

    public function bonos() {
        return $this->hasMany('App\Models\Bono');
    }

    public function users() {
        return $this->hasMany('App\Models\User');
    }

}


