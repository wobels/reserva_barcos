<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bono extends Model
{
    use HasFactory;

    protected $table = 'bonos';

    protected $fillable = [
        'barco_id', 'codigo', 'precio','precio_findesemana',
    ];

    public function barco() {
        
        return $this->belongsTo('App\Models\Barco');
        }

        public function BonoComprado() {
            return $this->hasMany('App\Models\BonoComprado');
       }

       public function reserva() {
        return $this->hasMany('App\Models\Reserva');
   }

   
}
