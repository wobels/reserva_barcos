<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DescontarBono extends Model
{
    use HasFactory;

    protected $table = 'descontar_bonos';

    protected $fillable = [
        'barco_id', 'user_id', 'trimestre','tipo','cantidad_entresemana','cantidad_findesemana',
    ];
}
