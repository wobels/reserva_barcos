<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    use HasFactory;

    
    protected $table = 'reservas';

    protected $fillable = [
        'barco_id', 'user_id', 'fecha', 'mañana','tarde','checking','user_name',
    ];

    public function barco() {
        
        return $this->belongsTo('App\Models\Barco');

    }

    public function imagenesIncidencia() {
        return $this->hasMany('App\Models\ImagenIncidencia');
   }



  
}
