<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImagenBarco extends Model
{
    use HasFactory;

    protected $table = 'imagenes_barco';

    protected $fillable = [
        'barco_id', 'nombre', 'nombre_archivo',
    ];

    public function barco() {
        // Phone tiene la clave ajena 'user_id'
        return $this->belongsTo('App\Models\Barco');
        }
    
}
