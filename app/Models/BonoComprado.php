<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BonoComprado extends Model
{
    use HasFactory;
    
    protected $table = 'bonos_comprados';

    protected $fillable = [
        'bono_id','usuario_id'. 'fecha', 'cantidad',
    ];

    public function barco() {
        
        return $this->belongsTo('App\Models\Barco');
        }

    
}
