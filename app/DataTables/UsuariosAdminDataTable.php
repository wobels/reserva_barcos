<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class UsuariosAdminDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $query2 = User::query()->get();

        foreach($query2 as $que)
        {
            $id = $que->id;
        }

    


        return datatables()
            ->eloquent($query)
            ->addColumn('Editar', '<a href="{{route("configuracion_usuario.index",$id)}}"><i style="font-size:20px" class="fas fa-edit"></i></a>')
           
            ->addColumn('Tipo de usuario', function($usuario){

                if( $usuario->premium=="1")
                {
                    return'Normal';

                }
                elseif( $usuario->premium=="2")
                {
                    return'Premium';


                }
           })

           ->addColumn('Tipo de pago', function($usuario){

                if( $usuario->tipo_pago=="1")
                {
                    return'Mensual';

                }
                elseif( $usuario->tipo_pago=="2")
                {
                    return'Semestral';



                }

                elseif( $usuario->tipo_pago=="3")
                {
                    return'Anual';



                }
            })
            ->addColumn('Fecha de inicio', function($usuario){

                $fecha_inicio = $usuario->created_at;

                $fecha_inicio = substr( $fecha_inicio, 0, 10);
                $porciones = explode("-", $fecha_inicio);

                $fecha_inicio = $porciones[2].'-'.$porciones[1].'-'.$porciones[0];
                
                    return  $fecha_inicio;
    
              
            })

            ->addColumn('Barco', function($usuario){

                $barco_id = $usuario->barco_id;

                if($barco_id == 1)
                {
                    return 'TERRETA - QUIKSILVER ACTIV 605 OPEN';

                } 

                
                if($barco_id == 2)
                {
                    return 'ARRELS - QUICKSILVER ACTIV 605 OPEN';

                } 

                if($barco_id == '')
                {
                    return 'Administrador';

                } 
                
    
              
            })

            ->addColumn('Renovacion', function($usuario){

                if($usuario->admin==0)
                {
                    $fecha_renovacion = $usuario->renovacion;

                    $fecha_renovacion = substr( $fecha_renovacion, 0, 10);
    
                    $porciones = explode("-", $fecha_renovacion);
    
                    $fecha_renovacion = $porciones[2].'-'.$porciones[1].'-'.$porciones[0];
                    return  $fecha_renovacion;
        

                }
                elseif($usuario->admin==1)
                {
                    

                    return  $fecha_renovacion="Administrador";
                }
               
              
            })
           
            ->rawColumns(['Editar','Tipo de usuario','Tipo de pago','Renovacion','Fecha de inicio']);
    

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('usuariosadmin-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'responsive' => 'false',
                        'scrollX' => 'true',
                      ])
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('Editar')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
                Column::make('id'),
                Column::make('name'),
                Column::make('nombre_barco'),
                Column::make('apellidos'),
                Column::make('dni'),
                Column::make('telefono'),
                Column::make('Tipo de pago'),
                Column::make('email'),
                Column::make('Tipo de usuario'),
                Column::make('numero_bonos_entresemana'),
                Column::make('numero_bonos_findesemana'),
                Column::make('numero_bonos_ultima_hora'),
                Column::make('Fecha de inicio'),
                Column::make('Renovacion'),


                




            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'UsuariosAdmin_' . date('YmdHis');
    }
}
