<?php

namespace App\DataTables;

use App\Models\Reserva;
use App\Models\Barco;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class IncidenciasCheckoutDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $hoy = date("Y-m-d");
        $query2 = Reserva::query()->where('checkout','2')->where('checkout_verificado','3');

        return datatables()
            ->eloquent($query2)
             ->addColumn('action', ' <a href="{{route("configuracion_reserva.index",$id)}}"><i class="fas fa-eye"></i></a> <a  href="{{route("verificar.checkout",$id)}}" ><i style="color:green;font-size:20px" class="fas fa-check"></i></a> ')
             ->addColumn('Tarde', function($reserva){

                if($reserva->tarde ==1)
                {
                    return "Si";
                }
                elseif($reserva->tarde==0)
                {
                    return "No";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Mañana', function($reserva){

                if($reserva->mañana ==1)
                {
                    return "Si";
                }
                elseif($reserva->mañana==0)
                {
                    return "No";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Checking', function($reserva){

                if($reserva->checking ==1)
                {
                    return "Correcto";
                }
                elseif($reserva->checking==2)
                {
                    return "Erroneo";
                }
                elseif($reserva->checking==0)
                {
                    return "No realizado";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Checkout', function($reserva){

                if($reserva->checkout ==1)
                {
                    return "Correcto";
                }
                elseif($reserva->checkout==2)
                {
                    return "Erroneo";
                }
                elseif($reserva->checkout==0)
                {
                    return "No realizado";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Fecha reserva', function($reserva){

          
                 return $reserva->start;
            })

            ->addColumn('Usuario', function($reserva){

                if( $reserva->user_name=="")
                {
                    return'Marnific';
                }
                else
                {
                    return $reserva->user_name;

                }
           })

           ->addColumn('Verificacion de checkout', function($reserva){

                if( $reserva->checkout_verificado=="3")
                {
                    return'No verificado';
                }
                elseif( $reserva->checkout_verificado=="1"){
                    return 'Verificado';
                }
           
             })
           
           ->addColumn('Barco', function($reserva){
                $barco=Barco::where('id',$reserva->barco_id)->first();
                return $barco->nombre;
            })
           
            ->rawColumns(['action','Tarde','Mañana','Checking','Checkout', 'Fecha reserva', 'Usuario','Barco','Verificacion de checkout']);
            

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Reserva $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Reserva $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('incidenciascheckout-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'responsive' => 'false',
                        'scrollX' => 'true',
                      ])
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
                  Column::make('id'),
                  Column::make('Barco'),
                  Column::make('Usuario'),
                  Column::make('Fecha reserva'),
                  Column::make('Tarde'),
                  Column::make('Mañana'),
      
                  Column::make('Checking'),
                  Column::make('Checkout'),
                  Column::make('Verificacion de checkout'),

      
      
      
                  // Column::make('add your columns'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'IncidenciasCheckout_' . date('YmdHis');
    }
}
