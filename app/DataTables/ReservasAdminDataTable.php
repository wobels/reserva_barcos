<?php

namespace App\DataTables;

use App\Models\Reserva;
use App\Models\Barco;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ReservasAdminDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        
        $hoy = date("Y-m-d");
        $query2 = Reserva::query()->whereBetween('start', [$hoy, '2100-12-12']);
        // $query2 = Reserva::query()->get();

        // foreach($query2 as $que)
        // {
        //     $id = $que->id;
        // }
        return datatables()
            ->eloquent($query2->orderBy('start', 'ASC'))
            ->addColumn('Ver', '<a href="{{route("configuracion_reserva.index",$id)}}"><i style="font-size:20px" class="fas fa-eye"></i></a>')
            ->addColumn('Tarde', function($reserva){

                if($reserva->tarde ==1)
                {
                    return "Si";
                }
                elseif($reserva->tarde==0)
                {
                    return "No";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Mañana', function($reserva){

                if($reserva->mañana ==1)
                {
                    return "Si";
                }
                elseif($reserva->mañana==0)
                {
                    return "No";
                }
                // return $reserva->tarde;
            })
            ->addColumn('Contratado', function($reserva){

                $fecha_inicio = $reserva->created_at;

                $fecha_inicio = substr( $fecha_inicio, 0, 10);
                $porciones = explode("-", $fecha_inicio);

                $fecha_inicio = $porciones[2].'-'.$porciones[1].'-'.$porciones[0];
                
                    return  $fecha_inicio;
    
              
            })

            ->addColumn('Check-in', function($reserva){

                if($reserva->checking ==1)
                {
                    return "Correcto";
                }
                elseif($reserva->checking==2)
                {
                    return "Erroneo";
                }
                elseif($reserva->checking==0)
                {
                    return "No realizado";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Checkout', function($reserva){

                if($reserva->checkout ==1)
                {
                    return "Correcto";
                }
                elseif($reserva->checkout==2)
                {
                    return "Erroneo";
                }
                elseif($reserva->checkout==0)
                {
                    return "No realizado";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Fecha reserva', function($reserva){

                $acortar_fecha = $fecha_start = substr($reserva->start, 0, 10);
                $porciones = explode("-", $acortar_fecha);


                return ($porciones[2].'-'.$porciones[1].'-'.$porciones[0]);

                //  return $fecha_start ;
            })

            ->addColumn('Usuario', function($reserva){

                if( $reserva->user_name=="")
                {
                    return'Marnific';
                }
                else
                {
                    return $reserva->user_name;

                }
           })
           
           ->addColumn('Barco', function($reserva){
                $barco=Barco::where('id',$reserva->barco_id)->first();
                return $barco->nombre;
            })
           
            
            
            ->rawColumns(['Ver','Tarde','Mañana','Check-in','Checkout', 'Fecha reserva', 'Usuario','Barco','Contratado']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Reserva $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Reserva $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $asc="asc";
        $desc="desc";

        return $this->builder()
                    ->setTableId('reservasadmin-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->parameters([
                        'responsive' => 'false',
                        'scrollX' => 'true',
                      ])
                      ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('Ver')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('id'),
            Column::make('Barco'),
            Column::make('Usuario'),
            Column::make('Fecha reserva'),
            Column::make('Tarde'),
            Column::make('Mañana'),
            Column::make('Check-in'),
            Column::make('Checkout'),



            // Column::make('add your columns'),
            Column::make('Contratado'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ReservasAdmin_' . date('YmdHis');
    }
}
