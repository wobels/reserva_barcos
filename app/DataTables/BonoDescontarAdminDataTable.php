<?php

namespace App\DataTables;
use App\Models\Reserva;
use App\Models\User;
use App\Models\Barco;


use App\Models\DescontarBono;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class BonoDescontarAdminDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', '<a href="{{route("denegar.bono.extra",$id)}}"><i style="color:red" class="fas fa-trash-alt"></i></a>')
                
           ->addColumn('Usuario', function($bono_descontar){
     
                $usuario = User::where('id',$bono_descontar->user_id)->first();

                return $usuario->name; 

            })

            ->addColumn('Barco', function($bono_descontar){
                $barco=Barco::where('id',$bono_descontar->barco_id)->first();
                return $barco->nombre;
            })
            
            ->addColumn('Contratado', function($bono_descontar){

                $fecha_inicio = $bono_descontar->created_at;

                $fecha_inicio = substr( $fecha_inicio, 0, 10);
                $porciones = explode("-", $fecha_inicio);

                $fecha_inicio = $porciones[2].'-'.$porciones[1].'-'.$porciones[0];
                
                    return  $fecha_inicio;
    
              
            })


            ->rawColumns(['action','Usuario','Barco','Contratado']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\DescontarBono $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DescontarBono $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('bonodescontaradmin-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'responsive' => 'false',
                        'scrollX' => 'true',
                      ])
                      ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::computed('action')
            //       ->exportable(false)
            //       ->printable(false)
            //       ->width(60)
            //       ->addClass('text-center'),
            Column::make('id'),
            Column::make('Barco'),
            Column::make('Usuario'),
            Column::make('reserva_id'),
            Column::make('trimestre'),

            Column::make('cantidad_entresemana'),
            Column::make('cantidad_findesemana'),
            Column::make('Contratado'),
         
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'BonoDescontarAdmin_' . date('YmdHis');
    }
}
