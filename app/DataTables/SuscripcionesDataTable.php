<?php

namespace App\DataTables;
use App\Models\User;

use App\Models\Suscripcione;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use Illuminate\Support\Facades\DB;

class SuscripcionesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $query2 = User::query()->where('recordatorio_renovacion',2);


        return datatables()
            ->eloquent($query2)
            ->addColumn('Editar', '<a href="{{route("configuracion_usuario.index",$id)}}"><i style="font-size:20px" class="fas fa-edit"></i></a>')
            
            ->addColumn('Nombre', function($usuario){

         


                
                    return   $usuario->name;
    
              
            })

            ->addColumn('Fecha de inicio', function($usuario){

                $fecha_inicio = $usuario->created_at;

                $fecha_inicio = substr( $fecha_inicio, 0, 10);
                $porciones = explode("-", $fecha_inicio);

                $fecha_inicio = $porciones[2].'-'.$porciones[1].'-'.$porciones[0];
                
                    return  $fecha_inicio;
    
              
            })
            ->addColumn('Renovacion', function($usuario){
                if($usuario->admin==0)
                {
                    $fecha_renovacion = $usuario->renovacion;

                    $fecha_renovacion = substr( $fecha_renovacion, 0, 10);
    
                    $porciones = explode("-", $fecha_renovacion);
    
                    $fecha_renovacion = $porciones[2].'-'.$porciones[1].'-'.$porciones[0];
                        return  $fecha_renovacion;

                }
                elseif($usuario->admin==1)
                {
                    

                    return  $fecha_renovacion="Administrador";
                }

         
    
              
            })
            ->rawColumns(['Nombre','Editar','Fecha de inicio', 'Renovacion']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('suscripciones-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                     'responsive' => 'false',
                     'scrollX' => 'true',
                   ])
                    ->buttons(
                        Button::make('excel'),
                    
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('Editar')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('id'),
            Column::make('Nombre'),
            Column::make('email'),
            Column::make('telefono'),


            
            Column::make('Fecha de inicio'),
            Column::make('Renovacion'),
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Suscripciones_' . date('YmdHis');
    }
}
