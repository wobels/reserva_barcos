<?php

namespace App\DataTables;
use App\Models\Reserva;
use App\Models\Barco;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;


use Auth;
class ReservasDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $query2 = Reserva::query()->where('user_id',Auth::user()->id);
     
        return datatables()
            ->eloquent($query2)
            ->addColumn('action', 'reservas.action')

            ->addColumn('Horario', function($reserva){

                if($reserva->tarde ==1 && $reserva->mañana ==0)
                {
                    return "Tarde";
                }
                elseif($reserva->tarde==0  && $reserva->mañana ==1)
                {
                    return "Mañana";

                }

                elseif($reserva->tarde==1  && $reserva->mañana ==1)
                {
                    return "Día completo";

                }
                // return $reserva->tarde;
            })

            // ->addColumn('Horario', function($reserva){

            //     if($reserva->mañana ==1)
            //     {
            //         return "Si";
            //     }
            //     elseif($reserva->mañana==0)
            //     {
            //         return "No";
            //     }
            //     // return $reserva->tarde;
            // })

            ->addColumn('Checking', function($reserva){

                if($reserva->checking ==1)
                {
                    return "Correcto";
                }
                elseif($reserva->checking==2)
                {
                    return "Erroneo";
                }
                elseif($reserva->checking==0)
                {
                    return "No realizado";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Checkout', function($reserva){

                if($reserva->checkout ==1)
                {
                    return "Correcto";
                }
                elseif($reserva->checkout==2)
                {
                    return "Erroneo";
                }
                elseif($reserva->checkout==0)
                {
                    return "No realizado";
                }
                // return $reserva->tarde;
            })

            ->addColumn('Fecha reserva', function($reserva){
                 $acortar_fecha = $fecha_start = substr($reserva->start, 0, 10);
                 $porciones = explode("-", $acortar_fecha);
                 return ($porciones[2].'-'.$porciones[1].'-'.$porciones[0]);
 
            })

            ->addColumn('Usuario', function($reserva){

                if( $reserva->user_name=="")
                {
                    return'Marnific';
                }
                else
                {
                    return $reserva->user_name;

                }
           })
           
           ->addColumn('Barco', function($reserva){
                $barco=Barco::where('id',$reserva->barco_id)->first();
                return $barco->nombre;
            })
            ->rawColumns(['Ver','Tarde','Mañana','Checking','Checkout', 'Fecha reserva', 'Usuario','Barco']);
            // ->addColumn('intro', 'Hi {{Auth::user()->name}}');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Reserva $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Reserva $model)
    {
        
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('reservas-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                       ->parameters([
                        'responsive' => 'false',
                        'scrollX' => 'true',
                      ])
                    ->buttons(
                        Button::make('excel'),
                        

                     

                    );
                 
               
                 
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::make('action'),
           
            Column::make('id'),
            Column::make('Barco'),
            Column::make('Usuario'),
            Column::make('Fecha reserva'),
            Column::make('Horario'),
            // Column::make('Mañana'),
            Column::make('Checking'),
            Column::make('Checkout'),





        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Reservas_' . date('YmdHis');
    }
}
